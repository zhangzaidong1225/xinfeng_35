'use strict'


import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import styles_modal1 from './styles/style_connection'

import ModalBox from './modal'
var UMeng = require('./UMeng');
import Language from './Language'
import Title from './title'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  ToastAndroid,
  Navigator,
  ScrollView,
  ListView,
  TextInput,
  NativeModules,
  NativeAppEventEmitter,
  // DeviceEventEmitter,
  Platform,
} from 'react-native';

var deviceWidth=Dimensions.get('window').width;
var deviceHeight=Dimensions.get('window').height;

var BlueToothUtil= require('./BlueToothUtil');

var sub_ble_state_ios = null;

// var sub_ble_state_android = null;
var hasSearch = true;//是否有搜索设备

var Connect = React.createClass({

  getInitialState(){

    return {
      isbond:Language.cancelthepairing,
      device_name:'',
      content:'',
    };
  },
  openModal: function() {
    UMeng.onEvent('connection_05');
    this.refs.modal.open();
  },

  ble_state_event_update:function(data){
    for (var i = 0; i < data.dev_list.length; i++) {
            if(data.dev_list[i].addr == this.props.deviceid){
              console.log(data.dev_list[i].name);
              if (data.dev_list[i].type == 0){
                hasSearch = true;
              }else {
                hasSearch = false;
              }
              
              this.setState({device_name:data.dev_list[i].name});
            }
    };
  },

  componentDidMount:function (){

    //ToastAndroid.show(this.props.device.id+'',ToastAndroid.SHORT);

    this.setState({device_name:this.props.device});
    // sub_ble_state_ios = NativeAppEventEmitter.addListener(
    //      'ble_state',
    //      (data) => {
    //         this.ble_state_event_update(data);
    //      }
    //  );
     // sub_ble_state_android = DeviceEventEmitter.addListener(
     //     'ble_state',
     //     (data) => {
     //        this.ble_state_event_update(data);
     //     }
     // );
     sub_ble_state_ios = NativeAppEventEmitter.addListener(
         'ble_state',
         (data) => {
            this.ble_state_event_update(data);
         }
     );

     BlueToothUtil.sendMydevice();
  },

  componentWillUnmount:function(){
      sub_ble_state_ios.remove();
      // sub_ble_state_android.remove();
  },



  render:function (){
    return (
      <View style = {styles_modal1.container}>
      

        <Title title = {Language.connectedbluetoothdevice} hasGoBack = {true}  navigator = {this.props.navigator}/>

        <View style = {styles_modal1.head}>
            <Text allowFontScaling={false} style = {styles_modal1.headText}>{this.state.device_name}</Text>
        </View>


          <View style = {styles_modal1.view1}>
                  <View style = {styles_modal1.view2}>
                    <TouchableOpacity
                        onPress = {()=> {this._dieconnect(this.props.deviceid);UMeng.onEvent('connection_02');}}>
                       <View style = {styles_modal1.view3}>
                        <Text allowFontScaling={false} style = {styles_modal1.view3_text1}>{Language.disconnect}</Text>
                       </View>
                    </TouchableOpacity>
                  </View>
          </View>

          <View style = {styles_modal1.view4}>

                  <View style = {styles_modal1.view5}>
                    <TouchableOpacity
                        onPress = {()=>{this.cancelBond(this.props.deviceid);UMeng.onEvent('connection_03');}}>
                       <View style = {styles_modal1.view6}>
                        <Text allowFontScaling={false} style = {styles_modal1.view6_txt}>{this.state.isbond}</Text>
                       </View>
                    </TouchableOpacity>
                  </View>
          </View>


          <SearchDevice deviceid = {this.props.deviceid}/>

          <View style = {styles_modal1.view7}>

                <View style = {styles_modal1.view8}>
                  <TouchableOpacity
                      onPress = {this.openModal}>
                     <View style = {styles_modal1.view9}>
                      <Text allowFontScaling={false} style = {styles_modal1.view9_txt}>{Language.rename}</Text>
                     </View>
                  </TouchableOpacity>
                </View>
          </View>


               <ModalBox
                style = {styles_modal1.modal2_container}
                position={'top'} 
                ref={'modal'}>

                  <View style = {styles_modal1.modal2_view}>

                    <View style = {styles_modal1.modal2_view1}>
                      <Text allowFontScaling={false} style= {styles_modal1.modal2_text}>{Language.rename}</Text>
                    </View>
 
                    <View style = {styles_modal1.modal2_view2}>
                    
                        <View style= {styles_modal1.modal2_wait}>
                          <TouchableOpacity onPress = {this._mRename}>
                             <View style = {styles_modal1.modal2_closetext}>
                              <Text allowFontScaling={false} style={[styles_feedback.commitText,{color:'rgb(23, 201, 180)',}]}>{Language.ok}</Text>
                             </View>
                          </TouchableOpacity>
                        </View>
                        <View style= {styles_modal1.modal2_cancel}>
                          <TouchableOpacity onPress={()=> {this.refs.modal.close();UMeng.onEvent('connection_07');}}>
                             <View style = {styles_modal1.modal2_closetext}>
                              <Text allowFontScaling={false} style={[styles_feedback.commitText,{color:'black'}]}>{Language.cancel}</Text>
                             </View>
                          </TouchableOpacity>
                        </View>
                    </View>
                  <View style= {styles_modal1.text_input}>
                      <TextInput
                          style = {styles_modal1.modal2_input}
                          placeholder={this.state.device_name} 
                          placeholderTextColor ='#E6E6E6'
                          autoFocus={true}
                          onFocus = {() => {this.setState({content:''})}}
                          onChangeText = {(text) => this.setState({content:text})}
                          value = {this.state.content}
                          underlineColorAndroid='transparent'
                          maxLength={16}/>
                    </View> 
                  </View>

              </ModalBox>

      </View>

    );
  },

  devRename:function(address){
      BlueToothUtil.devRename(address, this.state.content);
  },
  _mRename:function(){
    UMeng.onEvent('connection_06');
    if (this.state.content === ''){
        if (Platform.OS === 'android') {
          ToastAndroid.show(Language.namecannotempty,ToastAndroid.SHORT);
        } else {
        }
      
    }else {
      BlueToothUtil.devRename(this.props.deviceid, this.state.content);
      this.refs.modal.close();
    }
  },

  _dieconnect(addr){
    BlueToothUtil.disconnect(addr);

    BlueToothUtil.stopConnect(addr);
    this.props.navigator.pop();

  },
  cancelBond:function(address){
    BlueToothUtil.disconnect(address);
    BlueToothUtil.cancelBond(address);
    BlueToothUtil.stopConnect(address);
      
    this.props.navigator.pop();
  },





});
  var SearchDevice= React.createClass({
    render(){
      if(hasSearch){
          return (
          <View style = {styles_modal1.searchView}>

                  <View style = {styles_modal1.view10}>
                    <TouchableOpacity
                        onPress = {()=>{this.devCheck(this.props.deviceid);UMeng.onEvent('connection_04');}}>
                       <View style = {styles_modal1.view11}>
                        <Text allowFontScaling={false} style ={styles_modal1.view11_txt}>{Language.finddevice}</Text>
                       </View>
                    </TouchableOpacity>
                  </View>
            </View>
          );
      }else{
          return (
          <View/>
          );
      }
    },
      devCheck:function(address){
      // BlueToothUtil.readSN(address);
      // BlueToothUtil.setSN(address);
      // BlueToothUtil.readSN(address);

      BlueToothUtil.devCheck(address);
      // BlueToothUtil.disconnect(address);
  },
  });


module.exports = Connect;
