/*
//生成js的bundle指令
react-native bundle --platform ios --dev false --entry-file index.ios.js --bundle-output ./bundle-ios/main.jsbundle --assets-dest ./bundle-ios
*/
import React, { Component } from 'react';
import {
    AppRegistry,
} from 'react-native';

import Navigation from './Navigation'

AppRegistry.registerComponent('feibao', () => Navigation);

// import setting from './setting'

// AppRegistry.registerComponent('feibao', () => setting);



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

// import React, { Component } from 'react';
// import {
//   AppRegistry,
//   StyleSheet,
//   Text,
//   View
// } from 'react-native';

// export default class feibao extends Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>
//           Welcome to React Native!
//         </Text>
//         <Text style={styles.instructions}>
//           To get started, edit index.ios.js
//         </Text>
//         <Text style={styles.instructions}>
//           Press Cmd+R to reload,{'\n'}
//           Cmd+D or shake for dev menu
//         </Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//     borderWidth :1,
//     borderColor:'#646464',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//     borderWidth :1,
//     borderColor:'#646464',
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//     borderWidth :1,
//     borderColor:'#646464',
//   },
// });

// AppRegistry.registerComponent('feibao', () => feibao);

