'use strict'




import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    PixelRatio,
    Navigator,
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;

var ActionBar = React.createClass({

    render:function (){
      return (
          <View  style = {{height:60,flexDirection:'row',}}>
            <View style = {styles.container_actionbar}>
              <Text allowFontScaling={false} style = {styles.text}>
                  智能随身净化器
              </Text>
            </View>
            <View
              style = {{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
              <Image
                source = {require ('image!img_selecte_share_gray')}
                style = {styles.img_share}/>
              <Image
                source = {require ('image!img_selecte_set_gray')}
                style = {styles.img_set}/>
            </View>
          </View>
      );
    },


});

var styles = StyleSheet.create ({
    container_actionbar:{
      width:deviceWidth,
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:'#333C4B',

    },
    text:{
      fontSize:16,
      color:'#E6E6E6',
      textAlign:'center',

    },
    img_share:{
      width:34,
      height:34,
      resizeMode:'stretch',
      marginRight:10,
    },
    img_set:{
      width:17,
      height:17,
      resizeMode:'contain',
      marginRight:15,
    },
});


module.exports = ActionBar;
