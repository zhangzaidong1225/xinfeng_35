/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var RNFS = require('react-native-fs');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/

import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import styles_deviceUpload from './styles/style_deviceupload'
import  config from './config'

// var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
var DownLoad = require('./download');
import Language from './Language'
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Alert,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableWithoutFeedback,
  AsyncStorage,
  NativeModules,
  NetInfo,
  ToastAndroid,
  BackAndroid,
  // DeviceEventEmitter,
  NativeAppEventEmitter,
  ListView,
} from 'react-native';


var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;
var BlueToothUtil=require('./BlueToothUtil');
var path1 = RNFS.DocumentDirectoryPath + '/upload/dev_update.zip';
var path2 = RNFS.DocumentDirectoryPath + '/single/upload/dev_update.zip';
var path3 = RNFS.DocumentDirectoryPath + '/vehide/upload/dev_update.zip';
var Download = require('./download');
var alertMessage = '设备固件升级中，请稍后...';
var alertMessage1 = '设备升级失败，请重新升级';
var alertMessage2 = '设备升级成功';

var sub_ble_state = null;
// var sub_ble_state1 = null;



var DeviceUpload = React.createClass({

  getInitialState(){

    return {
      deviceid:'123',
      devicename:'',
      devicetype:'',
      msg:Language.clickgrade,
      percentage:'',
      isBack:true,
      disabled:false,
    };
  },

    componentWillUnmount:function(){
      sub_ble_state.remove();
      // sub_ble_state1.remove();
    },

    init_data:function(data){
      if (data.dev_list.length == 0 && !this.state.isBack) {
            this.setState({msg:Language.upgradefail,isBack:true,disabled:true});
                Alert.alert(
                    Language.prompt+'',
                    Language.upgrading+'',
                    [
                      {text: Language.ok+'', onPress: () => {this.props.navigator.pop();UMeng.onEvent('DeviceUpload_02');}},
                    ]
                  )
      };
      for (var i = 0;i < data.dev_list.length;i++){
        if (data.dev_list[i].addr == this.props.deviceid){
          if (data.dev_list[i].percentage != 0  && data.dev_list[i].percentage != null){
            this.setState({msg:data.dev_list[i].percentage+'%',isBack:false});
            console.log(data.dev_list[i].percentage);
          }
          if (data.dev_list[i].update_state == 101){
            console.log(data.dev_list[i].update_state+Language.upgradesuccessed);
              this.setState({msg:Language.upgradesuccessed,isBack:true,disabled:true});
              RCTDeviceEventEmitter.emit('back_up',true);
              console.log('升级完成');
                 Alert.alert(
                    Language.prompt+'',
                    Language.upgradesuccess+'',
                    [
                      {text: Language.ok+'', onPress: () => {this.props.navigator.pop();UMeng.onEvent('DeviceUpload_01');}},
                    ]
                  )
          }
          if (data.dev_list[i].update_state == 102){
             console.log(data.dev_list[i].update_state+Language.upgradefail);
            this.setState({msg:Language.upgradefail,isBack:true,disabled:true});
             RCTDeviceEventEmitter.emit('back_up',true);
                Alert.alert(
                    Language.prompt+'',
                    Language.upgradefailalert+'',
                    [
                      {text: Language.ok+'', onPress: () => {this.props.navigator.pop();UMeng.onEvent('DeviceUpload_02');}},
                     
                    ]
                  )
          }
        }
      }
    },

  componentDidMount:function (){

    this.setState({deviceid:this.props.deviceid,devicename:this.props.devicename});
    sub_ble_state = NativeAppEventEmitter.addListener('ble_state',(data) => {
      this.init_data(data);
    });

    // sub_ble_state1 = DeviceEventEmitter.addListener('ble_state',(data) => {
    //   this.init_data(data);
    // });

  },

  _onClick(){
    if (this.state.isBack){
        this.props.navigator.pop()
        this.setState({isBack:false});
    }else {
        Alert.alert(
        Language.prompt+'',
        Language.upgrading+'',
        [
          {text: Language.ok+'', onPress: () => {UMeng.onEvent('DeviceUpload_03');return true;}},
         
        ]
      )
    }
  },


  render(){
    var navigator = this.props.navigator;

    if (config.global_language =='en'){
      return (
        <View style = {styles_deviceUpload.container}>


            <View style={styles_feedback.container}>
                <TouchableOpacity style = {styles.touch_view}
                  onPress={() => {this._onClick();UMeng.onEvent('DeviceUpload_04');}}>
                  <Image style={[styles.image,{resizeMode: 'stretch'}]} source={require('image!ic_back')}/>
                </TouchableOpacity>
                <Text allowFontScaling={false} style={[styles.font3,styles.text1]}>{Language.firmwareupdate}</Text>
                <View style={[styles.image,]}></View>
            </View>

            <View style = {styles_deviceUpload.view1}>
              
                <View style = {styles_deviceUpload.borderView}>
                 <TouchableOpacity 
                      disabled = {this.state.disabled}
                      onPress = { () => {this.gotoUpload(this.props.deviceid,this.props.devicename,this.props.devicetype);UMeng.onEvent('DeviceUpload_05');}}>
                    <View style = {styles_deviceUpload.view2}>
                      <Text allowFontScaling={false} style = {styles_deviceUpload.text}>{Language.upgrade}</Text>
                    </View>
                 </TouchableOpacity> 

                </View>

            </View>

               
            <View style = {{height:150,marginTop:20,marginLeft:20,marginRight:20,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
                <View style = {{}}>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg1}</Text>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg2}</Text>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg3}</Text>
                </View>
            </View>
    </View>
      );
    }

    return(
    
    <View style = {styles_deviceUpload.container}>


            <View style={styles_feedback.container }>
                <TouchableOpacity style = {styles.touch_view}
                  onPress={() => {this._onClick();UMeng.onEvent('DeviceUpload_04');}}>
                  <Image style={[styles.image,{resizeMode: 'stretch'}]} source={require('image!ic_back')}/>
                </TouchableOpacity>
                <Text allowFontScaling={false} style={[styles.font3,styles.text1]}>{Language.firmwareupdate}</Text>
                <View style={[styles.image,]}></View>
            </View>

            <View style = {styles_deviceUpload.view1}>
              
                <View style = {styles_deviceUpload.borderView}>
                 <TouchableOpacity 
                      disabled = {this.state.disabled}
                      onPress = { () => {this.gotoUpload(this.props.deviceid,this.props.devicename,this.props.devicetype);UMeng.onEvent('DeviceUpload_05');}}>
                    <View style = {styles_deviceUpload.view2}>
                      <Text allowFontScaling={false} style = {styles_deviceUpload.text}>{this.state.msg}</Text>
                    </View>
                 </TouchableOpacity> 

                </View>

            </View>

               
            <View style = {{height:150,marginTop:20,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
                <View style = {{}}>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg1}</Text>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg2}</Text>
                  <Text allowFontScaling={false} style = {styles_deviceUpload.text2}>{Language.upgrademsg3}</Text>
                </View>
            </View>
    </View>
    );
  },

  gotoUpload:function(deviceid,devicename,devicetype){

    if (config.global_language == 'en'){
      this.setState({msg:Language.readygrade,isBack:false,disabled:true});
      RCTDeviceEventEmitter.emit('back_up',false);

      if (devicetype == 0){
            console.log('忻风升级');
           BlueToothUtil.deviceUpdate(deviceid,devicename,path1);
      }else if (devicetype == 1){
            console.log('单品升级 ');
           BlueToothUtil.deviceUpdate(deviceid,devicename,path2);
      }else {
          console.log('车载升级 ');
           BlueToothUtil.deviceUpdate(deviceid,devicename,path3);
      }
    }
    else {
      this.setState({msg:Language.readyupgrade,isBack:false,disabled:true});
      RCTDeviceEventEmitter.emit('back_up',false);

      if (devicetype == 0){
            console.log('忻风升级');
           BlueToothUtil.deviceUpdate(deviceid,devicename,path1);
      }else if (devicetype == 1){
            console.log('单品升级 ');
           BlueToothUtil.deviceUpdate(deviceid,devicename,path2);
      }else {
          console.log('车载升级 ');
           BlueToothUtil.deviceUpdate(deviceid,devicename,path3);
      }
    }


     //BlueToothUtil.copyFileFromAssets(deviceid,devicename,'dev_update',path1);
     
  },

});

module.exports =DeviceUpload;



