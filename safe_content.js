'use strict'

//防伪码
import  styles from './styles/style_setting';
import styles_feedback from './styles/style_feedback'
import style_safe from './styles/style_safe_code'
import style_login from './styles/style_login'
import tools from './tools'
import config from './config'
import Title from './title'
//var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    // AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    AsyncStorage,
    DeviceEventEmitter,
    NativeAppEventEmitter,
    ScrollView,
} from 'react-native';

 

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var Safe = React.createClass({
    
    getInitialState(){
        return {
          type:1,
          code:'',
          key:null,
          all_count:0,
          user_count:0,
        }
    },

    componentDidMount:function(){
    AsyncStorage.getItem('key',(error,result) => {
          if (error){}else {
            this.setState({
            key:result,
          });
        }
      });
    },
    render(){
        var navigator = this.props.navigator;
        return (
            <View style = {style_safe.container}>
               <ScrollView style={{height:deviceHeight-190,width:deviceWidth}} scrollEnabled={false}>
                 <View style = {style_safe.view1}>
                        <View style = {style_safe.view1_1}>
                            <View style = {style_safe.view1_2}>
                                <View style = {style_safe.view1_3}>
                                    <View style = {style_safe.image_view}>
                                        <Image style = {style_safe.image} source = {require('image!ic_security')}/>
                                    </View>
                                    <TextInput
                                          style = {{height:50,flex:1,marginLeft:10,}}
                                          KeyboardType = 'default'
                                          textAlignVertical='center'
                                          underlineColorAndroid='transparent'
                                          onChangeText = {(text) => this.setState({code:text})}
                                          value = {this.state.code}
                                          placeholderTextColor = '#CCCCCC'
                                          placeholder = {Language.sn}/> 
                                </View>
                                <View style = {style_safe.textView}>
                                    <View style = {style_safe.textView2}>
                                     <Text allowFontScaling={false} style = {style_safe.text1}>{Language.corresponding}</Text>
                                    </View>
                                </View>

                            </View>  
                        </View>
                </View>
                <View style = {style_safe.view_text}>
                      <View style = {style_safe.view_text1}>
                      <Text allowFontScaling={false} style = {style_safe.firstText1}>{Language.current_check}<Text allowFontScaling={false} style = {style_safe.firstText2}>  {this.state.all_count}  </Text><Text allowFontScaling={false} style = {style_safe.firstText3}>{Language.current_check_time}{Language.current_user}</Text> <Text allowFontScaling={false} style = {style_safe.firstText4}>  {this.state.user_count}  </Text><Text allowFontScaling={false} style = {style_safe.firstText5}>{Language.times}</Text></Text>
                      </View>
                 </View>
                </ScrollView>
                <View style = {style_login.viewBottom}>
                      <View style={style_login.viewBottom1_1}>
                        <TouchableOpacity
                            onPress = {()=>{this._handleSubmit();UMeng.onEvent('safe_content01');}}>
                           <View style = {styles_feedback.textBorder}>
                            <Text allowFontScaling={false} style={styles_feedback.commitText}>{Language.check}</Text>
                           </View>
                        </TouchableOpacity>
                      </View>
                </View>
            </View>
        );
    },
     _handleSubmit:function(){

    if (this.state.code === '' ){
      tools.alertShow(Language.anti_counterferting);
    }else {
       AsyncStorage.getItem('key',(error,result) => {
          if (error){}else {
            this.setState({
            key:result,
          });
        }
      });            
          NetInfo.isConnected.fetch().done(
            (isConnected) => { 
              if(isConnected){
              fetch(config.server_base + config.user_captcha_uri+'?code='+this.state.code+'&key='+this.state.key+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
                  .then ((response) => response.json())
                  .then((responseData) => {
                    if(responseData.code == 0){
                       this.setState({
                         all_count:responseData.data.all_count,
                         user_count:responseData.data.user_count,
                       });
                      tools.alertShow(responseData.msg+'');
                    }
                    if(responseData.code == 306){
                      tools.alertShow(responseData.msg+'');
                    }
                    if(responseData.code == 401){
                      tools.alertShow(responseData.msg+'');
                    }
                  })
                  .catch((error) => {
                     //ToastAndroid.show(''+error,ToastAndroid.SHORT);
                  });
                  
              }else{
                tools.alertShow(Language.connectnetwork);
              }
            });
        }
    
    },
});

module.exports = Safe;


