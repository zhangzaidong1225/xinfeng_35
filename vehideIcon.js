/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var Log = require('./net');
import tools from './tools'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var AsyncStorage = require('./AsyncStorage');
//import PercentageCircle from 'react-native-percentage-circle';
import * as Progress from 'react-native-progress';
import  styles from './styles/style_singleicon'
import  styles1 from './styles/style_icon'
import Language from './Language'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  DeviceEventEmitter,
  NativeModules,
  ToastAndroid,
  TouchableOpacity,
  PanResponder,
  Dimensions,
  NativeAppEventEmitter,
  Platform,
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var sub_getMode=null;
var wait = 0;
var pm25 = 250;
var isSwitch = true;
var misCharged = 0;
var mpm25Flag = -1;
var count = 0;
var mpm25PickTime = 0;
var msystemTime = 0;
var timeago = 0;
var mmodel = 0;
var animated;
var timeString = '';
var time_min;
var timenumber;
var power = 0;
var pm25String = '000';
var hasSingleString = ' ';
var statuesString = '';
var isOpentools = true;
var BlueToothUtil= require('./BlueToothUtil');
var TimerMixin = require('react-timer-mixin');
var sub_currentDevice = null;
var mcover;
var animation;
var f01_in = 0;
var f02_in = 0;
var f01_time = 0;
var f02_time = 0;
var img_filter,img_3;

var waitStatus = 0;//手动触发

var VehideIcon= React.createClass({

  mixins: [TimerMixin],
  // timer_wait:null,

  getInitialState: function() {
   return {
      pm25:250,
      time:20,
      isOpen:false,
      pm25Flag:0,
      ischarged:0,
      cover:0,
      color1:'#96FB69',
      address:'',
      isWait_V:false,
      wind_speed:-1,
      isOpenFilter:-1,
      isOpenFilter_1:-1,
    };
  },
  componentWillUnmount:function(){
      sub_currentDevice.remove();
      sub_getMode.remove();
      //BlueToothUtil.sendMydevice();
      // this.timer_wait && clearTimeout(this.timer_wait);

  },

  componentDidMount:function(){
    //this.countDown();
    this._run();
    this.startAnimation();
    sub_getMode = RCTDeviceEventEmitter.addListener('type_mode',(val)=>{
      
      if(val == null){
        console.log('val ····sub_getMode········NULL··· ');
      } else {
        if(val=='0'){
          this.setState({
            mode:0,
          });
        }else{
          this.setState({
            mode:1,
          });
        }
      }
      console.log('val ····sub_getMode···········'+val+'        '+this.state.mode);
    });
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val) => {
      if(val == null){
        console.log('val ···············');
      }else {
        // console.log('val···'+val.open_cap);

        if (val.open_cap == 1){

          this.setState({
            isOpenFilter:val.open_cap,
            // pm25Flag:val.Pm25Flag,
            isOpenFilter_1:1,
            // wind_speed:val.wind01_speed,
            // isWait_V : val.wind01_speed == 0? true:false, 
          });
          statuesString = Language.filerreplace;
          
        var mcover1=parseInt(val.battery_power);
        if(mcover1>=20){
          this.setState({
            color1:'#2AB9F1',
          });
          console.log('== 电池>60  '+mcover);
        } else {
          this.setState({
            color1:'#F86E68FF',
          });
          console.log('==电池<20  '+mcover);
        }
          
        console.log('val·1··'+val.open_cap+this.state.wind_speed + this.state.isWait_V);
        } else {
        console.log('val·0··'+val.open_cap+this.state.wind_speed + this.state.isWait_V);
        var mcover=parseInt(val.battery_power);

        power = mcover/100;
        this.setState({
         pm25:val.Pm25,
         pm25Flag:val.Pm25Flag,
         pm25PickTime:val.Pm25PickTime,
         isCharged:val.ischarged,
         cover:val.battery_power,
         address:val.addr,
         systemTime:val.SystemTime,
         model:val.model,
         type:val.type,
         hasSingle:val.vehide_model,
         isOpenFilter:val.open_cap,
         vehideModel:val.vehide_is_smart,

         filter01_in:val.filter01_in,
         filter01_use_time:val.filter01_use_time,
         filter02_in:val.filter02_in,
         filter02_use_time:val.filter02_use_time,

         wind_speed:val.wind01_speed,
         isWait_V : val.wind01_speed == 0? true:false, 
          isOpenFilter_1:0,
        });

        // if (val.vehide_model == 0 && val.type == 2){
        //   this.setState({
        //     pm25:0,
        //   });
        // } else {
        //   this.setState({
        //     pm25:val.Pm25,
        //   });
        // }


        //hasSingle:val.vehide_model,
        //mcover=parseInt(this.state.cover);
        f01_in = this.state.filter01_in;
        f02_in = this.state.filter02_in;
        f01_time =  Math.floor(this.state.filter01_use_time/60/60);
        f02_time =  Math.floor(this.state.filter02_use_time/60/60);

        // f01_time_rate = Math.floor(f01_time/90);
        // f02_time_rate = Math.floor(f02_time/90);


        // if(f01_in == 0||f02_in==0){
        //   img_3 = require('image!ic_filter_gray');
        //   img_filter = require('image!kong');
        //   console.log('滤芯--1-');
        // }else{
        //   console.log('滤芯--2-');
        //   animation.stop();
        //   if(f01_time>72||f02_time>72){
        //      img_filter = require('image!ic_filter_yellow');
        //   }else{
        //      img_filter = require('image!ic_filter_blue'); 
        //   }
        // }
        if (f01_in ==0 || f02_in ==0){
          img_3 = require('image!ic_nofilter');
          img_filter = require('image!kong');
          console.log('滤芯--1-');
        } else {
          console.log('滤芯--2-');
          animation.stop();
          if(f01_time > 72 || f02_time > 72){
             img_filter = require('image!ic_filter_four');
          } else if (f01_time > 54 || f02_time > 54) {
            img_filter = require('image!ic_filter_three');
          } else if (f01_time > 36 || f02_time > 36) {
            img_filter = require('image!ic_filter_two');
          } else if (f01_time > 18 || f02_time > 18) {
            img_filter = require('image!ic_filter_one');
          } else{
             img_filter = require('image!ic_filter_new'); 
          }
        }

        console.log('==是否充电'+this.state.isCharged);
        count=parseInt(this.state.pm25)/1000;
        mmodel=parseInt(this.state.model);
        misCharged=parseInt(this.state.isCharged);
        mpm25Flag=parseInt(this.state.pm25Flag);
        mpm25PickTime=parseInt(this.state.pm25PickTime);
        msystemTime=parseInt(this.state.systemTime);
        if(Math.floor((msystemTime-mpm25PickTime)/60)<0){
            timeago = 0;
        }else{
            timeago = Math.floor((msystemTime-mpm25PickTime)/60);
        }
        if(this.state.isOpenFilter == 1){
            statuesString = Language.filerreplace;
        }
        if(this.state.isWait_V && (this.state.wind_speed == 0)){
            statuesString = Language.sleep;
        }
        if((this.state.hasSingle == 0)&&(this.state.type == 2)){
          if (Platform.OS === 'android') {
            // pm25String = '     ?';
            // hasSingleString = '           '+Language.plug_monitor;
             pm25String = '?';
             hasSingleString = ''+Language.plug_monitor;
          } else {
            pm25String = '?';
            hasSingleString = Language.plug_monitor; 
          
          }


        }else{
            hasSingleString = '';
            if(parseInt(this.state.pm25)<=9&&parseInt(this.state.pm25)>=0){
                pm25String = '00'+parseInt(this.state.pm25);
            }else if(parseInt(this.state.pm25)<=99&&parseInt(this.state.pm25)>=10){
                pm25String = '0'+parseInt(this.state.pm25);
            }else{
                pm25String = parseInt(this.state.pm25);
            }
        }
        if(mcover>=20){
          this.setState({
            color1:'#2AB9F1',
          });
          console.log('==电池>60  '+mcover);
        }
        // else if (mcover>=20) {
        //   this.setState({
        //     color1:'#D0FB69FF',
        //   });
        //   console.log('==电池>=20  '+mcover);
        // }
        else{
          this.setState({
            color1:'#F86E68FF',
          });
          console.log('==电池<20  '+mcover);
        }
        this.setState({
            count1:count,
            mmodel1:mmodel,
            timeago1:timeago,
            pm25String1:pm25String,
            mpm25Flag1:mpm25Flag,
        });

        }
        
        //mpm25Flag1:mpm25Flag,
        console.log('val ·111··············'+Math.floor((msystemTime-mpm25PickTime)/60)+'           '+pm25String+'       '+mpm25PickTime+'  msystemTime     '+msystemTime+'  timeago    '+timeago+' mmodel  '+mmodel);

      }
      
    });
  },
   _run: function(){
    animated=Animated.timing(this.anim, {
      toValue: 0.5 * 60 * 60 * 24,
      duration: 1000 * 60 * 60 * 24,
      easing: Easing.linear
    })
    animated.start(() => this._run());
  },
  startAnimation:function() {
      //this.anim1.setValue(0);
      animation = Animated.sequence([ 
          Animated.timing(this.anim1, {
              toValue: 1,
              duration: 1500,
              easing: Easing.linear
          }),     
          Animated.timing(this.anim1, {
              toValue: 0,
              duration: 1500,
              easing: Easing.linear
          }),
      ])
      animation.start(() => this.startAnimation());
  },
  render: function(){
  this.anim = this.anim || new Animated.Value(0);
  this.anim1 = this.anim1 || new Animated.Value(0);
    var img4;
    var length;
    var length1;
    
    if(misCharged==1){
      img4=require('image!ic_chargeing_sc');
      length=0;
      length1=0; 
    }else{
      img4=require('image!ic_charge_sc');
      length=20;
      //length1=20-this.state.power; 
     length1=17/180*deviceWidth*power;
    }

    if(this.state.mode == '0'||this.state.type == 2){
      if(this.state.vehideModel==1){
        time_min = Language.automation_on;
      }else{
         time_min = '';
         timeString = '';
      }      
      console.log('val ····'+this.state.vehideModel+''+timeString);
    }else{
       console.log('val ····sub_getMode·······aaaaa····'+this.state.mode);
      timeString = ' min ago';
      time_min = timeago+timeString;
    }

    console.log('=======22====='+length1+'   '+length+'   '+count);
   if((this.state.mpm25Flag1==2) || (this.state.mpm25Flag1==3) || (this.state.isOpenFilter_1 == 1)||this.state.hasSingle == 0){
      
      animated.stop();
      if(this.state.mpm25Flag1==2){
         isOpentools = true;
      }
      if(this.state.mpm25Flag1==3&&isOpentools){
        tools.alertShow(Language.operationfailed);
        isOpentools = false;
        console.log('地址----------------------'+isOpentools);
      }
      if(this.state.isOpenFilter == 1){
                 console.log('isWait_V    开盖 ');
                  return (
                      <View style={[styles.background,{backgroundColor:'white'}]}>
                          <View style={[styles.image,styles.position]}>
                                <Image style={[styles.image,styles.position,{resizeMode: 'stretch'},]} source={require('image!ic_single_circle')}/>
                          </View>


                  <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>


                         <View style={{height:110,justifyContent:'center',alignItems: 'center'}}>
                             <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                                <Text allowFontScaling={false} style={[styles.font,{fontSize:30,}]}>  
                                  {statuesString}  
                                </Text> 
                             </View>
                         </View>
                             <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open_gray')}/>  
                        </View> 
                        <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                          <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                              opacity: this.anim1.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, 1],
                              }) 
                          }
                          ]} source={img_3}/>
                          <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
                    </TouchableOpacity>       
                      </View>
                    );

      } else if(this.state.isWait_V && (this.state.wind_speed == 0)&&this.state.isOpenFilter == 0){
      console.log('isWait_V    待机');
                  return (
                      <View style={[styles.background,{backgroundColor:'white'}]}>
                          <View style={[styles.image,styles.position]}>
                                <Image style={[styles.image,styles.position,{resizeMode: 'stretch'},]} source={require('image!ic_single_circle')}/>
                          </View>



                  <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
                         <View style={{height:110,justifyContent:'center',alignItems: 'center'}}>
                             <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                                <Text allowFontScaling={false} style={[styles.font,{fontSize:30,}]}>  
                                  {statuesString}  
                                </Text> 
                             </View>
                         </View>
                          
                        <TouchableOpacity
                                onPress={()=>{this.close();}}>
                             <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
                        </TouchableOpacity>  
                        </View>

                          <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                                <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                                    opacity: this.anim1.interpolate({
                                      inputRange: [0, 1],
                                      outputRange: [0, 1],
                                    }) 
                                }
                                ]} source={img_3}/>
                                <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
                          </TouchableOpacity>        
                      </View>
                    );

      } else if(this.state.vehideModel==1){
      console.log('isWait_V    智能界面');

       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
         <Image style={[styles.image,styles.position,{resizeMode: 'stretch'}]} source={require('image!ic_single_circle')}/>
          <View style={[styles.image,styles.position]}>
             <Progress.Circle size={123/180*deviceWidth-12} color = {'#2AB9F1'} thickness={4} progress={count} borderWidth={0} borderColor={'transparent'}/> 
          </View>

                  <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
                <View style={{height:110,justifyContent:'center',alignItems: 'center',}}>
                    <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                      <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',justifyContent:'center',alignItems: 'center'}]}>  
                         {pm25String} 
                         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>  
                             {' '}PM2.5
                                <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>{'\n'}{time_min}</Text>                          
                          </Text>                    
                      </Text>
                      <View style = {{height:10,width:50,}}></View>  
                   </View>
               </View>
              <TouchableOpacity
                      onPress={()=>{this.close();}}>
                   <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
              </TouchableOpacity>  
              </View> 
                  <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                        <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                            opacity: this.anim1.interpolate({
                              inputRange: [0, 1],
                              outputRange: [0, 1],
                            }) 
                        }
                        ]} source={img_3}/>
                        <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
                  </TouchableOpacity>           
          </View>
      );

      }else if(this.state.hasSingle == 0&&this.state.wind_speed != 0){ 
         console.log('isWait_V    未插入单品界面');
         console.log('isWait_V    界面555'+this.state.wind_speed);
       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
         <Image style={[styles.image,styles.position,{resizeMode: 'stretch'}]} source={require('image!ic_single_circle')}/>
          <View style={[styles.image,styles.position]}>
             <Progress.Circle size={123/180*deviceWidth-12} color = {'#2AB9F1'} thickness={4} progress={count} borderWidth={0} borderColor={'transparent'}/> 
          </View>

          <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
                <View style={{height:110,justifyContent:'center',alignItems: 'center',width:200}}>
                    <View style = {{position: 'absolute',bottom:2,justifyContent:'center',alignItems: 'center',width:200,}}>
                         <Text allowFontScaling={false} style={{fontSize:13,color:'#F1B02A',}}>{hasSingleString}</Text>  
                    </View>
                   
                    <View  style={{flexDirection: 'row',alignItems: 'flex-end',}}>
                      <View style = {{height:10,width:50,}}></View> 
                      <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',justifyContent:'center',alignItems: 'center',}]}>  
                         {pm25String} 
                         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',width:50}]}>  
                             {' '}PM2.5  
                          </Text>                   
                      </Text>
                       <View style = {{height:10,width:40,}}></View> 
                   </View>
               </View>
              <TouchableOpacity
                      onPress={()=>{this.close();}}>
                   <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
              </TouchableOpacity>  
              </View> 

          <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                    opacity: this.anim1.interpolate({
                      inputRange: [0, 1],
                      outputRange: [0, 1],
                    }) 
                }
                ]} source={img_3}/>
                <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
          </TouchableOpacity>           
          </View>
      );

      }else if(this.state.isOpenFilter == 0 && this.state.vehideModel==0){
      console.log('isWait_V    非智能界面');
       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
         <Image style={[styles.image,styles.position,{resizeMode: 'stretch'}]} source={require('image!ic_single_circle')}/>
          <View style={[styles.image,styles.position]}>
             <Progress.Circle size={123/180*deviceWidth-12} color = {'#2AB9F1'} thickness={4} progress={count} borderWidth={0} borderColor={'transparent'}/> 
          </View>

          <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
                <View style={{height:110,justifyContent:'center',alignItems: 'center',width:200,}}>
                    <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                      <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',justifyContent:'center',alignItems: 'center',}]}>  
                         {pm25String} 
                         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>  
                             {' '}PM2.5  
                              <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>{'\n'}{' '}</Text> 
                          </Text>                   
                      </Text>
                       <View style = {{height:10,width:50,}}></View> 
                   </View>
               </View>
              <TouchableOpacity
                      onPress={()=>{this.close();}}>
                   <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
              </TouchableOpacity>  
              </View> 

          <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                    opacity: this.anim1.interpolate({
                      inputRange: [0, 1],
                      outputRange: [0, 1],
                    }) 
                }
                ]} source={img_3}/>
                <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
          </TouchableOpacity>           
          </View>
      );
      }

   }else {
    console.log('isWait_V    检测界面');
       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
          <View style={[styles.image,styles.position]}>
                <Animated.Image style={[styles.image,styles.position,{resizeMode: 'stretch'},{
                  transform: [
                    {rotate: this.anim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [
                     '0deg', '360deg' 
                     ],
                     })},
                   ]}
                ]} source={require('image!ic_xuanzhuan')}/>
          </View>

          <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
         <View style={{height:110,justifyContent:'center',alignItems: 'center'}}>
             <View  style={{flexDirection: 'row',marginTop:20}}>
                <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',}]}>  
                   {pm25String} 
                   <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>  
                       {' '}PM2.5 
                       <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>{'\n'}{' '}{time_min}</Text> 
                    </Text> 
                     <Text allowFontScaling={false} style={{marginTop:20,fontSize:13,color:'#F1B02A'}}>{'\n'}{hasSingleString}</Text>
                </Text> 
                <View style = {{height:10,width:50,}}></View> 
             </View>
             
         </View>
          
        <TouchableOpacity
                onPress={()=>{this.close();}}>
             <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
        </TouchableOpacity>  
        </View>
                  <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                    opacity: this.anim1.interpolate({
                      inputRange: [0, 1],
                      outputRange: [0, 1],
                    }) 
                }
                ]} source={img_3}/>
                <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
          </TouchableOpacity>        
      </View>
      );
   }
  },
  gotoFilterPage:function() {
    this.props.navigator.push({
      id: 'FilterVehidePage',
    });
  },
  close:function() {

        if(!this.state.isWait_V){
          RCTDeviceEventEmitter.emit('isWait_V',true);
          console.log('isWait_V    待机');
          // waitStatus = 1;

          this.setState({
            isWait_V:true,
          });
        }else{
          RCTDeviceEventEmitter.emit('isWait_V',false); 
          // waitStatus = 1;

          this.setState({
            isWait_V:false,
          });
          console.log('isWait_V    唤醒');
        }
    


       
  },
});
module.exports = VehideIcon;
