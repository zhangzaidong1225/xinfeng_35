 //忻风icon
'use strict';

var Log = require('./net');
import tools from './tools'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
// var AsyncStorage = require('./AsyncStorage');
import * as Progress from 'react-native-progress';
import  styles from './styles/style_singleicon'
import  styles1 from './styles/style_icon'
import Language from './Language'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  Easing,
  DeviceEventEmitter,
  NativeModules,
  ToastAndroid,
  TouchableOpacity,
  PanResponder,
  Dimensions,
  NativeAppEventEmitter,
  Platform,
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var misCharged = 0;//充电标志 
var mwind_time;
var filter_in;
var mwind_time11,mwind_time1;


var count = 0;//进度条

// var time_min = "已过滤空气 ";

// var timenumber;
var power = 0;
var BlueToothUtil= require('./BlueToothUtil');
var TimerMixin = require('react-timer-mixin');
var sub_currentDevice = null;
var sub_currentWind  = null;//监听手动风速值
// var sub_wait = null;

var animation;
var img_filter,img_3;

var queue_wind;//风速管理队列

var XinfengIcon= React.createClass({
  mixins: [TimerMixin],
  getInitialState: function() {
   return {
      address:'',
      isWait_V:false,
      cover:100,
      isOpen:1,
      filter:0,
      S:'',
      num:28,
      wind_time:0,
      changefilter:'',
      power:null,
      color1:'#96FB69',
      isCharged:0,
      use_time_this:0,

    };
  },

  componentWillMount(){
    queue_wind = this.Queue();
  },


  componentWillUnmount:function(){
     queue_wind._clear();
      sub_currentDevice.remove();
      // sub_wait.remove();
      sub_currentWind.remove();
  },

  //   ble_state_event_update:function(data){
      
  //   for (var i = 0; i < data.dev_list.length; i++) {
  //           if(data.dev_list[i].addr == this.props.deviceid){
  //             console.log(data.dev_list[i].name);
  //             if (data.dev_list[i].type == 0){
  //               hasSearch = true;
  //             }else {
  //               hasSearch = false;
  //             }
              
  //             this.setState({device_name:data.dev_list[i].name});
  //           }
  //   };
  // },


  componentDidMount:function(){
    //this.countDown();
    // this._run();
    sub_currentWind = RCTDeviceEventEmitter.addListener('queue_wind',(val) => {
      // console.log('home----queue_wind------'+val);
      queue_wind._push(val);

      if (queue_wind._getStart() == 0 ) {
        if (!this.state.isOpen){
          this.setState({
            isOpen:true,
          });
        }

      } else {
        if (this.state.isOpen){
          this.setState({
            isOpen:false,
          });
        }
      }


    });


    this.startAnimation();

        //待机和唤醒的监听器
    // sub_wait = RCTDeviceEventEmitter.addListener('slider',(val)=>{
    //   console.log(' 待机和唤醒···'+val);

    //   BlueToothUtil.sendMydevice();

    //   if (val) {
    //     this.setState({
    //       isOpen:2,
    //     });
    //   }else{
    //     this.setState({
    //       isOpen:0,
    //     });
    //   }
    // });



    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val) => {
      if (val != null){

        var tmp_isopen = false;
        // if (val.speed == 0) {tmp_isopen = true};

        if (queue_wind._size() > 0){
          console.log('home``相等``'+queue_wind._quere());

          if (queue_wind._getStart() == 0 ) {tmp_isopen = true;}  
            // queue_wind._first();
          if (queue_wind._getStart() == val.speed) {queue_wind._clear();}
          console.log('home``相等``'+queue_wind._quere());
        } 
        else 
        {
          if (val.speed == 0) {tmp_isopen = true;}
        }


        let mcover=parseInt(val.battery_power);
         power = mcover/100;
        filter_in = val.filter_in;
        
        this.setState({
         isOpen:tmp_isopen,
         cover: val.battery_power,
         address:val.addr,
         num:val.use_time * 2,
         filter:val.use_time,
         isCharged:val.ischarged,
         wind_time:val.wind_time,
         use_time_this:val.use_time_this,

         // filter_in :val.filter_in,

        });
        mwind_time=parseInt(this.state.use_time_this)*1000;

       
        if (filter_in ==0){
          img_3 = require('image!ic_nofilter');
          img_filter = require('image!kong');
          console.log('滤芯--1-');
        } else {
          console.log('滤芯--2-');
          animation.stop();
            if(parseInt(this.state.filter)>=198000*0.8){
              img_filter=require('image!ic_filter_four');
            }else if (parseInt(this.state.filter)>=198000*0.75) {
              img_filter=require('image!ic_filter_three'); 
            }else if (parseInt(this.state.filter)>=198000*0.50) {
              img_filter=require('image!ic_filter_two');           
            }else if (parseInt(this.state.filter)>=198000*0.25) {
              img_filter=require('image!ic_filter_one');        
            }else {
              img_filter=require('image!ic_filter_new');        
            }
        }

        console.log('==是否充电 '+this.state.isCharged);
        count=parseInt(this.state.cover)/100;
        misCharged=parseInt(this.state.isCharged);

        if(mcover>=20){
          this.setState({
            color1:'#2AB9F1',
          });
        } else {
          this.setState({
            color1:'#F86E68FF',
          });
        }

      }
      
    });
  },
  //  _run: function(){
  //   animated=Animated.timing(this.anim, {
  //     toValue: 0.5 * 60 * 60 * 24,
  //     duration: 1000 * 60 * 60 * 24,
  //     easing: Easing.linear
  //   })
  //   animated.start(() => this._run());
  // },
  startAnimation:function() {
      //this.anim1.setValue(0);
      animation = Animated.sequence([ 
          Animated.timing(this.anim1, {
              toValue: 1,
              duration: 1500,
              easing: Easing.linear
          }),     
          Animated.timing(this.anim1, {
              toValue: 0,
              duration: 1500,
              easing: Easing.linear
          }),
      ])
      animation.start(() => this.startAnimation());
  },

/**
 * [Queue]
 * @param {[Int]} size [队列大小]
 */
 Queue:function(size) {
    var queue_ = [];

    //向队列中添加数据
    queue_._push = function(data) {
      if (data == null) {
        return false;
      }
      //如果传递了size参数就设置了队列的大小
      if (size != null && !isNaN(size)) {
        if (queue_.length == size) {
          queue_.pop();
        }
      }
      queue_.unshift(data);//在前端添加
      return true;
    };

    //从队列中取出数据
    queue_._pop = function() {
      return queue_.pop();
    };

    //返回队列的大小
    queue_._size = function() {
        return queue_.length;
    }

    //返回队列的内容
    queue_._quere = function() {
        return queue_;
    }

    queue_._clear = function() {
      while (queue_.length > 0) {
        queue_.pop();
      }
      return true;
    }

    //保留第一个元素
    queue_._first = function(){
      for (let i = queue_.length - 1;i >0;i--){
          queue_.pop();
      }
      return true;
    }

    //获取队列最后一个元素
    queue_._getEnd = function() {
      if (queue_.length > 0) {
        return queue_[queue_.length - 1];
      } else {
        return null;
      }
    }

    //获取队列第一个元素
    queue_._getStart = function() {
      if (queue_.length > 0) {
        return queue_[0];
      } else  {
        return null;
      }
    }

    return queue_;
},

  
  render: function() {
  // this.anim = this.anim || new Animated.Value(0);
  this.anim1 = this.anim1 || new Animated.Value(0);
    var img4;
    var length;
    var length1;
    //充电中
    if(misCharged==1){
      img4=require('image!ic_chargeing_sc');
      length=0;
      length1=0; 
    }else{
      img4=require('image!ic_charge_sc');
      length=20;
      //length1=20-this.state.power; 
     length1=17/180*deviceWidth*power;
    }
    //过滤量
     var string;
    if(mwind_time<1000000){
      mwind_time1=Math.floor(mwind_time/1000);
      string='dm³'; 
    }else if(mwind_time>=1000000){
      mwind_time11=Math.floor(mwind_time/10000);
      if(mwind_time11>=1000){
         mwind_time1=Math.floor(mwind_time11/10)/10;
      }else{
        mwind_time1=mwind_time11/100;
      }
      if(mwind_time1>100&&mwind_time1<=999){
        mwind_time1 = Math.floor(mwind_time1*10/10);
      }else if(mwind_time1>999){
        mwind_time1 = 999;
      }
      string='m³';           
    }

    //正常模式
    if (!this.state.isOpen){

       return (
      <View style={[styles.background,{backgroundColor:'white'}]}>
         <Image style={[styles.image,styles.position,{resizeMode: 'stretch'}]} source={require('image!ic_single_circle')}/>
          <View style={[styles.image,styles.position]}>
             <Progress.Circle size={123/180*deviceWidth-12} color = {'#2AB9F1'} thickness={4} progress={count} borderWidth={0} borderColor={'transparent'}/> 
          </View>

                  <View style={{justifyContent:'center',alignItems: 'center',}}>
                  <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
                      <View style={{marginRight:1.5,
                            marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
                      <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                            position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                            source={img4}/>            
                   </View>
                <View style={{height:110,justifyContent:'center',alignItems: 'center',}}>
                    <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                      <Text allowFontScaling={false} style={[styles.font,styles.fontSizeBig,{color:'#2AB9F1',justifyContent:'center',alignItems: 'center'}]}>  
                         {mwind_time1} 
                         <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>  
                             {' '}{string}
                              <Text allowFontScaling={false} style={[styles.font,styles.fontSizeSmall,{color:'#939393',}]}>{'\n'}{Language.airfiltered}</Text> 
                          </Text>                    
                      </Text> 
                      <View style = {{height:10,width:50,}}></View>   
                   </View>
               </View>
              <TouchableOpacity
                      onPress={()=>{this.close();}}>
                   <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
              </TouchableOpacity>  
              </View> 
                  <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                        <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                            opacity: this.anim1.interpolate({
                              inputRange: [0, 1],
                              outputRange: [0, 1],
                            }) 
                        }
                        ]} source={img_3}/>
                        <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
                  </TouchableOpacity>           
          </View>
      );
    }
        return (
            <View style={[styles.background,{backgroundColor:'white'}]}>
                <View style={[styles.image,styles.position]}>
                      <Image style={[styles.image,styles.position,{resizeMode: 'stretch'},]} source={require('image!ic_single_circle')}/>
                </View>

        <View style={{justifyContent:'center',alignItems: 'center',}}>
        <View style={{height:10/320*deviceHeight,width:20/180*deviceWidth,}}>
            <View style={{marginRight:1.5,
                  marginLeft:1.5,marginTop:1.5,marginBottom:1.5,width:length1,height:9/320*deviceHeight,backgroundColor:this.state.color1,
                  position:'absolute',justifyContent:'center',alignItems: 'center',}} />        
            <Image style={{width:20/180*deviceWidth,height:10/320*deviceHeight, resizeMode: 'stretch',
                  position:'absolute',justifyContent:'center',alignItems: 'center',}} 
                  source={img4}/>            
         </View>
               <View style={{height:110,justifyContent:'center',alignItems: 'center'}}>
                   <View  style={{flexDirection: 'row',alignItems: 'flex-end',marginTop:20}}>
                      <Text allowFontScaling={false} style={[styles.font,{fontSize:30,}]}>  
                        {Language.sleep}  
                      </Text> 
                   </View>
               </View>
                
              <TouchableOpacity
                      onPress={()=>{this.close();}}>
                   <Image style={[styles.filterImage,{resizeMode: 'stretch',}]} source={require('image!ic_open')}/> 
              </TouchableOpacity>  
              </View>

                <TouchableOpacity  style={{position: 'absolute',top: 55/320*deviceHeight,left: 95/180*deviceWidth,width:12.5/180*deviceWidth,height:16/320*deviceHeight,}} onPress={()=>{this.gotoFilterPage();}}>
                      <Animated.Image style={[{resizeMode: 'stretch',position: 'absolute',top: 0,left: 0,width:12.5/180*deviceWidth,height:16/320*deviceHeight,},{
                          opacity: this.anim1.interpolate({
                            inputRange: [0, 1],
                            outputRange: [0, 1],
                          }) 
                      }
                      ]} source={img_3}/>
                      <Image style={{resizeMode: 'stretch',width:12.5/180*deviceWidth,height:16/320*deviceHeight,position: 'absolute',top: 0,left: 0,}} source={img_filter}/>
                </TouchableOpacity>        
            </View>
          );

},

  gotoFilterPage:function() {
    this.props.navigator.push({
      id: 'FilterXinfengPage',
    });
  },
  close:function() {
       RCTDeviceEventEmitter.emit('alert',true);
  },
});
module.exports = XinfengIcon;

