'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var styles_feedback = StyleSheet.create({
	view_all:{
		backgroundColor:'#FFFFFF',	
	},
	container:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
        alignItems: 'center',
	},
	header:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
        alignItems: 'center',
	},
	first:{
		marginTop:50,
        flexDirection:'row',
		height:40,
		borderWidth:1/PixelRatio.get(),
	},
	selectList:{
		width:50,
		height:45,
        top:110,
        position:'absolute',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:40,
        borderColor:'#CCCCCC',
        borderWidth:1/PixelRatio.get(),
        borderRightWidth:0,
        borderTopWidth:0,
        borderLeftWidth:0,
	},
	mailView:{
		height:45,
		alignItems:'center',
		justifyContent:'center',
		flex:1,
		left:90,
		right:40,
        borderWidth:1/PixelRatio.get(),
        //borderLeftWidth:0,
        borderRightWidth:0,
        borderTopWidth:0,
        borderColor:'#CCCCCC',
        top:110,
        position:'absolute',
	},
	mail:{
		//paddingLeft:30,
		paddingRight:35,
	},
	
	detailView:{
		flex:3,
		position:'absolute',
		borderWidth:0,
		top:155,
		marginRight:40,
		marginLeft:40,
	},
	detail:{
		textAlign:'left',
		textAlignVertical:'top',
		marginLeft:10,
		marginRight:10,
		height:120,
		width:deviceWidth-80,
	},
	commitView:{
		height:80,
		marginLeft:20,
		marginRight:20,
        justifyContent:'center',
        alignItems: 'center',
	},
	borderView:{
		justifyContent:'center',
		alignItems: 'center',
		 width:deviceWidth-40,
		 height:40,
	     borderColor:'rgb(23, 201, 180)',
		 borderWidth: 2 / PixelRatio.get(),
		 borderRadius:20,
	},
	commitText:{
		textAlign:'center',
		fontSize:13,
		color:'rgb(23, 201, 180)',
	},
	commitText1:{
		textAlign:'center',
		fontSize:15,
		color:'rgb(242, 38, 19)',
	},
	textBorder:{
		width:deviceWidth-80,
		backgroundColor:'#FFFFFF',
	},
	borderView1:{
		height:40,
		width:deviceWidth-80,
		position:'absolute',top:290,
		marginLeft:40,
        flexDirection:'row',
        alignItems:'center',
	},
	image1:{
		height:10,
		width:10,
		marginLeft:20
	},
	text1:{
		fontSize:14,
		marginLeft:10,
		color:'black'
	},
	view2:{
		height:185,
		width:deviceWidth-60,
		marginRight:30,
		marginLeft:30,
		marginTop:40,
		borderWidth: 1 / PixelRatio.get(),
		borderRadius:5,
	},
	view3:{
		height:45,
		width:deviceWidth-60,
		flexDirection: 'row',
		borderWidth: 1 / PixelRatio.get(),
		borderLeftWidth:0,
		borderRightWidth:0,
		borderTopWidth:0,
	},
	view4:{
		height:45,
		flex:1,
		justifyContent:'center',
		alignItems: 'center',
	},
	view5:{
		height:19,
		width:16.5,
	},
	view6:{
		height:45,
		justifyContent:'center',
		alignItems: 'center',
	},
	view7:{
		height:40,
		width:0.5,
	},
	view8:{
		height:45,
		flex:5,
		justifyContent:'center',
		//alignItems: 'center',
	},
	view1_1:{
		justifyContent:'center',
		alignItems: 'center',
		height:140,
		width:deviceWidth-60,
		flexDirection: 'row',
		borderWidth: 1 / PixelRatio.get(),
		borderLeftWidth:0,
		borderRightWidth:0,
		borderTopWidth:0,
	},
	view1_2:{
		height:140,
		flex:6.2,
		justifyContent:'center',
		alignItems: 'center',
	},

	

});
module.exports = styles_feedback;
