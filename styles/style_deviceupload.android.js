'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var style_deviceUpload = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'#FFFFFF',
	},
	view1:{
		height:150,
		borderWidth:0,
		backgroundColor:'white',
		marginTop:30,
		justifyContent:'center',
		alignItems:'center',	
	},
	borderView:{
		width:150,
		height:150,
	   	borderRadius:75,
	   	borderWidth:2/PixelRatio.get(),
	   	borderColor:'rgb(23, 201, 180)',
	   	justifyContent:'center',
	   	alignItems:'center',	
	},
	view2:{
		width:100,
		height:100,
		margin:25,       
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white',
        borderWidth:0,
	},
	text:{
		fontSize:18,
		color:'rgb(23, 201, 180)',
		margin:30,
	},
	text2:{
		fontSize:16,
		color:'#323232',
		paddingTop:15,
		textAlign:'auto',
	},
});

module.exports = style_deviceUpload;
