'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var styles_viewpager = StyleSheet.create({
  container: {
    flex: 1,
    width:deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
   backgroundColor: '#F5FCFF',
  },

  img:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
});

module.exports = styles_viewpager;