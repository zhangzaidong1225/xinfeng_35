'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_singlemodel = StyleSheet.create({

	container:{
		width:deviceWidth,
	    height:deviceHeight * 132 /640,
	    justifyContent: 'center',
	    alignItems: 'center',
	    backgroundColor: '#FFFFFF',
	    flexDirection:'column',
	    position:'absolute',
	    // bottom:62,
	},
	circle:{
		width: (deviceWidth-80) / 2,
	    height: 40,
	    borderRadius:8,
	    borderWidth:1/PixelRatio.get(),
	    borderColor:"#2AB9F1",
	    position: 'absolute',
	    left: 0,
	    top: 0,
	},
	view1:{
		flex:1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	view2:{
		flex:1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	view3:{
		width:deviceWidth,
		height:40,
		backgroundColor:'#FFFFFF',
		justifyContent: 'center',
		alignItems: 'center',
	},
	view4:{
		width:deviceWidth-80,
		height:40,
        borderRadius:8,
        borderWidth:1/PixelRatio.get(),
        borderColor:'#2AB9F1',
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
        backgroundColor:'#FFFFFF',
	},
	view5:{
		flex:1,
		alignItems:'center',
		justifyContent:'center',
	},
	text1:{
		fontSize:14,
		// color:'black',
		textAlign:'center'
	},
	view6:{
		flex:1,
		alignItems:'center',
		justifyContent:'center',
	},
	text2:{
		fontSize:14,
		// color:'black',
		textAlign:'center'
	},
	timer_view1:{
		backgroundColor:'#FFFFFF',
		width:deviceWidth,
		height:50,
	},
	timer_view2:{
		backgroundColor:'#FFFFFF',
		width:deviceWidth,
		height:50,
		alignItems:'center',
		justifyContent:'center',
		flexDirection:'row',
	},
	timer_view3:{
		flex:1,
		backgroundColor:'#FFFFFF'
	},
	timer_view4:{
		flex:2,
		backgroundColor:'#FFFFFF'
	},
	timer_view5:{
		alignItems:'center',
		justifyContent:'center',
		flexDirection:'row',
	},
	timer_text1:{
		fontSize:18,
		color:'#2AB9F1',
		textAlign:'center',
	},
	timer_text2:{
		fontSize:14,
		color:'#2AB9F1',
		textAlign:'center',
	},
	timer_img1:{
		width:15,
		height:10,
		resizeMode:'stretch',
		
	},
	timer_view6:{
		flex:1,
		backgroundColor:'#FFFFFF',
		justifyContent:'flex-end',
	},
	timer_view7:{
		marginTop:10,
		marginBottom:10,
		width:100,
		height:40,
        // backgroundColor:'#2AB9F1',
        borderWidth:1/PixelRatio.get(),
        // borderColor:'#2AB9F1',
        borderRadius:8,
        alignItems:'center',
        justifyContent:'center',
	},
	timer_text3:{
		fontSize:14,
		color:'white',
		textAlign:'center',
	},

});

module.exports = styles_singlemodel;
