'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,

} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var styles_circle = StyleSheet.create({
  text:{
    fontSize:14,
    color:'#E6E6E6',
    textAlign:'center',
  },
  text_frame:{
    alignItems:'center',
    justifyContent:'center',
  },
  circle_frame:{
    width:250,
    alignItems:'center',
    justifyContent:'center',
  },
  icon_frame:{
    position:'absolute',
    height:70,
    width:70,
    top:0,
    left:200,
  },
  icon:{
    height:35,
    width:35,
    position:'absolute',
  },
});
module.exports = styles_circle;
