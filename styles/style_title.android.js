'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;


var styles_title = StyleSheet.create({
	title : {
		height : mheight / 10,
		width : mwidth,
	},

	textContainer : {
		backgroundColor:'#DCDCDC',
        height:mheight/10,
        // flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position : 'absolute',
        width : mwidth,
	},

	text : {
		fontSize:17,
    	color:'#323232',
	},

	backContainer : {
		position : 'absolute',
	},

	backImage : {
		marginTop : (mheight / 10 - 24) / 2,
		height:24,
    	width:24,
    	marginLeft : 28,
	},

	touch : {
		height : mheight / 10,
		width : mwidth / 5,
	},

	hometitle : {
		height:mheight / 10,
		alignItems: 'center',
		justifyContent: 'space-around',//'center',
		width : mwidth,
	},
});
module.exports = styles_title;