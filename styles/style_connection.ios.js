'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth=Dimensions.get('window').width;
var deviceHeight=Dimensions.get('window').height;


var styles_modal1 = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'#FFFFFF'
	},
	head:{
		alignItems:'center',
		justifyContent:'center',
		height:180,
	},
	headText:{
		fontSize:25,
		textAlign:'center',
		color:'#808080',
	},
	view1:{
		height:40,
		marginTop:40,
        marginRight:100,
        marginLeft:100,
        justifyContent:'center',
        alignItems: 'center',
	},
	view2:{
		justifyContent:'center',
		alignItems: 'center',
		position:'absolute',
        width:deviceWidth-200,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        bottom:10,
	},
	view3:{
		width:100,
		backgroundColor:'transparent',
	},
	view3_text1:{
		textAlign:'center',
		fontSize:18,
		color:'rgb(23, 201, 180)',
	},
	view4:{
		height:40,
		marginTop:20,
        marginRight:100,
        marginLeft:100,
        justifyContent:'center',
        alignItems: 'center',
	},
	view5:{
		justifyContent:'center',
		alignItems: 'center',
		position:'absolute',
        width:deviceWidth-200,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        bottom:10,
	},
	view6:{
		width:100,
		backgroundColor:'transparent'
	},
	view6_txt:{
		textAlign:'center',
		fontSize:18,
		color:'#B0B0B0',
	},
	view7:{
		height:40,
		marginTop:20,
        marginRight:100,
        marginLeft:100,
        justifyContent:'center',
        alignItems: 'center',
	},
	view8:{
		justifyContent:'center',
		alignItems: 'center',
		position:'absolute',
        width:deviceWidth-200,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        bottom:10,
	},
	view9:{
		width:100,
		backgroundColor:'transparent'
	},
	view9_txt:{
		textAlign:'center',
		fontSize:18,
		color:'#B0B0B0',
	},

	searchView:{
		height:40,
		marginTop:20,
        marginRight:100,
        marginLeft:100,
        justifyContent:'center',
        alignItems: 'center',
	},
	view10:{
		justifyContent:'center',
		alignItems: 'center',
		position:'absolute',
        width:deviceWidth-200,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        bottom:10,
	},
	view11:{
		width:100,
		backgroundColor:'transparent'
	},
	view11_txt:{
		textAlign:'center',
		fontSize:18,
		color:'#B0B0B0',
	},
  
    modal2_container:{
	    backgroundColor:'white',
	    alignItems:'center',
	    height:180,
	    width:deviceWidth-20,
	    marginTop:50,
    },
    modal2_view:{
      backgroundColor:'white',
      height:180,
      width:deviceWidth-20,
      position:'absolute',
    },
    modal2_view1:{
      backgroundColor:'white',
      position:'absolute',
      width:deviceWidth - 20,
      height:60,
      alignItems:'center',
      justifyContent:'center'
    },
    modal2_text:{
      color: 'black',
      fontSize: 18,
      textAlign:'center',
    },

    modal2_view2:{
      flexDirection:'row',
      marginLeft:10,
      marginRight:10,
      position:'absolute',
      marginBottom:10,
      marginTop:120,
      height:40,
      width:deviceWidth-40,
      justifyContent:'center',
      alignItems:'center',
    },

    modal2_close:{
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,bottom:0,
    },
    modal2_closetext:{
      width:50,
      backgroundColor:'transparent',

    },

    modal2_wait:{
      left:10,
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,
        bottom:0,
    },
    text_input:{
        marginTop:60,
        marginRight:40,
        marginLeft:40,
        justifyContent:'center',
        alignItems: 'center',
        height:40,
        borderWidth: 2 / PixelRatio.get(),
    },
    modal2_cancel:{
      right:10,
      justifyContent:'center',
      alignItems: 'center',
      position:'absolute',
        width:100,
        height:40,
        borderColor:'rgb(23, 201, 180)',
        borderWidth: 2 / PixelRatio.get(),
        borderRadius:20,bottom:0,
    },

    modal2_input:{
    	fontSize:13,
    	marginLeft:10,
    	width:deviceWidth-100,
    	height:40,
    },
});

module.exports = styles_modal1;