import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var style_safe = StyleSheet.create({
	scrollview:{
		height:deviceHeight-120,
		width:deviceWidth
	},
	container:{
		backgroundColor:'#FFFFFF'
	},
	view1:{
		height:deviceHeight * 1/4,
		backgroundColor:'white',
		marginTop:80,
	},
	view1_1:{
		marginLeft:20,
		marginRight:20,
		flexDirection:'column',
	},
	view1_2:{
		borderWidth:1/PixelRatio.get(),
		borderColor:'black',
		borderRadius:5,
        width:deviceWidth - 40,
        height:120,
        flexDirection:'column',
	},
	view1_3:{
		flexDirection:'row',
		borderBottomWidth:1/PixelRatio.get(),
		borderColor:'#C8C8C8',
	},
	image_view:{
		height:50,
		width:50,
		borderColor:'#C8C8C8',
        borderRightWidth:1/PixelRatio.get(),
        borderTopLeftRadius:5,
        backgroundColor:'#00000000',
        justifyContent:'center',
        alignItems:'center'
	},
	image:{
		width:30,
		height:20,
		resizeMode:'stretch'
	},
	textView:{
		height:70,
		flexDirection:'column',
		justifyContent:'center',
	},
	textView2:{
		marginLeft:10,
		justifyContent:'center',
        alignItems:'center'
		//marginTop:15,
	},
	text1:{
		fontSize:12,
		color:'rgb(23, 201, 180)',
		//padding:10,
	},

	view_text:{
		height:50,
		width:deviceWidth,
		justifyContent:'center',
		backgroundColor:'white',
		marginTop:20,
	},

	view_text1:{
		marginLeft:20,
		marginRight:20,
		justifyContent:'center',
		backgroundColor:'white',
	},

	firstText1:{
		fontSize:14,
		color:'black',
	},
	firstText2:{
		fontSize:14,
		color:'rgb(23, 201, 180)',
	},
	firstText3:{
		fontSize:14,
		color:'black',
	},
	firstText4:{
		fontSize:14,
		color:'rgb(23, 201, 180)',
	},
	firstText5:{
		fontSize:14,
		color:'black',
	},

});

module.exports = style_safe;
