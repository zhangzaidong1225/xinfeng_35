'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,

} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_slider = StyleSheet.create({
    container:{
      flexDirection:'column',
      alignItems:'stretch',
      marginLeft:10,
      marginRight:10,
    },
    textLH:{
      fontSize:11,
      color:'white',
    },
    container_gray:{
      flexDirection:'row',
      marginRight:10,
      alignItems:'stretch',
      marginLeft:10,
    },
    rectangle:{
      width:deviceWidth-20-50-10,
      height:40,
      backgroundColor:'#8C9696',
    },
    slider:{
      flex:1,
      height:40,
    },
    track: {
      height: 40,
      borderRadius: 1,
      backgroundColor: '#8C9696',
      marginTop:0,
    },
    thumb: {
      width: 1,
      height: 40,
      borderRadius: 1,
      backgroundColor: '#50C8FF',
    },
    img:{
      width:50,
      height:40,
      marginLeft:10,
      backgroundColor:'#50C8FF'
    },
    text:{
      textAlign:'center',
      fontSize:11,
      color:'#E5E5E5',
    },
});


module.exports = styles_slider;
