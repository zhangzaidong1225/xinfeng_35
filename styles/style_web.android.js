'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_web = StyleSheet.create({
	 container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection:'column',
    width:deviceWidth,
    height:deviceHeight,
  },
  welcome: {
    backgroundColor:'rgba(255,255,255,.5)',
    alignItems:'center',
    justifyContent:'center',
    height:30,
    width:60,
    position:'absolute',
    top:5,right:10,
  },

  webview:{
	 width:deviceWidth,
	 height:deviceHeight,
  },
  webview_page: {
    width:deviceWidth,
    height:deviceHeight * 0.8 - 19.5,
  },
  webview_protocol: {
    width:deviceWidth,
    flex:1,
  },
  webview_error: {

  },
  text:{
  	color:'#323C4B',
  	fontSize:18,
  	padding:3,
  },

  pbfillStyleShow: {
    height: 3,
  },
  pbfillStyleHide: {
    height: 0,
  },
  pbBackgroundStyle: {
    backgroundColor: '#ffffff', 
    borderRadius: 3,
  },
  pbStyleShow: {
    marginTop: 0, 
    width: deviceWidth, 
    
  }, 
  pbStyleHide: {
    height: 0,
  },
});

module.exports = styles_web;
