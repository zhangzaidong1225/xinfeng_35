'use strict'

var React = require('react-native');

var {
  StyleSheet,

} = React;


var styles_hometopview = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
    left:10,
    top:10,
    right:10
  },
  left_view:{
    paddingTop:14,
    paddingLeft:11,
    paddingRight:10,
    paddingBottom:10,
    fontSize:18,
    color:'black',
    lineHeight:5,
    textAlign:'left'
  },
  right_view:{
    padding:10,
    fontSize:16,
    color:'#EEEEEE',
    lineHeight:20,
    textAlign:'right',
  },
  center_view:{
    padding:10,
    fontSize:14,
    color:'black',
    lineHeight:5,
    textAlign:'center',
  },
  left_image:{
    left:-5,
    top:5,
    width:40,
    height:40,
  },
  right_image:{
    top:12,
    width:25,
    height:25,
    right:5,
    resizeMode:'contain',
  },
  left_little_text:{
    padding:10,
    color:'black',
    lineHeight:5,
    textAlign:'left',
    left:-10,
    fontSize:10
  },
  left_pm_view:{
    flexDirection:'row',
    top:-5
  }
});
module.exports = styles_hometopview;
