'use strict'

var React = require('react-native');

var {
  StyleSheet,
  Dimensions,

} = React;

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var styles_circle = StyleSheet.create({
  text:{
    fontSize:14,
    color:'#E6E6E6',
    textAlign:'center',
  },
  text_frame:{
    alignItems:'center',
    justifyContent:'center',
  },
  circle_frame:{
    width:deviceHeight*0.35,
    alignItems:'center',
    justifyContent:'center',
  },
  icon_frame:{
    position:'absolute',
    backgroundColor:'#00000000',
    height:50,
    width:50,
    top:0,
    left:deviceWidth/2,
  },
  icon:{
    height:35,
    width:35,
    position:'absolute',
  },
});
module.exports = styles_circle;
