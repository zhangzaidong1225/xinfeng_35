'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height - 19.5;


var styles_wheeltimerpicker = StyleSheet.create({

	background: {
		width:mwidth,
		height:mheight,
		top:0, 
		left: 0,
		backgroundColor:'rgba(0,0,0,0.4)',
		position:'absolute',
	},
	backgroundtouch : {
		width:mwidth,
		height:mheight * 218.5 / 320  - 5,
		top : 0,
	},

	container : {
	    justifyContent: 'center',
	    alignItems: 'center',
	    top : 0,
	    // top : mheight * 218.5 / 320 - 5,
	    width :  mwidth,
	    height : mheight * 101.5 / 320,
	},

	picker: {
		width : mwidth,
		height : mheight * 71.5 / 320,
		borderRadius : 10,
	},

	touchableOpacity : {
		justifyContent: 'center',
	    alignItems: 'center',
		width : mwidth,
		height : mheight * 22 / 320,
		marginTop : mheight * 3 / 320,
		backgroundColor : 'white',

		borderRadius : 10,
	},

	// itemStyle : {
	// 	color:'#2AB9F1', 
	// 	fontSize:20,
	// },

	text : {
		color: '#2AB9F1',
		fontSize: 25,
	},


});
module.exports = styles_wheeltimerpicker;