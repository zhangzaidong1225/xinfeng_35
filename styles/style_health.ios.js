'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_health = StyleSheet.create({
	scrollView:{
		height:deviceHeight,
		width:deviceWidth,
	},
	container:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
	},
	header:{
		backgroundColor:'#DCDCDC',
        height:deviceHeight/10,
        flexDirection: 'row',
	},
	first:{
		marginTop:50,
        flexDirection:'row',
		height:40,
		borderWidth:1/PixelRatio.get(),
	},
	selectList:{
		width:70,
		height:90,
        top:90,
        position:'absolute',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:20,
        backgroundColor:'#ffffffff',
        
        
	},
	mailView:{
		height:45,
		alignItems:'center',
		justifyContent:'center',
		flex:1,
		left:90,
		right:20,
        borderWidth:1/PixelRatio.get(),
        borderLeftWidth:0,
        borderColor:'gray',
        top:90,
        position:'absolute',

	},
	mail:{
		textAlign:'center',
		paddingLeft:5,
		height:20,
		fontSize:13,
	},
	
	detailView:{
		flex:3,
		width:deviceWidth-40,
		position:'absolute',
		borderWidth:1/PixelRatio.get(),
		top:150,
		marginRight:20,
		marginLeft:20,
		borderColor:'gray',
	},
	detail:{
		height:200,
		borderColor:'gray',
        borderWidth:1/PixelRatio.get(),
        paddingTop:20,
        paddingLeft:20,
        fontSize:13,
	},
	commitView:{
		height:45,
		position:'absolute',
		marginLeft:20,
		marginRight:20,
        bottom:10,
        justifyContent:'center',
        alignItems: 'center',
	},
	borderView:{
		justifyContent:'center',
		alignItems: 'center',
         position:'absolute',
		 width:deviceWidth-42,
		 height:46,
         borderColor:'rgb(23, 201, 180)',
		 borderWidth: 2 / PixelRatio.get(),
		 borderRadius:25,
         bottom:10,
	},
	commitText:{
		textAlign:'center',
		fontSize:18,
		color:'#323232',
	},
	textBorder:{
		width:220,
		height:30,
		justifyContent:'center',
  	    alignItems: 'center',
	},
	
	

});

module.exports = styles_health;
