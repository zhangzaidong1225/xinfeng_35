'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;
var styles_timerpicker = StyleSheet.create({

	itemStyleUndefined: {
		height : 0,
		width : 0,
	},

	itemTextStyleUndefined: {
		color : 'black',
		fontSize : 20, 
	},

});
module.exports = styles_timerpicker;