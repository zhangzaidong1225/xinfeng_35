'use strict'
import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height;


var styles_wheeltimepicker = StyleSheet.create({

	background: {
		width:mwidth,
		height:mheight,
		top:0, 
		left: 0,
		backgroundColor:'rgba(0,0,0,0.4)',
		position:'absolute',
	},
	backgroundtouch : {
		width:mwidth,
		height:mheight * 218.5 / 320  - 5,
		top : 0,
	},

	container : {
	    justifyContent: 'center',
	    alignItems: 'center',
	    top : 0,
	    // top : mheight * 218.5 / 320 - 5,
	    width :  mwidth,
	    height : mheight * 1 / 4,//mheight * 101.5 / 320,
	},

	picker: {
		width : mwidth,
		height : mheight * 71.5 / 320,
		borderRadius : 10,
	},

	hourpicker : {
		width : mwidth/2,
		height : mheight * 71.5 / 320,
		borderRadius : 10,
		position:'absolute',
	},

	minutepicker : {
		width : mwidth/2,
		marginLeft: mwidth/2,
		height : mheight * 71.5 / 320,
		borderRadius : 10,
		position:'absolute',

	},

	touchableOpacity : {
		justifyContent: 'center',
	    alignItems: 'center',
		width : mwidth,
		height : mheight * 22 / 320,
		marginTop : mheight * 3 / 320,
		backgroundColor : 'white',
		borderRadius : 10,
	},

	// itemStyle : {
	// 	color:'#2AB9F1', 
	// 	fontSize:20,
	// },

	text : {
		color: '#2AB9F1',
		fontSize: 25,
	},


});
module.exports = styles_wheeltimepicker;