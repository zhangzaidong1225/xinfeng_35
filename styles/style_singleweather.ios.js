'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,

} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_singleweather = StyleSheet.create({
	container:{
		backgroundColor:'#FFFFFF',
		flexDirection:'row',
		width:deviceWidth,
		height:deviceHeight * 65 / 640 ,
		// marginLeft:10,
		// marginRight:10,
	},
	view1:{
		backgroundColor:'#FFFFFF',
		flexDirection:'row',
		width:deviceWidth,
		height:deviceHeight* 65 / 640,
	},
	view2:{
		flex:1.5,
		flexDirection:'row',
		backgroundColor:'#FFFFFF',
	},
	view3:{
		flex:1.5,
		flexDirection:'column',
	},
	view4:{
		flex:1.5,
		backgroundColor:'#FFFFFF',
		justifyContent:'center',
		alignItems:'flex-start',
	},
	text1:{
		fontSize:20,
		color:'#6D6D6D',
		textAlign:'center',
	},
	view5:{
		flex:1.5,
		backgroundColor:'#FFFFFF',
		flexDirection:'row',
	},
	view6:{
		// flex:1.5,
		alignItems:'center',
		justifyContent:'flex-start',
	},
	text2:{
		fontSize:25,
		color:'#2AB9F1',
		textAlign:'center',
	},
	view7:{
		// flex:1,
		backgroundColor:'#FFFFFF',
		alignItems:'center',
		justifyContent:'flex-end',
		paddingLeft:5,
	},
	text3:{
		fontSize:9,
		color:'#939393',
		textAlign:'center',
		textAlignVertical:'bottom',
	},
	view8:{
		// flex:0.6,
		justifyContent:'center',
		alignItems:'center',
	},
	img1:{
		height:36,
		width:35,
		resizeMode:'stretch',
	},
	view9:{
		flex:1,
	},
	view10:{
		flex:1.2,
		flexDirection:'row',
		backgroundColor:'#FFFFFF',
	},
	view11:{
		flex:1.8,
		backgroundColor:'#FFFFFF',
		justifyContent:'center',
		alignItems:'flex-end',
	},
	img2:{
		height:23,
		width:18,
		resizeMode:'stretch'
	},
	view12:{
		flex:2,
		backgroundColor:'#FFFFFF',
		flexDirection:'column',
		justifyContent:'center',
	},
	text4:{
		fontSize:14,
		color:'#404040',
		textAlign:'center',
	},
	text5:{
		fontSize:10,
		color:'#404040',
		textAlign:'center',
	},
});

module.exports = styles_singleweather;

