'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var styles_selectList = StyleSheet.create({
	container1:{
		alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#ffffff',
      	borderWidth:1/PixelRatio.get(),
      	height:90,
	},
	textView1:{
		alignItems:'center',
        justifyContent:'center',
        borderRightWidth:0,
        backgroundColor:'#3BB495',
        borderColor:'gray',
        height:45,
        width:70,
	},
	text1:{
		textAlign:'center',
		fontSize:20,
		color:'#ffffff',
	},
    text2:{
		textAlign:'center',
		fontSize:20,
		color:'gray',
	},
	img1:{
		left:30,
		position:'absolute',
		bottom:0,
	},
	textView2:{
		alignItems:'center',
		justifyContent:'center',
		height:45,
		width:70,	
		backgroundColor:'#ffffff',	
		borderWidth:1/PixelRatio.get(),		
	},

	textView3:{
		alignItems:'center',
		justifyContent:'center',
        backgroundColor:'#3BB495',
        borderLeftWidth:1,
        borderTopWidth:1/PixelRatio.get(),
        borderBottomWidth:1/PixelRatio.get(),
        borderColor:'gray',
        height:45,
        width:70,		
	},
	textView4:{
	  alignItems:'center',
      justifyContent:'center',
      height:45,
      width:70,
      backgroundColor:'gray',
      opacity:0,
	},
});

module.exports = styles_selectList;
