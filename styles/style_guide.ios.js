'use strict'

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  PixelRatio,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var styles_guide = StyleSheet.create({
  container: {
    flex: 1,
    width:deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
	height:deviceHeight,
   backgroundColor: '#F5FCFF',
  },
  img:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    width:deviceWidth,
    height:deviceHeight,
  },
});

module.exports = styles_guide;
