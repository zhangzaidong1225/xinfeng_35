/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import styles_selectList from './styles/style_selectList'
import styles_health from './styles/style_health'
import  config from './config'
import tools from './tools'
var UMeng = require('./UMeng');
import Title from './title'
import Language from './Language'
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableHighlight,
  ToastAndroid,
  Navigator,
  TextInput,
  ScrollView, 
  NetInfo,
  Alert,
} from 'react-native';


var deviceWidth=Dimensions.get('window').width;
var deviceHeight=Dimensions.get('window').height;
var imagecommit=require('image!ic_commit_1');
//var DEFAULT_URL = 'http://feibao.esh001.com/iface/info/feedback';

var FeedPage = React.createClass({
	
getInitialState(){
    return {
      contact:'',
      content:'',
    }
  },

  render(){
	var navigator = this.props.navigator;	
    return (
	<View style = {styles_feedback.view_all}>
        <ScrollView style={styles_health.scrollView}  scrollEnabled={false}>
            <Title title = {Language.feedback} hasGoBack = {true}  navigator = {this.props.navigator}/>
            <View style={styles_feedback.view2}>
                     <View style={styles_feedback.view3}>
                                <View style={styles_feedback.view4}>
                                    <Image style={[{resizeMode:'stretch',},styles_feedback.view5]} source={require('image!ic_feedback')}/>
                                </View>
                                <View style={styles_feedback.view6}>
                                    <Image style={[{resizeMode:'stretch',},styles_feedback.view7]} source={require('image!ic_dividing_line')}/>
                                </View>
                                <View style={styles_feedback.view8}>
                                  <TextInput
                                      style = {styles_feedback.mail}
                                      underlineColorAndroid='transparent'
                                      textAlignVertical='center'
                                      onChangeText = {(text) => this.setState({contact:text})}
                                      onChange = {() => {imagecommit=require('image!ic_commit_2');}}
                                      value = {this.state.contact}
                                      placeholderTextColor = '#CCCCCC'
                                      placeholder = {Language.phonenumberrequired}/>
                                </View>
                      </View>
                      <View style={styles_feedback.view1_1}>
                                    <TextInput
                                      style = {[styles_feedback.detail, {textAlign:'left',textAlignVertical:'top'}]}
                                      multiline={true}
                                      underlineColorAndroid='transparent'
                                      textAlignVertical='top'
                                      onChangeText = {(text) => this.setState({content:text})}
                                      onChange = {() => {imagecommit=require('image!ic_commit_2');}}
                                      value = {this.state.content}
                                      placeholderTextColor = '#CCCCCC'
                                      placeholder = {Language.problem}/>
                      </View>               
            </View>
        </ScrollView>      
        <View style = {styles_feedback.commitView}>
              <View style={styles_feedback.borderView}>
                <TouchableOpacity
                    onPress = {this._handleSubmit}>
                   <View style = {styles_feedback.textBorder}>
                    <Text allowFontScaling={false} style={styles_feedback.commitText}>{Language.commit}</Text>
                   </View>
                </TouchableOpacity>
              </View>

        </View>
      </View>
    );
  },

  _handleSubmit(){
    UMeng.onEvent('feedback_02');
    if (this.state.contact === ''||this.state.contact === '' ){
        tools.alertShow(Language.phonenumbernotempty);
        return;
    }
    if(this.state.content === ''||this.state.content === ''){
      tools.alertShow('请输入反馈内容');
      return;
    }
    if (this.telRuleCheck2(this.state.contact) || this.mailCheck(this.state.contact)){
      NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if(isConnected){
          var DEFAULT_URL = config.server_base+config.feedback_uri+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;
          console.log('DEFAULT_URL: ' + DEFAULT_URL);
          fetch(DEFAULT_URL,{
              method:'post',
              headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
              },
              body:JSON.stringify({
                type:this.state.type,
                contact:this.state.contact,
                content:this.state.content,
              })
            })
              // ToastAndroid.show('提交成功',ToastAndroid.SHORT);
              tools.alertShow(Language.submitted);
              this.setState({
                contact:'',
                content:'',
              });
          }else{
            // ToastAndroid.show(Language.connectnetwork,ToastAndroid.SHORT);
            tools.alertShow(Language.connectnetwork);
          }
        });
      }else {
                // ToastAndroid.show(Language.phoneoremailnotcorrect,ToastAndroid.SHORT);
          tools.alertShow(Language.phoneoremailnotcorrect);
      }

  },

  telRuleCheck2(string) {
    var pattern = /^1[34578]\d{9}$/;
    if (pattern.test(string)) {
        return true;
    }
    return false;
  },

  mailCheck(string){
     var res= /\w@\w*\.\w/;
     if (res.test(string)){
       return true;
     }
     return false;
  },

});




module.exports = FeedPage;
