/**
* AsyncStorage:
* ios from AsyncStorage and android from RCTSZTYSharedPreferencesModule,
* 
*/

'use strict';


// var { NativeModules } = require('react-native');
var SZTYSharedPreferences = require('NativeModules').SZTYSharedPreferences;

var RCTAsyncStorage = require('./node_modules/react-native/Libraries/Storage/AsyncStorage');


var SharedPreferences = {

	getItem: function(
		key: string,
		// def: string,
    	callback?: ?(error: ?Error, result: ?string) => void) {

		SZTYSharedPreferences.getString(key, null, callback);
		// RCTAsyncStorage.getItem(key, callback);
	},

	setItem: function(
		key: string,
    	value: string,
    	callback?: ?(error: ?Error) => void) {

		SZTYSharedPreferences.putString(key, value, callback);
		// RCTAsyncStorage.setItem(key, value, callback);
	},


  removeItem: function(
    key: string,
    callback?: ?(error: ?Error) => void) {

  	SZTYSharedPreferences.remove(key, callback);

  },
};

module.exports = SharedPreferences;
