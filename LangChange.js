'use strict'
import Language from './Language'
import English from './English'
import Chinese from './Chinese'

var LangChange = {
	
	changeLang:function(lang) {
	  if (lang == 'zh'){
	  	Language.mydevice = Chinese.mydevice;
	  	Language.mall = Chinese.mall;
	  	Language.personal = Chinese.personal;
	  	Language.xinfeng = Chinese.xinfeng;
	  	Language.xinfengTest = Chinese.xinfengTest;
	  	Language.sleep = Chinese.sleep;
		Language.aqi = Chinese.aqi;
		Language.close = Chinese.close;
		Language.awake = Chinese.awake;
		Language.location = Chinese.location;
		Language.filterState = Chinese.filterState;
		Language.normal = Chinese.normal;

	  	Language.h = Chinese.h;
	  	Language.min = Chinese.min;
	  	Language.notdetected = Chinese.notdetected;
	  	Language.tutorial = Chinese.tutorial;
		Language.usage = Chinese.usage;

		Language.phonenumber = Chinese.phonenumber;
		Language.passwordaccount = Chinese.passwordaccount;
		Language.register = Chinese.register;
		Language.prompt = Chinese.prompt;
		Language.ok = Chinese.ok;
		Language.cancel = Chinese.cancel;

	  	Language.more = Chinese.more;
	  	Language.filterelement = Chinese.filterelement;
	  	Language.softwareupdate = Chinese.softwareupdate;
		Language.firmwareupdate = Chinese.firmwareupdate;
		Language.about = Chinese.about;

		Language.devicesecuritycheck = Chinese.devicesecuritycheck;
		Language.automatic = Chinese.automatic;
		Language.locationtitle = Chinese.locationtitle;
		Language.selected = Chinese.selected;

		Language.allareas = Chinese.allareas;
	  	Language.search = Chinese.search;
	  	Language.stop = Chinese.stop;
	  	Language.paireddevice = Chinese.paireddevice;
		Language.availabledevice = Chinese.availabledevice;

		Language.connected = Chinese.connected;
		Language.connecting = Chinese.connecting;
		Language.disconnect = Chinese.disconnect;
		Language.cancelthepairing = Chinese.cancelthepairing;
		Language.finddevice = Chinese.finddevice;
//1
		Language.rename = Chinese.rename;
	  	Language.connectedbluetoothdevice = Chinese.connectedbluetoothdevice;
	  	Language.quitapp = Chinese.quitapp;
		Language.phoenformatnotcorrent = Chinese.phoenformatnotcorrent;
		Language.namecannotempty = Chinese.namecannotempty;

		Language.phonenumbernotempty = Chinese.phonenumbernotempty;
		Language.submitted = Chinese.submitted;
		Language.connectnetwork = Chinese.connectnetwork;
		Language.phoneoremailnotcorrect = Chinese.phoneoremailnotcorrect;

		// Language.latestversion = Chinese.latestversion;
		Language.xinfenglatestversion = Chinese.xinfenglatestversion;
		Language.singlelatestver = Chinese.singlelatestver,
		Language.vehidelatestver = Chinese.vehidelatestver,
	  	Language.unupgrade = Chinese.unupgrade;
	  	Language.powerlow = Chinese.powerlow;
	  	Language.avaliablefirmwareupdate = Chinese.avaliablefirmwareupdate;
		Language.upgrading = Chinese.upgrading;

		Language.upgradefailalert = Chinese.upgradefailalert;
		Language.upgradesuccess = Chinese.upgradesuccess;
		Language.clickgrade = Chinese.clickgrade;
		Language.upgradefail = Chinese.upgradefail;
		Language.upgrademsg1 = Chinese.upgrademsg1;
		Language.upgrademsg2 = Chinese.upgrademsg2;
		Language.upgrademsg3 = Chinese.upgrademsg3;
		Language.upgradesuccessed = Chinese.upgradesuccessed;
	//2
		Language.readyupgrade = Chinese.readyupgrade;
	  	Language.login = Chinese.login;
	  	Language.retrievepassword = Chinese.retrievepassword;
		Language.sms = Chinese.sms;
		Language.passwordcombination = Chinese.passwordcombination;

		Language.getsms = Chinese.getsms;
		Language.useragreement = Chinese.useragreement;
		Language.accountlogin = Chinese.accountlogin;
		Language.loginsuccess = Chinese.loginsuccess;

		Language.feedback = Chinese.feedback;
	  	Language.hardware = Chinese.hardware;
	  	Language.software = Chinese.software;
	  	Language.phonenumberrequired = Chinese.phonenumberrequired;
		Language.problem = Chinese.problem;

		Language.version = Chinese.version;
		Language.product = Chinese.product;
		Language.xinfengair = Chinese.xinfengair;
		Language.servicephone = Chinese.servicephone;
		Language.url = Chinese.url;
		Language.score = Chinese.score;
		Language.unconnecteddevice = Chinese.unconnecteddevice;
//3
		Language.xinfengautheticfilter = Chinese.xinfengautheticfilter;
	  	Language.buy = Chinese.buy;
	  	Language.currentdevice = Chinese.currentdevice;
		Language.filterpurchase = Chinese.filterpurchase;
		Language.verification_code_empty = Chinese.verification_code_empty;

		Language.password_not_empty = Chinese.password_not_empty;
		Language.verification_code_error = Chinese.verification_code_error;
		Language.phone_not_exist = Chinese.phone_not_exist;
		Language.network_request_failed = Chinese.network_request_failed;

		Language.phone_password_error = Chinese.phone_password_error;
	  	Language.password_change_success = Chinese.password_change_success;
	  	Language.fearless = Chinese.fearless;
	  	Language.select_device = Chinese.select_device;
		Language.anti_counterferting = Chinese.anti_counterferting;

		Language.special_region = Chinese.special_region;
		Language.municipality = Chinese.municipality;
		Language.please_connect_device = Chinese.please_connect_device;
		Language.wind_speed = Chinese.wind_speed;
		Language.close_dev = Chinese.close_dev;
		Language.hazehits = Chinese.hazehits;
		Language.corresponding = Chinese.corresponding;
	//4
		Language.timesphonenumber = Chinese.timesphonenumber;
	  	Language.select_user_agreement = Chinese.select_user_agreement;
	  	Language.confirm_close_dev = Chinese.confirm_close_dev;
		Language.read_agree = Chinese.read_agree;
		Language.forgetpassword = Chinese.forgetpassword;

		Language.connect_smart_dev = Chinese.connect_smart_dev;
		Language.charger_unusual = Chinese.charger_unusual;
		Language.forcedtoupgrade = Chinese.forcedtoupgrade;
		Language.dev_force_upgrade = Chinese.dev_force_upgrade;

		Language.foundversion = Chinese.foundversion;
	  	Language.exit = Chinese.exit;
	  	Language.discount = Chinese.discount;
	  	Language.passwordreset = Chinese.passwordreset;
		Language.usestandardcharger = Chinese.usestandardcharger;

		Language.chargerabnormal = Chinese.chargerabnormal;
		Language.loginout = Chinese.loginout;
		Language.know = Chinese.know;
		Language.commit = Chinese.commit;
		Language.submit = Chinese.submit;
		Language.sn = Chinese.sn;
		Language.latestsoftversion = Chinese.latestsoftversion;
	  	Language.currentversion = Chinese.currentversion;
	  	Language.updateversion = Chinese.updateversion;
	  	Language.checknewversion = Chinese.checknewversion;
	  	Language.unconnected = Chinese.unconnected;
	  	Language.check = Chinese.check;
	  	Language.filterair = Chinese.filterair;
	  	Language.detectedversion = Chinese.detectedversion;
	  	Language.currentposition = Chinese.currentposition;
	  	Language.latestdevversion = Chinese.latestdevversion;
	  	Language.securityalert = Chinese.securityalert;
	  	Language. portablepurifier = Chinese.portablepurifier,
	  	Language.securitycheck = Chinese.securitycheck;

	//5
		Language.real_time = Chinese.real_time;
	  	Language.timed = Chinese.timed;
	  	Language.detectnow = Chinese.detectnow;
		Language.detecting = Chinese.detecting;
		Language.operationfailed = Chinese.operationfailed;

		Language.automation = Chinese.automation;
		Language.on = Chinese.on;
		Language.off = Chinese.off;
		Language.insertingdev = Chinese.insertingdev;

		Language.automation_on = Chinese.automation_on;
	  	Language.plugdetectiondev = Chinese.plugdetectiondev;
	  	Language.vehidefilterstate = Chinese.vehidefilterstate;
	  	Language.historydata = Chinese.historydata;
		Language.timedswitch = Chinese.timedswitch;

		Language.repeatevery = Chinese.repeatevery;
		Language.mon = Chinese.mon;
		Language.tue = Chinese.tue;
		Language.wed = Chinese.wed;
		Language.thu = Chinese.thu;
		Language.fri = Chinese.fri;
		Language.sat = Chinese.sat;
	  	Language.sun = Chinese.sun;

	  	Language.choose_repeat_cycle = Chinese.choose_repeat_cycle;
	  	Language.delete = Chinese.delete;
	  	Language.save = Chinese.save;
	  	Language.edit = Chinese.edit;
	  	Language.settings = Chinese.settings;
	  	Language.weekday = Chinese.weekday;
	  	Language.weekend = Chinese.weekend;
	  	Language.start_to_purify = Chinese.start_to_purify;
	  	Language.plug_monitor = Chinese.plug_monitor;

	  	Language.shop = Chinese.shop;
	  	Language.hepafilter = Chinese.hepafilter;
	  	Language.xinfengcarfilter = Chinese.xinfengcarfilter;
	  	Language.xinfengairboxfilter = Chinese.xinfengairboxfilter;

	  	Language.home_page = Chinese.home_page;
	  	Language.add_device = Chinese.add_device;
	  	Language.filerreplace = Chinese.filerreplace;
	  	Language.suggestedreplacement = Chinese.suggestedreplacement;
	  	Language.open_cap = Chinese.open_cap;
	  	Language.registered = Chinese.registered;
	  	Language.allprovinces = Chinese.allprovinces;
	  	Language.checknetwork = Chinese.checknetwork;
	  	Language.airfiltered = Chinese.airfiltered;

	  	Language.current_check = Chinese.current_check;
	  	Language.current_check_time = Chinese.current_check_time;
	  	Language.current_user = Chinese.current_user;
	  	Language.times = Chinese.times;

		Language.single = Chinese.single;
		Language.vehide = Chinese.vehide;

		Language.upgrade = Chinese.upgrade;
		Language.back = Chinese.back;
		Language.readygrade = Chinese.readygrade;
		Language.passwordNotFormat = Chinese.passwordNotFormat;

	  } else {
	  	Language.mydevice = English.mydevice;
	  	Language.mall = English.mall;
	  	Language.personal = English.personal;
	  	Language.xinfeng = English.xinfeng;
	  	Language.xinfengTest = English.xinfengTest;
	  	Language.sleep = English.sleep;
		Language.aqi = English.aqi;
		Language.close = English.close;
		Language.awake = English.awake;
		Language.location = English.location;
		Language.filterState = English.filterState;
		Language.normal = English.normal;

	  	Language.h = English.h;
	  	Language.min = English.min;
	  	Language.notdetected = English.notdetected;
	  	Language.tutorial = English.tutorial;
		Language.usage = English.usage;

		Language.phonenumber = English.phonenumber;
		Language.passwordaccount = English.passwordaccount;
		Language.register = English.register;
		Language.prompt = English.prompt;
		Language.ok = English.ok;
		Language.cancel = English.cancel;

	  	Language.more = English.more;
	  	Language.filterelement = English.filterelement;
	  	Language.softwareupdate = English.softwareupdate;
		Language.firmwareupdate = English.firmwareupdate;
		Language.about = English.about;

		Language.devicesecuritycheck = English.devicesecuritycheck;
		Language.automatic = English.automatic;
		Language.locationtitle = English.locationtitle;
		Language.selected = English.selected;

		Language.allareas = English.allareas;
	  	Language.search = English.search;
	  	Language.stop = English.stop;
	  	Language.paireddevice = English.paireddevice;
		Language.availabledevice = English.availabledevice;

		Language.connected = English.connected;
		Language.connecting = English.connecting;
		Language.disconnect = English.disconnect;
		Language.cancelthepairing = English.cancelthepairing;
		Language.finddevice = English.finddevice;
//1
		Language.rename = English.rename;
	  	Language.connectedbluetoothdevice = English.connectedbluetoothdevice;
	  	Language.quitapp = English.quitapp;
		Language.phoenformatnotcorrent = English.phoenformatnotcorrent;
		Language.namecannotempty = English.namecannotempty;

		Language.phonenumbernotempty = English.phonenumbernotempty;
		Language.submitted = English.submitted;
		Language.connectnetwork = English.connectnetwork;
		Language.phoneoremailnotcorrect = English.phoneoremailnotcorrect;

		// Language.latestversion = English.latestversion;
		Language.xinfenglatestversion = English.xinfenglatestversion;
		Language.singlelatestver = English.singlelatestver,
		Language.vehidelatestver = English.vehidelatestver,
	  	Language.unupgrade = English.unupgrade;
	  	Language.powerlow = English.powerlow;
	  	Language.avaliablefirmwareupdate = English.avaliablefirmwareupdate;
		Language.upgrading = English.upgrading;

		Language.upgradefailalert = English.upgradefailalert;
		Language.upgradesuccess = English.upgradesuccess;
		Language.clickgrade = English.clickgrade;
		Language.upgradefail = English.upgradefail;
		Language.upgrademsg1 = English.upgrademsg1;
		Language.upgrademsg2 = English.upgrademsg2;
		Language.upgrademsg3 = English.upgrademsg3;
		Language.upgradesuccessed = Chinese.upgradesuccessed;
	//2
		Language.readyupgrade = English.readyupgrade;
	  	Language.login = English.login;
	  	Language.retrievepassword = English.retrievepassword;
		Language.sms = English.sms;
		Language.passwordcombination = English.passwordcombination;

		Language.getsms = English.getsms;
		Language.useragreement = English.useragreement;
		Language.accountlogin = English.accountlogin;
		Language.loginsuccess = English.loginsuccess;

		Language.feedback = English.feedback;
	  	Language.hardware = English.hardware;
	  	Language.software = English.software;
	  	Language.phonenumberrequired = English.phonenumberrequired;
		Language.problem = English.problem;

		Language.version = English.version;
		Language.product = English.product;
		Language.xinfengair = English.xinfengair;
		Language.servicephone = English.servicephone;
		Language.url = English.url;
		Language.score = English.score;
		Language.unconnecteddevice = English.unconnecteddevice;
//3
		Language.xinfengautheticfilter = English.xinfengautheticfilter;
	  	Language.buy = English.buy;
	  	Language.currentdevice = English.currentdevice;
		Language.filterpurchase = English.filterpurchase;
		Language.verification_code_empty = English.verification_code_empty;

		Language.password_not_empty = English.password_not_empty;
		Language.verification_code_error = English.verification_code_error;
		Language.phone_not_exist = English.phone_not_exist;
		Language.network_request_failed = English.network_request_failed;

		Language.phone_password_error = English.phone_password_error;
	  	Language.password_change_success = English.password_change_success;
	  	Language.fearless = English.fearless;
	  	Language.select_device = English.select_device;
		Language.anti_counterferting = English.anti_counterferting;

		Language.special_region = English.special_region;
		Language.municipality = English.municipality;
		Language.please_connect_device = English.please_connect_device;
		Language.wind_speed = English.wind_speed;
		Language.close_dev = English.close_dev;
		Language.hazehits = English.hazehits;
		Language.corresponding = English.corresponding;
	//4
		Language.timesphonenumber = English.timesphonenumber;
	  	Language.select_user_agreement = English.select_user_agreement;
	  	Language.confirm_close_dev = English.confirm_close_dev;
		Language.read_agree = English.read_agree;
		Language.forgetpassword = English.forgetpassword;

		Language.connect_smart_dev = English.connect_smart_dev;
		Language.charger_unusual = English.charger_unusual;
		Language.forcedtoupgrade = English.forcedtoupgrade;
		Language.dev_force_upgrade = English.dev_force_upgrade;

		Language.foundversion = English.foundversion;
	  	Language.exit = English.exit;
	  	Language.discount = English.discount;
	  	Language.passwordreset = English.passwordreset;
		Language.usestandardcharger = English.usestandardcharger;

		Language.chargerabnormal = English.chargerabnormal;
		Language.loginout = English.loginout;
	  	Language.know = English.know;
	  	Language.commit = English.commit;
	  	Language.submit = English.submit;
	  	Language.sn = English.sn;
	  	Language.latestsoftversion = English.latestsoftversion;
	  	Language.currentversion = English.currentversion;
	  	Language.updateversion = English.updateversion;
	  	Language.checknewversion = English.checknewversion;
	  	Language.unconnected = English.unconnected;
	  	Language.check = English.check;
	  	Language.filterair = English.filterair;
	  	Language.detectedversion = English.detectedversion;
	  	Language.currentposition = English.currentposition;
	  	Language.latestdevversion = English.latestdevversion;
	  	Language.securityalert = English.securityalert;
	  	Language. portablepurifier = English.portablepurifier,
	  	Language.securitycheck = English.securitycheck;
	  	
	  	Language.real_time = English.real_time;
	  	Language.timed = English.timed;
	  	Language.detectnow = English.detectnow;
		Language.detecting = English.detecting;
		Language.operationfailed = English.operationfailed;

		Language.automation = English.automation;
		Language.on = English.on;
		Language.off = English.off;
		Language.insertingdev = English.insertingdev;

		Language.automation_on = English.automation_on;
	  	Language.plugdetectiondev = English.plugdetectiondev;
	  	Language.vehidefilterstate = English.vehidefilterstate;
	  	Language.historydata = English.historydata;
		Language.timedswitch = English.timedswitch;

		Language.repeatevery = English.repeatevery;
		Language.mon = English.mon;
		Language.tue = English.tue;
		Language.wed = English.wed;
		Language.thu = English.thu;
		Language.fri = English.fri;
		Language.sat = English.sat;
	  	Language.sun = English.sun;

	  	Language.choose_repeat_cycle = English.choose_repeat_cycle;
	  	Language.delete = English.delete;
	  	Language.save = English.save;
	  	Language.edit = English.edit;
	  	Language.settings = English.settings;
	  	Language.weekday = English.weekday;
	  	Language.weekend = English.weekend;
	  	Language.start_to_purify = English.start_to_purify;
	  	Language.plug_monitor = English.plug_monitor;

	  	Language.shop = English.shop;
	  	Language.hepafilter = English.hepafilter;
	  	Language.xinfengcarfilter = English.xinfengcarfilter;
	  	Language.xinfengairboxfilter = English.xinfengairboxfilter;

	  	Language.home_page = English.home_page;
	  	Language.add_device = English.add_device;
	  	Language.filerreplace = English.filerreplace;
	    Language.suggestedreplacement = English.suggestedreplacement;
	    Language.open_cap = English.open_cap;
	    Language.registered = English.registered;
	    Language.allprovinces = English.allprovinces;
	    Language.checknetwork = English.checknetwork;
	    Language.airfiltered = English.airfiltered;

	    Language.current_check = English.current_check;
	  	Language.current_check_time = English.current_check_time;
	  	Language.current_user = English.current_user;
	  	Language.times = English.times;

	  	Language.single = English.single;
		Language.vehide = English.vehide;

		Language.upgrade = English.upgrade;
		Language.back = English.back;
		Language.readygrade = English.readygrade;
		Language.passwordNotFormat = English.passwordNotFormat;
	  }
	},
};

module.exports = LangChange;