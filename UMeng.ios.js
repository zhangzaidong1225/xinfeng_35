/**
* UMeng:
* ios from AsyncStorage and android from RCTSZTYSharedPreferencesModule,
* 
*/

'use strict';


// var { NativeModules } = require('react-native');
var RCTUMeng = require('NativeModules').UMeng;

var UMeng = {
	onProfileSignIn: function(userId: string) {
		RCTUMeng.onProfileSignIn(userId);
	},
	onProfileSignOff: function() {
		RCTUMeng.onProfileSignOff();
	},

    onPageStart: function(pageName:string) {
		RCTUMeng.onPageStart();
	},

	onPageEnd: function(pageName:string) {
		RCTUMeng.onPageEnd();
	},

	onEvent: function(eventId: string) {
		RCTUMeng.onEvent(eventId);
	},

	onEventWithValue: function(eventId: string, key: sting, value: string) {
		RCTUMeng.onEventWithValue(eventId, key, value);
	},
};

module.exports = UMeng;