'use strict';

var { NativeModules } = require('react-native');


var BlueToothUtil = {
	'auto_connect':function(){
		
	},
	'sendMydevice':function(){
		NativeModules.BluetoothManager.rn_getConnectState();
	},
	'scanner':function(){
		NativeModules.BluetoothManager.rn_scanner();
	},
	'stopScan':function(){
		NativeModules.BluetoothManager.rn_stopScan();
	},
	'stopConnect':function(address){
		NativeModules.BluetoothManager.rn_stopConnect(address);
	},
	'cancelBond':function(address){
		NativeModules.BluetoothManager.rn_cancelBond(address);
	},
	'connectDevice':function(address){
		NativeModules.BluetoothManager.rn_connectDevice(address);
	},
	'shutDown':function(address){
		NativeModules.BluetoothManager.rn_shutDown(address);
	},
	'windSpeed':function(address,speed){
		NativeModules.BluetoothManager.rn_windSpeed(address,speed);
	},
	'deviceUpdate':function(address,name,path){
		NativeModules.BluetoothManager.rn_deviceUpdate(address,name,path);

	},
	'getHardwareVersion':function(address){
		//NativeModules.BlueToothUtil.getHardwareVersion(address);
	},
	'devCheck':function(address){
		NativeModules.BluetoothManager.rn_devCheck(address);
	},
	'devRename':function(address, name){
		NativeModules.BluetoothManager.rn_devRename(address, name);
	},
	'verifyInstallPackage':function (path,callback){
		NativeModules.BluetoothManager.file_md5(path,(error, md5_code)=>{
			callback(md5_code);
		});
	},

	'disconnect':function(address){
		NativeModules.BluetoothManager.rn_disconnect(address);
	},
	'readSN':function(address){
		NativeModules.BluetoothManager.rn_readSN(address);
	},
	'setSN':function(address){
		NativeModules.BluetoothManager.rn_setSN(address);
	},
	'eSmart':function(address, model) {
		NativeModules.BluetoothManager.rn_esmart(address, model);
	},
	'setInterval':function(address, space) {
		NativeModules.BluetoothManager.rn_setInterval(address, space);
	},
	'setSystemClock':function(address, time) {
		NativeModules.BluetoothManager.rn_setSystemClock(address, time);
	},
	'isVersion10': function(callback) {
		NativeModules.BluetoothManager.is_platform_version_10((error, flag)=>{
			callback(flag);
		})
	},

	'setVehideTime':function(address,minute,hour,week,type,mode){ 		
		NativeModules.BluetoothManager.rn_setVehideTime(address,minute,hour,week,type,mode); 	
	},

};
module.exports = BlueToothUtil;