/**单品天气*/
'use strict'

var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var NetWork=require('./NetWork');
import tools from './tools'

import styles_singleweater from './styles/style_singleweather'
import styles_hometopview from './styles/style_home_topview'
import Language from './Language'
import  config from './config'

import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    NetInfo,
    TouchableOpacity,
} from 'react-native';

var weather='室外严重污染 ';
var location='北京市朝阳区 ';
var pm25='305';
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var sub_chanage_local = null;
var subscription3;

var SingleWeather = React.createClass({

	mixins:[TimerMixin],

	_handle:null,

	PropTypes:{
		whether:PropTypes.string,
		location:PropTypes.string,
		number:PropTypes.number,
	},

	getInitialState:function(){
		return {
			v_pm25:'--',
	      	v_weather:'--',
	      	v_location:'--',
	      	v_city:'--',
	      	v_area:'--',
	      	v_load:false,
	      	v_cityid:null,
	      	v_icon:'',
		}
	},
	_handleConnectionInfoChange: function(connectionInfo) {
	    console.log(connectionInfo+'是否联网');
	    this.getData();
	 },

	 componentDidMount: function() {
	  
	    //检测网络变化
	    NetInfo.addEventListener(
	        'change',
	        this._handleConnectionInfoChange
	    );

	    NetInfo.isConnected.fetch().done((isConnected) => {
	      console.log(isConnected+'联网状态');
	      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
	      if (isConnected){
	        console.log(isConnected+'联网成功');
	      }
	    });


	    this._handle = this.setInterval(()=>{
	      this.setState({
	          v_load:false,
	        });
	      this.getData();
	    }, 1000*60*60);

		//this.fetchWeatherData(this.state.v_weather);
	  },

	componentWillMount: function(){
	    this.getData();
	    sub_chanage_local = RCTDeviceEventEmitter.addListener('msg_chanage_local',(val)=>{
	      this.getData();
	    });
	  subscription3 = RCTDeviceEventEmitter.addListener('weather',(val) => {
	      console.log(val+'获取的天气情况0'); 
	      this.setState({v_weather:val});
	      console.log(this.state.v_weather+'获取的天气情况'); 
	      this.fetchWeatherData(this.state.v_weather);
    	});
	  },

	componentWillUnmount: function(){
		this.clearInterval(this._handle);
	    sub_chanage_local.remove();
    	subscription3.remove();
	   NetInfo.removeEventListener(
	        'change',
	        this._handleConnectionInfoChange
	    );
	  },


	getData:function(){

		if (config.global_language == 'zh'){

		NetWork.fetch_all_data((city_name, text, temperature, max_min,wind_direction, wind_scale, pm25)=>{
	        var tmp1 = max_min+'℃' +'';

	        var tmp = city_name.split(',');
	          var tmp_str = '';
	          var province= '';
	          var city = '';
	          var area = '';
	          for (var i = tmp.length - 1; i >= 0; i--) {
	            //if (tmp[i] == '中国') continue;
	            //if (tmp[i] == '直辖市') continue;
	           
	            //if (i>0 && tmp[i] == tmp[i-1]) continue;

	            tmp_str += tmp[i];
	            tmp_str += ' ';


	            //定位
	            if (tmp.length == 1){
	            if (i == 0){
		            	province = tmp[i];
		            }
	            }else {
	            	if (tmp[2] == '直辖市') {
	            		console.log('地理位置 ··111··'+tmp[i]);
	            		if (tmp[1] == tmp[0]){
	            			console.log('地理位置 ··777··'+tmp[i]);
	            			province = tmp[1];
	            			//city = tmp[0];
	            		}else {
	            		console.log('地理位置 ··888··'+tmp[i]);
	            			province = tmp[1];
	            			city = tmp[0];
	            		}
	            		// console.log('地理位置 ··888··'+tmp[i]);
	            		// 	province = tmp[1];
	            		// 	city = tmp[0];
	            	} else if(tmp[2] == '特别行政区') {
	            			province = tmp[1];
	            			city = tmp[0];
	            	} else {
		            	if (i > 0 && tmp[i] == tmp[i-1]){
		            		console.log('地理位置 ··222··'+tmp[i]);
		            		if (i == 2){
		            				console.log('地理位置 ··333··'+tmp[i]);
		            			province = tmp[1];
		            			city = tmp[0];
		            			area = '';
		            			break;
		            		}else if (i == 1){
		            			console.log('地理位置 ··444··'+tmp[i]);
		            			province = tmp[2];
		            			city = tmp[1];
		            			area = '';
		            			break;
		            		}
		        
		            	}else {
							console.log('地理位置 ··555··'+tmp[i]);
		            		province = tmp[2];
		            		city = tmp[1];
		            		area = tmp[0];	
		            	}

	            	// console.log('地理位置 ··666··'+tmp[i]);
	            	// 	province = tmp[2];
	            	// 	city = tmp[1];
	            	// 	area = tmp[0];	
	            	}
	            }
	          }
	          console.log('地理位置 ····'+tmp_str);
	


			 RCTDeviceEventEmitter.emit('weather',text);
	          this.setState({
	            v_weather:tmp1,
	            v_pm25:pm25,
	            v_location:province,
	            v_city:city,
	            v_area:area,
	          });   
	    });

		} else {

			NetInfo.isConnected.fetch().done(
				(isConnected) => {
					if (isConnected){
						console.log('时间接口有网路啦');
					    let DEV_URL = 'https://mops-xinfeng-dev.lianluo.com//iface/weather/weaEn'+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;
					    console.log('https://mops-xinfeng-dev.lianluo.com//iface/weather/weaEn'+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language);
					    fetch (DEV_URL)
					    	.then ((response) => response.json())
					    	.then((responseData) => {
					    		if (responseData.code == 0){
 									let icon=require('image!ic_fine')
							          this.setState({
							            v_weather:responseData.data.tmp_max_min+'℃' +'',
							            v_pm25:responseData.data.pm25,
							            v_location:responseData.data.city_name,
							            v_city:'',
							            v_area:'',
							            v_icon :icon,
							          }); 
					    		}
					    	})
					    	.catch((error) => {
	 							console.log('外文接口错误');
					    	})
					}
				}
			);
		}  
	},

	fetchWeatherData:function (weather){

		var icon = null;
		switch(weather){
		      case '晴':{
		        icon=require('image!ic_fine');
		        break;
		      }
		      case '多云':
		      case '晴间多云':
		      case '大部多云':{
		        icon=require('image!ic_cloud');

		      }
		      case '阴':{
		        icon=require('image!ic_shade');
		        break;
		      }
		      case '雷阵雨':
		      case '雷阵雨伴有冰雹':{
		        icon=require('image!ic_thundershower');
		        break;
		      }
		      case '阵雨':
		      case '小雨':
		      case '大雨':
		      case '暴雨':
		      case '大暴雨':
		      case '特大暴雨':
		      case '中雨':
		      case '冻雨':{
		        icon=require('image!ic_rain');
		        break;
		      }
		      case '雨夹雪':
		      case '阵雪':
		      case '小雪':
		      case '中雪':
		      case '大雪':
		      case '暴雪':{
		        icon=require('image!ic_snow');
		        break;
		      }
		      case '浮尘':
		      case '扬沙':{
		        icon=require('image!ic_frift');
		        break;
		      }
		      case '沙尘暴':
		      case '强沙尘暴':{
		        icon=require('image!ic_sand_storm');
		        break;
		      }
		      case '雾':{
		        icon=require('image!ic_fog');
		        break;
		      }
		      case '霾':{
		         icon=require('image!ic_haze');
		         break;
		      }
		      case '风':
		      case '大风':
		      case '飓风':
		      case '热带风暴':
		      case '龙卷风':{
		        icon=require('image!ic_wind');
		        break;
		      }
		      default:{
		        icon=require('image!ic_unknow');
		        break;
		      }
		  }
		  if(this.state.v_icon != icon){
		    this.setState({
		      v_icon:icon,
		    });  
		  }
	},


	render:function() {
		
		return (
			<TouchableOpacity 
				style = {styles_singleweater.view1}
				onPress = {() => {this._gotoCity()}}>

			<View style={styles_hometopview.container}>
                <View style={{flex:1,flexDirection:'row'}}>
                    <View style={{flexDirection:'column',top:5}}>
                        <Text allowFontScaling={false} style={styles_hometopview.left_view}>
                          {this.state.v_weather}
                        </Text>
                        <View style={styles_hometopview.left_pm_view}>
                            <Text allowFontScaling={false} style={[styles_hometopview.left_view,{color:'#15b5f4'}]}>
                             {this.state.v_pm25}
                            </Text>
                            <Text allowFontScaling={false} style={styles_hometopview.left_little_text}>
                              PM2.5
                            </Text>
                        </View>

                    </View>
                    
                    <Image source = {this.state.v_icon} style={styles_hometopview.left_image}/>
        

                </View>
                <View style={{flex:1,flexDirection:'row-reverse',right:15}}>
                    
                    <View style={{flexDirection:'column',top:5}}>
                        
                      <Text allowFontScaling={false} style={styles_hometopview.center_view}>
                        {this.state.v_location}
                      </Text>

                      <Text allowFontScaling={false} style={[styles_hometopview.center_view,{top:-5,fontSize:12}]}>
                        {this.state.v_city}{' '}{this.state.v_area}
                      </Text>

                    </View>
                    <Image style={styles_hometopview.right_image} source={require('image!ic_dizhi')}/>
                  
                </View>
            </View>
            </TouchableOpacity>
		);
	},

	_gotoCity:function(){
	     NetInfo.isConnected.fetch().done(
	        (isConnected) => { 
	          if(isConnected){
	              this.props.navigator.push({
	              id: 'Provice',
	              params:{
	                start_page:1
	              }
	              });
	          }else{
	            tools.alertShow(Language.connectnetwork);
	          }
	        });
	},
});

module.exports = SingleWeather;

