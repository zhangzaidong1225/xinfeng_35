
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import Language from './Language'
// var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import Title from './title'

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  ToastAndroid,
  Navigator,
  ScrollView,
  ListView,
  NativeModules,
  DeviceEventEmitter,
  NativeAppEventEmitter,
  AsyncStorage,
} from 'react-native';

var type_img;
var deviceWidth=Dimensions.get('window').width;
var deviceHeight=Dimensions.get('window').height;

var BlueToothUtil= require('./BlueToothUtil');
var type = 1;

var number = [];
var number2=[];

var count=0;
var aa ;
var subscription1 = null;
var subscription2 = null;
var subscription3 = null;
var subscription4 = null;
var subscription  =null;
var subscription5=null;
var subscription6=null;

var sub_ble_state = null;
//var sub_isflash=null;
var DeviceStatus = React.createClass({

  getInitialState() {
        return {
          dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
          }),
          dataSource2: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
          }),
          loaded: false,
          isConnect:'',
          address:'',
          name:'---',
          id:1,
          scan:Language.search,
          isOpen:'',
          number:'',
          count:null,
          connect_list:[],
          connecting_list:[],
        }
         
    },

    gotoconnection: function() {
       this.props.navigator.push({
        id: 'Connect',
       });
    }, 
    
    scanDevice:function(){
    if(this.state.scan===Language.stop){
      BlueToothUtil.stopScan();
    }
    if(this.state.scan===Language.search){
      number=[];
      number2=[];
      BlueToothUtil.scanner();
    }
   },

    componentWillUnmount:function(){
      sub_ble_state.remove();
      //sub_isflash.remove();  
    },

  componentDidMount:function(){
    this.scanDevice();
    sub_ble_state = NativeAppEventEmitter.addListener(
         'ble_state',
         (data) => {
          var tmp_data = [];
          var tmp_data2 = [];
          var tmp_connect_list = [];
          var tmp_connecting_list = [];
          for (var i = 0; i < data.dev_list.length; i++) {
            if(data.dev_list[i].ismatchd){
              tmp_data2.push({id:data.dev_list[i].addr, name:data.dev_list[i].name,type:data.dev_list[i].type});  
            }else{
              tmp_data.push({id:data.dev_list[i].addr, name:data.dev_list[i].name,type:data.dev_list[i].type}); 
            }
            if(data.dev_list[i].isconnectd == 1){
              tmp_connect_list.push(data.dev_list[i].addr);
            }
            if(data.dev_list[i].isconnectd == 2){
              tmp_connecting_list.push(data.dev_list[i].addr);
            }
            if (data.dev_list[i].isconnectd == 1){
              AsyncStorage.setItem('select_device', data.dev_list[i].addr, (err)=>{});
            }
          };
          var scan_text = Language.search;
          if(data.scan_state){
            scan_text = Language.stop;
          }
          console.log('connect_list :' + tmp_connect_list.length);
          this.setState({
            dataSource: this.state.dataSource.cloneWithRows(tmp_data),
            dataSource2: this.state.dataSource.cloneWithRows(tmp_data2),
            scan:scan_text,
            connect_list:tmp_connect_list,
            connecting_list:tmp_connecting_list
          });
         }
     );

    BlueToothUtil.sendMydevice();
    },

   componentWillReceiveProps: function(){
    // BlueToothUtil.sendMydevice();
  },
   componentWillMount:function(){
     // this.scanDevice();
     BlueToothUtil.sendMydevice();

   },


  render:function (){
  var navigator = this.props.navigator;

		return (
			<View style = {{flex:1,backgroundColor:'#FFFFFF'}}>
			
				<Title title = {Language.mydevice} hasGoBack = {true}  navigator = {this.props.navigator}/>
        
        <ScrollView 
          showsVerticalScrollIndicator ={false}>

        <View style = {{marginTop:40,marginLeft:30,marginRight:30,}}>
          <Text allowFontScaling={false} style = {{fontSize:13,marginBottom:15,}} >{Language.paireddevice}</Text>
          <View style={styles.line}/>
        </View>



        <View style = {{marginTop:0,marginLeft:30,marginRight:30,}}>
    
        <View >
          <ListView
              dataSource={this.state.dataSource2}
              renderRow={this.renderDevice}
              style={styles.listView}
              removeClippedSubviews={false}/> 

        </View>


        </View>

        <View style = {{marginTop:40,marginLeft:30,marginRight:30,}}>
          <Text allowFontScaling={false} style = {{fontSize:13,marginBottom:15,}} >{Language.availabledevice}</Text>
          <View style={styles.line}/>
        </View>


        <View style = {{marginTop:0,marginLeft:30,marginRight:30,}}>
          <ListView
                  dataSource={this.state.dataSource}
                  renderRow={this.renderDevice1}
                  style={styles.listView}
                  removeClippedSubviews={false}/> 

        </View>

        <View style = {{height:40,marginTop:40,
          marginRight:30,marginLeft:30,justifyContent:'center',alignItems: 'center',}}>

                <View style={{justifyContent:'center',alignItems: 'center',position:'absolute',
             width:deviceWidth-60,height:40,borderColor:'rgb(23, 201, 180)',borderWidth: 2 / PixelRatio.get(),
             borderRadius:20,bottom:10,}}>
                  <TouchableOpacity onPress={()=>{this.scanDevice();UMeng.onEvent('deviceStatus_09');}}>
                     <View style = {{width:280,backgroundColor:'transparent'}}>
                      <Text allowFontScaling={false} style={styles_feedback.commitText}>{this.state.scan}</Text>
                     </View>
                  </TouchableOpacity>
                </View>
              </View>

           </ScrollView>
      </View>
    );
  },





      /*已配对设备*/
   renderDevice: function(device) {
    console.log('renderDevice  connect_list :' + this.state.connect_list.length);
    if(this.state.connect_list.indexOf(device.id)>=0){
      return <Item2 device = {device} navigator = {this.props.navigator}/>
    }
    else if(this.state.connecting_list.indexOf(device.id)>=0){
      return <Item3 device = {device} navigator = {this.props.navigator}/>
    }
    else{
      return <Item1 device = {device} navigator = {this.props.navigator}/>
    }
  },

  renderDevice1:function(device){
    if(this.state.connecting_list.indexOf(device.id)>=0){
      return <Item3 device = {device} navigator = {this.props.navigator}/>
    }
    else{
      return <Item device = {device} navigator = {this.props.navigator}/>
    }
  }


});


  var Item3 = React.createClass({

  render(){
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight ,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }
  var navigator = this.props.navigator;
      return (
        <View >
            <View style={{flexDirection: 'row',alignItems: 'center',height:60,}}>
              <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
                <Image style={image_style} source={type_img}/>
              </View>
              <Text allowFontScaling={false} style={{fontSize:15,color:'#50BCA1',marginLeft:10/180 * deviceWidth}}>{this.props.device.name}    </Text>
              <Text allowFontScaling={false} style={{position:'absolute',right:30,marginVertical:20,fontSize:13,color:'#969696',}}>{Language.connecting}</Text>
            <Image style={{height:13,width:7.5,position:'absolute',marginVertical:23,right:0,resizeMode:'stretch',justifyContent:'flex-end'}} source={require('image!ic_next')}/>
          </View> 
            <View style={styles.line}/>
            </View>
      );
  },

});

var Item2 = React.createClass({

  render(){
    console.log('Item2 render connect_list :' + this.props.device.type + this.props.device.id);
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight ,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }
  var navigator = this.props.navigator;
      return (
        <View >
            <TouchableOpacity      
                onPress={()=> {this.renderMovie3(this.props.device)}}>
            <View style={{flexDirection: 'row',alignItems: 'center',height:60,}}>
              <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
                  <Image style={image_style} source={type_img}/>
              </View>
              <Text allowFontScaling={false} style={{fontSize:15,color:'#50BCA1',marginLeft:10/180 * deviceWidth}}>{this.props.device.name}    </Text>
              <Text allowFontScaling={false} style={{position:'absolute',right:30,marginVertical:20,fontSize:13,color:'#969696',}}>{Language.connected}</Text>
            <Image style={{height:13,width:7.5,position:'absolute',marginVertical:23,right:0,resizeMode:'stretch',justifyContent:'flex-end'}} source={require('image!ic_next')}/>
          </View> 
          </TouchableOpacity> 
            <View style={styles.line}/>
            </View>
      );
  },

    renderMovie3: function(device) {
        UMeng.onEvent('deviceStatus_11');
       this.props.navigator.push({
       id: 'Connect',
       params:{
           deviceid:device.id,
           device:device.name,
          }
       });
    },

});



var Item1 = React.createClass({


  getInitialState() {
      return {
        device:'',
        isConnect:'',
        bg:'#323232',
        address:this.props.device.id,
      }
    },


  render(){
  var navigator = this.props.navigator;
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight ,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }

    return (
    <View  >
      <View style = {{flexDirection:'row',backgroundColor:'white'}}>
      <TouchableOpacity 
        style = {{height:60,width:(deviceWidth-60) * 0.7,flexDirection: 'row',alignItems: 'center',}}     
            onPress={()=> {this.renderMovie2(this.props.device)}}>
          <View style = {{width:15,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
              <Image style={image_style} source={type_img}/>
          </View>
         <Text allowFontScaling={false} style={{fontSize:15,color:'black',marginLeft:10/180 * deviceWidth}}>{this.props.device.name}</Text>
      
        </TouchableOpacity>

        <TouchableOpacity
        style = {{height:60,width:(deviceWidth-60) * 0.3,position:'absolute',right:0}}
          onPress = {() => {this.renderMovie3(this.props.device)}}>
      
          <Image style={{height:13,width:7.5,position:'absolute',marginVertical:23,right:0,resizeMode:'stretch',justifyContent:'flex-end'}} source={require('image!ic_next')}/>
    
      </TouchableOpacity>
      </View>

          <View style={styles.line}/>
        </View>
    );
  },

  renderMovie2: function(device) {
      UMeng.onEvent('deviceStatus_12');
      // this.setState({isConnect:'已连接',bg:'#50BCA1'});
      BlueToothUtil.connectDevice(device.id);

    },

    renderMovie3: function(device) {
        UMeng.onEvent('deviceStatus_13');
       this.props.navigator.push({
       id: 'Unconnect',
       params:{
           deviceid:device.id,
           device:device.name,
          }
       });
    },

});

var Item = React.createClass({



getInitialState() {
    return {
      device:'',
      isConnect:'',
      bg:'#323232',
      address:this.props.device.id,
      type:this.props.device.type,
    }
  },


render(){
var navigator = this.props.navigator;
    var type_img,image_style;
    if(this.props.device.type == 0){
        type_img = require('image!ic_xinfeng');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight ,resizeMode:'stretch'};
    }else if(this.props.device.type == 1){
        type_img = require('image!ic_single');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }else{
        type_img = require('image!ic_car');
        image_style = {height:15/320 * deviceHeight,width:15/320 * deviceHeight,resizeMode:'stretch'};
    }
  return (
  <View >
      <TouchableOpacity
          onPress={()=> {this.renderMovie2(this.props.device)}}>
        <View style={{flexDirection: 'row',alignItems: 'center',height:60,}}>
          <View style = {{width:13,marginLeft:5,alignItems: 'center',justifyContent:'center'}}>
              <Image style={image_style} source={type_img}/>
          </View>
          <Text allowFontScaling={false} style={{fontSize:13,marginLeft:10/180 * deviceWidth}}>{this.props.device.name}</Text>
          <Text allowFontScaling={false} style={{position:'absolute',right:30,marginVertical:20,fontSize:13,color:'#969696',}}></Text>
      </View>
      </TouchableOpacity>
        <View style={styles.line}/>
      </View>
  );
},

renderMovie2: function(device) {
    UMeng.onEvent('deviceStatus_14');
    BlueToothUtil.connectDevice(device.id);

  },

});





module.exports = DeviceStatus;



