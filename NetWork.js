'use strict';

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/

import  config from './config'
import Language from './Language'
//var AsyncStorage = require('./AsyncStorage');

var IP='ip';
// var AIR_REQUEST_URL = 'https://api.thinkpage.cn/v3/air/now.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&location=';
// var WEATHER_REQUEST_URL ='https://api.thinkpage.cn/v3/weather/daily.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&start=0&days=1&location=';
// var WIND_REQUEST_URL='https://api.thinkpage.cn/v3/weather/now.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&location=';
// var CITY_KEY_URL = 'https://api.thinkpage.cn/v3/location/search.json?key=BA57CE6C5B4FC19477D34CE78CD6E841&q='
// var IP_TO_CITY_URL = 'http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json';

var AIR_REQUEST_URL = config.region_base+config.air_request_url;
var WEATHER_REQUEST_URL = config.region_base+config.weather_request_url;
var WIND_REQUEST_URL= config.region_base+config.wind_request_url;
var CITY_KEY_URL = config.region_base+config.city_key_url; 
var IP_TO_CITY_URL = config.ip_to_city_url;

var pm25;
var quality;
var high;
var low;
var weather='tianqi';
var wind_scale;
var wind_direction;

var area=null;
var city=null;
var province=null;
var city_key = null;



import React, { Component } from 'react';
import {
ToastAndroid,
AsyncStorage,
} from 'react-native';


var async = require('async');

var NetWork = {

get_city:function (){
  console.log('get_city _ start');
  async.parallel([
          function(callback){
            AsyncStorage.getItem('cityid',(error,result)=>{
            if(error){}else{
                callback(null, result);
               }
            });
            
          },
          function(callback){
          AsyncStorage.getItem('provinceid',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
          function(callback){
          AsyncStorage.getItem('areaid',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
        ],
        function(err, results){
          console.log(results[2]);
          city_key = 'ip';
          if(results[2] != null){
            city_key = results[2];
          }
          //console.log(results[1]);
          //console.log(results[2]);
          
          
          AsyncStorage.setItem('city_key',city_key,(err)=>{
            console.log('get_city _ city_key' + city_key);
              RCTDeviceEventEmitter.emit('msg_chanage_local',null); 
          });
          
        });
},

//废弃方法
get_city_id:function(cb)
{
  AsyncStorage.getItem('city_key', (error, result)=>{
    cb(result);
  });
},


fetch_all_data:function(cb)
{
  console.log('Whether_PM' + 'NetWork fetch_all_data');
    AsyncStorage.getItem('city_key', (error, result_city_name)=>{
          console.log(config.server_base+config.weather_uri+'?city='+result_city_name+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language);
          fetch(config.server_base+config.weather_uri+'?city='+result_city_name+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
          .then((response) => response.json())
          .then((responseData) => {
            try{
                async.parallel([
                  function(callback){
                    AsyncStorage.getItem('city',(error,result)=>{
                    if(error){}else{
                        callback(null, result);
                       }
                    });
                    
                  },
                  function(callback){
                  AsyncStorage.getItem('province',(error,result)=>{
                    if(error){}else{
                      callback(null, result);
                     }
                    });
                  },
                  function(callback){
                  AsyncStorage.getItem('area',(error,result)=>{
                    if(error){}else{
                      callback(null, result);
                     }
                    });
                  },
                ],
                function(err, results){
                  var city = results[0];
                  var province = results[1];
                  var area = results[2];
                  var city_name = responseData.data.city_name
                  if(area != null){
                    city_name = area + ',' + city + ',' + province;
                  }
                  console.log(city_name);
                  cb(city_name, responseData.data.text, responseData.data.temperature, responseData.data.tmp_max_min,responseData.data.wind_direction, responseData.data.wind_scale, responseData.data.pm25);
                  
                });
              
            }catch(e){
              console.log('error.. fetchAirData');
            }
          }).catch((error) => {
               //ToastAndroid.show('网络错误', ToastAndroid.SHORT);
          });
    });
},

//废弃方法
fetchAirData:function (cb){
  AsyncStorage.getItem('city_key', (error, result_city_name)=>{
    
          fetch(AIR_REQUEST_URL+result_city_name+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
          .then((response) => response.json())
          .then((responseData) => {
            try{
              if(responseData.results[0].air.hasOwnProperty('city')){
                cb(responseData.results[0].air.city.pm25, responseData.results[0].air.city.quality, responseData.results[0].location.path);
              }else{
                cb('--', '优', responseData.results[0].location.path);
              }
            }catch(e){
              console.log('error.. fetchAirData');
            }
            //pm25= responseData.results[0].air.city.pm25;   
            //quality=responseData.results[0].air.city.quality;  
          }).catch((error) => {
               //ToastAndroid.show('网络错误', ToastAndroid.SHORT);
          });
    });
    
  },

//废弃方法
fetchAirData_bak:function (cb){
  AsyncStorage.getItem('city_key', (error, result_city_name)=>{
    async.parallel([
        function(callback){
          AsyncStorage.getItem(result_city_name+'_date', (error, result_date)=>{
            
            callback(null, result_date);
          });
        },
        function(callback){
          AsyncStorage.getItem(result_city_name+'_data', (error, result_data)=>{
            
            callback(null, result_data);
          });
        }
      ],function(err, results){
        var need_net = false;
        if(results[0] == null || results[1] == null){
          need_net = true;
        }else{
          if(Date.now() - parseInt(results[0]) > 60000*10){
            
            need_net = true;
          }
        }
        if(need_net){
          fetch(AIR_REQUEST_URL+result_city_name+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
          .then((response) => response.json())
          .then((responseData) => {
            AsyncStorage.setItem(result_city_name+'_date', '' + Date.now(), (error)=>{});
  
            AsyncStorage.setItem(result_city_name+'_data', JSON.stringify(responseData), (error)=>{});
            try{
              if(responseData.results[0].air.hasOwnProperty('city')){
                cb(responseData.results[0].air.city.pm25, responseData.results[0].air.city.quality, responseData.results[0].location.path);
              }else{
                cb('--', '优', responseData.results[0].location.path);
              }
            }catch(e){
              console.log('error.. fetchAirData');
            }
            //pm25= responseData.results[0].air.city.pm25;   
            //quality=responseData.results[0].air.city.quality;  
          }).catch((error) => {
               //ToastAndroid.show(''+error,ToastAndroid.SHORT);
            });
        }else{
          try{
            var responseData = JSON.parse(results[1]);
            if(responseData.results[0].air.hasOwnProperty('city')){
              cb(responseData.results[0].air.city.pm25, responseData.results[0].air.city.quality, responseData.results[0].location.path);
            }else{
              cb('--', '优', responseData.results[0].location.path);
            }
          }catch(e){
            console.log('error.. fetchAirData');
          }
          
        }
        
      });

    });
 	  
  },

// 废弃方法
fetchWeatherData:function (cb){
    AsyncStorage.getItem('city_key', (error, result_city_name)=>{
        fetch(WEATHER_REQUEST_URL+result_city_name+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
        .then((response) => response.json())
        .then((responseData) => {
          //AsyncStorage.setItem(result_city_name+'_date_01', '' + Date.now(), (error)=>{});
          //AsyncStorage.setItem(result_city_name+'_data_01', JSON.stringify(responseData), (error)=>{});
          try{

            cb(responseData.results[0].daily[0].high, responseData.results[0].daily[0].low, responseData.results[0].daily[0].text_day, responseData.results[0].location.path);     
          }catch(e){
            console.log('error.. fetchWeatherData');
          }
        }).catch((error) => {
        //ToastAndroid.show('网络错误', ToastAndroid.SHORT);
      });

    });
  },



  //废弃方法
  fetchWeatherData_bak:function (cb){
    AsyncStorage.getItem('city_key', (error, result_city_name)=>{
    async.parallel([
        function(callback){
          AsyncStorage.getItem(result_city_name+'_date_01', (error, result_date)=>{
            
            callback(null, result_date);
          });
        },
        function(callback){
          AsyncStorage.getItem(result_city_name+'_data_01', (error, result_data)=>{
            
            callback(null, result_data);
          });
        }
      ],function(err, results){
        var need_net = false;
        if(results[0] == null || results[1] == null){
          need_net = true;
        }else{
          if(Date.now() - parseInt(results[0]) > 60000*10){
            
            need_net = true;
          }
        }
        if(need_net){

        fetch(WEATHER_REQUEST_URL+result_city_name+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
        .then((response) => response.json())
        .then((responseData) => {
          AsyncStorage.setItem(result_city_name+'_date_01', '' + Date.now(), (error)=>{});
          AsyncStorage.setItem(result_city_name+'_data_01', JSON.stringify(responseData), (error)=>{});
          try{
            cb(responseData.results[0].daily[0].high, responseData.results[0].daily[0].low, responseData.results[0].daily[0].text_day, responseData.results[0].location.path);     
          }catch(e){
            console.log('error.. fetchWeatherData');
          }
        }).catch((error) => {
         ToastAndroid.show(''+error,ToastAndroid.SHORT);
      });

        }else{
          try{
            var responseData = JSON.parse(results[1]);
            cb(responseData.results[0].daily[0].high, responseData.results[0].daily[0].low, responseData.results[0].daily[0].text_day, responseData.results[0].location.path);   
          }catch(e){
            console.log('error.. fetchWeatherData');
          }
          
        }
        
      });

    });
  },

   fetchWindData:function (cb){
    AsyncStorage.getItem('city_key', (error, result)=>{
      fetch(WIND_REQUEST_URL+result+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
        .then((response) => response.json())
        .then((responseData) => {
          try{
            cb(responseData.results[0].now.wind_scale, responseData.results[0].now.wind_direction, responseData.results[0].location.path);  
          }catch(e){
            console.log('error.. fetchWindData');
          }

          
        }).catch((error) => {
         //ToastAndroid.show(''+error,ToastAndroid.SHORT);
      });
    });
  },

  get_ip_city:function (cb){
    fetch(IP_TO_CITY_URL+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language)
        .then((response) => response.json())
        .then((responseData) => {
          cb(responseData.country, responseData.province, responseData.city);   
          //high=responseData.results[0].daily[0].high;   
          //low=responseData.results[0].daily[0].low; 
          //weather=responseData.results[0].daily[0].text_day; 
        }).catch((error) => {
         //ToastAndroid.show(''+error,ToastAndroid.SHORT);
      });
  },

 getTemperature:function (){
 	 return high+'℃'+'/'+low+'℃';
 },

 getWind:function (){
 	  return wind_direction+wind_scale;
 },

 getWeather:function (){
 	return weather;
 },

 getPm25:function (){
 	return pm25;
 },

 getQuality:function (){
 	return quality;
 },


};
module.exports = NetWork;
