/*车载主页*/
'use strict'

var RNFS = require('react-native-fs');
var TimerMixin = require('react-timer-mixin');
//import AsyncStorage from './AsyncStorage'
import SingleWeather from './SingleWeather'
import VehideIcon from './vehideIcon'
import SingleMode from './singleMode'
import SliderVehide from './vehideModel'
import  config from './config'
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    Platform,
    DeviceEventEmitter,
    NativeAppEventEmitter,
} from 'react-native';

var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var BlueToothUtil=require('./BlueToothUtil');
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var sub_currentDevice = null;
var systimeFlag = 1;
var open_filter;

var CarPage = React.createClass({

	getInitialState:function(){
		return {
			navigator:'',
			code:'',
			system_time:'',
		}
	},


	componentDidMount:function(){
		            //检测网络变化
    NetInfo.addEventListener(
        'change',
        this._handleConnectionInfoChange
    );
    console.log('1211 home =1 =');
	//切换设备的监听器
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val)=>{
    	if (val != null){
    		if (this.state.system_time - val.SystemTime > 300){
    			if (systimeFlag == 1){
    				console.log('设置时间......');
    				BlueToothUtil.setSystemClock(val.addr,this.state.system_time);
    				systimeFlag = 0;
    			}

    		}
    	}

		    //上盖弹起
		      AsyncStorage.getItem('open_filter',(error,result) => {
		        console.log('1211 home  open result: '+result);
		          if (error) {

		          } else {
		            if (result=='1'||result==null) {
		                open_filter = '1';
                        console.log('1211==11  open true');
            		    if(val.open_cap == 1&&open_filter=='1'){
					    	open_filter=='0';
					        console.log('1211==11 home33');
					        RCTDeviceEventEmitter.emit('isOpenfilter',1);
					        AsyncStorage.setItem('open_filter','0',error => {});
					    }
		            } else {
		                open_filter = '0';
		                console.log('1211 home  open  false');
		            }
		          }
		      });
		    // if(val.vehide_model == 1&&open_filter=='1'){
		    // 	open_filter=='0';
		    //     console.log('1211==11 home33');
		    //     RCTDeviceEventEmitter.emit('isOpenfilter',1);
		    //     AsyncStorage.setItem('open_filter','0',error => {});
		    // }
		    if(val.open_cap==0){
		    	AsyncStorage.setItem('open_filter','1',error => {});
		    }


    });

	this._fetchData();

	},
	_handleConnectionInfoChange: function(connectionInfo) {

    console.log(connectionInfo+'是否联网');
	this._fetchData();
  },

	_fetchData:function(){

		NetInfo.isConnected.fetch().done(
			(isConnected) => {
				if (isConnected){
					console.log('时间接口有网路啦');
				    var DEV_URL = config.server_base + config.system_time+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;
				    console.log(DEV_URL);
				    fetch (DEV_URL)
				    	.then ((response) => response.json())
				    	.then((responseData) => {
				    		if (responseData.code == 0){
				    			this.setState({
				    				system_time:responseData.data.server_time,
				    			});
  								 //console.log('时间戳·····'+this.state.system_time);
				    		}
				    	})
				    	.catch((error) => {
 							console.log('时间戳·····使用错误');
				    	})
				}
			}
		);
	},

	componentWillUnmount:function(){
		sub_currentDevice.remove();

	    NetInfo.removeEventListener(
	        'change',
	        this._handleConnectionInfoChange
	    );
	},

	render:function(){
  		var navigator = this.props.navigator;
		return (
			<View style = {[styles.container,{alignItems: 'center'}]}>
				<SingleWeather navigator = {this.props.navigator}/>
				<VehideIcon navigator = {this.props.navigator}/>
				<SliderVehide navigator = {this.props.navigator}/>
			</View>

		);
	},

});

var styles = StyleSheet.create ({

	container:{
		flexDirection:'column',
		backgroundColor:'#FFFFFF',
		height:deviceHeight * 0.9,
	}
});

module.exports = CarPage;
