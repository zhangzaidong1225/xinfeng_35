'use strict'

var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');

import Title from './title'
var BlueToothUtil= require('./BlueToothUtil');
import Language from './Language'
import React, { Component } from 'react';

import {
    AppRegistry,
    styles_sliderheet,
    Text,
    View,
    PanResponder,
    Image,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    DeviceEventEmitter,
    NativeModules,
    ToastAndroid,
    NativeAppEventEmitter,
    Platform,
} from 'react-native';


var deviceWidth  = Dimensions.get ('window').width;
var deviceHeight = Dimensions.get ('window').height;

var _back;//button颜色
var txtColor;


var msg = '根据车内外空气质量，自动进行风速的切换，为您打造完美的智能净化体验。当然，千万不要忘记插入单品哦。';


var Esmart = React.createClass({

	getInitialState(){
		return {
			model:'',
			address:'',
		}
	},

	componentWillMount:function() {

	},

	componentDidMount:function(){

		this.setState({
			model:this.props.device.vehide_model,
			address:this.props.device.addr,
		});
		// if (this.state.model == 1){
		// 	_back = '#2DBAF1';
		// 	txtColor = '#FFFFFF';

		// }else {
		// 	_back = '#FFFFFF';
		// 	txtColor = '#2DBAF1';
		// }

	},

	componentWillUnmount:function() {

	},

	render(){
 var navigator = this.props.navigator;
		// if (this.state.model == 1){
		// 	_back = '#2DBAF1';
		// 	txtColor = '#FFFFFF';

		// }else {
		// 	_back = '#FFFFFF';
		// 	txtColor = '#2DBAF1';
		// }


	return (
       

		<View style = {{flex:1,backgroundColor:'#FFFFFF'}}>
			<Title title = {Language.automation} hasGoBack = {true}  navigator = {this.props.navigator}/>

			<View style = {{height:513 / 1080 * deviceWidth}}>
				<Image 
					source = {require('image!ic_esmart')}
					style  = {{width:deviceWidth,height:513 / 1080 * deviceWidth,resizeMode:'stretch'}}/>
			</View>

			<View style = {{height:30,marginTop:20,marginLeft:10,marginRight:10,}}>
				<Text allowFontScaling={false} style = {{color:'#999999',fontSize:14,textAlign:'auto'}}>{Language.insertingdev}</Text>
			</View>

			<View style = {{height:428 / 721 * deviceWidth,marginTop:10,}}>
				<Image 
					source = {require('image!ic_smart_single')}
					style  = {{width:deviceWidth,height:428 / 721 * deviceWidth,resizeMode:'stretch'}}/>
			</View>

			<View style = {{position:'absolute',width:deviceWidth,flexDirection:'row',bottom:20,marginTop:15,}}>
				<View style = {{flex:1,alignItems:'center',justifyContent:'center'}}>
					<TouchableOpacity 
						onPress = {() => {this._tran2Start(this.state.address)}}>
					<View style = {{backgroundColor:'#FFFFFF',alignItems:'center',justifyContent:'center',
							width:126,height:48,borderColor:'#2DBAF1',borderWidth:1,borderRadius:25,}}>
						<Text allowFontScaling={false} style = {{color:txtColor,fontSize:16,textAlign:'center'}}>{Language.on}</Text>
					</View>
					</TouchableOpacity>
				</View>
				<View style = {{flex:1,alignItems:'center',justifyContent:'center'}}>
					<TouchableOpacity 
						onPress = {() => {this._tran2Close(this.state.address)}}>
					<View style = {{backgroundColor:'#FFFFFF',alignItems:'center',justifyContent:'center',
								width:126,height:48,borderColor:'#2DBAF1',borderWidth:1,borderRadius:25,}}>
						<Text allowFontScaling={false} style = {{color:txtColor,fontSize:16,textAlign:'center'}}>{Language.off}</Text>
					</View>
					</TouchableOpacity>
				</View>
			</View>
		</View>
		);
	},

	_tran2Start:function(address){
		console.log('aaaaa````'+address);
	    if (this.state.address != null){
	    	console.log('bbbbbbb');
	         BlueToothUtil.eSmart(address,1)
	         this.props.navigator.pop();
	    }
	},

	_tran2Close:function(address){
			console.log('cccccc````'+address);
		if (this.state.address != null){
				console.log('ddddd');
         BlueToothUtil.eSmart(address,0)
               this.props.navigator.pop();
		}
	},
});

module.exports = Esmart;


