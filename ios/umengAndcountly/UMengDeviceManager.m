//
//  UMengDeviceManager.m
//  feibao
//
//  Created by Louis Liu on 16/6/21.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import "UMengDeviceManager.h"
#import "UMMobClick/MobClick.h"
#import "CharacteristicReader.h"
#import "Countly.h"

@implementation UMengDeviceManager


+ (UMengDeviceManager *)sharedInstance
{
  static UMengDeviceManager *sharedInstance = nil;
  if (sharedInstance == nil) {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
      sharedInstance = [[self alloc] init];
    });
  }
  return sharedInstance;
}

-(id)init
{
  if (self = [super init]) {
    
    self.flag_dictionary = [NSMutableDictionary dictionary];
    self.flag_array = [[NSMutableArray alloc] initWithObjects:@(false), @(false), @(false), @(false), @(false), nil];
    
    self.first = [[NSArray alloc] initWithObjects:@(2), @(1), @(1), @(2), @(2), @(4), @(1), nil];
    self.second = [[NSArray alloc] initWithObjects:@(4), @(4), @(4), @(4), nil];
    self.third = [[NSArray alloc] initWithObjects:@(4), nil];
    self.forth = [[NSArray alloc] initWithObjects:@(1), @(1), @(1), @(1), @(1), @(2), @(2), @(2), @(1), @(2), @(1), nil];
    self.fifth = [[NSArray alloc] initWithObjects:@(4), @(4), @(2), @(4), nil];
    
    self.prex_first = @("first");
    self.prex_second = @("second");
    self.prex_third = @("third");
    self.prex_forth = @("forth");
    self.prex_fifth = @("fifth");
  }
  
  return self;
}

- (void)parseData:(uint8_t **) val uuid:(NSString *) uuid {
  
  if(val == nil) {
    return;
  }
  
  if([CharacteristicReader readUInt8Value:val] != 0x50) {
    return;
  }
  
  NSMutableArray * tmp_flag_array = [self.flag_dictionary objectForKey:uuid];
  if (tmp_flag_array == nil) {
    [self.flag_dictionary setValue:[[NSMutableArray alloc] initWithObjects:@(false), @(false), @(false), @(false), @(false), nil] forKey:uuid];
  }
  
  switch([CharacteristicReader readUInt8Value:val]) {
    
    case 0x01:
      if ([[[self.flag_dictionary objectForKey:uuid] objectAtIndex:0] isEqual:@(true)]) {
        return;
      }
    
      [self parseData:val guide:self.first prex:self.prex_first uuid:uuid];
      [[self.flag_dictionary objectForKey:uuid] setObject:@(true) atIndex:0];
      break;
    
    case 0x02:
      if ([[[self.flag_dictionary objectForKey:uuid] objectAtIndex:1] isEqual:@(true)]) {
        return;
      }
    
      [self parseData:val guide:self.second prex:self.prex_second uuid:uuid];
      [[self.flag_dictionary objectForKey:uuid] setObject:@(true) atIndex:1];
      break;
    
    case 0x03:
      if ([[[self.flag_dictionary objectForKey:uuid] objectAtIndex:2] isEqual:@(true)]) {
        return;
      }
    
      [self parseData:val guide:self.third prex:self.prex_third uuid:uuid];
      [[self.flag_dictionary objectForKey:uuid] setObject:@(true) atIndex:2];
      break;
    case 0x04:
      if ([[[self.flag_dictionary objectForKey:uuid] objectAtIndex:3] isEqual:@(true)]) {
        return;
      }
    
      [self parseData:val guide:self.forth prex:self.prex_forth uuid:uuid];
      [[self.flag_dictionary objectForKey:uuid] setObject:@(true) atIndex:3];
      break;
    case 0x05:
      if ([[[self.flag_dictionary objectForKey:uuid] objectAtIndex:4] isEqual:@(true)]) {
        return;
      }
    
      [self parseData:val guide:self.fifth prex:self.prex_fifth uuid:uuid];
      [[self.flag_dictionary objectForKey:uuid] setObject:@(true) atIndex:4];
      break;
    
  }
  
}

- (void)parseData:(uint8_t **) val guide:(NSArray *)guide prex:(NSString *)prex uuid:(NSString *)uuid{
  if(val == nil) {
    return;
  }
  
  if (guide == nil) {
    return;
  }
  
  for (int i = 0; i < [guide count]; i++) {
    switch ([[guide objectAtIndex:i] intValue]) {
      case 1: {
        NSString * eventId = [NSString stringWithFormat:@"%@%@",prex,[NSString stringWithFormat:@"%d",i+1]];
        uint8_t value = [CharacteristicReader readUInt8Value:val];
        [self onEvent:eventId attribute:uuid counter:value];
        break;
      }
      
      case 2: {
        NSString * eventId = [NSString stringWithFormat:@"%@%@",prex,[NSString stringWithFormat:@"%d",i+1]];
        uint16_t value = [CharacteristicReader readUInt16Value:val];
        uint16_t values = ((value & 0x00FF) << 8) | ((value & 0xFF00) >> 8);
        [self onEvent:eventId attribute:uuid counter:values];
        break;
      }
      
      case 4: {
        NSString * eventId = [NSString stringWithFormat:@"%@%@",prex,[NSString stringWithFormat:@"%d",i+1]];
        uint32_t value = [CharacteristicReader readSInt32Value:val];
        uint32_t values = ((value & 0x000000FF) << 24) | ((value & 0x0000FF00) << 8) | ((value & 0x00FF0000) >> 8) | ((value & 0xFF000000) >> 24);
        [self onEvent:eventId attribute:uuid counter:values];
        break;
      }
      
      default:
      break;
    }
  }
}

- (void)onEvent:(NSString *) eventId attribute:(NSString *) uuid counter:(int) number{
  
  [MobClick event:eventId attributes:@{@"UUID" : uuid} counter:number];
  [Countly.sharedInstance recordEvent:eventId segmentation:@{@"UUID" : uuid} count:1 sum:number];
}




@end
