//
//  UMengDeviceManager.h
//  feibao
//
//  Created by Louis Liu on 16/6/21.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UMengDeviceManager : NSObject

@property (strong, nonatomic) NSMutableDictionary * flag_dictionary;
@property (strong, nonatomic) NSMutableArray * flag_array;

@property (strong, nonatomic) NSArray * first;
@property (strong, nonatomic) NSArray * second;
@property (strong, nonatomic) NSArray * third;
@property (strong, nonatomic) NSArray * forth;
@property (strong, nonatomic) NSArray * fifth;

@property (strong, nonatomic) NSString * prex_first;
@property (strong, nonatomic) NSString * prex_second;
@property (strong, nonatomic) NSString * prex_third;
@property (strong, nonatomic) NSString * prex_forth;
@property (strong, nonatomic) NSString * prex_fifth;


+ (id)sharedInstance;
- (void)parseData:(uint8_t **) val uuid:(NSString *) uuid;
- (void)onEvent:(NSString *) eventId attribute:(NSString *) uuid counter:(int) number;




@end
