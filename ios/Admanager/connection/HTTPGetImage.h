//
//  HTTPGetImage.h
//  feibao
//
//  Created by Louis Liu on 16/4/27.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>

@interface HTTPGetImage : NSObject

-(UIImage *) getImageFromURL:(NSString *)fileURL;
-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath;
//-(UIImage *) loadImage:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath;
-(void) removeImage:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath;

@end
