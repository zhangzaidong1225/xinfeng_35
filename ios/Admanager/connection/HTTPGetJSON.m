//
//  HTTPGetJSON.m
//  feibao
//
//  Created by Louis Liu on 16/4/27.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import "HTTPGetJSON.h"

@implementation HTTPGetJSON

-(NSDictionary *) getJsonFromUrl:(NSString *) urlString {
  

  NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
  
  [request setURL:[NSURL URLWithString:urlString]];
  
  [request setHTTPMethod:@"GET"];
  
  NSHTTPURLResponse* urlResponse = nil;
  
  NSError *error = [[NSError alloc] init];
  
  NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
  
//  NSMutableString *result = [[NSMutableString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//  NSLog(@"The result string is :%@",result);
  if(responseData == nil) {
    return nil;
  }

  NSMutableDictionary * data = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
  
  return data;
//  NSDictionary *dataInfo = [data objectForKey:@"data"];

}

@end
