//
//  AdManager.m
//  feibao
//
//  Created by Louis Liu on 16/4/27.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import "AdManager.h"
#import "HttpGetJSON.h"
#import "HttpGetImage.h"
#import "Utils.h"
@implementation AdManager

const static NSString * SERVER_BASE = @"https://mops.lianluo.com/vida/xinfeng";
const static NSString * SERVER_BASE_TEST = @"https://mops-xinfeng-dev.lianluo.com";//@"http://139.198.9.171/";
const static NSString * AD_URI = @"/iface/info/ad";

+ (AdManager *)sharedInstance
{
  static AdManager *sharedInstance = nil;
  if (sharedInstance == nil) {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
      sharedInstance = [[self alloc] init];
    });
  }
  return sharedInstance;
}

-(id)init
{
  if (self = [super init]) {
    self.data = [NSMutableDictionary dictionary];
  }
  
  return self;
}

- (void) get_ad_data {
  dispatch_async(dispatch_queue_create("admanager", NULL), ^{
    //获取当前已存储js版本
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    NSString * hotUpdateVersion = [userDefaultes objectForKey:@"hotUpdateVersion"];
    NSLog(@"当前版本:%@",hotUpdateVersion);
    if (hotUpdateVersion == nil) {
      hotUpdateVersion = @"1.2.2";
    }
    NSString * language = [Utils get_language];
    NSString * language_pramater;
    if ([@"zh" isEqualToString:language]) {
      language_pramater = [[NSString alloc] initWithFormat:@"&language=zh-CN"];
    } else  {
      language_pramater = [[NSString alloc] initWithFormat:@"&language=en-US"];
    }

    NSString * pramater = [[@"?platform=1&version=" stringByAppendingString:hotUpdateVersion] stringByAppendingString:language_pramater];
    //first: get json from ad_uri
    
    NSString * ad_uri;
    NSString * isTestXinfeng = [userDefaultes stringForKey:@"isTestXinfeng"];
    if (isTestXinfeng == nil || [isTestXinfeng isEqualToString:@("忻风")] || [isTestXinfeng isEqualToString:@("XinFeng")]) {
      ad_uri = [[NSString alloc] initWithFormat:@"%@%@%@", SERVER_BASE, AD_URI, pramater];
    } else {
      ad_uri = [[NSString alloc] initWithFormat:@"%@%@%@", SERVER_BASE_TEST, AD_URI, pramater];
    }
    
    NSLog(@"ad_uri:%@",ad_uri);
    
    HTTPGetJSON * httpGetJSON = [[HTTPGetJSON alloc] init];
    HTTPGetImage * httpGetImage = [[HTTPGetImage alloc] init];
    NSString *NSCachesDirectoryPath = [self getPathForDirectory:NSCachesDirectory];
    NSLog(@"NSCachesDirectoryPath:%@", NSCachesDirectoryPath);
    
    NSMutableDictionary * data = [httpGetJSON getJsonFromUrl:ad_uri];
    if(data == nil) {
      [httpGetImage removeImage:@"rctad" ofType:@"png" inDirectory:NSCachesDirectoryPath];
      self.data = data;
      [self save_ad_data];
      return;
    }
    
    NSMutableDictionary * dataInfo = [data objectForKey:@"data"];
    
    if (dataInfo == nil) {
      [httpGetImage removeImage:@"rctad" ofType:@"png" inDirectory:NSCachesDirectoryPath];
      self.data = data;
      [self save_ad_data];
      return;
    }
    NSLog(@"url:%@,action:%@",[dataInfo objectForKey:@"url"], [dataInfo objectForKey:@"action"]);
    
    NSString * url = [dataInfo objectForKey:@"url"];
    //second: jude should update
    [self read_ad_data];
    if(self.data != nil && [self.data objectForKey:@"data"] != nil && [url isEqualToString: [[self.data objectForKey:@"data"] objectForKey:@"url"]]) {
      NSLog(@"已经存在！！！！");
      self.data = data;
      [self save_ad_data];
      return;
    }
    [httpGetImage removeImage:@"rctad" ofType:@"png" inDirectory:NSCachesDirectoryPath];
  
    [httpGetImage saveImage:[httpGetImage getImageFromURL:url] withFileName:@"rctad" ofType:@"png" inDirectory: NSCachesDirectoryPath];
    self.data = data;
    [self save_ad_data];
    NSLog(@"保存成功！！！！");
    
    });
  
}

/*
 保存序列化数据
 */
-(void) save_ad_data
{

  if (self.data == nil) {
    return ;
  }
  
  NSData * archiveAdData = [NSKeyedArchiver archivedDataWithRootObject:self.data];
  [[NSUserDefaults standardUserDefaults] setObject:archiveAdData forKey:@"AdPage"];
  
}

/*
 读取序列化数据
 */
-(void) read_ad_data
{
  NSData * archiveAdData = [[NSUserDefaults standardUserDefaults] objectForKey:@"AdPage"];
  if(archiveAdData == nil) {
    return;
  }
  self.data = [NSKeyedUnarchiver unarchiveObjectWithData:archiveAdData];
    
}

- (NSString *)getPathForDirectory:(int)directory
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(directory, NSUserDomainMask, YES);
  return [paths firstObject];
}




@end
