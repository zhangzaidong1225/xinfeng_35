//
//  AdManager.h
//  feibao
//
//  Created by Louis Liu on 16/4/27.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AdManager : NSObject
@property (strong, nonatomic) NSMutableDictionary * data;

+ (AdManager *)sharedInstance;

- (void) get_ad_data;
- (void) save_ad_data;
- (void) read_ad_data;

- (NSString *)getPathForDirectory:(int)directory;

@end
