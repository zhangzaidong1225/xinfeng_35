//
//  MXBundleHelper.m
//  Knowme
//
//  Created by shenzw on 8/4/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "MXBundleHelper.h"
#import "RCTBundleURLProvider.h"
#import "SSZipArchive.h"
@implementation MXBundleHelper
+(NSURL *)getBundlePath{  
////#ifdef  DEBUG
////  NSURL *jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
////  return jsCodeLocation;
////#else
//  //需要存放和读取的document路径
//  //jsbundle地址
//  NSString *jsCachePath = [NSString stringWithFormat:@"%@/\%@",NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0],@"main.jsbundle"];
//  //assets文件夹地址
//  NSString *assetsCachePath = [NSString stringWithFormat:@"%@/\%@",NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0],@"assets"];
//  
//  //判断JSBundle是否存在
//  BOOL jsExist = [[NSFileManager defaultManager] fileExistsAtPath:jsCachePath];
//  //如果已存在
//  if(jsExist){
//    NSLog(@"js已存在: %@",jsCachePath);
//    //如果不存在
//  }else{
//    NSString *jsBundlePath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"jsbundle"];
//    [[NSFileManager defaultManager] copyItemAtPath:jsBundlePath toPath:jsCachePath error:nil];
//    NSLog(@"js已拷贝至Document: %@",jsCachePath);
//  }
//  
//  //判断assets是否存在
//  BOOL assetsExist = [[NSFileManager defaultManager] fileExistsAtPath:assetsCachePath];
//  //如果已存在
//  if(assetsExist){
//    NSLog(@"assets已存在: %@",assetsCachePath);
//    //如果不存在
//  }else{
//    NSString *assetsBundlePath = [[NSBundle mainBundle] pathForResource:@"assets" ofType:nil];
//    [[NSFileManager defaultManager] copyItemAtPath:assetsBundlePath toPath:assetsCachePath error:nil];
//    NSLog(@"assets已拷贝至Document: %@",assetsCachePath);
//  }
//  return [NSURL URLWithString:jsCachePath];
////#endif
  
  //APP进程路径
  NSString *mainBundleDirectory = [[NSBundle mainBundle] resourcePath];
  //APP自带 .zip包路径
  NSString *mainBundleZipPath = [NSString stringWithFormat:@"%@/\%@",mainBundleDirectory,@"bundle.zip"];
  //工作目录
  NSString *workDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  //工作目录 .zip包路径
  NSString *zipPath = [NSString stringWithFormat:@"%@/\%@",workDirectory,@"bundle.zip"];
  //工作目录 main.jsbundle路径
  NSString *jsbundlePath = [NSString stringWithFormat:@"%@/\%@",workDirectory,@"main.jsbundle"];
  //工作目录 assets文件夹地址
  NSString *assetsDirectory = [NSString stringWithFormat:@"%@/\%@",workDirectory,@"assets"];
  
  //检测工作目录是否有 .zip包
  BOOL zipPathExist = [[NSFileManager defaultManager] fileExistsAtPath:zipPath];
  
  if (zipPathExist) {
    NSLog(@"工作目录存在 %@.zip包", zipPath);
  } else {
    NSLog(@"工作目录不存在 %@.zip包", zipPath);
    //拷贝App自带bundle.zip包 到工作目录
    [[NSFileManager defaultManager] copyItemAtPath:mainBundleZipPath toPath:zipPath error:nil];
  }
  
  //解压工作目录中的.zip包
  NSError *error;
  [SSZipArchive unzipFileAtPath:zipPath toDestination:workDirectory overwrite:YES password:nil error:&error];
  if(!error){
    NSLog(@"解压成功");
  }else{
    NSLog(@"解压失败");
    // TODO 提示错误，关闭程序，重新打开
  }
  
  return [NSURL URLWithString:jsbundlePath];
}

@end
