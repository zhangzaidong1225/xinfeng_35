//
//  ModelShare.h
//  feibao
//
//  Created by Wiel on 15/12/19.
//  Copyright © 2015年 Facebook. All rights reserved.
//

#ifndef ModelShare_h
#define ModelShare_h

#import "RCTBridgeModule.h"
#import <UIKit/UIKit.h>


@interface ViewShareController : UIViewController{
  @public
  NSString * title;
  NSString * content;
  NSString * img;
  NSString * url;
  NSString * page;
  NSArray * array;
  NSNumber * filtration;
  NSNumber * time;
  NSNumber * filternum;
  
  UILabel *label;
  
}

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * img;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * page;
@property (nonatomic, retain) NSArray * array;
@property (nonatomic, retain) NSNumber * filtration;
@property (nonatomic, retain) NSNumber * time;
@property (nonatomic, retain) NSNumber * filternum;



@end


@interface Share : NSObject <RCTBridgeModule>

@property (nonatomic, strong) UIView *panelView;

- (UIViewController *)getPresentedViewController;
@end


#endif /* ModelShare_h */
