//
//  DeviceCountShare.h
//  feibao
//
//  Created by Louis Liu on 16/7/8.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DeviceCountShare : NSObject

+ (DeviceCountShare *)sharedInstance;
- ( UIImage *)createShareImage:( NSString *)str name:( NSString *)name number:( NSString *)number grade:( NSString *)grade;
-(UIImage *) createShareImageWith:(NSString *) deviceName filtration:(NSString *) filtration usetime:(NSString *)time filternum:(NSString *)filternum events:(NSArray *) events;

-(void) createShareImageWith:(NSString *) deviceName filtration:(NSNumber *) filtration usetime:(NSNumber *)time filternum:(NSNumber *)filternum events:(NSArray *) events save:(NSString *) name;

+(UIImage *) get_photo_from_app: (NSString *) name;
@end
