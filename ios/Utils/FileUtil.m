//
//  FileUtil.m
//  feibao
//
//  Created by Louis Liu on 16/8/24.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

#import "FileUtil.h"

@implementation FileUtil

- (void)purgeDocumentsDirectory
{
  NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"deviceupdate"];
  //加固件版本号后缀，解决app升级，
  //TODO 清除本地文件
  NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"deviceupdate_34"];
  NSLog(@"Source Path: %@\n Documents Path: %@ \n Folder Path: %@", sourcePath, documentsDirectory, folderPath);
  
  NSError *error;
  
  Boolean success = [[NSFileManager defaultManager] fileExistsAtPath:folderPath];
  if(success) {
    NSLog(@"file exists!");
    return;
  }
  [[NSFileManager defaultManager] copyItemAtPath:sourcePath
                                          toPath:folderPath
                                           error:&error];
  if (error != nil) {
    NSLog(@"Error description-%@ \n", [error localizedDescription]);
    NSLog(@"Error reason-%@", [error localizedFailureReason]);
  }
  
//  NSString *folderPath1 = [documentsDirectory stringByAppendingPathComponent:@"deviceupdate_33_1"];
//  [[NSFileManager defaultManager] copyItemAtPath:sourcePath
//                                          toPath:folderPath1
//                                           error:&error];
}

@end
