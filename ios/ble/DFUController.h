//
//  DFUController.h
//  feibao
//
//  Created by Louis Liu on 16/12/7.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

//#import <iOSDFULibrary/iOSDFULibrary-Swift.h>
#import <FMSwift/FMSwift.h>

@protocol DFUControllerDelegate

@required
- (void) didProgressNumber:(NSInteger)progress;
- (void) didDFUFail;
- (void) didDFUSuccess;
@optional

@end

@interface DFUController : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate, DFUServiceDelegate, DFUProgressDelegate, LoggerDelegate, UIAlertViewDelegate>

@property (nonatomic, assign) id <DFUControllerDelegate> delegate;

+ (DFUController *)sharedInstance;
- (void) secureDFUMode:(BOOL) secureDFU;
//- (void) setCentralManager:(CBCentralManager *) centralManager;
//- (void) setSelectedFileURL:(NSURL *) selectedFileURL;
//- (void) setTargetPeripheral:(CBPeripheral *) peripheral;
- (void) startDFUProcess;

@property (strong, nonatomic) CBPeripheral * dfuPeripheral;
@property (strong, nonatomic) DFUServiceController * dfuController;
@property (strong, nonatomic) CBCentralManager * centralManager;
@property (strong, nonatomic) DFUFirmware * selectedFirmware;
@property (strong, nonatomic) NSURL * selectedFileURL;
@property BOOL secureDFU;


@end
