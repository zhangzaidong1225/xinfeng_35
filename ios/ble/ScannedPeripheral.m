/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "ScannedPeripheral.h"

@implementation ScannedPeripheral
@synthesize peripheral;
@synthesize RSSI;
@synthesize isConnected;
@synthesize isMatchd;
@synthesize name;
@synthesize autoConnect;
@synthesize speed;
@synthesize battery_power;
@synthesize use_time;
@synthesize battery_over;
@synthesize filter_number;
@synthesize filter_time_use_this;
@synthesize filters_maxfan_usetime;
@synthesize type;
@synthesize pm25_flag;
@synthesize pm25_picktime;
@synthesize space;
@synthesize model;

@synthesize filter01_in;
@synthesize filter01_use_time;
@synthesize filter01_use_time_this;
@synthesize filter01_use_time_total;
@synthesize wind01_speed;

@synthesize filter02_in;
@synthesize filter02_use_time;
@synthesize filter02_use_time_this;
@synthesize filter02_use_time_total;
@synthesize wind02_speed;

@synthesize filter_use_time_vehide;
@synthesize filter_num_vehide;
@synthesize vehide_hour;
@synthesize vehide_minute;
@synthesize vehide_model;

@synthesize vehide_week;
@synthesize timeFlag01;
@synthesize timeFlag02;
@synthesize vehide_week02;
@synthesize vehide_hour02;
@synthesize vehide_minute02;
@synthesize timeFlag03;
@synthesize vehide_week03;
@synthesize vehide_hour03;
@synthesize vehide_minute03;
@synthesize vehide_time_state;
@synthesize vehide_time_state02;
@synthesize vehide_time_state03;
@synthesize vehide_is_smart;
@synthesize open_cap;


+ (ScannedPeripheral*) initWithPeripheral:(CBPeripheral*)peripheral rssi:(int)RSSI isPeripheralConnected:(BOOL)isConnected isPeripheraMatchd:(BOOL)isMatchd  PeripheraName:(NSString *)Name Type:(int)type
{
    ScannedPeripheral* value = [ScannedPeripheral alloc];
    value.peripheral = peripheral;
    value.RSSI = RSSI;
    value.isConnected = isConnected;
    value.isMatchd = isMatchd;
    value.name = Name;
    value.autoConnect = false;
    value.speed = 0;
    value.battery_power = 0;
    value.use_time  = 0;
    value.type = type;
  NSLog(@"init type:%d",type);
    return value;
}



-(BOOL)isEqual:(id)object
{
    ScannedPeripheral* other = (ScannedPeripheral*) object;
    return peripheral == other.peripheral;
}

@end
