//
//  DFUControllerDelegate.swift
//  feibao
//
//  Created by Louis Liu on 16/10/24.
//  Copyright © 2016年 Lianluo. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc protocol DFUControllerDelegate {
  func didProgressNumber(_ progress : Int)
  func didDFUFail()
  func didDFUSuccess()
}
