/*
 * Copyright (c) 2015, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ScannedPeripheral : NSObject

@property (strong, nonatomic) CBPeripheral* peripheral;
@property (assign, nonatomic) int RSSI;
@property (nonatomic, readwrite) NSString * name;
@property (nonatomic, readwrite) int isConnected;
@property (nonatomic, readwrite) BOOL isMatchd;
@property (nonatomic, readwrite) BOOL autoConnect;
@property (nonatomic, readwrite) int speed;
@property (nonatomic, readwrite) int battery_power;
@property (nonatomic, readwrite) int use_time;
@property (nonatomic, readwrite) int version;
@property (nonatomic, readwrite) int version_type; //版本类型
@property (nonatomic, readwrite) int battery_charge; //电池充电
@property (nonatomic, readwrite) int pm25; //pm2.5浓度
@property (nonatomic, readwrite) int fan_use_time_this; //本次开机风扇使用
@property (nonatomic, readwrite) int filter_in; //滤棉是否在位
@property (nonatomic, readwrite) int filter_use_time_this; //本次开机滤棉使用时长
@property (nonatomic, readwrite) int battery_over; //电池充电是否过压，电流是否过低
@property (nonatomic, readwrite) int filter_number;//滤棉使用个数
@property (nonatomic, readwrite) int filter_time_use_this;//滤棉使用总时长
@property (nonatomic, readwrite) int filters_maxfan_usetime;//滤棉折合全风速使用时长
@property (nonatomic, readwrite) int type;//产品类型 0--忻风, 1--单品，2--车载
@property (nonatomic, readwrite) int pm25_flag;//pm25采集标志位
@property (nonatomic, readwrite) int pm25_picktime;//pm25采集时间
@property (nonatomic, readwrite) int space;//采集间隔时间
@property (nonatomic, readwrite) int model;//设备采集模式
@property (nonatomic, readwrite) int systemTime;//系统时间戳

@property (nonatomic, readwrite) int filter01_in;//   滤棉1在位
@property (nonatomic, readwrite) int filter01_use_time;// 滤棉1使用时长
@property (nonatomic, readwrite) int filter01_use_time_this;// 滤棉1本次使用时长
@property (nonatomic, readwrite) int filter01_use_time_total;// 滤棉1使用总时长
@property (nonatomic, readwrite) int wind01_speed;// 风扇1等级

@property (nonatomic, readwrite) int filter02_in;//   滤棉2在位
@property (nonatomic, readwrite) int filter02_use_time;// 滤棉2使用时长
@property (nonatomic, readwrite) int filter02_use_time_this;// 滤棉2本次使用时长
@property (nonatomic, readwrite) int filter02_use_time_total;// 滤棉2使用总时长
@property (nonatomic, readwrite) int wind02_speed;// 风扇1等级

@property (nonatomic, readwrite) int filter_use_time_vehide; //设备滤棉使用总时长
@property (nonatomic, readwrite) int filter_num_vehide; //设备滤棉使用个数
@property (nonatomic, readwrite) int vehide_week;//星期
@property (nonatomic, readwrite) int vehide_hour; //小时
@property (nonatomic, readwrite) int vehide_minute; //分钟
@property (nonatomic, readwrite) int vehide_time_state;//0-关 1-开
@property (nonatomic, readwrite) int vehide_model;//有没有单品 0-无 1-有
@property (nonatomic, readwrite) int vehide_is_smart;//是否处于智能模式

@property (nonatomic, readwrite) int timeFlag01; //车载时钟序号1
@property (nonatomic, readwrite) int timeFlag02; //车载时钟序号2
@property (nonatomic, readwrite) int vehide_week02;//星期
@property (nonatomic, readwrite) int vehide_hour02; //小时
@property (nonatomic, readwrite) int vehide_minute02; //分钟
@property (nonatomic, readwrite) int vehide_time_state02;//0-关 1-开

@property (nonatomic, readwrite) int timeFlag03; //车载时钟序号3
@property (nonatomic, readwrite) int vehide_week03;//星期
@property (nonatomic, readwrite) int vehide_hour03; //小时
@property (nonatomic, readwrite) int vehide_minute03; //分钟
@property (nonatomic, readwrite) int vehide_time_state03;//0-关 1-开

@property (nonatomic, readwrite) int open_cap;// 1-打开 0-闭合





@property (strong, nonatomic)CBCharacteristic *uartRXCharacteristic;
@property (strong, nonatomic)CBCharacteristic *uartTXCharacteristic;

+ (ScannedPeripheral*) initWithPeripheral:(CBPeripheral*)peripheral rssi:(int)RSSI isPeripheralConnected:(BOOL)isConnected isPeripheraMatchd:(BOOL)isMatchd  PeripheraName:(NSString *)Name Type:(int)type;

- (NSString*) name;

@end
