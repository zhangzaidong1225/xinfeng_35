//
//  DFUController.m
//  feibao
//
//  Created by Louis Liu on 16/12/7.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "DFUController.h"

@implementation DFUController

//MARK: - Class Implementation
+ (DFUController *)sharedInstance
{
  
  printf("sharedInstance");
  static DFUController *sharedInstance = nil;
  if (sharedInstance == nil) {
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
      sharedInstance = [[self alloc] init];
    });
  }
  return sharedInstance;
}

- (void) secureDFUMode: (BOOL) secureDFU {
  printf("secureDFUMode");
  self.secureDFU = secureDFU;
}

//- (void) setCentralManager:(CBCentralManager *) centralManager {
//  printf("setCentralManager");
//  self.centralManager = centralManager;
//}

//- (void) setSelectedFileURL:(NSURL *) selectedFileURL {
//  printf("setSelectedFileURL");
//  self.selectedFileURL = selectedFileURL;
//}

//- (void) setTargetPeripheral:(CBPeripheral *) peripheral{
//  printf("setTargetPeripheral");
//  self.dfuPeripheral = peripheral;
//}

- (void) startDFUProcess {
  printf("startDFUProcess");
  if(self.dfuPeripheral == nil) {
    printf("No DFU peripheral was set");
    return;
  }
  
  self.selectedFirmware = [[DFUFirmware alloc] initWithUrlToZipFile:self.selectedFileURL];
  DFUServiceInitiator * dfuInitiator = [[DFUServiceInitiator alloc] initWithCentralManager:self.centralManager target:self.dfuPeripheral];
  [dfuInitiator withFirmwareFile:self.selectedFirmware];
  dfuInitiator.delegate = self;
  dfuInitiator.progressDelegate = self;
  dfuInitiator.logger = self;
  self.dfuController = [dfuInitiator start];

}

//MARK: - CBCentralManagerDelegate

- (void) centralManagerDidUpdateState:(CBCentralManager *) central{
  
//  logWith(LogLevel.verbose, message: "UpdatetState: \(centralManager?.state.rawValue)")
}

- (void) centralManager:(CBCentralManager *) central didConnect:(CBPeripheral *) peripheral {
//  print("Connected to peripheral: \(peripheral.name)")
}

- (void) centralManager:(CBCentralManager *) central didDisconnectPeripheral: (CBPeripheral *) peripheral error:(NSError *) error  {
//  print("Disconnected from peripheral: \(peripheral.name)")
}

//MARK: - DFUServiceDelegate
- (void) didStateChangedTo:(DFUState) state{
  switch (state) {
  case DFUStateAborted:

      break;
  case DFUStateSignatureMismatch:
    
      break;

  case DFUStateCompleted:
      [self.delegate didDFUSuccess];

      break;
  case DFUStateConnecting:
      break;
  case DFUStateDisconnecting:
      break;
  case DFUStateEnablingDfuMode:
      break;
  case DFUStateStarting:
      break;
  case DFUStateUploading:
      break;
    case DFUStateValidating:
      break;
  case DFUStateOperationNotPermitted:
      break;
  case DFUStateFailed:
//      [self.delegate didDFUFail];
      break;
  }
//    logWith(LogLevel.info, message: "Changed state to: \(state.description())")
}

- (void) didErrorOccur:(DFUError) error withMessage:(NSString *) message{
  [self.delegate didDFUFail];
//  logWith(LogLevel.error, message: message)
}

//MARK: - DFUProgressDelegate
-(void) onUploadProgress:(NSInteger) part totalParts:(NSInteger) totalParts progress:(NSInteger) progress currentSpeedBytesPerSecond:(double) currentSpeedBytesPerSecond avgSpeedBytesPerSecond:(double) avgSpeedBytesPerSecond  {
  [self logWith:LogLevelInfo message:@"onUploadProgress: \(progress)"];
  [self.delegate didProgressNumber:progress];
  
}

//MARK: - LoggerDelegate
-(void) logWith:(LogLevel) level message:(NSString *) message{
  printf("\(level.name()) : \(message)");
}

//MARK: - UIAlertViewDelegate
-(void) alertViewCancel:(UIAlertView *) alertView {
  [self logWith:LogLevelVerbose message: @"Abort cancelled"];
  if (self.dfuController.paused == true) {
    [self.dfuController resume];
  }

}

-(void) alertView:(UIAlertView *) alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
  if (self.dfuController == nil) {
    [self logWith:LogLevelError message: @"DFUController not set, cannot abort"];
    return;
  }
  
  switch (buttonIndex) {
  case 0:
      [self logWith:LogLevelVerbose message: @"Abort cancelled"];
      if (self.dfuController.paused == true) {
        [self.dfuController resume];
      }

      break;
    
  case 1:
      [self logWith:LogLevelVerbose message:@"Abort Confirmed"];
      [self.dfuController abort];
      break;
  default:
      break;
  }
}
@end
