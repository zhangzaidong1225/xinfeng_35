/**
* 关于页面
* 在此页面运用封装titile实现标题栏
* 引入listView实现列表
* author: Louis
* date:2016.04.13 
*/


'use strict';

import Title from './title'

import style_about from './styles/style_about'
import  config from './config'
import styles_web from './styles/style_web'
import Language from './Language'

import SharedPreferences from './AsyncStorage'

var UMeng = require('./UMeng');

var Utils = require('./utils')



var iconCliclkNum = 0;
var textClickNum = 0;
var testMarkStr = 'XinFeng';
import React, { Component } from 'react';
import {
	View,
	Image,
	Text,
	TouchableOpacity,
	Platform,
	Linking,
	AsyncStorage,
} from 'react-native';

var About = React.createClass({

	getInitialState:function () {
		return {
			testMark: testMarkStr,//忻风测试
		};
	},

	getDefaultProps:function () {
		return {
			//Title params
	        title:Language.about,
	        hasGoBack: true,
	        backAddress: null,
  		};
  	},

  	componentWillUnmount(){
  		iconCliclkNum = 0;
		textClickNum = 0;
		testMarkStr = this.state.testMark;
		Utils.SetTestMarkStr(testMarkStr);
  	},

  	componentWillMount(){
          AsyncStorage.getItem('isTestXinfeng', (error, result) => {
          	if (error) {

            } else {
              if (result == '忻风测试') {
                this.setState({
					testMark: Language.xinfengTest,
				});
              } else {
                this.setState({
					testMark:Language.xinfeng,
				});
              }
            }
          });
  	},

	render: function() {
		
	  if (Platform.OS === 'android') {
        return (
			<View style = {style_about.container}>
				<Title title = {Language.about} hasGoBack = {this.props.hasGoBack} backAddress = {this.props.backAddress} navigator = {this.props.navigator}/>
				
				<TouchableOpacity activeOpacity = {1} onPress={()=>{this._gotoIconTest()}}>
				<Image style={[style_about.appIcon,{resizeMode: 'stretch'}]} source={require('image!appicon')}/>
				</TouchableOpacity>
				<Text allowFontScaling={false} style={style_about.appName}>{this.state.testMark}</Text>
				<Text allowFontScaling={false} style={style_about.appVersion}>{Language.version}{':'} {config.varsion}</Text>

				<View style = {[style_about.splitLines,{marginTop: 20}]}/>
				<TouchableOpacity activeOpacity = {1} onPress={()=>this._gotoTxtTest()}>
				<View style = {style_about.goodsContainer}>
					<Text allowFontScaling={false} style={style_about.goodsName}>{Language.product}</Text>
					<Text allowFontScaling={false} style={style_about.xinfengName}>{Language.xinfengair}</Text>
				</View>
				</TouchableOpacity>
				<View style = {[style_about.splitLines,{marginTop: 0}]}/>

				<View style = {[style_about.splitLines,{marginTop: 10}]}/>
				<TouchableOpacity style={style_about.goodsContainer} onPress={()=>{this._gotoUserProtocol()}}>
					<Text allowFontScaling={false} style={style_about.goodsName}>{Language.useragreement}</Text>
					<Image style={style_about.nextImage} source={require('image!ic_next')}/>
				</TouchableOpacity>
				<View style = {[style_about.splitLines,{marginLeft: 0,marginTop: 0, backgroundColor: 'white'}]}>
					<View style = {[style_about.splitLines,{marginLeft: 20,marginTop: 0}]}>
					</View>
				</View>
				<TouchableOpacity style={style_about.goodsContainer} onPress={()=>{this._gotoFeedback()}}>
					<Text allowFontScaling={false} style={style_about.goodsName}>{Language.feedback}</Text>
					<Image style={style_about.nextImage} source={require('image!ic_next')}/>
				</TouchableOpacity>
				<View style = {[style_about.splitLines,{marginTop: 0}]}/>

				<View style = {[style_about.phoneContainer, {marginTop: 0,}]}>
					<Text allowFontScaling={false} style={style_about.phoneName}>{Language.servicephone}{':'} </Text>
					<Text allowFontScaling={false} style={style_about.phone}>400-968-5665</Text>
				</View>
				<View style = {[style_about.webContainer, {marginTop: 5,}]}>
					<Text allowFontScaling={false} style={style_about.phoneName}>{Language.url}{':'}</Text>
					<Text allowFontScaling={false} style={style_about.phone}>www.lianluo.com</Text>
				</View>

				<Image style={[style_about.lianluoCPIcon,{resizeMode: 'stretch'}]} source={require('image!ic_produce')}/>
				<Text allowFontScaling={false} style={[style_about.powerContainer,{marginTop: 5,}]}>Powered by @2016 lianluo.com</Text>
			</View>
		);
      }else {
      	return (
			<View style = {style_about.container}>
				<Title title = {Language.about} hasGoBack = {this.props.hasGoBack} backAddress = {this.props.backAddress} navigator = {this.props.navigator}/>
				
				<TouchableOpacity activeOpacity = {1} onPress={()=>{this._gotoIconTest()}}>
				<Image style={[style_about.appIcon,{resizeMode: 'stretch'}]} source={require('image!appicon')}/>
				</TouchableOpacity>
				<Text allowFontScaling={false} style={style_about.appName}>{this.state.testMark}</Text>
				<Text allowFontScaling={false} style={style_about.appVersion}>{Language.version} {':'} {config.varsion}</Text>

				<View style = {[style_about.splitLines,{marginTop: 20}]}/>
				<TouchableOpacity activeOpacity = {1} onPress={()=>this._gotoTxtTest()}>
				<View style = {style_about.goodsContainer}>
					<Text allowFontScaling={false} style={style_about.goodsName}>{Language.product}</Text>
					<Text allowFontScaling={false} style={style_about.xinfengName}>{Language.xinfengair}</Text>
				</View>
				</TouchableOpacity>
				<View style = {[style_about.splitLines,{marginTop: 0}]}/>

				<View style = {[style_about.splitLines,{marginTop: 10}]}/>
				<TouchableOpacity style={style_about.goodsContainer} onPress={()=>{this._gotoGrade()}}>
					<Text allowFontScaling={false} style={style_about.goodsName}>{Language.score}</Text>
					<Image style={style_about.nextImage} source={require('image!ic_next')}/>
				</TouchableOpacity>
				<View style = {[style_about.splitLines,{marginLeft: 0,marginTop: 0, backgroundColor: 'white'}]}>
					<View style = {[style_about.splitLines,{marginLeft: 20,marginTop: 0}]}>
					</View>
				</View>
				<TouchableOpacity style={style_about.goodsContainer} onPress={()=>{this._gotoUserProtocol()}}>
					<Text allowFontScaling={false} style={style_about.goodsName}>{Language.useragreement}</Text>
					<Image style={style_about.nextImage} source={require('image!ic_next')}/>
				</TouchableOpacity>
				<View style = {[style_about.splitLines,{marginLeft: 0,marginTop: 0, backgroundColor: 'white'}]}>
					<View style = {[style_about.splitLines,{marginLeft: 20,marginTop: 0}]}>
					</View>
				</View>
				<TouchableOpacity style={style_about.goodsContainer} onPress={()=>{this._gotoFeedback()}}>
					<Text allowFontScaling={false} style={style_about.goodsName}>{Language.feedback}</Text>
					<Image style={style_about.nextImage} source={require('image!ic_next')}/>
				</TouchableOpacity>
				<View style = {[style_about.splitLines,{marginTop: 0}]}/>

				<View style = {[style_about.phoneContainer, {marginTop: 0,}]}>
					<Text allowFontScaling={false} style={style_about.phoneName}>{Language.servicephone}{':'}</Text>
					<Text allowFontScaling={false} style={style_about.phone}>400-968-5665</Text>
				</View>
				<View style = {[style_about.webContainer, {marginTop: 5,}]}>
					<Text allowFontScaling={false} style={style_about.phoneName}>{Language.url}{':'}</Text>
					<Text allowFontScaling={false} style={style_about.phone}>www.lianluo.com</Text>
				</View>

				<Image style={[style_about.lianluoCPIcon,{resizeMode: 'stretch'}]} source={require('image!ic_produce')}/>
				<Text allowFontScaling={false} style={[style_about.powerContainer,{marginTop: 5,}]}>Powered by @2016 lianluo.com</Text>
			</View>
		);
      }
		
	},
	_gotoGrade: function() {
		UMeng.onEvent('about_gotograde');
		if (Platform.OS === 'android') {
			Utils.GotoGrade();
		} else {
			Linking.openURL("https://itunes.apple.com/cn/app/id1104743021?mt=8");
		}
	},

	_gotoUserProtocol: function() {
		var USER_PROTOCOL = config.server_base+config.user_protocol;//用户使用协议
		console.log(USER_PROTOCOL);
		UMeng.onEvent('about_gotoUserProtocol');
	    this.props.navigator.push({
	    	id: 'WebViewPage',
	      	params:{
	          	title:Language.useragreement + '',
	          	url: USER_PROTOCOL,
	          	hasGoBack: true,
	          	webviewStyle:styles_web.webview_protocol,
	        }
	    });
	},

	_gotoFeedback:function(){
		UMeng.onEvent('about_gotoFeedback');
	    this.props.navigator.push({
	      	id: 'FeedPage',
	    });
	},

	_gotoIconTest:function() {
		iconCliclkNum++;
	},

	_gotoTxtTest:function() {
		textClickNum++;
		if (iconCliclkNum == 3 && textClickNum == 2) {
			console.log('==this.state.testMark:' + this.state.testMark);
			if (this.state.testMark == Language.xinfeng+'') {
				this.setState({
	                testMark:Language.xinfengTest,
	            });
	            config.server_base = config.server_base_test;
	            console.log('==忻风config server base:' + config.server_base);

	            AsyncStorage.setItem('isTestXinfeng',
				    '忻风测试',error => {
				      if (error){
				      	console.log("==测试11"+this.state.testMark);
				      }else {
				        console.log("==测试22"+this.state.testMark);
				      }
				    });
					if (Platform.OS === 'android') {
						// SharedPreferences.setItem('isTestXinfeng','忻风测试');
					SharedPreferences.setItem('isTestXinfeng',
				    '忻风测试',error => {
				      if (error){
				      }else {
				      }
				    });
					} 

			} else {
			    console.log("==正式"+this.state.testMark);
				this.setState({
					testMark:Language.xinfeng,
				});
				config.server_base = config.server_base_normal;
				console.log('==正式config server base:' + config.server_base);
				AsyncStorage.setItem('isTestXinfeng',
				'忻风',error => {
				    if (error){
				    }else {
				    }
				});

					if (Platform.OS === 'android') {
						SharedPreferences.setItem('isTestXinfeng',
						'忻风',error => {
						    if (error){
						    }else {
						    }
						});
					}

			} 
		}
	},
});
module.exports = About;