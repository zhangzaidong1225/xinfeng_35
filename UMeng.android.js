/**
* UMeng:
* ios from AsyncStorage and android from RCTSZTYSharedPreferencesModule,
* 
*/

'use strict';


// var { NativeModules } = require('react-native');
var RCTUMengModule = require('NativeModules').UMengModule;

var UMeng = {
	onProfileSignIn: function(userId: string) {
		RCTUMengModule.onProfileSignIn(userId);
	},
	onProfileSignOff: function() {
		RCTUMengModule.onProfileSignOff();
	},

	onPageStart: function(pageName:string) {
		RCTUMengModule.onPageStart(pageName);
	},

	onPageEnd: function(pageName:string) {
		RCTUMengModule.onPageEnd(pageName);
	},

	onEvent: function(eventId: string) {
		RCTUMengModule.onEvent(eventId);
	},

	onEventWithValue: function(eventId: string, key: sting, value: string) {
		RCTUMengModule.onEventWithValue(eventId, key, value);
	},
};

module.exports = UMeng;