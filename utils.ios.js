'use strict';

var { NativeModules } = require('react-native');


var Utils = {
	'GetAdAction':function(callback) {
		NativeModules.Utils.get_ad_action(callback);
	},

	'SetTestMarkStr':function(mark) {
		NativeModules.Utils.set_test_mark_str(mark);
	},

	'getCurrentLanguage':function(callback) { 		
		NativeModules.Utils.is_english_language(callback); 
	},

	'getSelectItemAddr':function(callback) {
		NativeModules.Utils.get_select_item_addr(callback);
	},

	'setSelectItemAddr':function(addr) {
		NativeModules.Utils.set_select_item_addr(addr);
	},

};

module.exports = Utils;