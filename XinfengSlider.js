'use strict'

var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
import style_xinfengSlider from './styles/style_xinfengSlider' 

import tools from './tools'
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    styles_sliderheet,
    Text,
    View,
    PanResponder,
    Image,
    Animated,
    Dimensions,
    PixelRatio,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    DeviceEventEmitter,
    NativeModules,
    ToastAndroid,
    NativeAppEventEmitter,
    // AsyncStorage,
    Platform,
    Alert,
    Easing,
} from 'react-native';

var timing = Animated.timing;
var wave_animate = null;
var speed = 20;
var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;


var BlueToothUtil= require('./BlueToothUtil');



var Wait = React.createClass({

  getInitialState:function(){
    return {
    }
  },


  getDefaultProps: function () {
    return {

    };
  },

  render:function (){
    console.log('==speed==wait');
    return (
      <View>
        <View
          style = {style_xinfengSlider.wait}>

          <BackGround />


        </View>
    </View>
    );
  }
});

//风速条待机页面的显示
var BackGround = React.createClass({


  getDefaultProps: function () {
    return {
      // model:'',
    };
  },

    render:function(){
      return (
      		<View style = {style_xinfengSlider.back_container}>

              <View style = {style_xinfengSlider.back_left}>
              	<View style = {style_xinfengSlider.back_view1}>
                
                <Image source={require('image!ic_gray_wind_small')}
                    style = {style_xinfengSlider.back_img1}/>
                </View>
              </View>


            <View style ={style_xinfengSlider.back_center}>

		          <View style = {style_xinfengSlider.back_view2}>
	              <View
	                  style = {style_xinfengSlider.back_view3} >
	                  
	              </View>
	            </View>  

            </View>


          <View style = {style_xinfengSlider.back_right}>
          	<View style = {style_xinfengSlider.back_view4}>
            <Image source={require('image!ic_gray_wind_big')}
                style = {style_xinfengSlider.back_view5}/>
       		</View>
          </View>
        </View>
      );
    },

}); 

var  factor = (deviceWidth-140) / 70;// 风速滑动条调节因子
var sub_currentDevice; //当前设备变化的事件
var sub_wait;  // 待机和唤醒事件的监听
var queue;//风速管理队列
var windMap;//风速值map

var XinfengSlider = React.createClass({

  mixins:[TimerMixin],

  _panResponder: {},

  // timer_delay: null,

  // timer_windSpeed : null,
  // last_windSpeed: null,
 
  getInitialState() {

     // DeviceEventEmitter.addListener('getconnectstate',(e)=>{
     //   this.setState({
     //    isOpen: e.connectstate,
     //   });
     // });

      return {
          value:0,//滑动条值
          isOpen:'未连接', //判断开关机
          isVisiable:false, //判断风速文字是否显示
          // delay:true, //标记滑动延迟
          pselect_item:null, // device 选中的设备
          mvalue:50, // 记录滑动的值
          // fanclose:0,
          mclose:false, //待机和唤醒区分开
          // state_text:'', 

      }
  },  


  componentWillMount: function() {

    queue = this.Queue();

    windMap = new Map();
    // var keyString = "a string",
    //     keyObj = {},
    //     keyFunc = function () {};

    // // setting the values
    // windMap.set(keyString, "value associated with 'a string'");
    // windMap.set(keyObj, "value associated with keyObj");
    // windMap.set(keyFunc, "value associated with keyFunc");

    // windMap.size; // 3

    // // getting the values
    // console.log('zz``'+windMap.get(keyString));    // "value associated with 'a string'"
    // console.log('zz``'+windMap.get(keyObj));       // "value associated with keyObj"
    // console.log('zz```'+windMap.get(keyFunc));      // "value associated with keyFunc"

    this._panResponder = PanResponder.create({
      // 要求成为响应者：
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderGrant: (evt, gestureState) => {
        // 开始手势操作。给用户一些视觉反馈，让他们知道发生了什么事情！
        if (gestureState.x0 >= 75  && gestureState.x0 <= deviceWidth - 65){
        var x_val = (gestureState.x0 - 75)/factor + 30;
        // this.timer_delay && clearTimeout(this.timer_delay);

        if (x_val < 30) {
          x_val = 30;
          RCTDeviceEventEmitter.emit('slider_wait',x_val);
        }
        if (x_val > 100) {

          x_val = 100;
        }
        this.setState({isVisiable:true,value:Math.round(x_val)});
        
        }

        
        // gestureState.{x,y}0 现在会被设置为0
      },
      onPanResponderMove: (evt, gestureState) => {
        // 最近一次的移动距离为gestureState.move{X,Y}
        var x_val = (gestureState.x0+gestureState.dx -70)/factor+ 30;
        if (x_val < 30) {x_val = 30;}
        if (x_val > 100) {x_val = 100;}
        this.setState({value:Math.round(x_val)});
        
        
        // 从成为响应者开始时的累计手势移动距离为gestureState.d{x,y}
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        // 用户放开了所有的触摸点，且此时视图已经成为了响应者。
        // 一般来说这意味着一个手势操作已经成功完成。
        var x_val = (gestureState.x0+gestureState.dx - 70)/factor+ 30;
        // console.log('onPanResponderRelease x_val:' + x_val);
        if (x_val < 30) {x_val = 30;}
        if (x_val > 100) {x_val = 100;}
        this._tran(Math.round(x_val));

        this.setState({isVisiable:false,value:Math.round(x_val)});
      },
      onPanResponderEnd:(evt, gestureState) => {
      },
      onPanResponderReject: (e, gestureState) => {
        return true;
      },
      onPanResponderTerminate: (evt, gestureState) => {
        // 另一个组件已经成为了新的响应者，所以当前手势将被取消。
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        // 返回一个布尔值，决定当前组件是否应该阻止原生组件成为JS响应者
        // 默认返回true。目前暂时只支持android。
        //console.log(gestureState);
        return true;
      },
    });
  }, 

  componentDidMount:function(){
    var sendSpeed = -1;
        //切换设备的监听器
    sub_currentDevice = RCTDeviceEventEmitter.addListener('message_select_item',(val)=>{
      console.log("设备风速值前：" + val.speed);
      //蓝牙返回风速值与风速队列一值，设置val.speed为最后入队列的值
      //风速队列为空，处理自己的值
      if (queue._size() > 0 ) {
        if (queue._getEnd() == val.speed) {
          queue._clear();
        } else {
          val.speed = queue._getEnd();
          console.log("设备风速值中：" + val.speed + "queue._getEnd = " + queue._getEnd() + "queue._getStart = " + queue._getStart() + 'queue:' + queue._quere());
          if (sendSpeed != val.speed) {
            BlueToothUtil.windSpeed(this.state.pselect_item.addr,val.speed); 
            // windMap.set(this.state.pselect_item.addr,val.speed);

            sendSpeed = val.speed;           
          };
        }
      } else {
          if(val == null){
              this.setState({
               isOpen:'未连接',
              });
          }else{
            var tmp_mclose = true;

            if(val.speed > 0){tmp_mclose = false;}

            this.setState({
             isOpen:'已连接',
             value:val.speed,
             pselect_item:val,
             mclose:tmp_mclose,
            });
          }
        }
      console.log("设备风速值后：" + val.speed);

    });


      
    //待机和唤醒的监听器
    sub_wait = RCTDeviceEventEmitter.addListener('slider',(val)=>{
      console.log('待机和唤醒···'+val);

      if (val) {
        //手动改变风速值，直接将风速压入队列
        if (this.state.value != 0 && this.state.pselect_item.addr != null){
          
           windMap.set(this.state.pselect_item.addr,this.state.value);
           console.log('zz``'+windMap.get(this.state.pselect_item.addr)); 
           queue.push(0);
           RCTDeviceEventEmitter.emit('queue_wind',0+'');
          console.log("设备即将待机:" + queue._quere());
          this.setState({
            // mvalue:this.state.value,
            value:0,
            mclose:val,
          });
         

           BlueToothUtil.windSpeed(this.state.pselect_item.addr,0);

        }

      }else{
        console.log('zz``'+windMap.get(this.state.pselect_item.addr)); 

        if (!windMap.has(this.state.pselect_item.addr)){
          windMap.set(this.state.pselect_item.addr,50);
        }
        //手动改变风速值，直接将风速压入队列
        queue.push(windMap.get(this.state.pselect_item.addr));
        
        RCTDeviceEventEmitter.emit('queue_wind',windMap.get(this.state.pselect_item.addr));

        console.log("设备即将唤醒:" + queue._quere());
        this.setState({
          value:windMap.get(this.state.pselect_item.addr),
          mclose:val,
        });
        if (this.state.pselect_item.addr != null){
         BlueToothUtil.windSpeed(this.state.pselect_item.addr,windMap.get(this.state.pselect_item.addr));
        }

      };
    });
  },

  componentWillUnmount(){
    console.log('==speed==componentWillUnmount');
    sub_currentDevice.remove();
    sub_wait.remove();

    // this.timer_delay &&  clearTimeout(this.timer_delay);
  },


  _tran:function(value){

    if (value>0 && value <30) {
      value = 30;
    }
    // if (this.timer_windSpeed == null) {
      if (queue._size() <= 0) {
      BlueToothUtil.windSpeed(this.state.pselect_item.addr,value); 
        }
      queue.push(value);
      console.log("设备风速值压入：" + value);

      windMap.set(this.state.pselect_item.addr,value);

    RCTDeviceEventEmitter.emit('queue_wind',value); 
  },

    render:function (){
      // console.log('zz```'+windMap.size);

      var navigator = this.props.navigator;

      var wind_text;
      var tmp_img,tmp_back;

      if (this.state.isVisiable === false){
          wind_text = '';
          
      }else {
          wind_text=Language.wind_speed+Math.round (this.state.value);
      }

      //待机页面
      if(this.state.isOpen == '已连接' && this.state.mclose){
        console.log('==speed==2');
        return (
          <View >
            <Wait 
                navigator = {this.props.navigator}/>
          </View>
        );
      }else{
        //唤醒页面
          console.log('==speed==3' + this.state.value);
         // marginTop:42 / 640 * deviceHeight,
        return (
          <View style = {style_xinfengSlider.slide_container}>

          <View
            style = {style_xinfengSlider.slide_container2} {...this._panResponder.panHandlers}>

              <View style = {style_xinfengSlider.slide_left}>
              	<View style = {style_xinfengSlider.slide_view1}>
                
                <Image source={require('image!ic_blue_wind_small')}
                    style = {style_xinfengSlider.slide_img1}/>
                </View>
              </View>
              <View style= {style_xinfengSlider.slide_center}>
  		            <View style = {style_xinfengSlider.slide_view2}>
  		               <Text allowFontScaling={false} style = {style_xinfengSlider.slide_text1}>{wind_text}</Text>
  		            </View>
  		            <View style = {style_xinfengSlider.slide_view3} >
    	              <View
    	                  style = {style_xinfengSlider.slide_view4} >
    	                <View 
    	                	style = {[style_xinfengSlider.slide_view5,{width:(this.state.value-30)*factor}]}/>
    	              </View>
  	              </View>
    	            <View style = {style_xinfengSlider.slide_right} >

                  <View style = {{width:19,height:19,backgroundColor:'#2AB9F1',left:((this.state.value-30)* factor) - 7.5,
                              borderWidth:1/PixelRatio.get(),borderRadius:9.5,borderColor:'#2AB9F1',}}/>
    	            </View>      
              </View>

              <View style = {style_xinfengSlider.slide_right2}>
              	<View style = {style_xinfengSlider.slide_view6}>
                  <Image source={require('image!ic_blue_wind_big')}
                      style = {style_xinfengSlider.slide_img3}/>
           		  </View>
              </View>
          </View>



        </View>

        
        );
      }
      
  },

  changeSpeed(){
    wave_animate.stop();
      speed = 1000;
      this.startAnimation();

  },

/**
 * [Queue]
 * @param {[Int]} size [队列大小]
 */
 Queue:function(size) {
    var queue_ = [];

    //向队列中添加数据
    queue_._push = function(data) {
      if (data == null) {
        return false;
      }
      //如果传递了size参数就设置了队列的大小
      if (size != null && !isNaN(size)) {
        if (queue_.length == size) {
          queue_.pop();
        }
      }
      queue_.unshift(data);
      return true;
    };

    //从队列中取出数据
    queue_._pop = function() {
      return queue_.pop();
    };

    //返回队列的大小
    queue_._size = function() {
        return queue_.length;
    }

    //返回队列的内容
    queue_._quere = function() {
        return queue_;
    }

    queue_._clear = function() {
      while (queue_.length > 0) {
        queue_.pop();
      }
      return true;
    }

    //获取队列最后一个元素
    queue_._getEnd = function() {
      if (queue_.length > 0) {
        return queue_[queue_.length - 1];
      } else {
        return null;
      }
    }

    //获取队列第一个元素
    queue_._getStart = function() {
      if (queue_.length > 0) {
        return queue_[0];
      } else  {
        return null;
      }
    }

    return queue_;
}

});
    // Demo
    // 初始化没有参数的队列
  //  var queue = this.Queue();
  //   for (var i = 1; i <= 5; i++) {
  //       queue._push(i);
  //   }
  // console.log("Queue Demo Test:");  
  // console.log(queue._quere());
  //  var x = queue._pop();   //从队列中取出一个
  //  var size = queue._size();
  //  var end = queue._getEnd();
  //  console.log(queue._quere()); 
  //  console.log("queue_ pop = " + x + ';queue_ size = ' + size + ';queue_ end = ' + end);

 
module.exports = XinfengSlider;


