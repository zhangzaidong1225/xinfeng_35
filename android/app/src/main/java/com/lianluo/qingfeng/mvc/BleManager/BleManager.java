package com.lianluo.qingfeng.mvc.BleManager;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.ReactMethod;
import com.lianluo.qingfeng.MainActivity;
import com.lianluo.qingfeng.mvc.BleManager.BleCmd.BleCmdManager;
import com.lianluo.qingfeng.mvc.BleManager.BleCmd.DevMsgAction;
import com.lianluo.qingfeng.mvc.BleManager.Scan.ScanDevManager;
import com.lianluo.qingfeng.mvc.BleManager.UartService.UartService;
import com.lianluo.qingfeng.mvc.Interface.BleStatus;
import com.lianluo.qingfeng.mvc.Modal.BleDevManager;
import com.lianluo.qingfeng.mvc.Modal.DfuProgressListener.DfuProgressListener;
import com.lianluo.qingfeng.mvc.Modal.Update.DfuService;

import java.io.File;

import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;

/**
 * Created by zhangzaidong on 2016/9/12.
 */
public class BleManager {

    private static final String TAG = "BleManager";

    public static volatile BleManager mInstance;

    public  UartService uartService;

    public BluetoothAdapter mBluetoothAdapter;

    public BluetoothManager mBluetoothManager;

    public BleStatus bleStatus;
    public final static int REQUEST_SCAN = 110;
    public final static int REQUEST_CONNECT = 111;

    private static final int REQUEST_FINE_LOCATION = 0;
    private static final int REQUEST_FINE_LOCATION_ENABLE = 1;

    public static BleManager getInstance() {
        if (mInstance == null) {
            synchronized (BleManager.class){
                if (mInstance == null){
                    mInstance = new BleManager();
                }
            }
        }
        return mInstance;
    }

    public BleManager() {
    }

    public BleManager(MainActivity context) {
        init(context);
        initBlue(context);
    }

    //接口实例
    public void setBleStatus(BleStatus bleCallback) {
        this.bleStatus = bleCallback;
    }

    /**
     * 初始化，启动服务
     *
     * @param context
     */
    public void init(MainActivity context) {
        Log.e(TAG, "init--context--");
        Intent serviceIntent = new Intent(context, UartService.class);
        context.bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

    }

    public ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            uartService = ((UartService.UartServiceLocalBinder) service).getService();
            Log.e(TAG, "init--context-1-" + uartService);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e(TAG, "init--context-2-" + uartService);
            uartService = null;
        }
    };

    /**
     * blue初始化
     */
    public boolean initBlue(MainActivity context) {

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        }

        return true;
    }

    /**
     * 判断蓝牙是否enable
     */
    public boolean bleEnable() {
        if (!mBluetoothAdapter.isEnabled()) {
            return false;
        }
        return true;
    }

    /**
     * 请求蓝牙
     */
    public void requestBlue(Activity activity, String address, int requestCode) {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        intent.putExtra("address", address);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 涉及到权限
     */
    public void scan() {
        if (mBluetoothAdapter.isEnabled()) {
            if (ContextCompat.checkSelfPermission(MainActivity.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                ScanDevManager.getInstance().scan();
            }else {
                enableRequestLocation();
            }
        } else {
            requestBlue(MainActivity.mContext, "", REQUEST_SCAN);

        }
    }


    public void stopScan() {
        ScanDevManager.getInstance().stopScan();
    }

    /**
     *
     */
    public void enableRequestLocation() {
        if (ContextCompat.checkSelfPermission(MainActivity.mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(
                    MainActivity.mContext,Manifest.permission.ACCESS_COARSE_LOCATION)){
                Toast.makeText(MainActivity.mContext, "您已禁止GPS权限，需要重新开启。", Toast.LENGTH_SHORT).show();

            }else{
                ActivityCompat.requestPermissions(MainActivity.mContext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_FINE_LOCATION);
            }
        }else {
            ScanDevManager.getInstance().scan();

        }
    }

    public void mayRequestLocation(){
        if (ContextCompat.checkSelfPermission(MainActivity.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.mContext,Manifest.permission.ACCESS_COARSE_LOCATION)){
            }else{
                ActivityCompat.requestPermissions(MainActivity.mContext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_FINE_LOCATION);
            }
        }else {
            ScanDevManager.getInstance().scan();
        }
    }



    /**
     * 手动点击
     * 负责上层调用
     *
     * @param address
     */
    public void connectDevice(String address) {

        connect(address, true);
    }

    /**
     * 实际负责处理连接逻辑
     *
     * @param address
     * @param connecting
     */
    public void connect(String address, boolean connecting) {

        if (mBluetoothAdapter.isEnabled()){
            BleDevManager.getInstance().connect(address, connecting);

        }else {
            requestBlue(MainActivity.mContext,address,REQUEST_CONNECT);

        }

    }

    public void connect(String address) {
        Log.e(TAG, "CONNECT--" + address);

        if (address != null) {
            uartService.connect(address);
        }

    }

    /**
     * 自动连接
     */
    public void auto_connect() {
        BleDevManager.getInstance().autoConnecting();
    }


    public void stopConnect(String address) {
        BleDevManager.getInstance().stopConnect(address);

    }

    /**
     * 断开连接
     *
     * @param address
     */
    public void disConnect(String address) {
        BleCmdManager.getInstance().addMsg2Manager(address, "", DevMsgAction.Disconnect, 0);
    }

    /**
     * 取消配对
     *
     * @param address
     */
    public void cancelBonded(String address) {
        BleDevManager.getInstance().cancelBonded(address);

    }

    /**
     * 关机
     *
     * @param address
     */
    public void shutdown(String address) {
        BleCmdManager.getInstance().addMsg2Manager(address, "", DevMsgAction.ShutDown, 0);
    }

    /**
     * 调节风速
     *
     * @param address
     * @param speed
     */
    public void windSpeed(String address, int speed) {
        BleCmdManager.getInstance().addMsg2Manager(address, "", DevMsgAction.WindSpeed, speed);
    }

    /**
     * 查找设备
     *
     * @param address
     */
    public void devCheck(String address) {
        BleCmdManager.getInstance().addMsg2Manager(address, "", DevMsgAction.DevCheck, 0);
    }

    /**
     * 设备重命名
     *
     * @param address
     */
    public void devRename(String address, String rename) {

        BleCmdManager.getInstance().addMsg2Manager(address, rename, DevMsgAction.DevRename, 0);

        BleDevManager.getInstance().devRename(address, rename);

    }

    /**
     * 固件升级
     *
     * @param address
     * @param name
     * @param path
     */
    public void deviceUpdate(String address, String name, String path) {
        DfuServiceListenerHelper.registerProgressListener(MainActivity.mContext, new DfuProgressListener());
        final DfuServiceInitiator starter = new DfuServiceInitiator(address)
                .setDeviceName(name)
                .setKeepBond(true);
        Uri uri = Uri.fromFile(new File(path));
        starter.setZip(uri, path);
        starter.start(MainActivity.mContext, DfuService.class);
    }


}
