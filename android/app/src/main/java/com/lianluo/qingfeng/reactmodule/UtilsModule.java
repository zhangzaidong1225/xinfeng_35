package com.lianluo.qingfeng.reactmodule;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.lianluo.qingfeng.MainActivity;
import com.lianluo.qingfeng.utils.SZTYSharedPreferences;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by louis on 16/4/14.
 */
public class UtilsModule extends ReactContextBaseJavaModule {
    Context context;
    Map<String,Boolean> map;
    public UtilsModule(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
        map = new HashMap<>();
    }

    @Override
    public String getName() {
        return "Utils";
    }

    public Handler mHandler;
    /**
     * 评分
     */
    @ReactMethod
    public void GotoGrade(){
        Intent markIntent = new Intent(Intent.ACTION_VIEW);
        markIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        markIntent.setData(Uri.parse("market://details?id="
                + getReactApplicationContext().getPackageName()));
        PackageManager pkgMg = getReactApplicationContext().getPackageManager();
        List<ResolveInfo> infos = pkgMg.queryIntentActivities(markIntent,
                PackageManager.MATCH_DEFAULT_ONLY);
        if (infos.size() > 0) {
            getReactApplicationContext().startActivity(markIntent);
        } else {
            //TODO toast no market.
        }
    }
    @ReactMethod
    public void ShowDialog(String name, final String addr, final Callback callback){
        Log.e("ShowDialog", name + "===================ShowDialog");
        Dialog dialog = new AlertDialog.Builder(MainActivity.mContext)
                .setTitle("强制升级").setMessage(name+"设备需强制升级，否则无法正常显示")
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        map.put(addr,false);
                        callback.invoke(false);
//                        Toast.makeText(MainActivity.mContext, "取消升级。。。。", Toast.LENGTH_LONG)
//                                .show();
                        dialog.dismiss();
                    }
                }).setPositiveButton("强制升级", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        map.put(addr,true);
                        callback.invoke(true);
//                        Toast.makeText(MainActivity.mContext, "正在升级。。。。",
//                                Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                }).create();
        dialog.setCanceledOnTouchOutside(false);// 设置点击屏幕Dialog不消失
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
            {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                } else {
                    return false; //默认返回 false
                }
            }
        });
        dialog.show();
    }
    @ReactMethod
    public void GetAdAction(Callback successCallback) {

        try {
            String action = SZTYSharedPreferences.getInstance().getString(SZTYSharedPreferences.AD_ACTION, "");
            successCallback.invoke(action);

        } catch (IllegalViewOperationException e) {
            successCallback.invoke(e.getMessage());
        }
    }

    public void setHandler(Handler handler) {
        this.mHandler = handler;
    }
    @ReactMethod
    public void SetInputModeForWebView() {
        MainActivity.getInstance().SetInputModeForWebView();
    }

    @ReactMethod
    public void SetInputModeForTabBar() {
        MainActivity.getInstance().SetInputModeForTabBar();
    }

    @ReactMethod
    public void getCurrentLanguage(final Callback callback){
        /**
         * 获得当前系统语言
         */
        String mCurrentLanguage = Locale.getDefault().getLanguage();
        Log.e("BleApplication```2```",mCurrentLanguage);
        callback.invoke(mCurrentLanguage);
    }
}
