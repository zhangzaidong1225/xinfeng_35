package com.lianluo.qingfeng.mvc.BleManager.BleCmd;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by zhangzaidong on 2016/9/13.
 */
public class DevMsgManager {

    public String address;

    public ConcurrentLinkedQueue<DevMsg> devMsgs = new ConcurrentLinkedQueue<>();

    public DevMsgManager(String address){
        this.address = address;
    }

    public void addOneMsg(String address,String name,int action ,int speed){
        devMsgs.add(createDevMsg(address,name,action,speed));
    }

    public DevMsg peekOneMsg (){
        return devMsgs.peek();
    }

    public DevMsg pollOneMsg(){
        return devMsgs.poll();
    }

    public void clearMsg(){
        devMsgs.clear();
    }

    public boolean isEmpty() {
        return devMsgs.isEmpty();
    }

    public DevMsg createDevMsg(String address,String name,int action,int speed){
        return new DevMsg(address,name,action,speed);
    }
}
