package com.lianluo.qingfeng.mvc.BleManager.BleCmd;

import android.util.Log;

import com.lianluo.qingfeng.mvc.BleManager.BleManager;

/**
 * Created by zhangzaidong on 2016/9/13.
 * 协议
 */
public class BleCmd {

    public static volatile BleCmd mInstance;

    private BleCmd(){

    }

    public static BleCmd getInstance() {
        if (mInstance == null) {
            synchronized (BleCmd.class){
                if (null == mInstance){
                    mInstance = new BleCmd();
                }
            }
        }
        return mInstance;
    }

    /**
     * 调整风速
     *
     * @param address
     * @param speed
     */
    public void windSpeed(String address, int speed) {

        byte head = (byte) 0xAA;
        byte command = 0x10;
        byte sp = (byte) speed;
        byte time = 0;
        byte check = (byte) (head + command + sp + time);
        byte[] data = new byte[]{head, command, sp, time, check};

        BleManager.getInstance().uartService.writeRXCharacteristic(address, data);

    }

    /**
     * 查找设备
     *
     * @param address
     */
    public void devCheck(String address) {
        byte head = (byte) 0xAA;
        byte command = 0x13;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

        BleManager.getInstance().uartService.writeRXCharacteristic(address, data);

    }

    /**
     * 重命名
     * @param address
     * @param rename
     */
    public void devRename(String address, String rename) {
        //最大为4个汉字，16字节
        byte head = (byte) 0xAA;
        byte command = 0x12;
        byte[] buf1 = new byte[16];
        byte[] buf_name = new byte[30];
        int len = rename.length();
        int byteLen = rename.getBytes().length;

        int nameLen = (len > 4) ? 4:len;
        int byteNameLen = byteLen > 12 ? 12:byteLen;

        try {
            int buf_length = 0;
            for (int i = 0; i < nameLen; i++) {
                String tmp = rename.substring(i, i + 1);
                byte[] tmp_buf = tmp.getBytes("UTF8");
                if (buf_length + tmp_buf.length <= 30) {
                    for (int j = 0; j < tmp_buf.length; j++) {
                        buf_name[buf_length] = tmp_buf[j];
                        buf_length++;
                    }
                }
            }
            for (int i = 0; i < 30; i++) {
                if (i < 12) {
                    buf1[i + 3] = buf_name[i];
                }
            }
        } catch (Exception e) {
        }
        buf1[0] = head;
        buf1[1] = command;
        buf1[2] = (byte) byteNameLen;

        //校验位
        for (int i = 0; i < 15; i++) {
            buf1[15] += buf1[i];
        }
        BleManager.getInstance().uartService.writeRXCharacteristic(address, buf1);

    }

    /**
     * 断开连接
     *
     * @param address
     */
    public void disconnectConn(String address) {
        byte head = (byte) 0xAA;
        byte command = 0x07;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

        BleManager.getInstance().uartService.writeRXCharacteristic(address, data);
    }

    /**
     * 关机
     *
     * @param address
     */
    public void shutDown(String address) {

        byte head = (byte) 0xAA;
        byte command = 0x01;
        byte check = (byte) (head + command);
        byte[] data = new byte[]{head, command, check};

        BleManager.getInstance().uartService.writeRXCharacteristic(address, data);
    }

    /**
     * 获得名字
     * @param address
     */
    public void getName(String address){

        byte head = (byte) 0xAA;
        byte command = 0x14;
        byte length = 0;
        byte name = 0;
        byte check = (byte) (head + command + length + name);
        byte[] data = new byte[]{head, command, length,name, check};

        BleManager.getInstance().uartService.writeRXCharacteristic(address, data);
    }
}
