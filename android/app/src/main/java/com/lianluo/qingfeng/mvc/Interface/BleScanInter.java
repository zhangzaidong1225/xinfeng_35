package com.lianluo.qingfeng.mvc.Interface;

import android.bluetooth.BluetoothDevice;

/**
 * Created by zhangzaidong on 2016/9/12.
 * 负责蓝牙搜索数据
 */
public interface BleScanInter {

    void scanCallback(BluetoothDevice device);
    void startScanCallback(boolean status,String action);
    void stopScanCallback(boolean status,String action);
    void clearScanCallback(String action);



}
