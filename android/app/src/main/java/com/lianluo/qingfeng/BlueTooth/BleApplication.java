package com.lianluo.qingfeng.BlueTooth;

import android.app.Application;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import com.lianluo.qingfeng.MyReactPackage;
import com.lianluo.qingfeng.mvc.View.BluetoothUtil;
import com.lianluo.qingfeng.mvc.View.BluetoothPackage;
import com.lianluo.qingfeng.reactmodule.ShareModule;
import com.rnfs.BuildConfig;
import com.rnfs.RNFSPackage;
import com.zyu.ReactNativeWheelPickerPackage;
import com.umeng.analytics.MobclickAgent;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

public class BleApplication extends Application implements ReactApplication {

    private static final String TAG = "BleApplication";
    public static BluetoothUtil bluetoothUtil;
    public static ShareModule shareModule;

    public static BleApplication INSTANCE;

    public BleApplication() {
        INSTANCE = this;
    }

    public static BleApplication getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new BleApplication();
        }
        return INSTANCE;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
//        MobclickAgent.openActivityDurationTrack(false);
//        MobclickAgent.setDebugMode( true );
        MobclickAgent.enableEncrypt(true);//6.0.0版本及以后

    }
    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        protected boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new MyReactPackage(),
                    new BluetoothPackage(),
                    new RNFSPackage(),
                    new ReactNativeWheelPickerPackage()
            );
        }

        @Override
        public ReactInstanceManager getReactInstanceManager() {
            Log.e("getReactInstanceManager","进入getReactInstanceManager");
            return super.getReactInstanceManager();
        }

        @Nullable
        @Override
        protected String getJSBundleFile() {
            return super.getJSBundleFile();
//            String jsBundleFile = getFilesDir().getAbsolutePath() + "/index.android.bundle";
//            Log.e(TAG,"zip---"+jsBundleFile);
//            String jsBundle = getFilesDir().getAbsolutePath() + "/hotupload/bundle/index.android.bundle";
//
//            Log.e(TAG,"zip---"+jsBundle);
//            File file = new File(jsBundle);
//            Log.e(TAG,"zip---"+file.exists());
//            return file != null && file.exists() ? jsBundle : null;
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }
}
