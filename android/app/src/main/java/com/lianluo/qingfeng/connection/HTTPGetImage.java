package com.lianluo.qingfeng.connection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import org.apache.http.conn.scheme.Scheme;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

/**
 * Get Image from net by htt get method.
 *
 * Created by louis on 16/4/22.
 */
public class HTTPGetImage {

    public static String TAG = "HTTPGetImage";
    private static final HTTPGetImage sInstance = new HTTPGetImage();

    public static HTTPGetImage getInstance() {
        return sInstance;
    }

    private class GetMessage {
        String url;
        String param;

        String opTag;
        Handler handler;
        Context mContext;

        GetMessage(Context mContext,String url, String param, Handler handler, String opTag){
            this.url = url;
            this.param = param;

            this.handler = handler;
            this.opTag = opTag;
            this.mContext = mContext;
        }
    }

    public void getAsync(Context mContext,String url, String param, Handler handler, String op_tag){
        new HttpGetTask().execute(new GetMessage(mContext,url, param, handler, op_tag));
    }

    private class HttpGetTask extends AsyncTask<GetMessage, Void, byte[]> {
        Handler handler;
        String opTag;
        Context mContext;
        @Override
        protected byte[] doInBackground(GetMessage... messages) {

            GetMessage gm = messages[0];
            this.handler = gm.handler;
            this.opTag = gm.opTag;
            this.mContext = gm.mContext;
            byte[] data = null;
            try {
                data = getImage(gm.url + gm.param);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(byte[] result) {
            if(this.handler != null){
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString(HttpConstant.HTTP_OPR, this.opTag);
                bundle.putByteArray(HttpConstant.HTTP_RESULT, result);
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        }

        /**
         * Get image from newwork
         * @param path The path of image
         * @return byte[]
         * @throws Exception
         */
        public byte[] getImage(String path) throws Exception{
            //try {

                // Load CAs from an InputStream
                // (could be from a resource or ByteArrayInputStream or ...)

                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                // From
                // https://www.washington.edu/itconnect/security/ca/load-der.crt
                InputStream caInput = mContext.getAssets().open("_.lianluo.com_bundle.crt");
                //InputStream caInput = mContext.getAssets().open("nginx.crt");

                Certificate ca;
                try {
                    ca = cf.generateCertificate(caInput);
                } finally {
                    caInput.close();
                }

                // Create a KeyStore containing our trusted CAs
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);

                // Create a TrustManager that trusts the CAs in our KeyStore
                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory
                        .getInstance(tmfAlgorithm);
                tmf.init(keyStore);

                // Create an SSLContext that uses our TrustManager
                SSLContext context = SSLContext.getInstance("TLS");
                context.init(null, tmf.getTrustManagers(), null);

                URL url = new URL(path);
                HttpsURLConnection urlConnection = (HttpsURLConnection) url
                        .openConnection();
                //
                urlConnection.setConnectTimeout(5 * 1000);
                urlConnection.setRequestMethod("GET");
                //
                urlConnection.setSSLSocketFactory(context.getSocketFactory());

                InputStream in = urlConnection.getInputStream();

                if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    return readStream(in);
                }
                return null;

//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (NoSuchAlgorithmException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (KeyManagementException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (KeyStoreException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (CertificateException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }




//            URL url = new URL(path);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setConnectTimeout(5 * 1000);
//            conn.setRequestMethod("GET");
//            InputStream inStream = conn.getInputStream();
//            if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
//                return readStream(inStream);
//            }
//            return null;
        }

        /**
         * Get data from stream
         * @param inStream
         * @return byte[]
         * @throws Exception
         */
        public byte[] readStream(InputStream inStream) throws Exception{
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while( (len=inStream.read(buffer)) != -1){
                outStream.write(buffer, 0, len);
            }
            outStream.close();
            inStream.close();
            return outStream.toByteArray();
        }


        /**
         * Get image from newwork
         * @param path The path of image
         * @return InputStream
         * @throws Exception
         */
//        public InputStream getImageStream(String path) throws Exception{
//            URL url = new URL(path);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setConnectTimeout(5 * 1000);
//            conn.setRequestMethod("GET");
//            if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
//                return conn.getInputStream();
//            }
//            return null;
//        }
    }
}
