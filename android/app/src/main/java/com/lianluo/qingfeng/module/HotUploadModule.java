package com.lianluo.qingfeng.module;

/**
 * Created by Administrator on 2017/1/17.
 */

public class HotUploadModule {

    private String code;
    private String msg;
    private HotUploadDataModule data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public HotUploadDataModule getData() {
        return data;
    }

    public void setData(HotUploadDataModule data) {
        this.data = data;
    }
}
