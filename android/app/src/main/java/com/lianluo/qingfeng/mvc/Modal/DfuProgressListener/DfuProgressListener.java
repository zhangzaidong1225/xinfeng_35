package com.lianluo.qingfeng.mvc.Modal.DfuProgressListener;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.lianluo.qingfeng.MainActivity;
import com.lianluo.qingfeng.mvc.BleManager.UartService.UartService;
import com.lianluo.qingfeng.mvc.Modal.Bean.XinfengDev;
import com.lianluo.qingfeng.mvc.Modal.BleDevManager;
import com.lianluo.qingfeng.mvc.Modal.Update.DfuService;

import no.nordicsemi.android.dfu.DfuProgressListenerAdapter;

/**
 * Created by zhangzaidong on 2016/9/18.
 */
public class DfuProgressListener extends DfuProgressListenerAdapter {

    @Override
    public void onDeviceConnecting(String deviceAddress) {
        super.onDeviceConnecting(deviceAddress);
        Log.e("change,", "onDeviceConnecting");
    }

    @Override
    public void onDeviceConnected(String deviceAddress) {
        super.onDeviceConnected(deviceAddress);
    }

    @Override
    public void onDfuProcessStarting(String deviceAddress) {
        super.onDfuProcessStarting(deviceAddress);
        Log.e("change", "onDfuProcessStarting");
        Object bleObj = BleDevManager.getInstance().mData.get(deviceAddress);
        final XinfengDev device = (XinfengDev) bleObj;

        device.setUpdate_state(104);
        BleDevManager.getInstance().sendData(UartService.ACTION_DFU_UPDATE,deviceAddress);
        BleDevManager.getInstance().isUpdating = true;
    }

    @Override
    public void onDfuProcessStarted(String deviceAddress) {
        super.onDfuProcessStarted(deviceAddress);
    }

    @Override
    public void onProgressChanged(String deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal) {
        super.onProgressChanged(deviceAddress, percent, speed, avgSpeed, currentPart, partsTotal);
        Log.e("change", "onProgressChanged" + "   " + percent);
        Object bleObj = BleDevManager.getInstance().mData.get(deviceAddress);
        final XinfengDev device = (XinfengDev) bleObj;
        BleDevManager.getInstance().isUpdating = true;
        device.setPercentage(percent);
        device.setUpdate_state(103);
        BleDevManager.getInstance().sendData(UartService.ACTION_DFU_UPDATE,deviceAddress);
    }

    @Override
    public void onDeviceDisconnecting(String deviceAddress) {
        super.onDeviceDisconnecting(deviceAddress);
        Log.e("change", "onDeviceDisconnecting.....");

        Object bleObj = BleDevManager.getInstance().mData.get(deviceAddress);
        final XinfengDev device = (XinfengDev) bleObj;
        BleDevManager.getInstance().isUpdating = false;

        if (device.getUpdate_state() == 104){
            device.setUpdate_state(102);
            device.setPercentage(0);
            BleDevManager.getInstance().sendData(UartService.ACTION_DFU_UPDATE,deviceAddress);
            //device.setUpdate_state(0);

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    // if this activity is still open and upload process was completed, cancel the notification
                    final NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.cancel(DfuService.NOTIFICATION_ID);
                }
            },200);
        }
    }

    @Override
    public void onDeviceDisconnected(String deviceAddress) {
        super.onDeviceDisconnected(deviceAddress);
    }

    @Override
    public void onDfuCompleted(String deviceAddress) {
        super.onDfuCompleted(deviceAddress);
        Log.e("change", "成功了");
        Object bleObj = BleDevManager.getInstance().mData.get(deviceAddress);
        final XinfengDev device = (XinfengDev) bleObj;
        BleDevManager.getInstance().isUpdating = false;
        device.setPercentage(0);
        device.setUpdate_state(101);
        device.setVersion(0);
        BleDevManager.getInstance().sendData(UartService.ACTION_DFU_UPDATE,deviceAddress);


        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                // if this activity is still open and upload process was completed, cancel the notification
                device.setUpdate_state(0);
                final NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(DfuService.NOTIFICATION_ID);
            }
        },200);

    }

    @Override
    public void onError(String deviceAddress, int error, int errorType, String message) {
        super.onError(deviceAddress, error, errorType, message);
        Log.e("change", "失败了");
        Object bleObj = BleDevManager.getInstance().mData.get(deviceAddress);
         final XinfengDev device = (XinfengDev) bleObj;
        BleDevManager.getInstance().isUpdating = false;
        device.setUpdate_state(102);
        //清零
        device.setPercentage(0);
        BleDevManager.getInstance().sendData(UartService.ACTION_DFU_UPDATE,deviceAddress);


        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                // if this activity is still open and upload process was completed, cancel the notification
                device.setUpdate_state(0);
                final NotificationManager manager = (NotificationManager) MainActivity.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(DfuService.NOTIFICATION_ID);
            }
        },200);


    }
}
