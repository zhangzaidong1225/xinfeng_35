package com.lianluo.qingfeng;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.lianluo.qingfeng.BlueTooth.BleApplication;
import com.lianluo.qingfeng.mvc.BleManager.BleCmd.BleCmdManager;
import com.lianluo.qingfeng.mvc.BleManager.BleManager;
import com.lianluo.qingfeng.mvc.BleManager.Scan.ScanDevManager;
import com.lianluo.qingfeng.reactmodule.ShareModule;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;

import ly.count.android.sdk.Countly;
import ly.count.android.sdk.DeviceId;

import static java.security.AccessController.getContext;


public class MainActivity extends ReactActivity {

    public static MainActivity mContext;
    private String address = null;
    public static Handler mHandler = new Handler();
    public static final String TAG = "==MainActivity";
    private int mState = UART_PROFILE_DISCONNECTED;
    private static final int UART_PROFILE_CONNECTED = 20;
    private static final int UART_PROFILE_DISCONNECTED = 21;
    //    private GoogleApiClient client;
//    public static DeviceMap deviceMap = new DeviceMap();
//    public static XinfengService mService = null;

    private static final int REQUEST_FINE_LOCATION = 0;
    private static final int REQUEST_FINE_LOCATION_ENABLE = 1;
    private static final int REQUEST_WRITE = 2;
    private static final int INIT_REQUEST_WRITE = 3;

    //public static final ReactInfo reactInfo = new ReactInfo("feibao", null);
    public  BleManager bleManager = null;


    public MyHandler mMyHandler = new MyHandler(MainActivity.this);


    class MyHandler extends Handler {
        WeakReference<Activity> mActivityReference;

        MyHandler(Activity activity) {
            mActivityReference = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final Activity activity = mActivityReference.get();
            if (activity != null) {
                Bundle bundle = msg.getData();
                if (bundle == null) {
                    return;
                }
                String opTag = bundle.getString("InputModeForWhich");
                if (opTag == null) {
                    return;
                }

                if (opTag.equals("WebView")) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    return;
                }

                if (opTag.equals("TabBar")) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED);
                    return;
                }

            }
        }
    }

    public static MainActivity INSTANCE;

    public MainActivity() {
        INSTANCE = this;
    }

    public static MainActivity getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MainActivity();
        }
        return INSTANCE;
    }


    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
//    @Override
//    protected String getMainComponentName() {
//        return reactInfo.getMainComponentName();
//    }

//    @Override
//    public ReactInfo getReactInfo() {
//        return reactInfo;
//    }
    @Override
    protected String getMainComponentName() {
        return "feibao";
    }
    /**
     * Returns whether dev mode should be enabled.
     * This enables e.g. the dev menu.
     */
//    @Override
//    protected boolean getUseDeveloperSupport() {
//        return BuildConfig.DEBUG;
//    }

    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
//    @Override
//    protected List<ReactPackage> getPackages() {
//        mContext = this;
//        Log.e(TAG, "getPackages().....");
//        return Arrays.<ReactPackage>asList(
//                new MainReactPackage(),
//                new MyReactPackage1(),
//                new BlueToothtPackage(),
//                new RNFSPackage()
//        );
//    }


    public void requestBluetooth(int requestCode, String address) {
        this.address = address;
        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), requestCode);
    }

//    @Override
//    protected ReactInstanceManager createReactInstanceManager() {
//        return ReactInstanceManagerHelper.getInstance(MainActivity.this, getApplication()).getReactInstanceManager();
//    }
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate().....");
        Log.e("====getContext() 1  ",getContext()+"");
        mContext = this;

        initErrorHandler();
        //service_init();

        IntentFilter bluetoothFilter = new IntentFilter();
        bluetoothFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        bluetoothFilter.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(BluetoothReciever, bluetoothFilter);

        Countly.sharedInstance().init(this, "https://mops-countly.lianluo.com", "d4ea5db8a8cd37c301910c63cb969019ddb9d36b",null, DeviceId.Type.OPEN_UDID);
        Countly.sharedInstance().setViewTracking(true);
        Countly.sharedInstance().enableCrashReporting();

        bleManager = BleManager.getInstance();
        bleManager.initBlue(MainActivity.mContext);
        bleManager.init(MainActivity.mContext);

        //请求存储权限
        mayWritePermissioninit();

    }

    private BroadcastReceiver BluetoothReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
                int btState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.STATE_OFF);

                printBTState(btState);
            } else if (BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(intent.getAction())) {
                int cur_mode_state = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, BluetoothAdapter.SCAN_MODE_NONE);
                int previous_mode_state = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE, BluetoothAdapter.SCAN_MODE_NONE);

                Log.e(TAG, "### cur_mode_state ##" + cur_mode_state + " ~~ previous_mode_state" + previous_mode_state);

            }
        }

    };

    private void printBTState(int btState) {
        switch (btState) {
            case BluetoothAdapter.STATE_OFF:
//                Log.e(TAG, "BT State : BluetoothAdapter.STATE_OFF ###");
                BleManager.getInstance().uartService.close();

                break;
            case BluetoothAdapter.STATE_TURNING_OFF:

//                Log.e(TAG, "BT State :  BluetoothAdapter.STATE_TURNING_OFF ###");
                break;
            case BluetoothAdapter.STATE_TURNING_ON:
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mService.initialize();
//                    }
//                },300);

                //mService.initialize();
                BleManager.getInstance().initBlue(MainActivity.this);

               // Log.e(TAG, "BT State :service初始化....");
               // Log.e(TAG, "BT State :BluetoothAdapter.STATE_TURNING_ON ###");
                break;
            case BluetoothAdapter.STATE_ON:

                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //修改延迟执行的方式
/*                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mService.auto_Scanner();
                    }
                }, 500);*/

                //mService.auto_Scanner();
                ScanDevManager.getInstance().autoScan();

                Log.e(TAG, "BT State :BluetoothAdapter.STATE_ON ###");
                break;
            default:
                break;
        }
    }


//    private void service_init() {
//        Log.e(TAG, "Running.....service_init");
//        Intent bindIntent = new Intent(this, XinfengService.class);
//        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
//    }

    private void initErrorHandler() {
        /*
         * if (BuildConfig.DEBUG) { StrictMode.setThreadPolicy(new
         * StrictMode.ThreadPolicy.Builder() .detectDiskReads()
         * .detectDiskWrites() .detectNetwork() // or .detectAll() for all
         * detectable problems .penaltyLog() .build());
         * StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
         * .detectLeakedSqlLiteObjects() .detectLeakedClosableObjects()
         * .penaltyLog() .penaltyDeath() .build()); return; }
         */
        CrashHandler handler = CrashHandler.getInstance();
        handler.init(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        MobclickAgent.onResume(this);

        Resources res = getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

//    private ServiceConnection mServiceConnection = new ServiceConnection() {
//        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
//            //mService = ((UartService.LocalBinder) rawBinder).getService();
//            Log.e(TAG, "onServiceConnected mService= ");
//
//            mService = ((XinfengService.XinfengLocalBinder) rawBinder).getService();
//
//            mService.InitData();
//            Log.e(TAG, "onServiceConnected mService= " + mService);
//            if (!mService.initialize()) {
//                Log.e(TAG, "Unable to initialize Bluetooth");
//                finish();
//            }
//        }
//
//        public void onServiceDisconnected(ComponentName classname) {
//            ////     mService.disconnect(mDevice);
//            Log.e(TAG, "onServiceDisconnected mService= " + mService);
//            mService = null;
//        }
//    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BleCmdManager.getInstance().stopThread();

        unregisterReceiver(BluetoothReciever);
        try {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(BleApplication.bluetoothUtil.UartStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(TAG, ignore.toString());
        }

        unbindService(bleManager.mServiceConnection);

        bleManager.uartService.stopSelf();
        bleManager.uartService = null;

        //mService.stopSelf();
        //mService = null;


        if (UpdateService.mStartd) {
            Log.e("UpdateService", "+++++++++++++++++++++++++++++++" + UpdateService.mStartd);
            UpdateService.stop(this);
            //stopService(ShareModule.intent);
            //UpdateService.notificationManager.cancelAll();
            // this.stopService(new Intent(this, UpdateService.class));
        }
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
        }

        MobclickAgent.onKillProcess(this);
        System.exit(0);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == BleManager.REQUEST_SCAN) {
                //动态申请蓝牙权限
                mayRequestLocation();

            }
            if (requestCode == BleManager.REQUEST_CONNECT) {
                Log.e("请求蓝牙", "蓝牙已打开");
                if (this.address != null) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
//                    BleApplication.bluetoothUtil.connectDevice(this.address, true);
                    BleManager.getInstance().connectDevice(this.address);

                }

            }
        }
    }

    public void mayWritePermissioninit() {

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                //已经禁止提示了
                Toast.makeText(MainActivity.this, "您已禁止存储权限，需要重新开启。", Toast.LENGTH_SHORT).show();
            }else{
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, INIT_REQUEST_WRITE);
            }
        }
    }


    /**
     * 申请读写权限
     */
    public void mayWritePermission() {

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                //已经禁止提示了
                Toast.makeText(MainActivity.this, "您已禁止存储权限，需要重新开启。", Toast.LENGTH_SHORT).show();
            }else{
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE);
            }
        }
    }


    /**
     * 在蓝牙未打开的情况下请求权限
     * 申请GPS权限
     */

    private void mayRequestLocation() {
        Log.e(TAG, "mayRequestLocation----");

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,Manifest.permission.ACCESS_COARSE_LOCATION)){
                //已经禁止提示了
                Toast.makeText(MainActivity.this, "您已禁止GPS权限，需要重新开启。", Toast.LENGTH_SHORT).show();
            }else{
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_FINE_LOCATION);
            }
        } else {
//            mService.scanner();
            ScanDevManager.getInstance().scan();
        }

    }

    /**
     * 在蓝牙打开的情况下请求权限
     * 申请GPS权限
     */
    public void enableRequestLocation() {
        Log.e(TAG, "enableRequestLocation");

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_FINE_LOCATION_ENABLE);

        } else {
//            mService.scanner();
            ScanDevManager.getInstance().scan();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    mService.scanner();
                    ScanDevManager.getInstance().scan();
                } else {
                    //用户拒绝授权
                    Toast.makeText(MainActivity.this, "GPS权限被拒绝，请手动开启权限", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_FINE_LOCATION_ENABLE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    mService.scanner();
                    ScanDevManager.getInstance().scan();
                }else {
                    //用户拒绝授权
                    Toast.makeText(MainActivity.this, "GPS权限被拒绝，请手动开启权限", Toast.LENGTH_SHORT).show();
                }
                break;

            case REQUEST_WRITE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    BleApplication.shareModule.share(ShareModule.shareData.title,ShareModule.shareData.url,ShareModule.shareData.content,
                            ShareModule.shareData.imagePath,ShareModule.shareData.page,
                            ShareModule.shareData.filtration,ShareModule.shareData.time,
                            ShareModule.shareData.filternum,ShareModule.shareData.array);
                } else {
                    Toast.makeText(MainActivity.this, "存储权限被拒绝，请手动开启权限", Toast.LENGTH_SHORT).show();
                }

                break;
            case INIT_REQUEST_WRITE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG,"给予存储权限");
                } else {
                    Toast.makeText(MainActivity.this, "存储权限被拒绝，请手动开启权限", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

    }


    private void goSplashActivity() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), SplashActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Countly.sharedInstance().onStart(MainActivity.this);

    }

    @Override
    protected void onStop() {
        Countly.sharedInstance().onStop();
        super.onStop();

    }

    public void SetInputModeForWebView() {
        Message msg = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("InputModeForWhich", "WebView");
        msg.setData(bundle);
        mMyHandler.sendMessage(msg);
    }

    public void SetInputModeForTabBar() {
        Message msg = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("InputModeForWhich", "TabBar");
        msg.setData(bundle);
        mMyHandler.sendMessage(msg);
    }


}
