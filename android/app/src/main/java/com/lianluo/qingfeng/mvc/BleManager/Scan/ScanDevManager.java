package com.lianluo.qingfeng.mvc.BleManager.Scan;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.util.Log;

import com.lianluo.qingfeng.BlueTooth.BleApplication;
import com.lianluo.qingfeng.mvc.BleManager.BleManager;
import com.lianluo.qingfeng.mvc.BleManager.UartService.UartService;
import com.lianluo.qingfeng.mvc.Interface.BleScanInter;
import com.lianluo.qingfeng.mvc.Modal.Bean.XinfengDev;
import com.lianluo.qingfeng.mvc.Modal.BleDevManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

/**
 * Created by zhangzaidong on 2016/9/12.
 */
public class ScanDevManager {

    public static final String TAG = "ScanDevManager";

    public static volatile  ScanDevManager manager;

    private  BluetoothLeScannerCompat scanner, scannerAuto,scanConn;
    private ScanCallback scanCallback, scanCallbackAuto,scanCallbackConn;
    private static ParcelUuid mUuid;
    private final static String PARAM_UUID = "param_uuid";
    private final long SCAN_PERIOD = 5000; //scanning for 5 seconds

    private BleScanInter bleScanInter;
    public static boolean scan_state = false;

    public static ScanDevManager getInstance() {
        if (manager == null) {
            synchronized (ScanDevManager.class){
                if (null == manager){
                    manager = new ScanDevManager();
                }
            }

        }
        return manager;
    }

    public void setBleScanInter(BleScanInter bleScanInter){
        this.bleScanInter = bleScanInter;
    }


    /**
     * 针对忻风协议
     */
    public void scan() {

        scan_state = true;
        bleScanInter.startScanCallback(scan_state, UartService.START_SCAN);

        if (!BleDevManager.getInstance().mData.isEmpty()){
            bleScanInter.clearScanCallback(UartService.CLEAR_DATA_1);
        }


        scanner = BluetoothLeScannerCompat.getScanner();
        final ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(1000)
                .setUseHardwareBatchingIfSupported(false)
                .build();
        final List<ScanFilter> filters = new ArrayList<>();

        final Bundle args = new Bundle();
        mUuid = args.getParcelable(PARAM_UUID);

        scanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                super.onBatchScanResults(results);

                for (ScanResult result : results) {

                    List<ParcelUuid> uuids = result.getScanRecord().getServiceUuids();

                    byte[] manufactureData = result.getScanRecord().getManufacturerSpecificData(30326);
                    if (manufactureData != null && manufactureData[0] == 1) {
                        return;
                    }
                    byte[] adData = result.getScanRecord().getManufacturerSpecificData(0x7676);
                    int flags = result.getScanRecord().getAdvertiseFlags();
                    boolean isQingfeng = false;

                    if (uuids != null) {
                        if (uuids.size() > 0) {
                            String struuid = uuids.get(0).toString();
                            if (struuid.substring(0, 8).equals("00007676")) {
                                if (adData != null) {
                                    if (adData[0] == 0) {
                                        isQingfeng = true;
                                    }
                                } else {
                                    isQingfeng = true;
                                }
                            }
                        }
                    }

                    if (!isQingfeng) break;

                    if (isQingfeng){
                        bleScanInter.scanCallback(result.getDevice());
                    }

                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
            }
        };
        filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
        scanner.startScan(filters, settings, scanCallback);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (scanner != null){
                    Log.e(TAG, "Scanner---------STOP scanner---");
                    scanner.stopScan(scanCallback);
                    scanCallback = null;
                    scanner = null;
                }
            }
        }, 5000);

    }


    public void stopScan() {
        if (scanner != null){
            scanner.stopScan(scanCallback);
            scan_state = false;
            scanner = null;
            scanCallback = null;


            bleScanInter.stopScanCallback(scan_state,UartService.STOP_SCAN);
        }

    }

    /**
     * 针对蓝牙关闭在开启时执行自动搜索操作
     */
    public void autoScan() {

        scannerAuto = BluetoothLeScannerCompat.getScanner();
        final ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(1000).setUseHardwareBatchingIfSupported(false).build();
        final List<ScanFilter> filters = new ArrayList<>();

        final Bundle args = new Bundle();
        mUuid = args.getParcelable(PARAM_UUID);
        scanCallbackAuto = new ScanCallback() {
            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                //搜索到每个蓝牙对象是调用

                for (ScanResult result : results) {

                    Log.e(TAG, "搜索到蓝牙设备---auto_Scanner---" + result.getDevice().getName() + " " + result.getDevice().getAddress());

                    //Log.e("result", result + "");

                    //List<ParcelUuid> uuids = result.getScanRecord().getServiceUuids();

                    // Log.e("uuid", uuids + "");

//                    boolean isQingfeng = false;
//
//                    if (uuids != null) {
//                        if (uuids.size() > 0) {
//                            String struuid = uuids.get(0).toString();
//
//                            if (struuid.substring(0, 8).equals("00007676")) {
//                                isQingfeng = true;
//                            }
//                        }
//                    }
//
//                    if (!isQingfeng) break;

//                    boolean is_contain = false;
//                    for (String key : mData.keySet()) {
//                        if (result.getDevice().getAddress().equals(key)) {
//                            is_contain = true;
//                            Log.e(TAG, "IS_CONTAIN...");
//                            //break;
//                        }
//                        is_contain = false;
//                    }
                }
                super.onBatchScanResults(results);
            }
        };

        filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
        scannerAuto.startScan(filters, settings, scanCallbackAuto);


        autoStop();
    }

    public void autoStop() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (scannerAuto != null){
                    scannerAuto.stopScan(scanCallbackAuto);
                    scan_state = false;
                    scannerAuto = null;
                    scanCallbackAuto = null;
                    bleScanInter.stopScanCallback(scan_state,UartService.STOP_SCAN);
                }

            }
        };
        timer.schedule(task, SCAN_PERIOD);

    }


    /**
     * 自动连接时，扫描搜索，执行连接
     * @param connecting
     */
    public void scanAddressAndConnect(final boolean connecting){
        Log.e(TAG, "scanAddressAndConnect---------");
        scanConn = BluetoothLeScannerCompat.getScanner();
        final ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(1000).setUseHardwareBatchingIfSupported(false).build();
        final List<ScanFilter> filters = new ArrayList<>();

        final Bundle args = new Bundle();
        mUuid = args.getParcelable(PARAM_UUID);


        scanCallbackConn = new ScanCallback() {
            @Override
            public void onBatchScanResults(List<ScanResult> results) {

                for (ScanResult result : results) {

                    //Log.e(TAG, "搜索到蓝牙设备---scanAddressAndConnect---" + result.getDevice().getName() + " " + result.getDevice().getAddress());

                    for (String key : BleDevManager.getInstance().mData.keySet()) {
                        if (result.getDevice().getAddress().equals(key)) {
                            XinfengDev device = BleDevManager.getInstance().mData.get(key);
                            if (device.ismatched()){
                                if ((device.getIsConnected() == 0) && (device.isAutoConnect())){

                                    Log.e(TAG, "Scanner---------Connect---");
                                    if (scanConn != null){
                                        scanConn.stopScan(scanCallbackConn);
                                        scanConn = null;
                                        scanCallbackConn = null;
                                    }
                                    if (UartService.mBluetoothGattMap.isEmpty()){
                                        BleDevManager.getInstance().connect(key, connecting);
                                    }

                                    if (!UartService.mBluetoothGattMap.containsKey(key)){
                                        BleDevManager.getInstance().connect(key, connecting);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                super.onBatchScanResults(results);
            }
        };
        filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
        Log.e(TAG,"scanConn: "+scanConn + "``" + "scanCallbackConn: " + scanCallbackConn);

        if (scanConn != null && scanCallbackConn != null) {
            scanConn.startScan(filters, settings, scanCallbackConn);
        }


        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                //Log.e(TAG, "auto_Scanner---------STOP scanner---");
                if (scanConn != null){
                    Log.e(TAG, "scanAddressAndConnect---------STOP scanner---");
                    scanConn.stopScan(scanCallbackConn);
                    scanConn = null;
                    scanCallbackConn = null;
                }

            }
        }, 3500);
//
/*        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                if (scanConn != null){
                    Log.e(TAG, "scanAddressAndConnect---------STOP scanner---");
                    scanConn.stopScan(scanCallbackConn);
                    scanConn = null;
                    scanCallbackConn = null;
                }
            }
        };
        timer.schedule(task, 5000);*/
    }
}
