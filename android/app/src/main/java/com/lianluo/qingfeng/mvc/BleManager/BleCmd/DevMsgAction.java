package com.lianluo.qingfeng.mvc.BleManager.BleCmd;

/**
 * Created by zhangzaidong on 2016/9/13.
 * 通讯协议
 */
public class DevMsgAction {

    public static final int WindSpeed = 0x01;
    public static final int DevCheck = 0x02;
    public static final int DevRename = 0x03;
    public static final int Disconnect = 0x04;
    public static final int ShutDown = 0x05;
    public static final int GetName = 0x06;
    

}
