package com.lianluo.qingfeng.reactmodule;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

import ly.count.android.sdk.Countly;

/**
 * Created by louis on 16/6/1.
 */
public class RCTUMengModule extends ReactContextBaseJavaModule {
    public RCTUMengModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "RCTUMengModule";
    }

    @ReactMethod
    public void onProfileSignIn(String userId) {
        MobclickAgent.onProfileSignIn(userId);

        HashMap<String,String> userData = new HashMap<String,String>();
        userData.put("name",userId);
        userData.put("phone",userId);
        Countly.userData.setUserData(userData);

    }

    @ReactMethod
    public void onProfileSignOff() {
        MobclickAgent.onProfileSignOff();
    }

    @ReactMethod
    public void onPageStart(String pageName) {
        MobclickAgent.onPageStart(pageName);
    }

    @ReactMethod
    public void onPageEnd(String pageName) {
        MobclickAgent.onPageEnd(pageName);
    }

    @ReactMethod
    public void onEvent(String eventId) {
        MobclickAgent.onEvent(getReactApplicationContext().getApplicationContext(), eventId);
        Countly.sharedInstance().recordEvent(eventId,1);
    }

    @ReactMethod
    public void onEventWithValue(String eventId, String key, String value) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(key, value);

        MobclickAgent.onEvent(getReactApplicationContext().getApplicationContext(), eventId, map);
        Countly.sharedInstance().recordEvent(eventId,map,1);

    }

}
