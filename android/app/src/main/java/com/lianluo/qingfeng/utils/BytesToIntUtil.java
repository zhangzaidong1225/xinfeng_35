package com.lianluo.qingfeng.utils;

import android.util.Log;

/**
 * Created by louis on 16/6/16.
 */
public class BytesToIntUtil {
    public static int hexArrayToInt(byte byte1){
        return 0x000000FF & ((int) byte1);
    }

    public static int hexArrayToInt(byte byteh, byte bytel) {

        int inth = hexArrayToInt(byteh);//(0x000000FF & ((int) byteh));
        int intl = hexArrayToInt(bytel);//(0x000000FF & ((int) bytel));

        return ((int) (inth << 8 | intl)) & 0xFFFFFFFF;
    }

    public static int hexArrayToInt(byte bytehh, byte byteh, byte bytel, byte bytell) {
        int inthh = hexArrayToInt(bytehh);
        int inth = hexArrayToInt(byteh);
        int intl = hexArrayToInt(bytel);
        int intll = hexArrayToInt(bytell);
        return (((int) (intll | intl << 8 | inth << 16 | inthh << 24)) & 0xFFFFFFFF);
    }

    /**
     * 将指定字符串src，以每两个字符分割转换为16进制形式
     * 如："2B44EFD9" --> byte[]{0x2B, 0x44, 0xEF, 0xD9}
     * @param src String
     * @return byte[]
     */
    public static byte[] HexString2Bytes(String src){
        byte[] ret = new byte[src.length()/2];
        byte[] tmp = src.getBytes();
        String[] hexArray = new String[4];
        int buf_length = 0;
        byte[] buf_name = new byte[30];
        for (int i = 0;i< src.length()/2;i++){
            ret[i] = (uniteBytes(tmp[i*2],tmp[i*2 +1]));

            String hex = Integer.toHexString(ret[i] & 0xFF);
            Log.e("MainActivity","-HexString2Bytes```"+hex+"``"+ret[i]);

            hexArray[i] = hex;
            byte[] tmp_buf = hex.getBytes();

            if (buf_length + tmp_buf.length <= 30) {
                for (int j = 0; j < tmp_buf.length; j++) {
                    buf_name[buf_length] = tmp_buf[j];
                    //Log.e("MainActivity", "devRename--" + String.format("%x", buf_name[buf_length]));
                    buf_length++;
                }
            }
            //Log.e("MainActivity","-HexString2Bytes-"+hexArray[i]);
            // Log.e("MainActivity","-HexString2Bytes-"+hex);
            // Log.e("MainActivity","-HexString2Bytes-"+ret[i]);
        }
        return ret;
    }
    /**
     * 将两个ASCII字符合成一个字节；
     * 如："EF"--> 0xEF
     * @param src0 byte
     * @param src1 byte
     * @return byte
     */
    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}));
        _b0 = (byte)(_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}));
        byte ret = (byte)(_b0 | _b1);
        return ret;
    }


    /*
     * byte数据转整形
     *
     * @param byte[] src 要转换的整形
     *
     * @return int result
     */
    public static int ByteArrayToInt(byte[] src)
    {
        int result = 0;
        int num = 0;
        int[] sb = new int[src.length];
        for (int i = 0; i < src.length; i++)
        {
            if (src[i] < 0)
            {
                sb[i] = src[i] + 256;
            } else
            {
                sb[i] = (int) src[i];
            }
            num = sb[i];
            result = result + num * ((int) Math.pow(256, src.length - i - 1));
        }
        return result;
    }
    /*
     * short转字节数组
     *
     * @param short src
     *
     * @return byte[] result
     */
    public static byte[] shortToByteArray(short src)
    {
        byte[] result = new byte[2];
        for (int i = 0; i < 2; i++)
        {
            int offset = (result.length - 1 - i) * 8;
            result[1 - i] = (byte) ((src >>> offset) & 0xff);
        }
        return result;
    }

    /*
     * int转字节数组
     *
     * @param int src
     *
     * @return byte[] result
     */
    public static byte[] intToByteArray(int src)
    {
        byte[] result = new byte[4];
        for (int i = 0; i < 4; i++)
        {
            int offset = (result.length - 1 - i) * 8;
            result[3 - i] = (byte) ((src >>> offset) & 0xff);
        }
        return result;
    }

    //小端
    /**
     * int to byte[] 支持 1或者 4 个字节
     * @param i
     * @param len
     * @return
     */
    public static byte[] intToByte(int i,int len) {
        byte[] abyte=null;
        if(len==1){
            abyte = new byte[len];
            abyte[0] = (byte) (0xff & i);
        }else{
            abyte = new byte[len];
            abyte[0] = (byte) (0xff & i);
            abyte[1] = (byte) ((0xff00 & i) >> 8);
            abyte[2] = (byte) ((0xff0000 & i) >> 16);
            abyte[3] = (byte) ((0xff000000 & i) >> 24);
        }
        return abyte;
    }
    public  static int bytesToInt(byte[] bytes) {
        int addr=0;
        if(bytes.length==1){
            addr = bytes[0] & 0xFF;
        }else{
            addr = bytes[0] & 0xFF;
            addr |= ((bytes[1] << 8) & 0xFF00);
            addr |= ((bytes[2] << 16) & 0xFF0000);
            addr |= ((bytes[3] << 24) & 0xFF000000);
        }
        return addr;
    }

    //大端
    /**
     * int to byte[] 支持 1或者 4 个字节
     * @param i
     * @param len
     * @return
     */
    public static byte[] intToByteBig (int i,int len) {
        byte[] abyte=null;
        if(len==1){
            abyte = new byte[len];
            abyte[0] = (byte) (0xff & i);
        }else if (len == 4){
            abyte = new byte[len];
            abyte[0] = (byte) ((i >>> 24) & 0xff);
            abyte[1] = (byte) ((i >>> 16) & 0xff);
            abyte[2] = (byte) ((i >>> 8) & 0xff);
            abyte[3] = (byte) (i & 0xff);
        }else {
            abyte = new byte[len];
            abyte[0] = (byte) ((i >>> 8) & 0xff);
            abyte[1] = (byte)(i & 0xff);
            Log.e("类型转换","```"+abyte[0]+"```"+abyte[1]);

        }
        return abyte;
    }
    public  static int bytesToIntBig (byte[] bytes) {
        int addr=0;
        if(bytes.length==1){
            addr = bytes[0] & 0xFF;
        }else{
            addr = bytes[0] & 0xFF;
            addr = (addr << 8) | (bytes[1] & 0xff) ;
            addr = (addr << 8) | (bytes[2] & 0xff) ;
            addr = (addr << 8) | (bytes[3] & 0xff) ;
        }
        return addr;
    }


}
