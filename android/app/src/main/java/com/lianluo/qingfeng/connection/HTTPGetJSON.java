/**
 * Get JSON String from net by http get method.
 *
 * Created by louis on 16/4/21
 */
package com.lianluo.qingfeng.connection;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

/**
 * @author wang
 *
 */
public class HTTPGetJSON {
	public static String TAG = "HTTPGetJSON";


	private static final HTTPGetJSON sInstance = new HTTPGetJSON();

	public static HTTPGetJSON getInstance() {
		return sInstance;
	}


	private class GetMessage {
		String url;
		String param;
	
		String opTag;
		Handler handler;
		Context mContext;
		GetMessage(Context mContext,String url, String param, Handler handler, String opTag){
			this.url = url;
			this.param = param;

			this.handler = handler;
			this.opTag = opTag;
			this.mContext = mContext;
		}
	}

	public void getAsync(Context mContext, String url, String param, Handler handler, String op_tag){
		new HttpGetTask().execute(new GetMessage(mContext,url, param, handler, op_tag));
	}


//	public String getSync(String url, String param){
//
//		HttpResponse httpRes = null;
//		String result = null;
//
//		Log.i(TAG, "GET request: " + url+param);
//		URI uri = null;
//		try {
//			uri = new URI(url+param);
//		} catch (URISyntaxException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		HttpGet getReq = new HttpGet(uri);
//		// 第2步：使用execute方法发送HTTP GET请求，并返回HttpResponse对象
//		try {
//			httpRes = new DefaultHttpClient().execute(getReq);
//
//			if (httpRes.getStatusLine().getStatusCode() == 200) {
//
//				// 取出响应字符串
//				result = EntityUtils.toString(httpRes.getEntity());
//			} else {
//				// TODO  mTextView1.setText("Error Response: "+httpResponse.getStatusLine().toString());
//			}
//		} catch (ClientProtocolException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		return result;
//	}
	

	private class HttpGetTask extends AsyncTask<GetMessage, Void, String>{

		Handler handler;
		String opTag;
		Context mContext;

		@Override
		protected String doInBackground(GetMessage... messages) {
			Log.e("=json==","====进入");

			String result = null;
			GetMessage gm = messages[0];

			this.handler = gm.handler;

			this.opTag = gm.opTag;
			this.mContext = gm.mContext;
			KeyStore keyStore;
			URI url = null;
			HttpGet req = null;
			try{

				AssetManager am = gm.mContext.getAssets();
				Log.e("=json==","====1");
				InputStream ins = am.open("_.lianluo.com_bundle.crt");
				//InputStream ins = am.open("nginx.crt");
				Log.e("=json==","====2");
				try {

					//读取证书

					CertificateFactory cerFactory = CertificateFactory.getInstance("X.509");  //问1

					Certificate cer = cerFactory.generateCertificate(ins);

					//创建一个证书库，并将证书导入证书库

					keyStore = KeyStore.getInstance("PKCS12", "BC");   //问2
					Log.e("=json==","====3");
					keyStore.load(null, null);

					keyStore.setCertificateEntry("trust", cer);

					Log.e("=json==","====4");
				} finally {

					ins.close();

				}

				//把咱的证书库作为信任证书库

				SSLSocketFactory socketFactory = new SSLSocketFactory(keyStore);

				Scheme sch = new Scheme("https", socketFactory, 443);
				Log.e("=json==","====5");
				//完工

				HttpClient mHttpClient = new DefaultHttpClient();
				Log.e("=json==","====6");
				mHttpClient.getConnectionManager().getSchemeRegistry().register(sch);
				url = new URI(gm.url+gm.param);
				Log.e("=json==","===="+url);
				//url = new URI(gm.url+gm.param);
				req = new HttpGet(url);

				Log.e("=json==","====7");

				HttpGet requestGet = new HttpGet(url);
				Log.e("=json==","====8"+requestGet);
				HttpResponse httpResponse = mHttpClient.execute(requestGet);
				Log.e("=json==","====9");
				if (httpResponse.getStatusLine().getStatusCode() == 200) {
					HttpEntity httpEntity = httpResponse.getEntity();
					result = EntityUtils.toString(httpEntity);
					result = result.replaceAll("\r", "");
					Log.e("=json==","====10");
				}
				Log.e("=json==","====11");
			}catch (Exception e){

			}



	
//			HttpGet req = null;
//			HttpResponse httpResponse = null;
//			URI url = null;
//
//			try {
//
//
//				url = new URI(gm.url+gm.param);
//				req = new HttpGet(url);
//
//				httpResponse = new DefaultHttpClient().execute(req);
//
//
//				if (httpResponse.getStatusLine().getStatusCode() == 200) {
//					Log.d(TAG, "response=200");
//
//					result = EntityUtils.toString(httpResponse.getEntity());
//
//					result = result.replaceAll("\r", "");
//					Log.d(TAG, result);
//					}
//				} catch (URISyntaxException e) {
//					Log.e(TAG, e.getMessage().toString());
//				} catch (ClientProtocolException e) {
//					Log.e(TAG, e.getMessage().toString());
//				} catch (IOException e) {
//					Log.e(TAG, e.getMessage().toString());
//				}catch (Exception e){
//
//				}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			if(this.handler != null){
				 Message msg = new Message();
				 Bundle bundle = new Bundle();
				 bundle.putString(HttpConstant.HTTP_OPR, this.opTag);
				 bundle.putString(HttpConstant.HTTP_RESULT, result);
				 msg.setData(bundle);
				 handler.sendMessage(msg);
			}
		}
		
	}
}
