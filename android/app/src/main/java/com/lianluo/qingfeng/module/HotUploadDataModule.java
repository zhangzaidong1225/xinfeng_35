package com.lianluo.qingfeng.module;

/**
 * Created by Administrator on 2017/1/17.
 */

public class HotUploadDataModule {

    private String url = "";
    private String ver = "";
    private String md5 = "";
    private String force = "";

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getForce() {
        return force;
    }

    public void setForce(String force) {
        this.force = force;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

}
