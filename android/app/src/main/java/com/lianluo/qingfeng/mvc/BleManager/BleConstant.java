package com.lianluo.qingfeng.mvc.BleManager;

/**
 * Created by zhangzaidong on 2016/9/13.
 *Callback状态值
 */
public class BleConstant {

    public static final int connected = 0x01;
    public static final int disconnected= 0x02;
    public static final int devNotSupport = 0x03;
    public static final int devDataChanged = 0x04;
    
}
