package com.lianluo.qingfeng.mvc.BleManager.BleCmd;

/**
 * Created by zhangzaidong on 2016/9/13.
 */
public class DevMsg {

    public String address;
    public String name;
    public int action;
    public int speed;

    public DevMsg(String address,String name,int action){
        this.address = address;
        this.name = name;
        this.action = action;
    }

    public DevMsg(String address,String name,int action,int speed){
        this.address = address;
        this.name = name;
        this.action  = action;
        this.speed = speed;
    }
}
