package com.lianluo.qingfeng.mvc.Modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.lianluo.qingfeng.mvc.Modal.Bean.XinfengDev;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangzaidong on 2016/9/18.
 */
public class DeviceMap implements Parcelable {

    public Map<String,XinfengDev> map = new HashMap<String,XinfengDev>();

    public DeviceMap(Map<String, XinfengDev> map) {
        this.map = map;
    }
    public DeviceMap(){

    }

    protected DeviceMap(Parcel in) {
        map = in.readHashMap(HashMap.class.getClassLoader());

    }

    public static final Creator<DeviceMap> CREATOR = new Creator<DeviceMap>() {
        @Override
        public DeviceMap createFromParcel(Parcel in) {
            return new DeviceMap(in);
        }

        @Override
        public DeviceMap[] newArray(int size) {
            return new DeviceMap[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeMap(map);

    }
}
