package com.lianluo.qingfeng.mvc.Modal;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.lianluo.qingfeng.MainActivity;
import com.lianluo.qingfeng.mvc.BleManager.BleConstant;
import com.lianluo.qingfeng.mvc.BleManager.BleManager;
import com.lianluo.qingfeng.mvc.BleManager.Scan.ScanDevManager;
import com.lianluo.qingfeng.mvc.BleManager.UartService.UartService;
import com.lianluo.qingfeng.mvc.Interface.BleDevParseData;
import com.lianluo.qingfeng.mvc.Interface.BleScanInter;
import com.lianluo.qingfeng.mvc.Interface.BleStatus;
import com.lianluo.qingfeng.mvc.Modal.Bean.XinfengDev;
import com.lianluo.qingfeng.utils.UMengDeviceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by zhangzaidong on 2016/9/12.
 */
public class BleDevManager implements BleStatus, BleScanInter {

    private final String TAG = "BleDevManager";
    private BluetoothAdapter adapter;

    public Map<String, XinfengDev> mData = new ConcurrentHashMap<>();
    private DeviceMap deviceMap = null;

    private static volatile BleDevManager manager;

//    public Handler mHandler = null;
//
//    public BleDevParseData bleDevParseData;

    private static boolean scan_state = false;
    private HashMap<String, Timer> timer = new HashMap<String, Timer>();


    public boolean isUpdating = false;//固件升级标志

    private static final String SHAREPREFERENCE_NAME = "XINGFENG";

    private Map<String, Boolean> mDeviceDisconnectedState;//处理主动断开连接



    public static BleDevManager getInstance() {
        if (manager == null) {
            synchronized (BleDevManager.class){
                if (null == manager){
                    manager = new BleDevManager();
                }
            }
        }
        return manager;
    }

    private BleDevManager() {
        BleManager.getInstance().setBleStatus(this);
        ScanDevManager.getInstance().setBleScanInter(this);

        deviceMap = new DeviceMap();
        deviceMap.map = new HashMap<>();
        deviceMap.map = mData;

        mDeviceDisconnectedState = new HashMap<String, Boolean>();


        readData();
    }

//    public void setBleDevParseData(BleDevParseData bleDevParseData) {
//        this.bleDevParseData = bleDevParseData;
//    }
//
//    public void setHandler(Handler handler) {
//        if (handler != null) {
//            this.mHandler = handler;
//        }
//    }


    /**
     * 连接成功
     *
     * @param result
     * @param address
     */
    @Override
    public void connectedCallback(int result, String address) {

        if (result == BleConstant.connected) {
            XinfengDev device = mData.get(address);
            device.setIsConnected(1);
            device.setIsmatched(true);
            device.setAutoConnect(true);
            device.setUpdate_state(0);
            updateData(address, device);
            sendData(UartService.ACTION_GATT_CONNECTED, address);
        }

    }

    @Override
    public void disconnCallback(int result, String address) {
        XinfengDev device = mData.get(address);


        if (result == BleConstant.disconnected) {
            if (device.ismatched()) {
                device.setIsConnected(0);
                device.setUpdate_state(0);
                updateData(address, device);
                if (mDeviceDisconnectedState.get(address) != null) {
                    mDeviceDisconnectedState.remove(address);
                }

                BleManager.getInstance().uartService.close(address);

            } else {
                if (device.getIsConnected() == 1) {
                    removeData(address);
                    BleManager.getInstance().uartService.close(address);

                } else {
                    device.setIsConnected(0);
                    device.setUpdate_state(0);
                    updateData(address, device);
                    BleManager.getInstance().uartService.close(address);

                }
            }
            sendData(UartService.ACTION_GATT_DISCONNECTED, address);
        }
    }

    @Override
    public void devNotSupported(int result, String address) {
        XinfengDev xfengDev = mData.get(address);

        if (result == BleConstant.devNotSupport) {
            sendData(UartService.DEVICE_DOES_NOT_SUPPORT_UART,address);

        }
    }

    @Override
    public void devDataChange(int result, BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        if (result == BleConstant.devDataChanged) {
            boolean isChange = false;
            XinfengDev xfengDev = mData.get(gatt.getDevice().getAddress());

            byte[] Value = characteristic.getValue();
            //bleDevParseData.ParseData(gatt.getDevice().getAddress(),Value);
            isChange = parseData(xfengDev, Value);
            if (isChange) {
                updateData(gatt.getDevice().getAddress(), xfengDev);
                sendData(UartService.ACTION_DATA_AVAILABLE,gatt.getDevice().getAddress());
            }
        }
    }

    @Override
    public void scanCallback(BluetoothDevice dev) {
        boolean isContain = false;

        for (String key : mData.keySet()) {
            if (dev.getAddress().equals(key)) {
                isContain = true;
                break;
            }
            isContain = false;
        }

        if (!isContain) {
            XinfengDev xinfengDev = new XinfengDev();
            if (dev.getName() == null) {
                xinfengDev.setName("未命名");
            } else {
                xinfengDev.setName(dev.getName());
            }
            xinfengDev.setAddress(dev.getAddress());
            xinfengDev.setIsmatched(false);

            addData(xinfengDev);
        }
    }

    @Override
    public void startScanCallback(boolean status, String action) {
        scan_state = status;
        sendData(UartService.START_SCAN);
    }

    @Override
    public void stopScanCallback(boolean status, String action) {
        scan_state = status;
        sendData(UartService.STOP_SCAN);
    }

    @Override
    public void clearScanCallback(String action) {
        clearData(1);
        sendData(UartService.CLEAR_DATA_1);
    }


    public void addData(XinfengDev xinfengDev) {
        mData.put(xinfengDev.getAddress(), xinfengDev);
        Log.e(TAG, "addData---" + xinfengDev.getName() + xinfengDev.getAddress());
        sendData(UartService.ADD_DEVICE);
        saveData();


    }


    /**
     * 自动连接
     * 修改为搜索到自动连接
     */
    public void autoConnecting() {

        final Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                adapter = BluetoothAdapter.getDefaultAdapter();
                if (!adapter.isEnabled()) return;

                if (mData.isEmpty()) return;

                boolean isNeedAutoConnectFlag = false;
                for (String key : mData.keySet()) {
                    XinfengDev device = mData.get(key);
                    if (device.ismatched()){
                        if ((device.getIsConnected() == 0) && (device.isAutoConnect())){
                            Log.e(TAG, "---AutoConnect---NoNeed" + device.getAddress()+ device.getName());
                            isNeedAutoConnectFlag = true;

                        }
                    }
                }

                if (!isNeedAutoConnectFlag) {
                    Log.e(TAG, "---AutoConnect---NoNeed");
                    return;
                }

                if (BleManager.getInstance().uartService != null){
                    if (!isUpdating) {
                        ScanDevManager.getInstance().scanAddressAndConnect(false);
                    }
                }


//                Iterator iter = mData.entrySet().iterator();
//                while (iter.hasNext()) {
//                    Map.Entry entry = (Map.Entry) iter.next();
//                    final XinfengDev device = (XinfengDev) entry.getValue();
//
//                    if (device.ismatched()) {
//                        if ((device.getIsConnected() == 0) && (device.isAutoConnect())) {
//                            if (!isUpdating) {
//                                connect(device.getAddress(), false);
//
//                            }
//                        }
//                    }
//                }
            }
        };
        timer.schedule(task, 100, 5000);
    }

    /**
     * 负责处理连接逻辑
     *
     * @param address
     * @param connecting
     */
    public void connect(String address, boolean connecting) {

        adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter.isEnabled()) {

            if (!mData.isEmpty() && mData.containsKey(address)) {
                Object bleObj = mData.get(address);
                XinfengDev device = (XinfengDev) bleObj;

                if (device.getIsConnected() == 0) {
                    if (connecting) {
                        device.setIsConnected(2);
                    } else {
                        device.setIsConnected(3);
                    }
                    //此处需要发送数据上去
//                    mdeviceMap.map = XinfengService.mData;
//                    sendMyDevice(mdeviceMap);
                    sendData(UartService.ACTION_CONNECTING,address);

                    BleManager.getInstance().uartService.connect(address);


                    if (connecting) {
                        Timer temp = new Timer();
                        temp.schedule(new BleTask(address), 60000);
                        timer.put(address, temp);
                    }
                } else {
                    if (device.getIsConnected() == 3) {
                        if (connecting) {
                            device.setIsConnected(2);
                        }
                        //此处需要发送数据上去
//                    mdeviceMap.map = XinfengService.mData;
//                    sendMyDevice(mdeviceMap);
                        sendData(UartService.ACTION_CONNECTING,address);

                        if (connecting) {
                            Timer temp = new Timer();
                            temp.schedule(new BleTask(device.getAddress()), 60 * 1000);
                            timer.put(device.getAddress(), temp);
                            Log.e(TAG, "开始时间-----");
                        }
                    }
                }


            }
        }

    }

    class BleTask extends TimerTask {

        public String address;

        public BleTask(String address) {
            this.address = address;
        }

        @Override
        public void run() {

            if (!mData.isEmpty() && mData.containsKey(address)) {
                Object bleObj = mData.get(address);
                final XinfengDev device = (XinfengDev) bleObj;

                if ((device.getIsConnected() == 2) || (device.getIsConnected() == 3)) {
                    device.setIsConnected(0);

                    BleManager.getInstance().uartService.disconnect(address);

                    BleManager.getInstance().uartService.close(address);
                    //此处需要发送数据上去
//                    mdeviceMap.map = XinfengService.mData;
//                    sendMyDevice(mdeviceMap);
                    sendData(UartService.ACTION_CLOSE_GATT,address);

                }
            }

        }
    }

    public void stopTimer(String address) {
        if (timer.containsKey(address)) {
            timer.get(address).cancel();
            timer.get(address).purge();
            timer.remove(address);
        }
    }

    /**
     * 判断设备是否连接
     *
     * @param address
     * @return
     */
    public Boolean isConnected(String address) {

        if (address == null) {
            return false;
        }
        if (!mData.isEmpty() && mData.containsKey(address)) {
            XinfengDev device = mData.get(address);
            if (device.getIsConnected() == 0) {
                return false;
            } else if (device.getIsConnected() == 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * 取消配对时调用
     *
     * @param address
     */
    public void disconnected(String address) {
        if (address != null) {
            if (!mData.isEmpty() && mData.containsKey(address)) {
                XinfengDev device = mData.get(address);
                if (device.getIsConnected() == 1) {
                    device.setAutoConnect(false);
                }
            }
        }
    }

    /**
     * 断开连接
     * @param address
     */
    public void stopConnect(String address){
        if (!isConnected(address)){
            return;
        }
        disconnected(address);

        BleManager.getInstance().uartService.disconnect(address);

    }

    /**
     * 取消配对
     * @param address
     */
    public void  cancelBonded(String address){

        Iterator iter = mData.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry entry = (Map.Entry) iter.next();
            XinfengDev device = (XinfengDev) entry.getValue();

            if (device.getAddress().equals(address)){
                device.setIsmatched(false);
                if (device.getIsConnected() != 1){
                    iter.remove();
                    removeData(address);

                }
            }
        }
        //发送数据
        sendData(UartService.CANCEL_BONDED);
    }

    public void devRename(String address,String rename){
        if (address != null){
            Object bleObj = mData.get(address);
            XinfengDev device = (XinfengDev) bleObj;

            device.setName(rename);
            sendData(UartService.RENAME);
        }
    }

    /**
     * 发送数据
     */

    public void sendData(String action) {
        final Intent intent = new Intent(action);
        deviceMap.map = mData;
        intent.putExtra("map", deviceMap);
        intent.putExtra("scan_state", scan_state);
        LocalBroadcastManager.getInstance(MainActivity.mContext).sendBroadcast(intent);
    }


    public void sendData(String action, String address) {
        final Intent intent = new Intent(action);
        deviceMap.map = mData;
        intent.putExtra("map", deviceMap);
        intent.putExtra("device_address", address);
        intent.putExtra("scan_state", scan_state);
        LocalBroadcastManager.getInstance(MainActivity.mContext).sendBroadcast(intent);
    }

    /**
     * 更新数据
     *
     * @param address
     * @param item
     */
    public void updateData(String address, Object item) {
        mData.put(address, (XinfengDev) item);
        saveData();
    }

    /**
     * 搜索时候清除未连接设备
     */
    public void clearData(int mark) {
        if (mark == 0) {
            mData.clear();
            //SaveData();
            SharedPreferences sp = MainActivity.mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            editor.apply();
        }
        if (mark == 1) {
            if (mData.size() > 0) {
                for (Iterator iter = mData.entrySet().iterator(); iter.hasNext(); ) {
                    Map.Entry element = (Map.Entry) iter.next();
                    Object strKey = element.getKey();
                    Object strObj = element.getValue();

                    final XinfengDev device = (XinfengDev) strObj;

                    if (device.getIsConnected() == 0 && !device.ismatched()) {
                        iter.remove();
                    }
                }
            }
        }
    }

    /***
     * 清除一条数据
     * 取消配对时删除数据
     *
     * @param address
     */
    public void removeData(String address) {
        Log.e(TAG, "removeData");

        Iterator iter = mData.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            Object key = entry.getKey();
            Object val = entry.getValue();
            XinfengDev bleDevice = (XinfengDev) val;
            if (bleDevice.getAddress().equals(address)) {
                iter.remove();
            }
        }

        //取消配对时，删除指定数据

        if (mData.isEmpty()) {
            clearData(0);
        } else {
            saveData();
        }

    }

    /***
     * 序列化数据
     * 将连接成功的数据 进行存储
     */

    public void saveData() {
        JSONArray jsonArray = new JSONArray();
        Collection values = mData.values();

        for (Iterator iterator = values.iterator(); iterator.hasNext(); ) {
            Object obj = iterator.next();
            XinfengDev device = (XinfengDev) obj;

            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("address", device.getAddress());
                jsonObject.put("name", device.getName());
                jsonObject.put("autoconnect", device.isAutoConnect());
                jsonObject.put("matched", device.ismatched());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SharedPreferences sp = MainActivity.mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_APPEND);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("BLE_DATA", jsonArray.toString());
            editor.apply();
        }
    }

    /***
     * 反序列化数据
     * app启动的时候读取数据
     */
    public void readData() {
        mData.clear();

        SharedPreferences sp = MainActivity.mContext.getSharedPreferences(SHAREPREFERENCE_NAME, Context.MODE_APPEND);
        String result = sp.getString("BLE_DATA", "");

        try {
            JSONArray jsonArray = new JSONArray(result);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject itemObject = jsonArray.getJSONObject(i);
                XinfengDev device = new XinfengDev();
                device.setAddress((String) itemObject.get("address"));
                device.setName((String) itemObject.get("name"));
                device.setAutoConnect((boolean) itemObject.get("autoconnect"));
                device.setIsmatched((boolean) itemObject.get("matched"));
                device.setIsConnected(0);
                device.setUpdate_state(0);

                addData(device);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public boolean parseData(XinfengDev bleDevice, byte[] Value) {
        UMengDeviceUtil.getInstance().parseData(Value, bleDevice.getAddress());

        boolean isChange = false;

        if (Value[1] == 0x01) {
            Log.e(TAG, "关机命令" + Value[1] + "");


            if (mDeviceDisconnectedState.get(bleDevice.getAddress()) == null) {
                BleManager.getInstance().uartService.disconnect(bleDevice.getAddress());
                mDeviceDisconnectedState.put(bleDevice.getAddress(), true);
                isChange = true;
            }



        }

        if (Value[1] == 0x03) {
            Log.e(TAG, "蓝牙关闭" + Value[1] + "");
            bleDevice.setAutoConnect(false);
            //针对三星S6，关闭设备蓝牙，app显示仍然连接问题。
            BleManager.getInstance().uartService.disconnect(bleDevice.getAddress());

            isChange = true;
        }

        //手动断开连接
        if(Value[1] == 0x07) {
            Log.e(TAG, "disconnect```````````" + bleDevice.getAddress());
        }

        if (Value[1] == 0x15){

            byte[] dataName = new byte[Value[2]];


            for (int i = 0;i< Value[2];i++){
                dataName[i] = Value[3+i];
            }
            String devName = new String(dataName);
            //Log.e(TAG, "名字---长度---" + Value[2] + "----"+ devName);


            if (!bleDevice.getName().equals(devName)){
                bleDevice.setName(devName);
                isChange = true;
            }

        }

        if (Value[1] == 0x54) {
            //获取固件版本
            //增加固件版本为小数
            int firstByte = (0x000000FF & ((int) Value[2]));
            int secondByte = (0x000000FF & ((int) Value[3]));
            int unsignedInt = ((int) (secondByte | firstByte << 8));
            //Log.e(TAG,"固件版本---"+unsignedInt + "");

            if (bleDevice.getVersion() != unsignedInt) {
                bleDevice.setVersion(unsignedInt);
                isChange = true;
            }
        }

        if (Value[1] == 0x50) {

            if (Value[2] == 0x4) {
                //Log.e("电量", Value[3] + "");
                //Log.e("充电中", Value[6] + "");


                //防止电量重复发送
                if (bleDevice.getBattery_power() != Value[3]) {
                    bleDevice.setBattery_power(Value[3]);
                    isChange = true;
                }

                if (bleDevice.getBattery_charge() != Value[6]) {
                    bleDevice.setBattery_charge(Value[6]);
                    isChange = true;
                }

                //Log.e(TAG, Value[17] + "----电压过压");

                if((Value[17] & 0x01) == 0x01){
                    //Log.e(TAG, (Value[17] &0x01) + "--01--电压过压");
                    bleDevice.setBattery_over(1);
                    isChange = true;
                }else if ((Value[17] & 0x10) == 0x10){
                    //Log.e(TAG, (Value[17] &0x10) + "--10--电流过低");
                    bleDevice.setBattery_over(1);
                    isChange = true;
                }else {
                    //Log.e(TAG, (Value[17]) + "--0--电压正常");
                    if (bleDevice.getBattery_over() != Value[17]){
                        Log.e(TAG, (Value[17]) + "--0--电压正常");
                        bleDevice.setBattery_over(0);
                        isChange = true;
                    }
                }

            }

            if (Value[2] == 0x5) {

                int firstByte = (0x000000FF & ((int) Value[11]));
                int secondByte = (0x000000FF & ((int) Value[12]));

                int unsignedInt = ((int) (firstByte << 8 | secondByte));

                //Log.e("本次风扇使用时长", unsignedInt + "   ");

                if ((unsignedInt - bleDevice.getWind_use_time_this()) > 10 || (bleDevice.getWind_use_time_this() - unsignedInt) > 0) {
                    bleDevice.setWind_use_time_this(unsignedInt);
                    isChange = true;
                }
            }

            if (Value[2] == 0x3){

                int firstByte = (0x000000FF & ((int)Value[10]));
                int secondByte = (0x000000FF & ((int)Value[9]));
                int threeByte = (0x000000FF & ((int)Value[8]));
                int fourByte = (0x000000FF & ((int)Value[7]));


                int unsignedInt = ((int) (firstByte | secondByte << 8 | threeByte << 16 | fourByte << 24));
                //Log.e("本次滤棉使用总时长", unsignedInt + "   ");

                if ((unsignedInt - bleDevice.getFilter_use_time_this() > 60 ) || bleDevice.getFilter_use_time_this() - unsignedInt > 0){
                    bleDevice.setFilter_use_time_this(unsignedInt);
                    isChange = true;

                }

                int firstByteFan = (0x000000FF & ((int)Value[14]));
                int secondByteFan = (0x000000FF & ((int)Value[13]));
                int threeByteFan = (0x000000FF & ((int)Value[12]));
                int fourByteFan = (0x000000FF & ((int)Value[11]));

                int unsignedIntFan = ((int) (firstByteFan | secondByteFan << 8 | threeByteFan << 16 | fourByteFan << 24));

                //Log.e("滤棉折合全风速使用市场", unsignedIntFan + "   ");

                if ((unsignedIntFan - bleDevice.getFilters_maxfan_usetime()) > 60 || bleDevice.getFilters_maxfan_usetime() -unsignedIntFan > 0){
                    bleDevice.setFilters_maxfan_usetime(unsignedIntFan);
                    isChange = true;
                }

            }

            if (Value[2] == 0x2) {
                int firstByte = (0x000000FF & ((int) Value[5]));
                int secondByte = (0x000000FF & ((int) Value[6]));
                int unsignedInt = ((int) (secondByte | firstByte << 8));
                //Log.e("Pm2.5标准浓度", unsignedInt + "");

                if (bleDevice.getPm25() != unsignedInt) {

                    bleDevice.setPm25(unsignedInt);
                    isChange = true;
                }
            }

            if (Value[2] == 0x1) {
                //Log.e("风扇等级", Value[5] + "");

                if (bleDevice.getSpeed() != Value[5]) {
                    bleDevice.setSpeed(Value[5]);
                    isChange = true;
                }

                int firstByte = (0x000000FF & ((int) Value[14]));
                int secondByte = (0x000000FF & ((int) Value[13]));
                int threeByte = (0x000000FF & ((int) Value[12]));
                int fourByte = (0x000000FF & ((int) Value[11]));

                int unsignedInt = ((int) (firstByte | secondByte << 8 | threeByte << 16 | fourByte << 24));
                //Log.e("滤棉使用时长", unsignedInt + "");


                if ((unsignedInt - bleDevice.getUse_time()) > 60 || bleDevice.getUse_time() - unsignedInt > 0) {
                    bleDevice.setUse_time(unsignedInt);
                    isChange = true;
                }


                int firstByteThis = (0x000000FF & ((int) Value[8]));
                int secondByteThis = (0x000000FF & ((int) Value[7]));
                int unsignedIntThis = ((int) (firstByteThis | secondByteThis << 8));

                //Log.e("本次滤棉使用时长", unsignedIntThis + "");

                if ((bleDevice.getUse_time_this() - unsignedIntThis > 0) || (unsignedIntThis - bleDevice.getUse_time_this()) > 10) {
                    bleDevice.setUse_time_this(unsignedIntThis);
                    isChange = true;
                }

                //Log.e("滤棉在位", Value[15] + "");
                if (bleDevice.getFilter_in() != Value[15]) {
                    bleDevice.setFilter_in(Value[15]);
                    isChange = true;

                }

                int firstByteNumber = (0x000000FF & ((int) Value[10]));
                int secondByteNumber = (0x000000FF & ((int) Value[9]));

                int unsignedIntNumber = ((int) (firstByteNumber | secondByteNumber << 8));

                if (bleDevice.getFilter_number() != unsignedIntNumber){
                    bleDevice.setFilter_number(unsignedIntNumber);
                    isChange = true;
                }
            }
        }
        return isChange;

    }
}
