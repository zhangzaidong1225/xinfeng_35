package com.lianluo.qingfeng.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.lianluo.qingfeng.connection.HttpConstant;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * save image to external storage directory.
 * <p>
 * Created by louis on 16/4/22.
 */
public class SaveImageUtil extends Thread {

    private final static String TAG = "SavaImageUtil";
    public final static String ALBUM_PATH
            = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/";
    //            = Environment.getExternalStorageDirectory() + "/download_test/";
    public final static String SAVE_IMAGE
            = "save_image";

    private byte[] mData = null;
    private String mFileName = "";
    private String mFileTempName = "";
    private Handler mHandler = null;

    public SaveImageUtil() {

    }

    public SaveImageUtil(byte[] data, String fileName, Handler handler) {
        this.mData = data;
        this.mFileName = fileName + ".png";
        this.mFileTempName = fileName + "_temp.png";
        this.mHandler = handler;
    }

    public void run() {
        if (mData == null) {
            return;
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(mData, 0, mData.length);// bitmap
        try {
            saveFile(bitmap, mFileTempName);
            renameFile(ALBUM_PATH, mFileTempName, mFileName);
            SZTYSharedPreferences.getInstance().putBoolean(SZTYSharedPreferences.IS_LOAD, true);
            Log.e("SaveImageUtil", ALBUM_PATH + "Save Ad File success.");
            if (this.mHandler != null) {
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString(SAVE_IMAGE, mFileName);
//                bundle.putString(HttpConstant.HTTP_RESULT, result);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * 保存文件
     *
     * @param bm
     * @param fileName
     * @throws IOException
     */
    public void saveFile(Bitmap bm, String fileName) throws IOException {

        File dirFile = new File(ALBUM_PATH);
        if (dirFile.exists()) {
            deleteDir(dirFile);
        }
        dirFile.mkdir();
        File myCaptureFile = new File(ALBUM_PATH + fileName);
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
        bm.compress(Bitmap.CompressFormat.PNG, 80, bos);
        bos.flush();
        bos.close();
    }

    /** */
    /**
     * 文件重命名
     *
     * @param path    文件目录
     * @param oldname 原来的文件名
     * @param newname 新文件名
     */
    public void renameFile(String path, String oldname, String newname) {
        if (!oldname.equals(newname)) {//新的文件名和以前文件名不同时,才有必要进行重命名
            File oldfile = new File(path + "/" + oldname);
            File newfile = new File(path + "/" + newname);
            if (!oldfile.exists()) {
                return;//重命名文件不存在
            }
            if (newfile.exists())//若在该目录下已经有一个文件和新文件名相同，则不允许重命名
                System.out.println(newname + "已经存在！");
            else {
                oldfile.renameTo(newfile);
            }
        } else {
            System.out.println("新文件名和旧文件名相同...");
        }
    }


    /**
     * 递归删除目录下的所有文件及子目录下所有文件
     *
     * @param dir 将要删除的文件目录
     * @return boolean Returns "true" if all deletions were successful.
     * If a deletion fails, the method stops attempting to
     * delete and returns "false".
     */
    public boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();

            if (children == null || (children != null && children.length == 0)){
                return  false;
            }

            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }

        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

}
