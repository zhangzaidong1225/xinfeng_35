package com.lianluo.qingfeng.mvc.BleManager.UartService;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.lianluo.qingfeng.mvc.BleManager.BleCmd.BleCmdManager;
import com.lianluo.qingfeng.mvc.BleManager.BleCmd.DevMsgAction;
import com.lianluo.qingfeng.mvc.BleManager.BleConstant;
import com.lianluo.qingfeng.mvc.BleManager.BleManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zhangzaidong on 2016/9/12.
 */
public class UartService extends Service {

    private static final String TAG = "UartService";

    private List<String> mAddressList;
    public static Map<String, BluetoothGatt> mBluetoothGattMap;
    public static Map<String, BluetoothGattService> mBluetoothGattService;
    public static Map<String, BluetoothGattCharacteristic> RXBluetoothGattCharacteristic;
    public static Map<String, BluetoothGattCharacteristic> TXBluetoothGattCharacteristic;

    public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID RX_CHAR_UUID = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
    public static final UUID TX_CHAR_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

    public final static String ACTION_GATT_CONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.nordicsemi.nrfUART.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.nordicsemi.nrfUART.ACTION_DATA_AVAILABLE";
    public final static String DEVICE_DOES_NOT_SUPPORT_UART =
            "com.nordicsemi.nrfUART.DEVICE_DOES_NOT_SUPPORT_UART";

    public final static String ADD_DEVICE = "com.nordicsemi.nrfUART.ADD_DEVICE";
    public final static String CLEAR_DATA_1 = "com.nordicsemi.nrfUART.CLEAR_DATA_1";
    public final static String STOP_SCAN = "com.nordicsemi.nrfUART.STOP_SCAN";
    public final static String START_SCAN = "com.nordicsemi.nrfUART.START_SCAN";
    public final static String CANCEL_BONDED = "com.nordicsemi.nrfUART.CANCEL_BONDED";
    public final static String RENAME = "com.nordicsemi.nrfUART.RENAME";
    public final static String ACTION_CONNECTING = "com.nordicsemi.nrfUART.ACTION_CONNECTING";
    public final static String ACTION_CLOSE_GATT = "com.nordicsemi.nrfUART.ACTION_CLOSE_GATT";
    public final static String ACTION_DFU_UPDATE = "com.nordicsemi.nrfUART.ACTION_DFU_UPDATE";


    public final IBinder mUartServiceBinder = new UartServiceLocalBinder();

    public class UartServiceLocalBinder extends Binder {

        public UartService getService() {
            return UartService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mUartServiceBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mAddressList = new ArrayList<>();
        mBluetoothGattMap = new HashMap<String, BluetoothGatt>();
        mBluetoothGattService = new HashMap<String, BluetoothGattService>();
        RXBluetoothGattCharacteristic = new HashMap<String, BluetoothGattCharacteristic>();
        TXBluetoothGattCharacteristic = new HashMap<String, BluetoothGattCharacteristic>();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        close();
    }

    public boolean connect(String address) {

        if (BleManager.getInstance().mBluetoothAdapter == null || address == null) {
            return false;
        }

        final BluetoothDevice device = BleManager.getInstance().mBluetoothAdapter.getRemoteDevice(address);

        if (device == null) {
            return false;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            BluetoothGatt gatt = device.connectGatt(UartService.this, false, createGattCallback());
            mBluetoothGattMap.put(address, gatt);
        } else {
            BluetoothGatt gatt = device.connectGatt(UartService.this, false, createGattCallback());
            mBluetoothGattMap.put(address, gatt);
        }

        if (mAddressList.indexOf(address) == -1) {
            mAddressList.add(address);
        }

        return true;

    }

    public boolean disconnect(String address) {

        if (BleManager.getInstance().mBluetoothAdapter == null || mBluetoothGattMap.get(address) == null) {
            return false;
        }
        mBluetoothGattMap.get(address).disconnect();
        return true;
    }

    public void close(){
        Iterator iter = mBluetoothGattMap.entrySet().iterator();

        while (iter.hasNext()) {

            Map.Entry entry = (Map.Entry) iter.next();
            Object key = entry.getKey();
            Object val = entry.getValue();
            BluetoothGatt gatt = (BluetoothGatt) val;
            gatt.disconnect();
            gatt.close();
        }

        mAddressList.clear();
        mBluetoothGattMap.clear();

        mBluetoothGattService.clear();
        RXBluetoothGattCharacteristic.clear();
        TXBluetoothGattCharacteristic.clear();

        BleManager.getInstance().mBluetoothManager = null;
    }

    public void close(String address) {

        if (mBluetoothGattMap.get(address) == null) {
            return;
        }
        mAddressList.remove(address);
        mBluetoothGattMap.get(address).close();
        mBluetoothGattMap.remove(address);

    }

    /**
     * Enables or disables notification on a give characteristic.
     */
    public boolean isRequiredServiceSupported(final BluetoothGatt gatt) {

        if (gatt == null){
            return false;
        }
        final BluetoothGattService service = gatt.getService(RX_SERVICE_UUID);
        if (service == null) {
            return false;
        }

        BluetoothGattCharacteristic mRXCharacteristic, mTXCharacteristic;

        mBluetoothGattService.put(gatt.getDevice().getAddress(), service);
        mRXCharacteristic = service.getCharacteristic(RX_CHAR_UUID);
        RXBluetoothGattCharacteristic.put(gatt.getDevice().getAddress(), mRXCharacteristic);
        mTXCharacteristic = service.getCharacteristic(TX_CHAR_UUID);
        TXBluetoothGattCharacteristic.put(gatt.getDevice().getAddress(), mTXCharacteristic);

        boolean writeRequest = false;
        boolean writeCommand = false;

        if (mRXCharacteristic != null) {
            final int rxProperties = mRXCharacteristic.getProperties();

            writeRequest = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0;
            writeCommand = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) > 0;

            // Set the WRITE REQUEST type when the characteristic supports it. This will allow to send long write (also if the characteristic support it).
            // In case there is no WRITE REQUEST property, this manager will divide texts longer then 20 bytes into up to 20 bytes chunks.
            if (writeRequest)
                mRXCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
        }

        boolean isService =  mRXCharacteristic != null && mTXCharacteristic != null && (writeRequest || writeCommand);

        return isService;
    }

    /**
     * Enable Notification on TX characteristic
     *
     * @return
     */
    public void enableTXNotification(BluetoothGatt gatt) {

        if (gatt == null){
            return;
        }
        String address = gatt.getDevice().getAddress();

        BluetoothGattCharacteristic TxChar = TXBluetoothGattCharacteristic.get(address);
        mBluetoothGattMap.get(address).setCharacteristicNotification(TxChar, true);
        BluetoothGattDescriptor descriptor = TxChar.getDescriptor(CCCD);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGattMap.get(address).writeDescriptor(descriptor);

        /**
         * 连接成功
         */
        BleManager.getInstance().bleStatus.connectedCallback(BleConstant.connected,gatt.getDevice().getAddress());

    }


    public boolean writeRXCharacteristic(String address, byte[] value) {

        if (address == null || value == null){
            return false;
        }

        BluetoothGattCharacteristic RxChar = RXBluetoothGattCharacteristic.get(address);

        RxChar.setValue(value);
        boolean status = mBluetoothGattMap.get(address).writeCharacteristic(RxChar);

        return status;

    }

    public BluetoothGattCallback createGattCallback(){
        return new BluetoothGattCallbackNew();
    }

    public void showMSG(String msg) {
        Log.e(TAG, msg);
    }

    public class BluetoothGattCallbackNew extends BluetoothGattCallback {

        private final static String TAG = "BluetoothGattCallback";

        private int count = 0;

        public BluetoothGattCallbackNew() {
            super();
        }

        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, int status, int newState) {
            Log.e(TAG, "Connected to GATT server.``````" + status + "------" + newState);

            if (status == BluetoothGatt.GATT_SUCCESS){
                if (newState == BluetoothProfile.STATE_CONNECTED){
                    Log.e(TAG, "Connected to GATT server.");

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (gatt.getDevice().getBondState() != BluetoothDevice.BOND_BONDING) {
                                gatt.discoverServices();
                            }
                        }
                    },600);
                }
                else if(newState == BluetoothProfile.STATE_DISCONNECTED){
                    Log.e(TAG, "Disconnected from GATT server.");
                    BleManager.getInstance().bleStatus.disconnCallback(BleConstant.disconnected,gatt.getDevice().getAddress());
                }
            }
            else {
                if (status == 133){
                    if (count == 0){
                        gatt.disconnect();
                        connect(gatt.getDevice().getAddress());
                        count++;
                    }else {
                        count = 0;
                        if (newState == BluetoothProfile.STATE_DISCONNECTED){
                            gatt.disconnect();
                            BleManager.getInstance().bleStatus.disconnCallback(BleConstant.disconnected,gatt.getDevice().getAddress());
                        }
                    }
                }else {
                    gatt.disconnect();
                    if (newState == BluetoothProfile.STATE_DISCONNECTED){
                        BleManager.getInstance().bleStatus.disconnCallback(BleConstant.disconnected,gatt.getDevice().getAddress());
                    }
                }
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS){
                if (gatt.getDevice() == null){
                    gatt.disconnect();
                }

                if (isRequiredServiceSupported(gatt)){
                    enableTXNotification(gatt);
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            getName(gatt.getDevice().getAddress());
                            BleCmdManager.getInstance().addMsg2Manager(gatt.getDevice().getAddress(),"", DevMsgAction.GetName,0);

                        }
                    },600);
                }else {
                    BleManager.getInstance().bleStatus.devNotSupported(BleConstant.devNotSupport,gatt.getDevice().getAddress());
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
//            broadcastUpdate(ACTION_DATA_AVAILABLE, gatt.getDevice().getAddress(), characteristic);
            if (TX_CHAR_UUID.equals(characteristic.getUuid())){
                BleManager.getInstance().bleStatus.devDataChange(BleConstant.devDataChanged,gatt,characteristic);
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
        }
    }


}
