package com.lianluo.qingfeng.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

import android.os.Environment;
import android.util.Log;

/**
 * 类描述：FileUtil
 *  @author hexiaoming
 *  @version  
 */
public class FileUtil {

	private static final String TAG = "FileUtil";

	public static File updateDir = null;
	public static File updateFile = null;
	/***********保存升级APK的目录***********/
	public static final String KonkaApplication = "konkaUpdateApplication";
	
	public static boolean isCreateFileSucess;

	/** 
	* 方法描述：createFile方法
	* @see FileUtil
	*/
	public static void createFile(String app_name) {
		
		if (android.os.Environment.MEDIA_MOUNTED.equals(android.os.Environment.getExternalStorageState())) {
			isCreateFileSucess = true;
			
			updateDir = new File(Environment.getExternalStorageDirectory()+ "/" + KonkaApplication +"/");
			updateFile = new File(updateDir + "/" + app_name + ".apk");

			if (!updateDir.exists()) {
				updateDir.mkdirs();
			}
			if (!updateFile.exists()) {
				try {
					updateFile.createNewFile();
				} catch (IOException e) {
					isCreateFileSucess = false;
					e.printStackTrace();
				}
			}

		}else{
			isCreateFileSucess = false;
		}
	}

	/**
	 * 文件夹copy
	 * @param srcDir
	 * @param dstDir
     */
	public static void copyFileOrDirectory(String srcDir, String dstDir) {

		try {
			File src = new File(srcDir);
			File dst = new File(dstDir, src.getName());
			Log.e(TAG,"src==" +src + "dst==" + dst);

			if (src.isDirectory()) {

				String files[] = src.list();
				int filesLength = files.length;
				for (int i = 0; i < filesLength; i++) {
					String src1 = (new File(src, files[i]).getPath());
					String dst1 = dst.getPath();
					copyFileOrDirectory(src1, dst1);

				}
			} else {
				copyFile(src, dst);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * copy单个文件
	 * @param sourceFile
	 * @param destFile
	 * @throws IOException
     */
	public static void copyFile(File sourceFile, File destFile) throws IOException {
		if (!destFile.getParentFile().exists())
			destFile.getParentFile().mkdirs();

		if (!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		} finally {
			if (source != null) {
				source.close();
			}
			if (destination != null) {
				destination.close();
			}
		}
	}

	/**
	 * MD5校验
	 * @param packagePath
	 * @return
     */
	public static String verifyInstallPackage(String packagePath){
		try {
			MessageDigest sig = MessageDigest.getInstance("MD5");
			File packageFile = new File(packagePath);
			InputStream signedData = new FileInputStream(packageFile);
			byte[] buffer = new byte[4096];
			long toRead = packageFile.length();
			long soFar = 0;
			boolean interrupted = false;

			while (soFar < toRead) {
				interrupted = Thread.interrupted();
				if (interrupted) break;
				int read = signedData.read(buffer);
				soFar += read;
				sig.update(buffer, 0, read);
			}

			final byte[] digest = sig.digest();
			String digestStr = bytesToHexString(digest);
			digestStr = digestStr.toLowerCase();
			Log.e(TAG, "生成的MD5------" + digestStr);
			return digestStr;

		} catch (Exception e) {
			Log.e(TAG, "CRC Error");
			e.printStackTrace();
			return null;
		}
	}

	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		int i = 0;
		while (i < src.length) {
			int v;
			String hv;
			v = (src[i] >> 4) & 0x0F;
			hv = Integer.toHexString(v);
			stringBuilder.append(hv);

			v = src[i] & 0x0F;
			hv = Integer.toHexString(v);
			stringBuilder.append(hv);
			i++;
		}
		return stringBuilder.toString();
	}

	/**
	 * 递归删除目录下的所有文件及子目录下所有文件
	 *
	 * @param dir 将要删除的文件目录
	 * @return boolean Returns "true" if all deletions were successful.
	 * If a deletion fails, the method stops attempting to
	 * delete and returns "false".
	 */
	public static  boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();

			if (children == null || (children != null && children.length == 0)){
				return  false;
			}

			//递归删除目录中的子目录下
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}

		}
		// 目录此时为空，可以删除
		return dir.delete();
	}




}