package com.lianluo.qingfeng.reactmodule;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.lianluo.qingfeng.BlueTooth.BleApplication;
import com.lianluo.qingfeng.MainActivity;
import com.lianluo.qingfeng.R;
import com.lianluo.qingfeng.UpdateService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;


public class ShareModule extends ReactContextBaseJavaModule {

    public static final String appName = "忻风";


    private HashMap<String, String> map = new HashMap<String, String>();

    // private ArrayList<String> arrayList = new ArrayList<String>();
    public ReadableArray arrayList;

    private final static String FILE_SAVEPATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static String pathfile = FILE_SAVEPATH + "/Screenshot.png";

    //传输数据
    public static ShareData  shareData;
    public int requestFlag = 1;



    public ShareModule(ReactApplicationContext reactContext) {
        super(reactContext);
        putMap();
        BleApplication.shareModule = this;

    }

    public void putMap() {
        map.put("0min", "开启忻风健康旅程");
        map.put("60min", "相当于云南某森林氧吧呼吸60分钟");
        map.put("180min", "相当于云南某森林氧吧漫步3小时");
        map.put("360min", "相当于云南某森林氧吧自驾游半天");
        map.put("720min", "相当于云南某森林氧吧露营1夜");
        map.put("1440min", "相当于云南某森林氧吧放松1天");
        map.put("2880min", "相当于云南某森林氧吧游玩2天");
        map.put("4320min", "相当于云南某森林氧吧畅游3天");
        map.put("10080min", "相当于云南某森林氧吧畅游7天");

        map.put("1m³", "相当于少抽34支二手烟");
        map.put("5m³", "相当于少吸1.8L汽车尾气10分钟");
        map.put("20m³", "相当于少抽680支二手烟");
        map.put("50m³", "相当于少吸4.0L汽车尾气50分钟");
        map.put("350m³", "相当于少吸10L卡车尾气60分钟");
        map.put("500m³", "相当于少抽17000支二手烟");
        map.put("3000m³", "相当于少吸4.0L汽车尾气50小时");
        map.put("6000m³", "相当于少抽20万支二手烟");
        map.put("20000m³", "相当于少吸10L汽车尾气60小时");

        map.put("1个", "您已经使用了1个滤芯");
        map.put("2个", "您已经使用了2个滤芯");
        map.put("5个", "您已经使用了5个滤芯");
        map.put("8个", "您已经使用了8个滤芯");
        map.put("10个", "您已经使用了10个滤芯");
        map.put("12个", "您已经使用了12个滤芯");
        map.put("24个", "您已经使用了24个滤芯");
        map.put("48个", "您已经使用超过48个滤芯");
    }

    @Override
    public String getName() {
        return "Share";
    }

    @ReactMethod
    public void CanvasBitmap(final String title, final int filtration, final int time, final int filternum, ReadableArray array) {

        Log.e("shouldComponentUpdate", "---3ssss----");
        arrayList = array;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("shouldComponentUpdate", "---55555555----");
                createBitmapShare(title, filtration, time, filternum);
            }
        }).start();


    }
    @ReactMethod
    public void mayWritePermission(){
        MainActivity.mContext.mayWritePermission();
    }

    @ReactMethod
    public void pullMenu(final String title, String url, String content, String imagePath, String page, final int filtration, final int time, final int filternum, ReadableArray array) {
        //请求存储权限
        //mayWritePermission();
        if (ContextCompat.checkSelfPermission(MainActivity.mContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            if (requestFlag == 1){
                MainActivity.mContext.mayWritePermission();
                shareData = new ShareData(title,url,content,imagePath,page,filtration,time,filternum,array);

                requestFlag = 0;
            }else {

                Toast.makeText(MainActivity.mContext, "存储权限被拒绝，请手动开启权限", Toast.LENGTH_SHORT).show();

            }

        }else {
            share(title,url,content,imagePath,page,filtration,time,filternum,array);
        }



        /*ShareSDK.initSDK(getReactApplicationContext());


        if (page.equals("home")) {

            OnekeyShare oks = new OnekeyShare();
            //关闭sso授权
            oks.disableSSOWhenAuthorize();

            // title标题：微信、QQ（新浪微博不需要标题）
            oks.setTitle(title);  //最多30个字符

            // text是分享文本：所有平台都需要这个字段
            oks.setText(content);  //最多40个字符

            // imagePath是图片的本地路径：除Linked-In以外的平台都支持此参数
            //oks.setImagePath(Environment.getExternalStorageDirectory() + "/meinv.jpg");//确保SDcard下面存在此张图片

            //网络图片的url：所有平台
            oks.setImageUrl(imagePath);//网络图片rul

            // url：仅在微信（包括好友和朋友圈）中使用
            oks.setUrl(url);   //网友点进链接后，可以看到分享的详情

            // Url：仅在QQ空间使用
            oks.setTitleUrl(url);  //网友点进链接后，可以看到分享的详情

            // 启动分享GUI
            oks.show(getReactApplicationContext());


        }

        if (page.equals("count")) {

            arrayList = array;

            try {
                File file = new File(pathfile);
                if (!file.exists()) {
                    Log.e("shouldComponentUpdate", "文件不存在-------");
                    createBitmapShare(title, filtration, time, filternum);
                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("shouldComponentUpdate", "---6666666666----");
                            createBitmapShare(title, filtration, time, filternum);
                        }
                    }).start();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            OnekeyShare oks = new OnekeyShare();
            //关闭sso授权
            oks.disableSSOWhenAuthorize();

            // title标题：微信、QQ（新浪微博不需要标题）
            oks.setTitle(title);  //最多30个字符

            // text是分享文本：所有平台都需要这个字段
            oks.setText(content);  //最多40个字符

            // imagePath是图片的本地路径：除Linked-In以外的平台都支持此参数
            oks.setImagePath(pathfile);//确保SDcard下面存在此张图片

            //网络图片的url：所有平台
            //oks.setImageUrl(imagePath);//网络图片rul

            // url：仅在微信（包括好友和朋友圈）中使用
            //oks.setUrl(url);   //网友点进链接后，可以看到分享的详情

            // Url：仅在QQ空间使用
            oks.setTitleUrl(url);  //网友点进链接后，可以看到分享的详情

            // 启动分享GUI
            oks.show(getReactApplicationContext());
        }*/

    }

    public class ShareData {

        public String title,url,content,imagePath,page;
        public int filtration,time,filternum;
        public ReadableArray array;

        public ShareData (final String title, String url, String content, String imagePath, String page, final int filtration, final int time, final int filternum, ReadableArray array){
            this.title = title;
            this.url = url;
            this.content = content;
            this.imagePath = imagePath;
            this.page = page;
            this.filtration = filtration;
            this.time = time;
            this.filternum = filternum;
            this.array = array;
        }
    }

    public  void share(final String title, String url, String content, String imagePath, String page, final int filtration, final int time, final int filternum, ReadableArray array){

        ShareSDK.initSDK(getReactApplicationContext());


        if (page.equals("home")) {

            OnekeyShare oks = new OnekeyShare();
            //关闭sso授权
            oks.disableSSOWhenAuthorize();

            // title标题：微信、QQ（新浪微博不需要标题）
            oks.setTitle(title);  //最多30个字符

            // text是分享文本：所有平台都需要这个字段
            oks.setText(content);  //最多40个字符

            // imagePath是图片的本地路径：除Linked-In以外的平台都支持此参数
            //oks.setImagePath(Environment.getExternalStorageDirectory() + "/meinv.jpg");//确保SDcard下面存在此张图片

            //网络图片的url：所有平台
            oks.setImageUrl(imagePath);//网络图片rul

            // url：仅在微信（包括好友和朋友圈）中使用
            oks.setUrl(url);   //网友点进链接后，可以看到分享的详情

            // Url：仅在QQ空间使用
            oks.setTitleUrl(url);  //网友点进链接后，可以看到分享的详情

            // 启动分享GUI
            oks.show(getReactApplicationContext());


        }

        if (page.equals("count")) {

            arrayList = array;

            try {
                File file = new File(pathfile);
                if (!file.exists()) {
                    Log.e("shouldComponentUpdate", "文件不存在-------");
                    createBitmapShare(title, filtration, time, filternum);
                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("shouldComponentUpdate", "---6666666666----");
                            createBitmapShare(title, filtration, time, filternum);
                        }
                    }).start();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            OnekeyShare oks = new OnekeyShare();
            //关闭sso授权
            oks.disableSSOWhenAuthorize();

            // title标题：微信、QQ（新浪微博不需要标题）
            oks.setTitle(title);  //最多30个字符

            // text是分享文本：所有平台都需要这个字段
            oks.setText(content);  //最多40个字符

            // imagePath是图片的本地路径：除Linked-In以外的平台都支持此参数
            oks.setImagePath(pathfile);//确保SDcard下面存在此张图片

            //网络图片的url：所有平台
            //oks.setImageUrl(imagePath);//网络图片rul

            // url：仅在微信（包括好友和朋友圈）中使用
            //oks.setUrl(url);   //网友点进链接后，可以看到分享的详情

            // Url：仅在QQ空间使用
            oks.setTitleUrl(url);  //网友点进链接后，可以看到分享的详情

            // 启动分享GUI
            oks.show(getReactApplicationContext());
        }
    }




    @ReactMethod
    public void show(final String downUrl, boolean flag, final String msg) {
        //请求存储权限
        // mayWritePermission();
        if (ContextCompat.checkSelfPermission(MainActivity.mContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            if (requestFlag == 1){
                Log.e("升级","121");
                MainActivity.mContext.mayWritePermission();
                update(downUrl,flag,msg);
                requestFlag = 0;
            }else {
                Toast.makeText(MainActivity.mContext, "存储权限被拒绝，请手动开启权限", Toast.LENGTH_SHORT).show();
            }

        }else {
            Log.e("升级","131");
            update(downUrl,flag,msg);
        }

    }
    public void update(final String downUrl, boolean flag, final String msg){

        if (!flag) {
            Intent intent = new Intent(getReactApplicationContext(), UpdateService.class);
            intent.putExtra("Key_App_Name", appName);
            intent.putExtra("Key_Down_Url", downUrl);
            getReactApplicationContext().startService(intent);
        } else {
            Toast.makeText(getReactApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }


    private void createBitmapShare(String title, int filtration, int time, int filternum) {

        Log.e("shouldComponentUpdate", "CanvasBitmap------");
        Bitmap srcBit = BitmapFactory.decodeResource(getReactApplicationContext().getResources(), R.drawable.ic_background);
        Bitmap imgMarker1 = BitmapFactory.decodeResource(getReactApplicationContext().getResources(), R.drawable.ic_circle);
        Bitmap imgMarker2 = BitmapFactory.decodeResource(getReactApplicationContext().getResources(), R.drawable.ic_rectangle);


        //获取图片的宽高
        int srcWidth = srcBit.getWidth();
        int srcHeight = srcBit.getHeight();

        Log.e("aa", "----6---" + srcWidth);
        Log.e("aa", "----7---" + srcHeight);


        //因子 小数点
        double factorX1 = srcWidth / 750.0;
        double factorY1 = srcHeight / 1334.0;

        float factorX = (float) factorX1;
        float factorY = (float) factorY1;


        //要绘制的区域，高度是变化的
        int sw = srcBit.getWidth();
        int sh = (int) (621 * factorY + arrayList.size() * 120 * factorY);

        if (sh <= 1334) {
            sh = 1334;
        } else {
            sh = (int) (621 * factorY + arrayList.size() * 120 * factorY);
        }

        Log.e("aa", "---" + sw);
        Log.e("aa", "---" + sh);
        Log.e("aa", "---" + factorX);
        Log.e("aa", "---" + factorY);


        Rect src = new Rect(0, 0, sw, sh);
        Rect dst = new Rect(0, 0, sw, sh);

        //create the new black bitmp
        //创建一个新的和src长宽一样的位图
        Bitmap newb = Bitmap.createBitmap(sw, sh, Bitmap.Config.ARGB_8888);
        Canvas cv = new Canvas(newb);
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setFilterBitmap(true);

        cv.drawColor(Color.parseColor("#7781AD"));

        cv.drawBitmap(srcBit, src, dst, paint);

        cv.drawBitmap(imgMarker1, 55 * factorX, 190 * factorY, null);


        //画名字
        drawTextWithName(cv, title, 285 * factorX, 300 * factorY, 50 * factorX);

        cv.drawBitmap(imgMarker2, 0, 417 * factorY, null);

        //画线
        Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        linePaint.setColor(Color.WHITE);
        linePaint.setStrokeWidth((float) 1.0);
        cv.drawLine(249 * factorX, 446 * factorY, 249 * factorX, 593 * factorY, linePaint);
        cv.drawLine(515 * factorX, 446 * factorY, 515 * factorX, 593 * factorY, linePaint);


        //15m3,15min,1pcs
        drawTextValue(cv, filtration, 50 * factorX, 520 * factorY, 70 * factorX);
        drawTextValue(cv, time, 290 * factorX, 520 * factorY, 70 * factorX);
        drawTextValue(cv, filternum, 563 * factorX, 520 * factorY, 70 * factorX);


        //立方米
        Paint unitPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        unitPaint.setColor(Color.WHITE);
        unitPaint.setTextSize(30 * factorX);
        unitPaint.setTypeface(Typeface.DEFAULT);
        cv.drawText("m³", 188 * factorX, 528 * factorY, unitPaint);
        cv.drawText("min", 448 * factorX, 528 * factorY, unitPaint);
        cv.drawText("pcs", 687 * factorX, 528 * factorY, unitPaint);


        //单位
        Paint measurePaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        measurePaint.setColor(Color.WHITE);
        measurePaint.setTextSize(30 * factorX);
        measurePaint.setTypeface(Typeface.DEFAULT);

        cv.drawText("净化空气量", 50 * factorX, 580 * factorY, measurePaint);
        cv.drawText("使用时长", 315 * factorX, 580 * factorY, measurePaint);
        cv.drawText("滤芯使用量", 550 * factorX, 580 * factorY, measurePaint);


        //画线
        //将612变为630调整划线位置  --2016.9.6
        for (int i = 1; i <= arrayList.size(); i++) {
            float x = 0, y = 0, w = 0, h = 0;
            //h= 100，高度100，半径为5.\
            //修改只有一个元素的时候
            if (i == 1 && arrayList.size() == 1) {
                x = 174 * factorX;
                y = 630 * factorY;
                w = 1.0f;
                h = 100 * factorY;
                drawLineToPoint(cv, x, y, w, h, 5 * factorX);

//                x = 174  * factorX;
//                y = (612 + (i) * 100 + (i) * 5) * factorY;
//                w = 1.0f;
//                h = 50 *factorX;
//                //最后一个多画线
//                drawLine(cv,x,y,w,h);
            } else {
                if (i == 1) {
                    x = 174 * factorX;
                    y = 630 * factorY;
                    w = 1.0f;
                    h = 100 * factorY;
                } else {
                    x = 174 * factorX;
                    y = (630 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                    w = 1.0f;
                    h = 100 * factorY;
                }
                drawLineToPoint(cv, x, y, w, h, 5 * factorX);
            }

        }

        for (int i = 1; i <= arrayList.size(); i++) {

            float x, y, w, h;

            if (i == 1 && arrayList.size() == 1) {
                x = 145 * factorX;
                y = (723 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                w = 28 * factorX;
                h = 0;
                //key
                if (arrayList.getString(i - 1).equals("0min")) {
                    drawTextWithKey(cv, "初次", x, y, w, h);
                }

                x = 220 * factorX;
                y = (671 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                w = 500 * factorX;
                h = 80 * factorX;
                //画圆角矩形
                drawTangle(cv, x, y, w, h);

                x = 267 * factorX;
                y = (723 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                w = 30 * factorX;
                h = 0;
                //value

                drawTextWithValue(cv, map.get(arrayList.getString(i - 1)), x, y, w, h);

                x = 174 * factorX;
                y = (630 + (i) * 100 + (i) * 5) * factorY;
                w = 1.0f;
                h = 50 * factorX;
                //最后一个多画线
                drawLine(cv, x, y, w, h);

            } else {

                if (i == 1) {
                    x = 145 * factorX;
                    y = 723 * factorY;
                    w = 28 * factorX;
                    h = 0;
                    //key
                    drawTextWithKey(cv, arrayList.getString(i - 1), x, y, w, h);

                    x = 220 * factorX;
                    y = 671 * factorY;
                    w = 500 * factorX;
                    h = 80 * factorX;
                    //绘制圆角矩形
                    drawTangle(cv, x, y, w, h);

                    x = 267 * factorX;
                    y = 723 * factorY;
                    w = 30 * factorX;
                    h = 0;
                    //value
                    drawTextWithValue(cv, map.get(arrayList.getString(i - 1)), x, y, w, h);

                } else {
                    if (i == arrayList.size()) {
                        x = 145 * factorX;
                        y = (723 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                        w = 28 * factorX;
                        h = 0;
                        //key
                        if (arrayList.getString(i - 1).equals("0min")) {
                            drawTextWithKey(cv, "初次", x, y, w, h);
                        }

                        x = 220 * factorX;
                        y = (671 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                        w = 500 * factorX;
                        h = 80 * factorX;
                        //画圆角矩形
                        drawTangle(cv, x, y, w, h);

                        x = 267 * factorX;
                        y = (723 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                        w = 30 * factorX;
                        h = 0;
                        //value

                        drawTextWithValue(cv, map.get(arrayList.getString(i - 1)), x, y, w, h);

                        x = 174 * factorX;
                        y = (630 + (i) * 100 + (i) * 5) * factorY;
                        w = 1.0f;
                        h = 50 * factorX;
                        //最后一个多画线
                        drawLine(cv, x, y, w, h);


                    } else {

                        x = 145 * factorX;
                        y = (723 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                        w = 28 * factorX;
                        h = 0;
                        //key
                        drawTextWithKey(cv, arrayList.getString(i - 1), x, y, w, h);

                        x = 220 * factorX;
                        y = (671 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                        w = 500 * factorX;
                        h = 80 * factorX;
                        drawTangle(cv, x, y, w, h);

                        x = 267 * factorX;
                        y = (723 + (i - 1) * 100 + (i - 1) * 5) * factorY;
                        w = 30 * factorX;
                        h = 0;
                        //value
                        drawTextWithValue(cv, map.get(arrayList.getString(i - 1)), x, y, w, h);
                    }
                }

            }

        }


        cv.save(Canvas.ALL_SAVE_FLAG);
        cv.restore();
        //保存图片
        saveBitmap(newb);

        //资源释放
        srcBit.recycle();
        imgMarker1.recycle();
        imgMarker2.recycle();
        newb.recycle();

        System.gc();


    }

    private void drawTextWithName(Canvas canvas, String key, float x, float y, float w) {

        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        textPaint.setTextSize(w);
        textPaint.setTypeface(Typeface.DEFAULT);
        textPaint.setColor(Color.WHITE);

        canvas.drawText(key, x, y, textPaint);
    }


    private void drawTextValue(Canvas canvas, int key, float x, float y, float w) {
        Paint numPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        numPaint.setColor(Color.WHITE);
        numPaint.setTextSize(w);
        numPaint.setTypeface(Typeface.DEFAULT);
        //numPaint.setTextAlign(Paint.Align.CENTER);
        String keyStr = key + "";

        canvas.drawText(keyStr, x, y, numPaint);

    }


    private void drawTextWithKey(Canvas canvas, String key, float x, float y, float w, float h) {

        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(w);
        textPaint.setTypeface(Typeface.DEFAULT);
        textPaint.setTextAlign(Paint.Align.RIGHT);
        float xLeft = 23.5f;

        canvas.drawText(key, x, y, textPaint);
    }

    private void drawTextWithValue(Canvas canvas, String key, float x, float y, float w, float h) {

        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(w);
        textPaint.setTypeface(Typeface.DEFAULT);
        //textPaint.setTextAlign(Paint.Align.RIGHT);
        float xLeft = 23.5f;

        canvas.drawText(key, x, y, textPaint);
    }


    private void drawLineToPoint(Canvas canvas, float x, float y, float w, float h, float radius) {

        Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        linePaint.setColor(Color.WHITE);
        linePaint.setStrokeWidth(w);
        float stopY = (float) (y + h);

        canvas.drawLine(x, y, x, stopY, linePaint);

        Paint pointPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        pointPaint.setColor(Color.WHITE);
        pointPaint.setStyle(Paint.Style.FILL);

        canvas.drawCircle(x, stopY, radius, pointPaint);

    }

    private void drawLine(Canvas canvas, float x, float y, float w, float h) {

        Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        linePaint.setColor(Color.WHITE);
        linePaint.setStrokeWidth((float) 1.0);

        canvas.drawLine(x, y, x, y + h, linePaint);

    }

    private void drawTangle(Canvas canvas, float x, float y, float w, float h) {

        Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DEV_KERN_TEXT_FLAG);
        linePaint.setStyle(Paint.Style.FILL);
        linePaint.setColor(Color.argb(40, 255, 255, 255));
        linePaint.setAntiAlias(true);
        RectF oval3 = new RectF(x, y, x + w, y + h);

        canvas.drawRoundRect(oval3, 6, 6, linePaint);

    }

    private void saveBitmap(Bitmap bit) {

        File saveDir = new File(FILE_SAVEPATH);
        if (!saveDir.exists()) {
            saveDir.mkdirs();
        }

        FileOutputStream out = null;

        try {
            out = new FileOutputStream(pathfile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //Toast.makeText(this,"保存失败",Toast.LENGTH_SHORT).show();

        }

        try {
            if (null != out) {
                bit.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            }
            //Toast.makeText(this,"保存成功",Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
            //Toast.makeText(this,"保存失败1",Toast.LENGTH_SHORT).show();

        }

    }
}

