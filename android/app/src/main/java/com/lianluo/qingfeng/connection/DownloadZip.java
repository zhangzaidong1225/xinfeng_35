package com.lianluo.qingfeng.connection;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class DownloadZip {

    private static final String TAG = "DownloadZip";
    private static final DownloadZip sInstance = new DownloadZip();
    private  File mFile,fileChild;
//    public static  String fileStr = "";


//    private ProgressReportingOutputStream mOutputStream;

    public static DownloadZip getInstance(){
        return sInstance;
    }

    private class GetMessage {
        String url;
        String out;
        String param;

        String opTag;
        Handler handler;
        Context mContext;

        GetMessage(Context mContext,String url, String param, String out,Handler handler, String opTag){
            this.url = url;
            this.param = param;
            this.out = out;

            this.handler = handler;
            this.opTag = opTag;
            this.mContext = mContext;
        }
    }

    public void getAsync(Context mContext,String url,String param,String out, Handler handler, String op_tag){
        new DownloadZipTask().execute(new GetMessage(mContext,url,param,out, handler, op_tag));
    }

    private class DownloadZipTask extends AsyncTask<GetMessage,Void,Long> {

        Handler handler;
        String opTag;
        Context mContext;
        URL mUrl;

        @Override
        protected Long doInBackground(GetMessage... messages) {
            GetMessage gm = messages[0];
            this.handler = gm.handler;
            this.opTag = gm.opTag;
            this.mContext = gm.mContext;
            Log.e(TAG, "url="+gm.url +"=param=="+ gm.param +"=out="+gm.out);

            try {
                mUrl = new URL(gm.url);
                String fileName = new File(mUrl.getFile()).getName();
                mFile = new File(gm.out);
                if (!mFile.exists()){
                    mFile.mkdirs();
                }
                 fileChild = new File(mFile,fileName);
//                fileStr = fileChild.getAbsolutePath();
                FilePathConstant.getInstance().downloadPath = fileChild.getAbsolutePath();

                Log.e(TAG, "out="+gm.out+", name="+fileName+",mUrl.getFile()="+mUrl.getFile()+"fileChild="+fileChild.getAbsolutePath());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            Long data = null;
            try {
                data = download(gm.url +  gm.param);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return data;

        }

        @Override
        protected void onPostExecute(Long result) {
            if(this.handler != null){
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString(HttpConstant.HTTP_OPR, this.opTag);
//                bundle.putLong(HttpConstant.HTTP_RESULT, result);
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        }

        private Long download(String path) throws Exception {

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            // From
            // https://www.washington.edu/itconnect/security/ca/load-der.crt
            InputStream caInput = mContext.getAssets().open("_.lianluo.com_bundle.crt");
            //InputStream caInput = mContext.getAssets().open("nginx.crt");

            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
            } finally {
                caInput.close();
            }

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory
                    .getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);


            URL url = new URL(path);

            HttpsURLConnection urlConnection = (HttpsURLConnection) url
                    .openConnection();
            //
            urlConnection.setConnectTimeout(5 * 1000);
            urlConnection.setRequestMethod("GET");
            //
            urlConnection.setSSLSocketFactory(context.getSocketFactory());
            long bytesCopied = 0;

            int length = urlConnection.getContentLength();

            if (fileChild.exists() && length == fileChild.length()){
                Log.e(TAG, "file "+fileChild.getName()+" already exits!!");
                return 0l;
            }
//            mOutputStream = new ProgressReportingOutputStream(mFile);
            FileOutputStream fos = new FileOutputStream(fileChild);

            bytesCopied = copy(urlConnection.getInputStream(),fos);
            if(bytesCopied != length && length!=-1){
                Log.e(TAG, "Download incomplete bytesCopied="+bytesCopied+", length"+length);
            }
            fos.close();

            return bytesCopied;
        }

        private long copy(InputStream input, OutputStream output){
            byte[] buffer = new byte[1024*8];
            BufferedInputStream in = new BufferedInputStream(input, 1024*8);
            BufferedOutputStream out  = new BufferedOutputStream(output, 1024*8);
            long count = 0L;
            int n = 0;

            try {
                while((n=in.read(buffer, 0, 1024*8))!=-1){
                    out.write(buffer, 0, n);
                    count+=n;
                }
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return count;
        }




    }

    private final class ProgressReportingOutputStream extends FileOutputStream {

        public ProgressReportingOutputStream(File file)
                throws FileNotFoundException {
            super(file);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void write(byte[] buffer, int byteOffset, int byteCount)
                throws IOException {
            // TODO Auto-generated method stub
            super.write(buffer, byteOffset, byteCount);
//                mProgress += byteCount;
//                publishProgress(mProgress);
        }

    }






}
