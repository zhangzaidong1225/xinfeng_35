package com.lianluo.qingfeng.mvc.Interface;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Created by zhangzaidong on 2016/9/13.
 */
public interface BleStatus {

    void connectedCallback(int result, String address);

    void disconnCallback(int result, String address);

    void devNotSupported(int result, String address);

    void devDataChange(int result, BluetoothGatt gatt, BluetoothGattCharacteristic bluetoothGattCharacteristic);

}
