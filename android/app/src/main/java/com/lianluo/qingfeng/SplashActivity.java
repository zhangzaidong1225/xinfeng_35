package com.lianluo.qingfeng;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.lianluo.qingfeng.connection.DownloadZip;
import com.lianluo.qingfeng.connection.FilePathConstant;
import com.lianluo.qingfeng.connection.HTTPGetImage;
import com.lianluo.qingfeng.connection.HTTPGetJSON;
import com.lianluo.qingfeng.connection.HttpConstant;
import com.lianluo.qingfeng.module.AdDataModule;
import com.lianluo.qingfeng.module.AdModule;
import com.lianluo.qingfeng.module.HotUploadDataModule;
import com.lianluo.qingfeng.module.HotUploadModule;
import com.lianluo.qingfeng.mvc.View.BluetoothUtil;
import com.lianluo.qingfeng.utils.FileUtil;
import com.lianluo.qingfeng.utils.SZTYSharedPreferences;
import com.lianluo.qingfeng.utils.SaveImageUtil;
import com.lianluo.qingfeng.utils.ZipUtil;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by louis on 16/4/21.
 */
public class SplashActivity extends Activity {

    private static final String TAG = "==M SplashActivity";

    class MyHandler extends Handler {
        WeakReference<Activity> mActivityReference;

        MyHandler(Activity activity) {
            mActivityReference = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final Activity activity = mActivityReference.get();
            if (activity != null) {
                Bundle bundle = msg.getData();
                if (bundle == null) {
                    return;
                }
                String opTag = bundle.getString(HttpConstant.HTTP_OPR);
                if (opTag == null) {
                    return;
                }

                if (opTag.equals(GET_AD_URI)) {
                    //second: download png from url which get from step first
                    downloadAd(bundle.getString(HttpConstant.HTTP_RESULT));
                    return;
                }
                if (opTag.equals(DOWNLOAD_AD)) {
                    saveAd(bundle.getByteArray(HttpConstant.HTTP_RESULT));
                }
                if (opTag.equals(HOT_UPLOAD)){
                    downloadZip(bundle.getString(HttpConstant.HTTP_RESULT));
                    return;
                }
                if (opTag.equals(DOWNLOAD_ZIP)){
                    Log.e(TAG,"===="+"下载完成");
/*                    String checkMd5 = FileUtil.verifyInstallPackage(DownloadZip.fileStr);
                    if (md5.equals(checkMd5)){
                        Log.e(TAG,"===="+"copy file");
                        File  src = new File(DownloadZip.fileStr);
                        final File copyFile = new File(hotFile,src.getName());

                        FileUtil.copyFileOrDirectory(DownloadZip.fileStr,hotFile);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ZipUtil.unzip(copyFile.getAbsolutePath(),hotFileSave);
//                                ZipUtil.traverseFolder2(hotFileSave);
                            }
                        }).start();

                        SZTYSharedPreferences.getInstance().putString(SZTYSharedPreferences.HOT_UPLOAD_VER,localVer);
                    }
                    else {
                        SZTYSharedPreferences.getInstance().putString(SZTYSharedPreferences.HOT_UPLOAD_VER,"");
                    }*/
                    String checkMd5 = FileUtil.verifyInstallPackage(FilePathConstant.getInstance().downloadPath);
                    if (md5.equals(checkMd5)){
                        Log.e(TAG,"===="+"copy file");
                        File  src = new File(FilePathConstant.getInstance().downloadPath);
                        //合成文件
                        final File copyFile = new File(FilePathConstant.getInstance().downloadPathCopy,src.getName());

                        FileUtil.copyFileOrDirectory(FilePathConstant.getInstance().downloadPath,FilePathConstant.getInstance().downloadPathCopy);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ZipUtil.unzip(copyFile.getAbsolutePath(),FilePathConstant.getInstance().unzipPath);
                                ZipUtil.traverseFolder2(FilePathConstant.getInstance().unzipPath);
                            }
                        }).start();

                        SZTYSharedPreferences.getInstance().putString(SZTYSharedPreferences.HOT_UPLOAD_VER,localVer);
                    }
                    else {
                        SZTYSharedPreferences.getInstance().putString(SZTYSharedPreferences.HOT_UPLOAD_VER,"");
                    }

                    return;
                }

            }
        }
    }

    public static int SPLASH_OVER = 0xff01;
    public static String GET_AD_URI = "get_ad_uri";
    public static String DOWNLOAD_AD = "download_ad";
    public static String HOT_UPLOAD = "hot_upload";
    public static String DOWNLOAD_ZIP = "download_zip";
    public static String md5 = "";
    public static String localVer = "";

    Handler mHandler = new Handler();
    private MyHandler mMyHandler = new MyHandler(SplashActivity.this);
    private WebView webView;
    private ImageView imageView;
//    private static String filePath = "";//获取当前程序路径
//    private  String hotFileTmp = "";//临时目录
//    private String hotFile = "";//复制路径
//    private String hotFileSave = "";//存储目录


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
       // ReactPreLoader.init(this, MainActivity.reactInfo);

        FilePathConstant.getInstance().init(SplashActivity.this);

//        filePath  = getApplicationContext().getFilesDir().getAbsolutePath();
//        hotFileTmp = filePath + "/hot_upload_tmp/";
//        hotFile = filePath + "/hot_upload/";
//        hotFileSave = filePath + "/hotupload/";
        Log.e("======",""+getApplicationContext().getFilesDir().getAbsolutePath());
        BluetoothUtil.copyFileFromAssets("dev_update.zip", getApplicationContext().getFilesDir().getAbsolutePath(),"/dev_update_33.zip");
        imageView = (ImageView) this.findViewById(R.id.splash_ad_image);
        if (!SZTYSharedPreferences.getInstance().getBoolean(SZTYSharedPreferences.FIRST_START,false)) {
            webView = (WebView) this.findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.equals("vida://directlyuse/")) {
                        SZTYSharedPreferences.getInstance().putBoolean(SZTYSharedPreferences.FIRST_START, true);
                        goMainActivity();
                        return true;
                    } else {
                        return false;
                    }

                }
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                //透明状态栏
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                //透明导航栏
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

                webView.loadUrl("file:///android_asset/guide/guide.html");
            } else {
                webView.loadUrl("file:///android_asset/guide_no_navigation/guide_no_navigation.html");
            }

            delayDisplayWebView();

        }   else {
            delayGoMainActivity();
        }

        //ReactInstanceManagerHelper.getInstance(SplashActivity.this, getApplication());
        HttpConstant.versionName = getVersion();

        getAdUri();
    }

    private void delayDisplayWebView() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                webView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
            }
        }, 5000);
    }

    private void delayGoMainActivity() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                goMainActivity();
            }
        }, 5000);
    }

    private void goMainActivity() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), MainActivity.class);
        intent.setFlags(SPLASH_OVER);
        startActivity(intent);
        this.finish();
    }

    private void getAdUri() {

        //first: get url from ad_uri
        String isTestXinfeng = SZTYSharedPreferences.getInstance().getString("isTestXinfeng", "忻风");
        if (isTestXinfeng.equals("忻风")){
            HTTPGetJSON.getInstance().getAsync(SplashActivity.this,HttpConstant.AD_URI, "", mMyHandler, GET_AD_URI);
        } else {
            HTTPGetJSON.getInstance().getAsync(SplashActivity.this,HttpConstant.AD_URI_TEST, "", mMyHandler, GET_AD_URI);
            HTTPGetJSON.getInstance().getAsync(SplashActivity.this,HttpConstant.HOT_UPLOAD_URI_TEST,"",mMyHandler,HOT_UPLOAD);
        }
        //Log.e("SplashActivity", "getAdUri");
        //third: save the png data to the path

    }

    private void downloadZip(String rslt) {
//        Log.e(TAG, "downloadZip");
        if (rslt == null) {
            return;
        }
        HotUploadModule hotUploadModule;
        Log.e(TAG, "downloadZip---"+rslt);

        try {
            hotUploadModule = new Gson().fromJson(rslt, HotUploadModule.class);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getMessage());
            return;
        }
        HotUploadDataModule hotUploadDataModule = hotUploadModule.getData();

        localVer = hotUploadDataModule.getVer();

        String url = hotUploadDataModule.getUrl();
        md5 = hotUploadDataModule.getMd5();
        String server_ver = hotUploadDataModule.getVer();
        Log.e(TAG, "downloadZip------"+server_ver);
        if (url == null) {
            return;
        }

        Log.e(TAG, "downloadZip------"+url);
        String version = SZTYSharedPreferences.getInstance().getString(SZTYSharedPreferences.HOT_UPLOAD_VER, "");
        Log.e(TAG, "downloadZip------"+version);
        Log.e(TAG, "downloadZip------"+server_ver.compareTo(version));
        File tmpFile = new File(FilePathConstant.getInstance().tmpFilePath);
        File tmpSaveFile = new File(FilePathConstant.getInstance().downloadPathCopy);
        File tmpSaveFileEnd = new File(FilePathConstant.getInstance().unzipPath);


        if (version.isEmpty() || version.equals("")){

            if (tmpFile.exists()){
                Log.e(TAG, "exists-----"+version);
                FileUtil.deleteDir(tmpFile);
                FileUtil.deleteDir(tmpSaveFile);
                FileUtil.deleteDir(tmpSaveFileEnd);
            }
            //download
            DownloadZip.getInstance().getAsync(SplashActivity.this,url,"",FilePathConstant.getInstance().tmpFilePath,mMyHandler,DOWNLOAD_ZIP);
        }

        if (server_ver.compareTo(version) > 0 && !version.isEmpty()){
            if (tmpFile.exists()){
                Log.e(TAG, "exists-----"+version);
                FileUtil.deleteDir(tmpFile);
                FileUtil.deleteDir(tmpSaveFile);
                FileUtil.deleteDir(tmpSaveFileEnd);
            }
            //download
            DownloadZip.getInstance().getAsync(SplashActivity.this,url,"",FilePathConstant.getInstance().tmpFilePath,mMyHandler,DOWNLOAD_ZIP);
        }
    }

    private void saveAd(byte[] data) {
        Log.e(TAG, "saveAd");
        if (data == null) {
            return;
        }

        new SaveImageUtil(data, "rctad", null).start();
        Log.e(TAG, "saveAd-----------");
    }

    private void downloadAd(String rslt){
        Log.e(TAG, "downloadAd");
        if (rslt == null) {
            return;
        }
        AdModule adModule;
        Log.e(TAG, "downloadAd---"+rslt);

        try {
            adModule = new Gson().fromJson(rslt, AdModule.class);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getMessage());
            return;
        }
        AdDataModule adDataModule = adModule.getData();
        if (adDataModule == null) {
            File dir = new File(SaveImageUtil.ALBUM_PATH);
            if (dir.exists()) {
                new SaveImageUtil().deleteDir(dir);
            }
            return;
        }

        String url = adDataModule.getUrl();
        if (url == null) {
            return;
        }
        Log.e(TAG, "downloadAd------"+url);
        SZTYSharedPreferences.getInstance().putString(SZTYSharedPreferences.AD_ACTION, adDataModule.getAction());
        String adUrl = SZTYSharedPreferences.getInstance().getString(SZTYSharedPreferences.AD_URL, "");
        boolean isLoad = SZTYSharedPreferences.getInstance().getBoolean(SZTYSharedPreferences.IS_LOAD, false);
        if (adUrl.equals(url) && isLoad) {
            return;
        }
        SZTYSharedPreferences.getInstance().putString(SZTYSharedPreferences.AD_URL, url);
        SZTYSharedPreferences.getInstance().putBoolean(SZTYSharedPreferences.IS_LOAD, false);
        HTTPGetImage.getInstance().getAsync(SplashActivity.this,url, "", mMyHandler, DOWNLOAD_AD);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }


    /**
     * 获取版本号
     * @return 当前应用的版本号
     */
    public String getVersion() {
        String version = "1.2.0";
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            version = info.versionName;

            return version;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }
}
