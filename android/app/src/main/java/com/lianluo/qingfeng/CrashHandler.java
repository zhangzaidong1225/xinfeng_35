package com.lianluo.qingfeng;

/**
 * Created by louis on 16/3/28.
 */
import android.content.Context;
import android.util.Log;

import java.lang.Thread.UncaughtExceptionHandler;


public class CrashHandler implements UncaughtExceptionHandler {
    public static final String TAG = CrashHandler.class.getSimpleName();
    private static CrashHandler INSTANCE = new CrashHandler();
    private Thread.UncaughtExceptionHandler mDefaultHandler;


    private CrashHandler() {

    }


    public static CrashHandler getInstance() {
        return INSTANCE;
    }

    public void init(Context ctx) {
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        ex.printStackTrace();
        Log.e("qingfeng", "CrashHandler.uncaughtException 昕风异常捕捉", ex);
        try{
            Thread.sleep(1000);
        }catch (Exception e){}

        System.exit(0);


        //ZhongSouActivityMgr.getInstance().errorExit();
    }
}
