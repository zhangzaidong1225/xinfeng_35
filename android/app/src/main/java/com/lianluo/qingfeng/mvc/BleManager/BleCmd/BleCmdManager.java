package com.lianluo.qingfeng.mvc.BleManager.BleCmd;

import android.os.Handler;
import android.os.Message;

import com.lianluo.qingfeng.mvc.Modal.BleDevManager;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by zhangzaidong on 2016/9/13.
 */
public class BleCmdManager {

    private final static String TAG = "BleCmdManager";


    public static volatile BleCmdManager mInstance;

    private ManagerThread thread;
    private boolean stopFlag = false;

    public ConcurrentHashMap<String, DevMsgManager> map = new ConcurrentHashMap<>();

    public Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg == null) {
                return;
            }
            DevMsg devMsg = (DevMsg) msg.obj;

            switch (msg.what) {
                case DevMsgAction.WindSpeed:
                    if (BleDevManager.getInstance().isConnected(devMsg.address)) {
                        BleCmd.getInstance().windSpeed(devMsg.address, devMsg.speed);
                    }
                    break;
                case DevMsgAction.DevCheck:
                    if (BleDevManager.getInstance().isConnected(devMsg.address)) {
                        BleCmd.getInstance().devCheck(devMsg.address);

                    }
                    break;
                case DevMsgAction.DevRename:
                    if (BleDevManager.getInstance().isConnected(devMsg.address)) {
                        BleCmd.getInstance().devRename(devMsg.address, devMsg.name);

                    }
                    break;
                case DevMsgAction.Disconnect:
                    if (BleDevManager.getInstance().isConnected(devMsg.address)) {

                        BleCmd.getInstance().disconnectConn(devMsg.address);
                    }
                    break;
                case DevMsgAction.ShutDown:
                    if (BleDevManager.getInstance().isConnected(devMsg.address)) {
                        BleCmd.getInstance().shutDown(devMsg.address);
                    }
                    break;
                case DevMsgAction.GetName:
                    if (BleDevManager.getInstance().isConnected(devMsg.address)) {
                        BleCmd.getInstance().getName(devMsg.address);
                    }
                    break;
                default:
                    break;

            }

        }
    };

    public static BleCmdManager getInstance() {
        if (mInstance == null) {
            synchronized (BleCmdManager.class){
                if (null == mInstance){
                    mInstance = new BleCmdManager();
                }
            }
        }
        return mInstance;
    }

    private BleCmdManager() {
        thread = new ManagerThread();
        thread.start();
    }

    public void stopThread() {
        this.stopFlag = true;
    }

    private class ManagerThread extends Thread {

        @Override
        public void run() {
            super.run();
            while (!stopFlag) {
                synchronized (this) {

                    for (Iterator iter = map.entrySet().iterator(); iter.hasNext(); ) {
                        Map.Entry element = (Map.Entry) iter.next();
                        DevMsgManager msgManager = (DevMsgManager) element.getValue();
                        if (!msgManager.isEmpty()) {
                            DevMsg predevMsg = msgManager.peekOneMsg();
                            DevMsg devMsg = predevMsg;

                            if (predevMsg.action == DevMsgAction.WindSpeed) {
                                while (predevMsg.action == DevMsgAction.WindSpeed) {
                                    devMsg = msgManager.pollOneMsg();
                                    if (!msgManager.isEmpty()) {
                                        predevMsg = msgManager.peekOneMsg();
                                    } else {
                                        break;
                                    }
                                }
                            } else {
                                devMsg = msgManager.pollOneMsg();
                            }
                            handler.obtainMessage(devMsg.action, devMsg).sendToTarget();
                        }
                    }
                    try {
                        wait(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void addMsg2Manager(String address, String name, int action, int speed) {
        if (map.containsKey(address)) {
            map.get(address).addOneMsg(address, name, action, speed);
        } else {
            map.put(address, new DevMsgManager(address));
            map.get(address).addOneMsg(address, name, action, speed);
        }

        synchronized (thread) {
            if (thread.getState() == Thread.State.WAITING) {
                thread.notify();
            }
        }
    }


}
