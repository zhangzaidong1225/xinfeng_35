package com.lianluo.qingfeng.mvc.Modal.Bean;

/**
 * Created by zhangzaidong on 2016/9/12.
 */
public class BaseDevice {

    String name;
    String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
