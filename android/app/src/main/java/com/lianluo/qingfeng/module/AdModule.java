package com.lianluo.qingfeng.module;

/**
 * Created by louis on 16/4/25.
 */
public class AdModule {
    private String code;
    private String msg;
    private AdDataModule data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public AdDataModule getData() {
        return data;
    }

    public void setData(AdDataModule data) {
        this.data = data;
    }
}
