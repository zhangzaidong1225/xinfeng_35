package com.lianluo.qingfeng.mvc.View;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.lianluo.qingfeng.BlueTooth.BleApplication;
import com.lianluo.qingfeng.mvc.BleManager.BleManager;
import com.lianluo.qingfeng.mvc.BleManager.UartService.UartService;
import com.lianluo.qingfeng.mvc.Modal.Bean.XinfengDev;
import com.lianluo.qingfeng.mvc.Modal.BleDevManager;
import com.lianluo.qingfeng.mvc.Modal.DeviceMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by zhangzaidong on 2016/9/12.
 */
public class BluetoothUtil extends ReactContextBaseJavaModule {

    public static final String TAG = "BluetoothUtil";

    public DeviceMap mdeviceMap = new DeviceMap();
    public static boolean scan_state = false;
    public static String mAddress;

    public static ReactApplicationContext reactApplicationContext;


    public BluetoothUtil(ReactApplicationContext reactContext) {
        super(reactContext);
        reactApplicationContext = reactContext;
        BleApplication.bluetoothUtil = this;
        LocalBroadcastManager.getInstance(getReactApplicationContext()).registerReceiver(UartStatusChangeReceiver, makeGattUpdateIntentFilter());

    }

    @Override
    public String getName() {
        return "BluetoothUtil";
    }

    public static ReactApplicationContext getRRRContext() {
        return reactApplicationContext;
    }

    @ReactMethod
    public void auto_connect() {
        BleManager.getInstance().auto_connect();
    }

    @ReactMethod
    public void sendMydevice() {

        WritableArray writableArray = Arguments.createArray();
        Iterator iter = mdeviceMap.map.entrySet().iterator();

        while(iter.hasNext()){

            Map.Entry entry = (Map.Entry) iter.next();

            XinfengDev device = (XinfengDev) entry.getValue();

            WritableMap writableMap = Arguments.createMap();
            writableMap.putInt("use_time", device.getUse_time());
            writableMap.putInt("use_time_this", device.getUse_time_this());
            writableMap.putInt("speed", device.getSpeed());
            writableMap.putInt("battery_power", device.getBattery_power());
            writableMap.putInt("isconnectd", device.getIsConnected());
            writableMap.putBoolean("ismatchd", device.ismatched());
            writableMap.putBoolean("autoConnect", device.isAutoConnect());
            writableMap.putString("addr", device.getAddress());
            writableMap.putString("name", device.getName());
            writableMap.putInt("version", device.getVersion());
            writableMap.putInt("percentage", device.getPercentage());
            writableMap.putInt("Pm25", device.getPm25());
            writableMap.putInt("update_state", device.getUpdate_state());
            writableMap.putInt("ischarged", device.getBattery_charge());
            writableMap.putInt("wind_time", device.getWind_use_time_this());
            writableMap.putInt("battery_over", device.getBattery_over());
            writableMap.putInt("filter_in", device.getFilter_in());
            writableMap.putInt("filter_number", device.getFilter_number());
            writableMap.putInt("filter_use_time_this", device.getFilter_use_time_this());
            writableMap.putInt("filters_maxfan_usetime", device.getFilters_maxfan_usetime());
            writableArray.pushMap(writableMap);
        }

        WritableMap params = Arguments.createMap();
        params.putBoolean("scan_state",scan_state);
        params.putArray("dev_list",writableArray);
        BluetoothUtil.getRRRContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("ble_state",params);
    }

    @ReactMethod
    public void scanner() {
        BleManager.getInstance().scan();
    }

    @ReactMethod
    public void stopScan() {
        BleManager.getInstance().stopScan();
    }

    @ReactMethod
    public void stopConnect(String address) {
        BleManager.getInstance().stopConnect(address);
    }

    @ReactMethod
    public void disconnect(String address) {
        BleManager.getInstance().disConnect(address);

    }

    @ReactMethod
    public void cancelBond(String address) {
        BleManager.getInstance().cancelBonded(address);

    }

    @ReactMethod
    public void connectDevice(String address) {
        BleManager.getInstance().connectDevice(address);
    }

    @ReactMethod
    public void shutDown(String address) {
        BleManager.getInstance().shutdown(address);
    }

    @ReactMethod
    public void windSpeed(String address, int speed) {
        BleManager.getInstance().windSpeed(address, speed);
    }

    @ReactMethod
    public void devCheck(String address) {
        BleManager.getInstance().devCheck(address);
    }

    @ReactMethod
    public void devRename(String address, String rename) {
        BleManager.getInstance().devRename(address, rename);
    }

    @ReactMethod
    public void verifyInstallPackage(String packagePath, final Callback callback1) {
        try {
            MessageDigest sig = MessageDigest.getInstance("MD5");
            File packageFile = new File(packagePath);
            InputStream signedData = new FileInputStream(packageFile);
            byte[] buffer = new byte[4096];
            long toRead = packageFile.length();
            long soFar = 0;
            boolean interrupted = false;

            while (soFar < toRead) {
                interrupted = Thread.interrupted();
                if (interrupted) break;
                int read = signedData.read(buffer);
                soFar += read;
                sig.update(buffer, 0, read);
            }

            final byte[] digest = sig.digest();
            String digestStr = bytesToHexString(digest);
            digestStr = digestStr.toLowerCase();
            Log.e(TAG, "生成的MD5------" + digestStr);
            callback1.invoke(digestStr);

        } catch (Exception e) {
            Log.e(TAG, "CRC Error");
            callback1.invoke(false);
        }

    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        int i = 0;
        while (i < src.length) {
            int v;
            String hv;
            v = (src[i] >> 4) & 0x0F;
            hv = Integer.toHexString(v);
            stringBuilder.append(hv);

            v = src[i] & 0x0F;
            hv = Integer.toHexString(v);
            stringBuilder.append(hv);
            i++;
        }
        return stringBuilder.toString();
    }

    @ReactMethod
    public void deviceUpdate(String address, String name, String path) {
        BleManager.getInstance().deviceUpdate(address, name, path);
    }
    //*********************文件操作*******//

    public static File getFilePath(String filePath,
                                   String fileName) {
        File file = null;
        makeRootDirectory(filePath);
        try {
            file = new File(filePath + fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void makeRootDirectory(String filePath) {
        File file = null;
        try {
            file = new File(filePath);
            if (!file.exists()) {
                file.mkdir();
            }
        } catch (Exception e) {

        }
    }

    public static void copyFileFromAssets(String name, String filePath, String fileName) {
        boolean flag = false;
        FileOutputStream fos;
        try {
            InputStream is = BleApplication.getInstance().getApplicationContext().getAssets().open("dev_update.zip");
            Log.e("======", "is  " + is);

            File file = getFilePath(filePath, fileName);
            if (file.exists()) {
                flag = true;
                Log.e("======", "file.exists  " + file.exists());
            }
            if (!flag) {
                Log.e("======", " copying  " + file);
                fos = new FileOutputStream(file);
                Log.e("======", "fos  " + fos);
                byte[] buffer = new byte[1024];
                int count = 0;
                while (true) {
                    count++;
                    int len = is.read(buffer);
                    if (len == -1) {
                        break;
                    }
                    fos.write(buffer, 0, len);
                }
                fos.flush();
                fos.close();
            }
            is.close();

        } catch (Exception e) {
            Log.e("======", "err  " + e.getMessage());
        }
    }


    public BroadcastReceiver UartStatusChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            final Intent mIntent = intent;
            mdeviceMap = mIntent.getParcelableExtra("map");
            scan_state = mIntent.getBooleanExtra("scan_state", false);
            mAddress = mIntent.getStringExtra("device_address");

            switch (action) {
                case UartService.ADD_DEVICE:
                case UartService.CLEAR_DATA_1:
                case UartService.START_SCAN:
                case UartService.STOP_SCAN:
                case UartService.RENAME:
                case UartService.CANCEL_BONDED:
                case UartService.ACTION_CLOSE_GATT:
                case UartService.ACTION_CONNECTING:
                case UartService.ACTION_DFU_UPDATE:
                    sendMydevice();
                    break;
                case UartService.ACTION_GATT_CONNECTED:
                    BleDevManager.getInstance().stopTimer(mAddress);
                    sendMydevice();
                    break;
                case UartService.ACTION_GATT_DISCONNECTED:
                    BleDevManager.getInstance().stopTimer(mAddress);
                    sendMydevice();
                    break;
                case UartService.ACTION_DATA_AVAILABLE:
                    sendMydevice();
                    break;
                case UartService.DEVICE_DOES_NOT_SUPPORT_UART:
                    BleManager.getInstance().disConnect(mAddress);
                    BleDevManager.getInstance().stopTimer(mAddress);
                    break;
                default:
                    break;
            }
        }
    };

    public IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.ADD_DEVICE);
        intentFilter.addAction(UartService.START_SCAN);
        intentFilter.addAction(UartService.STOP_SCAN);
        intentFilter.addAction(UartService.CLEAR_DATA_1);
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.RENAME);
        intentFilter.addAction(UartService.CANCEL_BONDED);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        intentFilter.addAction(UartService.ACTION_CONNECTING);
        intentFilter.addAction(UartService.ACTION_CLOSE_GATT);
        intentFilter.addAction(UartService.ACTION_DFU_UPDATE);

        return intentFilter;
    }


}
