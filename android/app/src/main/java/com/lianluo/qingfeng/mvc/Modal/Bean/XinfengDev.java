package com.lianluo.qingfeng.mvc.Modal.Bean;

/**
 * Created by zhangzaidong on 2016/9/12.
 */
public class XinfengDev extends BaseDevice {

    //虑棉使用时长
    private int use_time;
    //本次滤棉使用时长
    private int use_time_this;
    //本次滤棉使用总时长
    private int filter_use_time_this;
    //滤棉折合全风速的使用量
    private int filters_maxfan_usetime;
    //风扇等级
    private  int speed;
    //电量
    private int battery_power;
    //连接状态
    private int isConnected;
    //配对
    private boolean ismatched;
    //自动连接
    private boolean autoConnect;
    //版本
    private int version;
    //升级百分比
    private  int percentage;
    //Pm2.5
    private int Pm25;
    //失败信息
    //101---升级成功
    //102---升级失败
    //103---升级中
    //104---升级开始
    private int update_state;
    //充电标志
    private int battery_charge;
    //本次风扇使用时长
    private  int wind_use_time_this;
    //滤棉在位
    private int filter_in;
    //电压过低
    private int battery_over;
    //滤棉使用个数
    private  int filter_number;

    public int getFilter_use_time_this() {
        return filter_use_time_this;
    }

    public void setFilter_use_time_this(int filter_use_time_this) {
        this.filter_use_time_this = filter_use_time_this;
    }

    public int getUse_time() {
        return use_time;
    }

    public void setUse_time(int use_time) {
        this.use_time = use_time;
    }

    public int getUse_time_this() {
        return use_time_this;
    }

    public void setUse_time_this(int use_time_this) {
        this.use_time_this = use_time_this;
    }

    public int getFilters_maxfan_usetime() {
        return filters_maxfan_usetime;
    }

    public void setFilters_maxfan_usetime(int filters_maxfan_usetime) {
        this.filters_maxfan_usetime = filters_maxfan_usetime;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getBattery_power() {
        return battery_power;
    }

    public void setBattery_power(int battery_power) {
        this.battery_power = battery_power;
    }

    public int getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

    public boolean ismatched() {
        return ismatched;
    }

    public void setIsmatched(boolean ismatched) {
        this.ismatched = ismatched;
    }

    public boolean isAutoConnect() {
        return autoConnect;
    }

    public void setAutoConnect(boolean autoConnect) {
        this.autoConnect = autoConnect;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public int getPm25() {
        return Pm25;
    }

    public void setPm25(int pm25) {
        Pm25 = pm25;
    }

    public int getUpdate_state() {
        return update_state;
    }

    public void setUpdate_state(int update_state) {
        this.update_state = update_state;
    }

    public int getBattery_charge() {
        return battery_charge;
    }

    public void setBattery_charge(int battery_charge) {
        this.battery_charge = battery_charge;
    }

    public int getWind_use_time_this() {
        return wind_use_time_this;
    }

    public void setWind_use_time_this(int wind_use_time_this) {
        this.wind_use_time_this = wind_use_time_this;
    }

    public int getFilter_in() {
        return filter_in;
    }

    public void setFilter_in(int filter_in) {
        this.filter_in = filter_in;
    }

    public int getBattery_over() {
        return battery_over;
    }

    public void setBattery_over(int battery_over) {
        this.battery_over = battery_over;
    }

    public int getFilter_number() {
        return filter_number;
    }

    public void setFilter_number(int filter_number) {
        this.filter_number = filter_number;
    }
}
