/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import styles from './styles/style_setting'
import styles_feedback from './styles/style_feedback'
import styles_selectList from './styles/style_selectList'
import  config from './config'

import style_register from './styles/style_register'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
import Language from './Language'
var TimerMixin = require('react-timer-mixin');
var UMeng = require('./UMeng');


import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableHighlight,
  ToastAndroid,
  NetInfo,
  WebView,
  AppRegistry,
  IntentAndroid,
  Navigator,
  LinkingIOS,
  Platform,
} from 'react-native';

var wait=60;
var iscount=true;
var deviceWidth=Dimensions.get('window').width;
var deviceHeight=Dimensions.get('window').height;
var DEFAULT_URL = config.server_base+config.privilege_uri+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;


var Privilege = React.createClass({
   mixins: [TimerMixin],
	
  getInitialState(){
    return {
      status:'No Page Loaded',
      loaded:false,
      url:'',
      number:1,
      next:'',
      action:'',
    }
  },
  fetchData:function (){
      console.log('DEFAULT_URL' + DEFAULT_URL);
      fetch (DEFAULT_URL)
        .catch((error) => {
          // ToastAndroid.show(''+error,ToastAndroid.SHORT);
        });
    },
  render(){
    console.log('-----------------------------------------------');
    console.log('DEFAULT_URL' + DEFAULT_URL);
  	var navigator = this.props.navigator;
        return (
    	     <View
            style = {style_register.container}>
            <View style={styles_feedback.container}>
                <TouchableOpacity style = {styles.touch_view}
                  onPress={() => {this.props.navigator.pop();UMeng.onEvent('privilege_01');}}>
                  <Image style={[styles.image,{resizeMode: 'stretch'}]} source={require('image!ic_back')}/>
                </TouchableOpacity>
                <Text allowFontScaling={false} style={[styles.font3,styles.text1]}>{Language.discount}</Text>
                <View style={styles.image}/>
            </View>
            <View style={style_register.privilege_view}>
                <WebView  url={DEFAULT_URL} style={style_register.privilege_view}></WebView>
            </View>
        </View>
        )
},
});
module.exports = Privilege;
