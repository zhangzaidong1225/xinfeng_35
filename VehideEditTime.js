'use strict'

var TimerMixin = require ('react-timer-mixin');
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import tools from './tools'


import Title from './title'
var BlueToothUtil= require('./BlueToothUtil');

import  WheelTimePicker from './WheelTimePicker'
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    styles_sliderheet,
    Text,
    View,
    PanResponder,
    Image,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    DeviceEventEmitter,
    NativeModules,
    ToastAndroid,
    NativeAppEventEmitter,
    Platform,
    Switch,
    PixelRatio,
} from 'react-native';


var deviceWidth  = Dimensions.get ('window').width;
var deviceHeight = Dimensions.get ('window').height;
var tmp01 = 0x01;
var tmp02 = 0x02;
var tmp03 = 0x04;
var tmp04 = 0x08;
var tmp05 = 0x10;
var tmp06 = 0x20;
var tmp07 = 0x40;

var monday_back,monday_txt;
var tuesday_back,tuesday_txt;
var wednesday_back,wednesday_txt;
var thursday_back,thursday_txt;
var friday_back,friday_txt;
var saturday_back,saturday_txt;
var sunday_back,sunday_txt;


var dayArray2 ;

var sub_time = null;


var VehideEditTime = React.createClass({

    getInitialState(){

        return {
            address:'',
            hour:'01',
            minute:'01',
            order:'',
            week:'',
            monday:false,
            tuesday:false,
            wednesday:false,
            thursday:false,
            friday:false,
            saturday:false,
            sunday:false,
            
        }
    },

      //组件默认props值
  getDefaultProps: function() {
        return {
          time: '',

        };
   },

    componentWillMount:function() {

        dayArray2 = new Array();
        for (var i = 0;i< 7;i++){
            dayArray2[i] = false;
        }

        
    console.log('星期``7``'+this.props.time.hour+this.props.time.minute+this.props.time.order+this.props.time.week);

        this.setState({
            hour:this.props.time.hour,
            minute:this.props.time.minute,
            order:this.props.time.order,
            week:this.props.time.week,
            address:this.props.address,
        });
    },


    _parseWeek:function(week:String){

        console.log('星期``3``'+week);
        //var str = '周一 周二 周三 周四';
        //var num = '0,1,2,5';
        //var strArray = str.split(' ');
        if (week.indexOf(',') == -1){
        console.log('星期``3``bbb');
            return;
        }
        var numArray = week.split(',');

        console.log('星期``4``'+numArray.length);

        for (var i = 0; i < numArray.length-1; i++){
             console.log('星期``5``'+numArray[i]);
            dayArray2.splice(numArray[i],1,true);
        }
            this.setState({
                monday:dayArray2[0],
                tuesday:dayArray2[1],
                wednesday:dayArray2[2],
                thursday:dayArray2[3],
                friday:dayArray2[4],
                saturday:dayArray2[5],
                sunday:dayArray2[6],
            });

            dayArray2.length = 0;

    },

    componentDidMount:function(){

        

        this._parseWeek(this.state.week);

        sub_time = RCTDeviceEventEmitter.addListener('wheel_time_picker',(val) => {
            console.log('星期``wheel_time_picker```'+val.hour +'````'+val.minute);


            if (val != null){
                this.setState({
                    hour:val.hour,
                    minute:val.minute,
                });
            }

        });
    },

    componentWillUnmount:function() {
        sub_time.remove();
    },

    render:function() {
        var navigator = this.props.navigator;

        monday_back = this.state.monday ? '#2CB7ED' :'#E1E1E1';
        monday_txt  = this.state.monday ? '#FFFFFF' :'#E1E1E1';

        tuesday_back = this.state.tuesday ? '#2CB7ED' :'#E1E1E1';
        tuesday_txt  = this.state.tuesday ? '#FFFFFF' :'#E1E1E1';

        wednesday_back = this.state.wednesday ? '#2CB7ED' :'#E1E1E1';
        wednesday_txt  = this.state.wednesday ? '#FFFFFF' :'#E1E1E1';

        thursday_back = this.state.thursday ? '#2CB7ED' :'#E1E1E1';
        thursday_txt  = this.state.thursday ? '#FFFFFF' :'#E1E1E1';

        friday_back = this.state.friday ? '#2CB7ED' :'#E1E1E1';
        friday_txt  = this.state.friday ? '#FFFFFF' :'#E1E1E1';

        saturday_back = this.state.saturday ? '#2CB7ED' :'#E1E1E1';
        saturday_txt  = this.state.saturday ? '#FFFFFF' :'#E1E1E1';

        sunday_back = this.state.sunday ? '#2CB7ED' :'#E1E1E1';
        sunday_txt  = this.state.sunday ? '#FFFFFF' :'#E1E1E1';


/*        if (!this.state.monday){
           monday_back = '#E1E1E1';
           monday_txt = '#E1E1E1';

        }else {
            monday_back = '#2CB7ED';
            monday_txt = '#FFFFFF';
        }

        if (!this.state.tuesday){
           tuesday_back = '#E1E1E1';
           tuesday_txt = '#E1E1E1';
        }else {
            tuesday_back = '#2CB7ED';
           tuesday_txt = '#FFFFFF';
        }

        if (!this.state.wednesday){
            wednesday_back = wednesday_txt = '#E1E1E1';
        }else {
            wednesday_back = '#2CB7ED';
            wednesday_txt = '#FFFFFF';

        }

        if (!this.state.thursday){
            thursday_back = thursday_txt = '#E1E1E1';
        }else {
            thursday_back = '#2CB7ED';
            thursday_txt = '#FFFFFF';
        }

        if (!this.state.friday){
            friday_back = friday_txt = '#E1E1E1';
        }else {
            friday_back = '#2CB7ED';
            friday_txt = '#FFFFFF';
        }

        if (!this.state.saturday){
            saturday_back = saturday_txt = '#E1E1E1';
        }else {
            saturday_back = '#2CB7ED';
            saturday_txt = '#FFFFFF';
        }

        if (!this.state.sunday){
            sunday_back = sunday_txt = '#E1E1E1';
        }else {
            sunday_back = '#2CB7ED';
            sunday_txt = '#FFFFFF';
        }*/

        return (
            <View style = {{flex:1,backgroundColor:'#FFFFFF'}}>
                <Title title = {Language.edit} hasGoBack = {true}  navigator = {this.props.navigator}/>

                <View style = {{width:deviceWidth,backgroundColor:'white',height:deviceHeight * 1/4, }}>
                    <WheelTimePicker hour = {this.props.time.hour} minute={this.props.time.minute} showFlag = {true}/>
                </View>

                <View style = {{height:20,marginTop:20,marginLeft:10,}}>
                    <Text allowFontScaling={false} style = {{color:'#2CB7ED',fontSize:16,textAlign:'auto'}}>{Language.repeatevery}</Text>
                </View>

                <View style = {{marginTop:15,marginRight:10,marginLeft:10,flexDirection:'column',height:100}}>
                    <View style = {{flex:1,flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between'}}>
                        <TouchableOpacity
                            onPress = {() => {this._tran2Monday(this.state.monday)}}>
                        <View style = {{width:55,height:45,backgroundColor:monday_back,alignItems:'center',justifyContent:'center',
                                            borderWidth:1/PixelRatio.get(),borderColor:'#E1E1E1',borderRadius:6}}>

                            <View style = {{}}>
                                <Text allowFontScaling={false} style = {{color:'#FFFFFF',fontSize:16,textAlign:'center'}}>{Language.mon}</Text>
                            </View>

                            <View style = {{position:'absolute',left:43,bottom:0}}>
                                <Text allowFontScaling={false} style = {{color:monday_txt,fontWeight:'normal',fontSize:10,textAlign:'center'}}>{'√'}</Text>
                            </View>
                        </View>
                        </TouchableOpacity>

                       <TouchableOpacity
                            onPress = { () => {this._tran2Tuesday(this.state.tuesday)}}>
                        <View style = {{width:55,height:45,backgroundColor:tuesday_back,alignItems:'center',justifyContent:'center',
                                            borderWidth:1/PixelRatio.get(),borderColor:'#E1E1E1',borderRadius:6}}>

                            <View style = {{}}>
                                <Text allowFontScaling={false} style = {{color:'#FFFFFF',fontSize:16,textAlign:'center'}}>{Language.tue}</Text>
                            </View>

                            <View style = {{position:'absolute',left:43,bottom:0}}>
                                <Text allowFontScaling={false} style = {{color:tuesday_txt,fontWeight:'normal',fontSize:10,textAlign:'center'}}>{'√'}</Text>
                            </View>
                        </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress = {() => {this._tran2Wednesday(this.state.wednesday)}}>
                        <View style = {{width:55,height:45,backgroundColor:wednesday_back,alignItems:'center',justifyContent:'center',
                                            borderWidth:1/PixelRatio.get(),borderColor:'#E1E1E1',borderRadius:6}}>

                            <View style = {{}}>
                                <Text allowFontScaling={false} style = {{color:'#FFFFFF',fontSize:16,textAlign:'center'}}>{Language.wed}</Text>
                            </View>

                            <View style = {{position:'absolute',left:43,bottom:0}}>
                                <Text allowFontScaling={false} style = {{color:wednesday_txt,fontWeight:'normal',fontSize:10,textAlign:'center'}}>{'√'}</Text>
                            </View>
                        </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress = {() => {this._tran2Thursday(this.state.thursday)}}>
                        <View style = {{width:55,height:45,backgroundColor:thursday_back,alignItems:'center',justifyContent:'center',
                                            borderWidth:1/PixelRatio.get(),borderColor:'#E1E1E1',borderRadius:6}}>

                            <View style = {{}}>
                                <Text allowFontScaling={false} style = {{color:'#FFFFFF',fontSize:16,textAlign:'center'}}>{Language.thu}</Text>
                            </View>

                            <View style = {{position:'absolute',left:43,bottom:0}}>
                                <Text allowFontScaling={false} style = {{color:thursday_txt,fontWeight:'normal',fontSize:10,textAlign:'center'}}>{'√'}</Text>
                            </View>
                        </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress = {() => {this._tran2Friday(this.state.friday)}}>
                              <View style = {{width:55,height:45,backgroundColor:friday_back,alignItems:'center',justifyContent:'center',
                                            borderWidth:1/PixelRatio.get(),borderColor:'#E1E1E1',borderRadius:6}}>

                            <View style = {{}}>
                                <Text allowFontScaling={false} style = {{color:'#FFFFFF',fontSize:16,textAlign:'center'}}>{Language.fri}</Text>
                            </View>

                            <View style = {{position:'absolute',left:43,bottom:0}}>
                                <Text allowFontScaling={false} style = {{color:friday_txt,fontWeight:'normal',fontSize:10,textAlign:'center'}}>{'√'}</Text>
                            </View>
                        </View>
                         </TouchableOpacity>

                    </View>

                    <View style = {{flex:1,marginTop:10,flexDirection:'row',justifyContent:'space-between'}}>
                        <TouchableOpacity
                            onPress = {() => {this._tran2Saturday(this.state.saturday)}}>
                        <View style = {{width:55,height:45,backgroundColor:saturday_back,alignItems:'center',justifyContent:'center',
                                            borderWidth:1/PixelRatio.get(),borderColor:'#E1E1E1',borderRadius:6}}>

                            <View style = {{}}>
                                <Text allowFontScaling={false} style = {{color:'#FFFFFF',fontSize:16,textAlign:'center'}}>{Language.sat}</Text>
                            </View>

                            <View style = {{position:'absolute',left:43,bottom:0}}>
                                <Text allowFontScaling={false} style = {{color:saturday_txt,fontWeight:'normal',fontSize:10,textAlign:'center'}}>{'√'}</Text>
                            </View>
                        </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress = {() => {this._tran2Sunday(this.state.sunday)}}>
                        <View style = {{width:55,height:45,backgroundColor:sunday_back,alignItems:'center',justifyContent:'center',
                                            borderWidth:1/PixelRatio.get(),borderColor:'#E1E1E1',borderRadius:6}}>

                            <View style = {{}}>
                                <Text allowFontScaling={false} style = {{color:'#FFFFFF',fontSize:16,textAlign:'center'}}>{Language.sun}</Text>
                            </View>

                            <View style = {{position:'absolute',left:43,bottom:0}}>
                                <Text allowFontScaling={false} style = {{color:sunday_txt,fontWeight:'normal',fontSize:10,textAlign:'center'}}>{'√'}</Text>
                            </View>
                        </View>
                        </TouchableOpacity>
                        <View style = {{width:55,height:45,alignItems:'center',justifyContent:'center',backgroundColor:'#FFFFFF',borderRadius:5,borderColor:'#FFFFFF',borderWidth:1,}}>
       
                        </View>
                        <View style = {{width:55,height:45,alignItems:'center',justifyContent:'center',backgroundColor:'#FFFFFF',borderRadius:5,borderColor:'#FFFFFF',borderWidth:1,}}>
                  
                        </View>
                        <View style = {{width:55,height:45,alignItems:'center',justifyContent:'center',backgroundColor:'#FFFFFF',borderRadius:5,borderColor:'#FFFFFF',borderWidth:1,}}>
                         
                        </View>
                    </View>
                </View>
                <View style = {{position:'absolute',width:deviceWidth-20,marginLeft:10,marginRight:10,flexDirection:'row',bottom:50,}}>
                    <View style = {{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity 
                            onPress = {() => {this._tran2Save(this.props.address,this.state.hour,this.state.minute,this.state.order)}}
                            style = {{backgroundColor:'#FFFFFF',width:deviceWidth,height:48,}}>
                        <View style = {{backgroundColor:'#FFFFFF',alignItems:'center',justifyContent:'center',
                                    width:deviceWidth,height:48,borderColor:'#2DBAF1',borderWidth:1,borderRadius:25,}}>
                            <Text allowFontScaling={false} style = {{color:'#2DBAF1',fontSize:16,textAlign:'center'}}>{Language.save}</Text>
                        </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    },


    _tran2Monday:function(monday){
        this.setState({monday:!this.state.monday});
    },

    _tran2Tuesday:function(tuesday){
        this.setState({tuesday:!this.state.tuesday});
    },

    _tran2Wednesday:function(wednesday){
          this.setState({wednesday:!this.state.wednesday});
    },

    _tran2Thursday:function(thursday){

        this.setState({thursday:!this.state.thursday});
    },

    _tran2Friday:function(friday){
     this.setState({friday:!this.state.friday});

    },

    _tran2Saturday:function(saturday){
     this.setState({saturday:!this.state.saturday});
    },

    _tran2Sunday:function(sunday){
  this.setState({sunday:!this.state.sunday});
    },

    _tran2Save:function(address,hour,minute,order){
        console.log('星期``12``'+address+'``'+order+'```'+hour+'````'+minute);

        var dex = 0x0;

        dex = this.state.monday    ? dex | tmp01 : dex;
        dex = this.state.tuesday   ? dex | tmp02 : dex;
        dex = this.state.wednesday ? dex | tmp03 : dex;
        dex = this.state.thursday  ? dex | tmp04 : dex;
        dex = this.state.friday    ? dex | tmp05 : dex;
        dex = this.state.saturday  ? dex | tmp06 : dex;
        dex = this.state.sunday    ? dex | tmp07 : dex;

        if (dex == 0x0){
            tools.alertShow(Language.choose_repeat_cycle);
            return;
        }


        // if (this.state.monday){
        //     dex |= tmp01;
        // }
        // if (this.state.tuesday){
        //     dex |= tmp02;
        // }
        // if (this.state.wednesday){
        //     dex |= tmp03;
        // }

        // if (this.state.thursday){
        //     dex |= tmp04;
        // }       
        //  if (this.state.friday){
        //     dex |= tmp05;
        // }        
        // if (this.state.saturday){
        //     dex |= tmp06;
        // }        
        // if (this.state.sunday){
        //     dex |= tmp07;
        // }


        console.log('16进制```'+dex);

        var time = {hour:'',minute:'',order:'',week:'',};

        time.hour = hour;
        time.minute = minute;
        time.order   = order;
        time.week   = dex;

        console.log('星期``13``'+time.week+'``'+time.order+'```'+time.hour+'````'+time.minute);

        RCTDeviceEventEmitter.emit('vehide_time_week', time);

       BlueToothUtil.setVehideTime(address,parseInt(minute),parseInt(hour),parseInt(dex),parseInt(order),1);

        this.props.navigator.pop();
    },

});

module.exports = VehideEditTime;
