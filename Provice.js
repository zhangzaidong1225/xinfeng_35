/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import  styles from './styles/style_setting';
import  config from './config'
//var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');

import Title from './title'
import Language from './Language'

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  ToastAndroid,
  ListView,
  AsyncStorage,
  SwitchAndroid,
} from 'react-native';


var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;

var NetWork=require('./NetWork');
var city_data = require('./city_data');
var async = require('async');


//var REQUEST_URL='http://feibao.esh001.com/iface/info/area_list';
var  REQUEST_URL=config.server_base+config.area_list_uri;
var City=require('./City');

var mprovince;
var city;
var area;
var mprovinceid;

var Location= React.createClass({  
    getInitialState() {
        return {
           dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
            value:mheight,
            m:mwidth,
            id:null,
            trueSwitchIsOn: true,
      falseSwitchIsOn: false,
      colorTrueSwitchIsOn: true,
      colorFalseSwitchIsOn: false,
      eventSwitchIsOn: false,
      ipcity: Language.location,
      select_id: null
        }
    },


   componentWillMount:function(){
      /*NetWork.get_ip_city((country, province, city)=>{
        var tmp = province + ' ' + city; 
        if(province == city) tmp = city;
        this.setState({
          ipcity:tmp
        });
      });
*/
   },


    componentDidMount: function() {
      //this.getLocation();
      this.fetchData();
      this.setState({
        id:333
      });
    },
    clearLocation:function(){
      async.parallel([
          (callback)=>{
            AsyncStorage.removeItem('area',(err)=>{callback(null, null);});
          },
          (callback)=>{
            AsyncStorage.removeItem('areaid',(err)=>{callback(null, null);});
          },
          (callback)=>{
            AsyncStorage.removeItem('province',(err)=>{callback(null, null);});
          },
          (callback)=>{
            AsyncStorage.removeItem('provinceid',(err)=>{callback(null, null);});
          },
          (callback)=>{
            AsyncStorage.removeItem('city',(err)=>{callback(null, null);});
          },
          (callback)=>{
            AsyncStorage.removeItem('cityid',(err)=>{callback(null, null);});
          },
          
        ],
        function(err, results){
            NetWork.get_city();
        });

    },

    getLocation:function(cb){

      async.parallel([
          function(callback){
            AsyncStorage.getItem('province',(error,result)=>{
            if(error){}else{
                callback(null, result);
               }
            });
            
          },
          function(callback){
          AsyncStorage.getItem('city',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
          function(callback){
          AsyncStorage.getItem('area',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
          function(callback){
          AsyncStorage.getItem('provinceid',(error,result)=>{
            if(error){}else{
              callback(null, result);
             }
            });
          },
        ],
        function(err, results){
          mprovince=results[0];
          city=results[1];
          area=results[2];
          mprovinceid=results[3];
          cb();
          console.log(results);
        });
    },

fetchData: function() {
    //fetch(REQUEST_URL)   
    //  .then((response) => response.json())
    //  .then((responseData) => { 

    this.getLocation(()=>{
        var responseData = city_data.get_provice_list('CH');
        var index=-1;
        for(var i=0;i<responseData.length;i++){
          if(responseData[i].id===mprovinceid){  
            index=i;
            break;
          }
        }
        if(index!=-1){
          responseData.splice(index,1);
        }
       
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData),
          loaded: true,
        });   
    });
    
    //  });
  },  

  rederview:function(){
    if(mprovince==null){    
      var navigator = this.props.navigator;
        return(
          <View>
      
        <Title title = {Language.locationtitle} hasGoBack = {true}  navigator = {this.props.navigator}/>

       <ScrollView  style={styles.view8}> 
      <View style={styles.item1}>
        <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>{Language.location}</Text>  
       </View>
       <View style={styles.line2}/>
        <TouchableOpacity style={styles.view} 
              onPress={()=>this.clear() }>
            <Text allowFontScaling={false} style={styles.font1}>{this.state.ipcity}</Text>
            <View style={styles.direction}>
            </View>
      </TouchableOpacity>
       <View style={styles.line2}/>  

       <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>全部省份</Text>  
       </View>
       <View style={styles.line2}/>
      
       
        <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderMovie}
        style={styles.listView}
        removeClippedSubviews={false}
      /> 
        
  </View>                 
  </ScrollView>
    </View>
)
    }else{
      var navigator = this.props.navigator;
      return(
    
        <View>
             
       <Title title = {Language.locationtitle} hasGoBack = {true}  navigator = {this.props.navigator}/>
 

   <View style={styles.view8}>
    <ScrollView  showsVerticalScrollIndicator ={false}>   
    <View style={styles.item1}>
       
        <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>{Language.location}</Text>  
       </View>
       <View style={styles.line2}/>

        <TouchableOpacity style={styles.view} 
              onPress={()=>this.clear() }>
            <Text allowFontScaling={false} style={styles.font1}>{this.state.ipcity}</Text>
            <View style={styles.direction}>
            </View>
      </TouchableOpacity>
       <View style={styles.line2}/>  

       <View style={styles.view} >
            <Text allowFontScaling={false} style={styles.font2}>全部省份</Text>  
       </View>
       <View style={styles.line2}/>

   
         <View>
       <TouchableOpacity style={styles.view} onPress={()=>this.gotoCity2()}>
            <Text allowFontScaling={false} style={styles.font1}>{mprovince}</Text>  
            <View style={styles.direction}>
            <Text allowFontScaling={false} style={styles.font2}>{Language.selected}</Text>
            </View>
       </TouchableOpacity>
       <View style={styles.line2}/>
        </View>
        
   <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderMovie}
        style={styles.listView}
        removeClippedSubviews={false}
      />  
    
  </View>  
  </ScrollView>               
      </View>
    </View>
    
)
    }
  },

  render: function(){
    
    return  this.rederview();
   
  },


  renderMovie: function(province) {
    return (
     <View >
      <TouchableOpacity style={styles.view} onPress={()=>this.gotoCity(province)}>
            <Text allowFontScaling={false} style={styles.font1}>{province.name}</Text>                      
       </TouchableOpacity>   
       <View style={styles.line2}/>
      </View>
    );        
  },

  gotoCity:function(province){  
    UMeng.onEvent('provice_03');

    this.props.navigator.push({
      id: 'City',
      params:{
          provinceid:province.id,
          province:province.name,
          start_page:this.props.start_page,
        }
    });  
  },

  gotoCity2:function(){
    UMeng.onEvent('provice_03');  
    this.props.navigator.push({
      id: 'City',
      params:{
          province:mprovince,
          provinceid:mprovinceid,
        }
    });
  },
  clear:function(){
    UMeng.onEvent('provice_05');
    this.clearLocation();
    this.props.navigator.pop();
  },
  


});
module.exports = Location;
