'use strict';

var { NativeModules } = require('react-native');


var BluetoothUtil = {
	'auto_connect':function(){
		NativeModules.BluetoothUtil.auto_connect();
	},
	'sendMydevice':function(){
		NativeModules.BluetoothUtil.sendMydevice();
	},
	'scanner':function(){
		NativeModules.BluetoothUtil.scanner();
	},
	'stopScan':function(){
		NativeModules.BluetoothUtil.stopScan();
	},
	'stopConnect':function(address){
		NativeModules.BluetoothUtil.stopConnect(address);
	},
	'cancelBond':function(address){
		NativeModules.BluetoothUtil.cancelBond(address);
	},
	'connectDevice':function(address){
		NativeModules.BluetoothUtil.connectDevice(address);
	},
	'shutDown':function(address){
		NativeModules.BluetoothUtil.shutDown(address);
	},
	'windSpeed':function(address,speed){
		NativeModules.BluetoothUtil.windSpeed(address,speed);
	},
	'deviceUpdate':function(address,name,path){
		NativeModules.BluetoothUtil.deviceUpdate(address,name,path);
	},
	'getHardwareVersion':function(address){
		NativeModules.BluetoothUtil.getHardwareVersion(address);
	},
	'devCheck':function(address){
		NativeModules.BluetoothUtil.devCheck(address);
	},
	'devRename':function(address, name){
		NativeModules.BluetoothUtil.devRename(address, name);
	},
	'verifyInstallPackage':function (path,callback){
		NativeModules.BluetoothUtil.verifyInstallPackage(path,callback);
	},

	'disconnect':function(address){
		NativeModules.BluetoothUtil.disconnect(address);
	},
	'eSmart':function(address,model){
		NativeModules.BluetoothUtil.esmart(address,model);
	},
	'setInterval':function(address,space){
		NativeModules.BluetoothUtil.setInterval(address,space);
	},
	'setSystemClock':function(address,time){
		NativeModules.BluetoothUtil.setSystemClock(address,time);
	},
	
	'setVehideTime':function(address,hour,minute,week,type,mode){
		NativeModules.BluetoothUtil.setVehideTime(address,hour,minute,week,type,mode);
	},
	'unZip':function(path){
		NativeModules.BluetoothUtil.unZip(path);
	},

};
module.exports = BluetoothUtil;