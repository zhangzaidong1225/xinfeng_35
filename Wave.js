/**
 * 水波纹动画
 * @louis liu 
 * 01/13/2016
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ART
} from 'react-native';


const {Surface, Shape, Path, Text, Group} = ART;

var TimerMixin = require ('react-timer-mixin');
//PI
const π = 3.1415926;

//水波纹起始相位变量
var φxFast = 0;
var φxSlow = 0;
export default class Wave extends Component {
    constructor(props) {
        super(props);
        this.state={
            pathFast:ART.Path(),
            pathSlow:ART.Path(),
        }

        this.φx = 0;//水波纹起始相位变量
        this.ΔFast = π / 20; //快水波纹每次偏移的变量
        this.ΔSlow = π / 40; //慢水波纹每次偏移的变量
        this.A = 10; //峰值（即纵向拉伸压缩的倍数）
        this.style = {}; //水波纹动画放置位置
        this.width = 0; //水波纹动画宽
        this.height = 0; // 水波纹动画高 （水波纹动画绘画原点在左上角）
        this.stop = false; // 停止动画

    };
    
   componentWillMount(){

   		this.init();
        this.timer();

   }
	componentWillUnmount() {
	    // 如果存在this.timer，则使用clearTimeout清空。
	    // 如果你使用多个timer，那么用多个变量，或者用个数组来保存引用，然后逐个clear
	    this.interval && clearInterval(this.interval);
	}
    //绘制直线
     render(){
     	if (this.props.stop) {
     		console.log('clearInterval');
     		this.stopTimer();
     	} else {
     		if (this.interval == null) {
     			console.log('startInterval');
     			this.timer();
     		}
     	}

        return(
            <View style={this.props.style}>
                <ART.Surface width={this.props.width} height={this.props.height}>
                    <Group>
                        <ART.Shape d={this.state.pathFast} stroke="#2AB9F188" fill="#2AB9F188" strokeWidth={1} />
                        <ART.Shape d={this.state.pathSlow} stroke="#2AB9F188" fill="2AB9F188" strokeWidth={1} />
                    </Group>
                </ART.Surface>
            </View>

        )
    }

    init() {
    	φxFast = this.props.φx;
    	φxSlow = this.props.φx;

    	this.calculate(φxFast, φxSlow);

        φxFast += this.props.ΔFast;
        φxSlow += this.props.ΔSlow;
    }

    timer() {

        this.interval = setInterval(() => {
            
            this.calculate(φxFast, φxSlow);
            φxFast += this.props.ΔFast;
        	φxSlow += this.props.ΔSlow;
        }  , 100); 

    }

    stopTimer() {
    	this.interval && clearInterval(this.interval);
    	this.interval = null;
    }

    calculate(φxFast, φxSlow) {
    	// console.log('this.props.height' + this.props.height);
    	// console.log('this.props.A' + this.props.A);

        const path = ART.Path();

        var ω = - 4* π / this.props.width; //决定周期（最小正周期 T = 2π/|ω|）
        var h = this.props.A; //表示波形在Y轴的位置关系或纵向移动距离（上加下减）

        //绘制路径移动到起始坐标点(0,1)
        // path.moveTo(-1, this.props.A * Math.sin(φxFast) + h);
        path.moveTo(0, this.props.height);
        // path.lineTo(0, 100);
        //按像素点一直加到整个宽度
        for (var i = 0; i < this.props.width; i++) {
            path.lineTo(i, this.props.A * Math.sin(ω * i + φxFast) + h);
            
        };
        path.lineTo(this.props.width + 1,this.props.height);
        // path.close();

        const path1 = ART.Path();
        // path.moveTo(1,1); //将起始点移动到(1,1) 默认(0,0)
        // path.lineTo(300,1); //连线到目标点(300,1)
        // path.lineTo(200, Math.sin(200));

        //绘制区域是宽300，高为2，所以得到起始坐标点为(0, 10)
        // const π1 = 3.1415926;
        // var A1 = 100; //峰值（即纵向拉伸压缩的倍数）
        // var φ1 = φx; //初相位 （决定波形与X轴位置关系或横向移动距离（左加右减））
        // var ω1 = 2 * π1 / this.props.width; //决定周期（最小正周期 T = 2π/|ω|）
        // var h1 = 100; //表示波形在Y轴的位置关系或纵向移动距离（上加下减）

        //绘制路径移动到起始坐标点(0,1)
        // path1.moveTo(-1, this.props.A * Math.sin(φxSlow) + h);
        path1.moveTo(0, this.props.height);
        //按像素点一直加到整个宽度
        for (var i1 = 0; i1 < this.props.width; i1++) {
            path1.lineTo(i1, this.props.A * Math.sin(ω * i1 * 1 + φxSlow) + h);
            
        };
        path1.lineTo(this.props.width + 1,this.props.height);
        // path1.close();
        // var path2 = path;
        // var path3 = path1;
        this.setState({
            pathFast: path,
            pathSlow: path1,
        });
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
