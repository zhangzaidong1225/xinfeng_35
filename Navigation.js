'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  TouchableOpacity,
  BackAndroid,
  Alert,
  // DeviceEventEmitter,
  Dimensions,
  PixelRatio,
  Platform,
  AsyncStorage,
  NativeAppEventEmitter,
} from 'react-native';

var UMeng = require('./UMeng');
var TimerMixin = require ('react-timer-mixin');
var type;
import TabbarPage from './tabbar'
import SetPage from './setting'
import HomePage from './home'
import ProvicePage from './Provice'
import CityPage from './City'
import AreaPage from './Area'
import WebPage from './web'
import Update from './Update'
import FeedPage from './feedback'
import GuidePage from './guide'
import FilterXinfengPage from './filter_xinfeng'
import FilterVehidePage from './filter_vehide'
import CottonXinfengPage from './cotton_xinfeng'
import CottonVehidePage from './cotton_vehide'
import Register from './register'
import Privilege from './privilege'
import Forgot from './forgot'
import DeviceStatus from './deviceStatus'
import DeviceUpdate from './deviceUpdate'
import DeviceUpload from './deviceUpload'
import ObligatoryDeviceUpload from './obligatoryDeviceUpload'
import Login from './login'
import Account from './account_login'
import Safe_code from './safe_code'
import Unconnect from './unconnect'
import WebViewPage from './webViewPage'
import About from './about'
import SharePage from './sharePage'
import ModalBox from './modal'
import config from './config'
import SingleWeather from './SingleWeather'
import WheelTimerPicker from './WheelTimerPicker'
import Esmart from './Esmart'
import WheelTimePicker from './WheelTimePicker'
import VehideTime from './VehideSetTime'
import VehideEditTime from './VehideEditTime'

import styles_wheeltimerpicker from './styles/style_wheeltimerpicker'
import styles_wheeltimepicker from './styles/style_wheeltimepicker'

import styles_navigation from './styles/style_navigation'
import styles_modal1 from './styles/style_home_modal'
import styles_feedback from './styles/style_feedback'
import Language from './Language'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');


var Connect = require('./connection');
var NetWork = require('./NetWork');
var alertMessage = '您是否想退出程序？';
var alertMessage1 = '发现新版本，不升级将无法使用该程序，您确定离开吗？';
var alertMessage2 = '设备固件升级中，请稍后...';
var alertMessage3 = '充电器异常,请使用5V/1A标准充电器';
var alertMessage4 = '上盖已开启...';
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;
var BlueToothUtil=require('./BlueToothUtil');
var Utils=require('./utils');
var _navigator;
//var sub_ble_state = null;
var sub_ble_state2 = null;
var navigator_push;

var subscription2;
var subscription3;
var subscription4;
var back_up = 0;
var update_force;
var isOpenfilter;
var battery_over = 0;
var Varsion = config.min_device_varsion;
var device_map;
var battery_over_map;

//是否正在强制升级处理标志
var isObligatory = false;
var change_obligatory;

var wheel_timer_picker_open;
var wheel_timer_picker_close;
var timerValue = '10';

// var wheel_time_picker_open;
// var wheel_time_picker_close;
// var timeValue = {hour:'00', minute: '00'};

subscription2 = RCTDeviceEventEmitter.addListener('back_up',(val) => {
    if (val != null){
        back_up = val;
    }
   
});
subscription3 = RCTDeviceEventEmitter.addListener('update_force',(val) => {
    if (val != null&&val != ''){       
        update_force = val;
    }
});
subscription4 = RCTDeviceEventEmitter.addListener('isOpenfilter',(val) => {
    if (val != null&&val != ''){       
        isOpenfilter = val;
    }
    if (val == 1){
        console.log('1211==11'+ val);
        Alert.alert(
          Language.prompt+'',
          Language.open_cap +'',
          [
            {text: Language.ok+'', onPress: () => { return true;}},
          ]
        )
         //AsyncStorage.setItem('open_filter','1',error => {});
        return true;
    }
});
BackAndroid.addEventListener('hardwareBackPress', function() {
  RCTDeviceEventEmitter.emit('loading',false);
  if (_navigator && _navigator.getCurrentRoutes().length > 1) {
        for(var i = 0;i < _navigator.getCurrentRoutes().length;i++){
          if (_navigator.getCurrentRoutes()[i].id ==='DeviceUpload' || _navigator.getCurrentRoutes()[i].id ==='ObligatoryDeviceUpload'){
            if (back_up === false){
            Alert.alert(
              Language.prompt+'',
              Language.upgrading+'',
              [
                {text: Language.ok+'', onPress: () => {return true;}},
              ]
            )
             return true;
            }
          }

          if (_navigator.getCurrentRoutes()[i].id ==='Update' && update_force === 1){
              Alert.alert(
                Language.prompt+'',
                Language.foundversion,
                [
                  {text: Language.ok+'', onPress: () => {BackAndroid.exitApp();}},
                  {text: Language.cancel+'', onPress: () => {return true;}},
                ]
              )
              return true;
          }
         }
       _navigator.pop();
      return true; 
  }else{
    if (_navigator.getCurrentRoutes()[0].id ==='WebViewPage') {
      _navigator.replace({
        id: 'TabbarPage',
      });
      return true;
    }
    Alert.alert(
      Language.prompt+'',
      Language.quitapp +'',
      [
        {text: Language.ok+'', onPress: () => {BackAndroid.exitApp();}},
        {text: Language.cancel+'', onPress: () => {return true;}},
      ]
    )
  }
  return true;
});

class Navigation extends Component {

  constructor(props : any) {
    super(props);
    this.state = {
      wheeltimerflag:false,
      wheeltimeflag:false,
    };
  }

  componentWillMount(){
    battery_over_map = this.getMap();
    NetWork.get_city();
    device_map = this.getMap();
  }

  componentDidMount(){

    // sub_ble_state = DeviceEventEmitter.addListener(
    //      'ble_state',
    //      (data) => {
    //         this.init_data(data)
    //      }
    //  );
    sub_ble_state2 = NativeAppEventEmitter.addListener(
         'ble_state',
         (data) => {
            this.init_data(data)
         }
     );
    
    navigator_push = RCTDeviceEventEmitter.addListener(
      'navigator_push',
      (device) => {
        _navigator.push({
                           id: 'ObligatoryDeviceUpload',
                           params:{
                               deviceid:device.addr,
                               devicename:device.name,
                               devicetype:device.type,
                              }
                           });
                           device_map.put(device.addr,0);
      }

    );

    wheel_timer_picker_close = RCTDeviceEventEmitter.addListener(
         'wheel_timer_picker_close',
         () => {
          this.setState({
            wheeltimerflag: false
          });
         }
    );

    wheel_timer_picker_open = RCTDeviceEventEmitter.addListener(
         'wheel_timer_picker_open',
         (value) => {
          timerValue = value;
          this.setState({
            wheeltimerflag: true
          });
         }
    );

    // wheel_time_picker_close = RCTDeviceEventEmitter.addListener(
    //      'wheel_time_picker_close',
    //      () => {
    //       this.setState({
    //         wheeltimeflag: false
    //       });
    //      }
    // );

    // wheel_time_picker_open = RCTDeviceEventEmitter.addListener(
    //      'wheel_time_picker_open',
    //      (value) => {
    //       timerValue = value;
    //       this.setState({
    //         wheeltimeflag: true
    //       });
    //      }
    // );


    change_obligatory = RCTDeviceEventEmitter.addListener(
      'change_obligatory',
      (flag) => {
        isObligatory = flag;
      }
    );
    BlueToothUtil.sendMydevice();

  }

  init_data(data){

    for (var i = 0;i < data.dev_list.length;i++){
      // console.log(data.dev_list.length+'length-----battery_over');
      // console.log(data.dev_list[i].battery_over+'---Navigator--battery_over');
      // console.log(battery_over+'-----battery_over');
      if(data.dev_list[i].type==0){
          Varsion = config.min_device_varsion;
      }else if(data.dev_list[i].type==1){
          Varsion = config.single_device_minvarsion;
      }else if(data.dev_list[i].type==2){
          Varsion = config.car_min_device_minvarsion;
      }
       console.log(data.dev_list[i].version+'。。。。。。强制升级'+parseInt(Varsion)+isObligatory);
      if(data.dev_list[i].version != 0 && data.dev_list[i].version < parseInt(Varsion) && !isObligatory){
          console.log(data.dev_list[i].version+'强制升级'+parseInt(Varsion));
          if(data.dev_list[i].isconnectd==1){
            var device = data.dev_list[i];
            var add = data.dev_list[i].addr;
            var name = data.dev_list[i].name;
            var type = data.dev_list[i].type;
            var power = data.dev_list[i].battery_power;
            console.log(name+'  add  '+device_map.get(add));
           if (device_map.get(add) !== 1){
                console.log(name+'  add 1============================================== '+device_map.get(add));
                device_map.put(add, 1);
                if (Platform.OS === 'android') {
                      isObligatory = true;
                      Utils.ShowDialog(name,add,(message) => {
                                  console.log(name+'  add 2 '+device_map.get(add));
                        if(message){
                          _navigator.push({
                           id: 'ObligatoryDeviceUpload',
                           params:{
                               deviceid:add,
                               devicename:name,
                                           devicetype:type,
                              }
                           });
                           device_map.put(add,0);
                        }else{
                          BlueToothUtil.cancelBond(add);
                          BlueToothUtil.stopConnect(add);
                          device_map.put(add,0);
                                      this.timer = setTimeout(
                                      () => {RCTDeviceEventEmitter.emit('change_obligatory', false);}, 1000);
                                      
                        }
                      });
                } else {
                  console.log('============================================================');
                  // for ( var i = 0; i < _navigator.getCurrentRoutes().length; i ++) {
                  //   console.log('route ' + i + ': ' + _navigator.getCurrentRoutes()[i].id);
                  // }
                      isObligatory = true;
                      Alert.alert(
                        Language.forcedtoupgrade,
                        name+Language.dev_force_upgrade,
                        [
                          {text: Language.forcedtoupgrade, onPress: () => {
                            RCTDeviceEventEmitter.emit('navigator_push', device);
                          }},
                          {text: Language.cancel+'', onPress: () => {
                          BlueToothUtil.cancelBond(add);
                          BlueToothUtil.stopConnect(add);
                          device_map.put(add,0);
                          isObligatory = false;
                          }},
                        ]
                      )
                }

            }

          }
      }
      console.log('data.dev_list[i].ischarged' + data.dev_list[i]);
      if (data.dev_list[i].ischarged === 1){

        if (data.dev_list[i].battery_over === 1){
          //弹出提示框---
         // console.log(battery_over+'---battery_over');
          
          if (battery_over_map.get(data.dev_list[i].addr) === 0){

              // Alert.alert(
              //     Language.prompt+'',
              //     alertMessage3,
              //     [
              //       {text: Language.ok+'', onPress: () => {return true;}},
              //     ]
              // )
            this.refs.modal2.open();
            // battery_over = 1;
            battery_over_map.put(data.dev_list[i].addr, 1);

          }
        }
      }else {
        battery_over_map.put(data.dev_list[i].addr, 0);
          // battery_over = 0;
      }
      
    }
  }



  componentWillUnmount(){
    this.timer && clearTimeout(this.timer);
    BackAndroid.removeEventListener('hardwareBackPress');
    subscription2.remove();
    subscription3.remove();
    subscription4.remove();
    //sub_ble_state.remove();
    sub_ble_state2.remove();
    navigator_push.remove();
    change_obligatory.remove();
    wheel_timer_picker_close.remove();
    wheel_timer_picker_open.remove();
  }

    render (){
      return (
        <View style = {{flex:1}}>
        <Navigator
          isOpenGesture={false}
          initialRoute={{id:'GuidePage'}}
          renderScene = {this.renderScene}
          configureScene={() => ({
               ...Navigator.SceneConfigs.HorizontalSwipeJump,//转场动画
               gestures: { pop: false }
          })} />
              <ModalBox
                style = {styles_navigation.container}
                position={'center'} 
                ref={'modal2'}
                animationDuration = {1}>
                  <View style = {styles_navigation.view1}>
                    <View style = {styles_navigation.view2}>
                      <Text allowFontScaling={false} style= {styles_navigation.text1}>{Language.chargerabnormal}</Text>
                      <Text allowFontScaling={false} style= {styles_navigation.text2}>{Language.usestandardcharger}</Text>
                    </View>

                    <View style = {styles_navigation.view3}>                    
                        <View style= {styles_navigation.view4}>
                          <TouchableOpacity onPress={() => {this.refs.modal2.close()}}>
                             <View style = {styles_navigation.view5}>
                              <Text allowFontScaling={false} style={styles_navigation.text3}>{Language.know}</Text>
                             </View>
                          </TouchableOpacity>
                        </View>
                    </View>
                  </View>
              </ModalBox>

              <View style = {[styles_wheeltimerpicker.background, this.state.wheeltimerflag?{}:{height : 0} ]}>

                <TouchableOpacity activeOpacity = {1} style = {[styles_wheeltimerpicker.backgroundtouch, this.state.wheeltimerflag?{}:{height : 0}]} onPress={() => {RCTDeviceEventEmitter.emit('wheel_timer_picker_close')}}>

                </TouchableOpacity>
                <WheelTimerPicker  value = {timerValue} showFlag = {this.state.wheeltimerflag}/>
              </View>

              
      </View>


      );
    }

// <View style = {[styles_wheeltimepicker.background, this.state.wheeltimeflag?{}:{height : 0} ]}>

//                 <TouchableOpacity activeOpacity = {1} style = {[styles_wheeltimepicker.backgroundtouch, this.state.wheeltimeflag?{}:{height : 0}]} onPress={() => {RCTDeviceEventEmitter.emit('wheel_time_picker_close')}}>

//                 </TouchableOpacity>
//                 <WheelTimePicker value = {timeValue} showFlag = {this.state.wheeltimeflag}/>
//               </View>
// this.state.timerPickerShow?{width : 0}:{},
    renderScene(route,navigator,onComponentRef){
      var routeId = route.id;
      _navigator = navigator;
    if (routeId === 'GuidePage'){
        return (
          <GuidePage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'FilterXinfengPage'){
        return (
          <FilterXinfengPage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'FilterVehidePage'){
        return (
          <FilterVehidePage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'CottonXinfengPage'){
        return (
          <CottonXinfengPage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'CottonVehidePage'){
        return (
          <CottonVehidePage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'WebPage'){
        return (
          <WebPage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'TabbarPage'){
        return (
          <TabbarPage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'Update'){
        return (
          <Update
            navigator = {navigator}/>
        );
      }
      if (routeId === 'HomePage'){
        return (
          <HomePage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'Forgot'){
        return (
          <Forgot {...route.params}
            navigator = {navigator}/>
        );
      }
      if (routeId === 'SetPage'){
        return (
          <SetPage {...route.params}
            navigator = {navigator}/>
        );
      }
      if (routeId === 'Register'){
        return (
          <Register {...route.params}
            navigator = {navigator}/>
        );
      }
      if (routeId === 'Privilege'){
        return (
          <Privilege {...route.params}
            navigator = {navigator}/>
        );
      }       
      if (routeId === 'DeviceStatus'){
        return (
          <DeviceStatus
            {...route.params} navigator = {navigator}/>
        );
      }
      if (routeId === 'Connect'){
        return (
          <Connect {...route.params} navigator = {navigator}/>
        );
      }
    if (routeId === 'Unconnect'){
        return (
          <Unconnect {...route.params} navigator = {navigator}/>
        );
      }
      if (routeId === 'DeviceUpload'){
        return (
          <DeviceUpload 
          {...route.params} 
          navigator = {navigator}/>

        );
      }

      if (routeId === 'ObligatoryDeviceUpload'){
        return (
          <ObligatoryDeviceUpload 
          {...route.params} 
          navigator = {navigator}/>

        );
      }

      if (routeId === 'FeedPage'){
        return (
          <FeedPage
            navigator = {navigator}/>
        );
      }
      if (routeId === 'DeviceUpdate'){
        return (
          <DeviceUpdate
            navigator = {navigator}/>
        );
      }
      if (routeId === 'Provice'){
        return (
          <ProvicePage {...route.params} navigator = {navigator}/>
        );
      }

      if (routeId === 'City'){
        return <CityPage {...route.params} navigator={navigator} />
      }

      if (routeId === 'Area'){
        return <AreaPage {...route.params} navigator={navigator} />
      }
      if (routeId === 'Login'){
        return <Login {...route.params} navigator = {navigator}/>
      }
      if (routeId === 'Account'){
        return (
          <Account
            navigator = {navigator}/>
        );
      }
      if (routeId === 'Safe_code'){
        return (
          <Safe_code
            navigator = {navigator}/>
        );
      }

      if (routeId === 'WebViewPage'){
        return (
          <WebViewPage {...route.params}
            navigator = {navigator}/>
        );
      }

      if (routeId === 'About') {
        return (
          <About 
            navigator = {navigator}/>
        );
      }

      if (routeId === 'SharePage'){
        return (
          <SharePage
             {...route.params}  navigator = {navigator}/>
        );
      }
      if (routeId === 'SingleWeather'){
        return (
          <SingleWeather
             {...route.params}  navigator = {navigator}/>
        );
      }

      if (routeId === 'Esmart'){
        return (
          <Esmart
             {...route.params}  navigator = {navigator}/>
        );
      }

      if (routeId === 'VehideTime'){
        return (
          <VehideTime
             {...route.params}  navigator = {navigator}/>
        );
      }
      if (routeId === 'VehideEditTime'){
        return (
          <VehideEditTime
             {...route.params}  navigator = {navigator}/>
        );
      }

    }
        //定义简单Map  
    getMap() {//初始化map_,给map_对象增加方法，使map_像Map    
         var map_ = new Object();    
         map_.put = function(key, value) {    
             map_[key+'_'] = value;    
         };    
         map_.get = function(key) {    
             return map_[key+'_'];    
         };    
         map_.remove = function(key) {    
             delete map_[key+'_'];    
         };    
         map_.keyset = function() {    
             var ret = '';    
             for(var p in map_) {    
                 if(typeof p == 'string' && p.substring(p.length-1) == '_') {    
                     ret += ',';    
                     ret += p.substring(0,p.length-1);    
                 }    
             }    
             if(ret == '') {    
                 return ret.split(',');    
             } else {    
                 return ret.substring(1).split(',');    
             }    
         };    
         return map_;    
}
}

module.exports = Navigation;
