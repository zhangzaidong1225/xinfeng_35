'use strict'

import  styles from './styles/style_setting';
import styles_feedback from './styles/style_feedback'
import Language from './Language'
var AsyncStorage = require('./AsyncStorage');
import  config from './config'

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    // AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    DeviceEventEmitter,
    NativeAppEventEmitter,
} from 'react-native';

var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var Account = React.createClass({
    
    getInitialState(){
        return {
          type:1,
          contact:'',
          password:'',
          hidden:false,
        }
    },

    hidden(){

        if (!this.state.hidden){
            this.setState({hidden:true});
        }else {
           this.setState({hidden:false});
        }
    
    },

    render(){
        var navigator = this.props.navigator;
        var img;
        if (this.state.hidden){
                   img = require('image!ic_hide'); 
        }else{
            img = require('image!ic_show');
        }

        return (
            <View style = {{flex:1,backgroundColor:'#FFFFFF'}}>
                <View style={styles.head}>
                    <TouchableOpacity
                      onPress={() => navigator.pop()}>
                      <Image style={[styles.image,{resizeMode: 'stretch'}]} source={require('image!ic_back')}/>
                    </TouchableOpacity>
                    <Text allowFontScaling={false} style={[styles.font3,styles.text1]}>{Language.accountlogin}</Text>
                     <View style={[styles.image,]}></View>
                </View>


                <View style = {{height:deviceHeight * 1/4,backgroundColor:'white',marginTop:80,}}>
                        <View style = {{marginLeft:20,marginRight:20,flexDirection:'column',}}>
                            <View style = {{borderWidth:1/PixelRatio.get(),borderColor:'#C8C8C8',borderRadius:5,
                                            width:deviceWidth - 40,height:100,flexDirection:'column',}}>

                                <View style = {{flexDirection:'row',borderBottomWidth:1/PixelRatio.get(),borderColor:'#C8C8C8',}}>
                                    <View style = {{height:50,width:50,borderColor:'#C8C8C8',
                                        borderRightWidth:1/PixelRatio.get(),borderTopLeftRadius:5,backgroundColor:'#00000000',justifyContent:'center',alignItems:'center'}}>
                                        <Image style = {{width:20,height:30,resizeMode:'stretch'}} source = {require('image!ic_feedback')}/>
                                    </View>
                                    <TextInput
                                          style = {{height:50,flex:1,marginLeft:20,}}
                                          multiline={true}
                                          KeyboardType = 'default'
                                          textAlign='start'
                                          textAlignVertical='center'
                                          underlineColorAndroid='transparent'
                                          onChangeText = {(text) => this.setState({contact:text})}
                                          value = {this.state.contact}
                                          placeholderTextColor = '#CCCCCC'
                                          placeholder = {Language.phonenumber}/> 
                                </View>
                                <View style = {{flexDirection:'row',}}>
                                    <View style = {{height:50,width:50,borderColor:'#C8C8C8',
                                        borderRightWidth:1/PixelRatio.get(),borderTopLeftRadius:5,backgroundColor:'#00000000',justifyContent:'center',alignItems:'center'}}>
                                        <Image style = {{width:20,height:30,resizeMode:'stretch'}} source = {require('image!ic_lock')}/>
                                    </View>
                                    <TextInput
                                          style = {{height:50,flex:1,marginLeft:20,}}
                                          multiline={true}
                                          KeyboardType = 'default'
                                          textAlign='start'
                                          textAlignVertical='center'
                                          underlineColorAndroid='transparent'
                                          onChangeText = {(text) => this.setState({password:text})}
                                          value = {this.state.password}
                                          placeholderTextColor = '#CCCCCC'
                                          placeholder = {Language.passwordaccount}/>
                                    <TouchableOpacity
                                        onPress = {() => {this.hidden()}}>
                                    <View style = {{height:50,width:50,borderColor:'#C8C8C8',
                                        borderLeftWidth:1/PixelRatio.get(),borderTopLeftRadius:5,backgroundColor:'#00000000',justifyContent:'center',alignItems:'center'}}>
                                        <Image style = {{width:25,height:15,resizeMode:'stretch'}} source = {img}/>
                                    </View>
                                    </TouchableOpacity>

                                </View>

                            </View>
                            <View style = {{height:20,width:deviceWidth-40,marginTop:10,flexDirection:'row',}}>
 
                                <View style = {{position:'absolute',left:10,}}>
                                 <TouchableOpacity
                                    onPress = {() => {}}>
                                    <Text allowFontScaling={false} style = {{fontSize:12,color:'#8FE49E',}}>{Language.register}</Text>
                                 </TouchableOpacity>
                                </View>
                           
                                <View style={{position:'absolute',right:10,}}>
                                <TouchableOpacity
                                    onPress = {() => {}}>
                                    <Text allowFontScaling={false} style = {{fontSize:12,color:'#E6E6E6',}}>{Language.forgetpassword}{'?'}</Text>
                                </TouchableOpacity>
                                </View>
                               
                            </View>
                        </View>
                </View>


                <View style = {[styles_feedback.commitView,{bottom:10,}]}>

                      <View style={styles_feedback.borderView}>
                        <TouchableOpacity
                            onPress = {this._handleSubmit}>
                                 <View style = {[styles_feedback.textBorder,]}>
                                    <Text allowFontScaling={false} style={[styles_feedback.commitText,{color:'#B2B2B2'}]}>登录</Text>
                                 </View>
                        </TouchableOpacity>
                      </View>
                </View>


            </View>
        );
    },

    _handleSubmit:function(){

    if (this.state.contact === '' ){
      ToastAndroid.show(Language.phonenumbernotempty + '',ToastAndroid.SHORT);
    }else {
          if (this.telRuleCheck2(this.state.contact)){
        
          NetInfo.isConnected.fetch().done(
            (isConnected) => { 
              if(isConnected){
              fetch('http://feibao.esh001.com/iface/info/feedback'+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language,{
                  method:'post',
                  headers:{
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                  },
                  body:JSON.stringify({
                    type:this.state.type,
                    contact:this.state.contact,
                    password:this.state.password,
                  })
                })
                  ToastAndroid.show(Language.loginsuccess+'',ToastAndroid.SHORT);
                  this.setState({
                    contact:'',
                    password:'',
                  });
              }else{
                ToastAndroid.show(Language.connectnetwork + '',ToastAndroid.SHORT); 
              }
            });
        }
        else {
                    ToastAndroid.show(Language.phoenformatnotcorrent+'',ToastAndroid.SHORT);
            }
    }
    },

    telRuleCheck2(string) {
        var pattern = /^1[34578]\d{9}$/;
        if (pattern.test(string)) {
            return true;
        }
          return false;
     },



});

module.exports = Account;


