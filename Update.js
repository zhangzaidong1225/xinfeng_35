/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';


import  styles from './styles/style_update'
import  style_setting from './styles/style_setting'
import  config from './config'
import tools from './tools'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
//var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
import React, { Component } from 'react';
import Language from './Language'
import {
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  NetInfo,
  TouchableOpacity,
  ToastAndroid,
  IntentAndroid,
  Dimensions,
  Navigator,
  BackAndroid,
  ScrollView,
  PixelRatio,
  TouchableWithoutFeedback,
   AsyncStorage,
  NativeModules,
} from 'react-native';
var isloading=0;
var code1=false;
var sub_loading;
var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height;  
var Share= NativeModules.Share;
var DownLoud= NativeModules.Share;
var Share= NativeModules.Share;
var alertMessage = '检测到有新版本，请先升级软件，不然您将无法使用该程序！ ';
var TimerMixin = require ('react-timer-mixin');
import Spinner from 'react-native-loading-spinner-overlay';
 //var REQUIRE_URL = config.server_base + config.upload_uri + '?var=' + config.varsion + '&os=' + config.os;
var Varsion = config.varsion;
var UpdateA = React.createClass({
  getInitialState:function (){
      return {
          url:'',
          code:1,
          version:'',
          info:'',
          mNowVersion:Varsion,
          msg:'',
          force:'',
          isNewest:false,
      }
  },
  componentDidMount:function (){
    this.fetchData();
  },
  fetchData:function (){
  var REQUIRE_URL = config.server_base + config.upload_uri + '?var=' + config.varsion + '&os=' + config.os+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;
    fetch (REQUIRE_URL)
      .then ((response) => response.json())
      .then((responseData) => {
        code1=true;
        if(responseData.code == 101)
        {
          this.setState({
            code:responseData.code,
            msg:responseData.msg,
            url: responseData.data.url,
            info: responseData.data.info,
            version: responseData.data.ver,
            force:responseData.data.force,
          });
          RCTDeviceEventEmitter.emit('update_force',this.state.force);
        }
        else
        {
          this.setState({
            code:responseData.code,
            msg:responseData.msg,
          });
        }
      })
      .catch((error) => {
          this.setState({
            NetStatus:true,
          });
      });
      
  },
  render: function() {
    if(this.state.code === 101){
       console.log('updatecode============101=============');
      return this._render1();
    }
    if(this.state.NetStatus){
      return this._render3();
    }
    if(this.state.code === 100||(this.state.code!=101&&this.state.code!=100&&this.state.code!=1)){
      return this._render2();
      console.log('updatecode============102=============');
    }
    return this._render4();
    console.log('updatecode============103============');
  },
   _render1: function() {
    return (
          <View>
          <View style={styles.uview1}>
            <View style={styles.uview2}>
             <Text allowFontScaling={false} style={[style_setting.font1_11,{textAlign: 'center'}]}>{Language.checknewversion}</Text>
             <Text allowFontScaling={false} style={[style_setting.font1_11,{textAlign: 'center'}]}>{Language.currentversion}{this.state.mNowVersion}</Text>
             <Text allowFontScaling={false} style={[style_setting.font1_11,{textAlign: 'center',color:'rgb(23, 201, 180)',}]}>{Language.updateversion}{this.state.version}</Text>
            </View>
            <View style={styles.uview6}>
            <View style={[styles.uline1,{marginTop:0,marginBottom:0,}]}/>
                <ScrollView style={{height:mheight * 2/10,width:mwidth}}>
                  <View style={{height:mheight * 2/10, justifyContent:'center',alignItems: 'center',}}>
                    <View style={{width:200,}}>
                      <Text allowFontScaling={false} style={[style_setting.font1_11]}>{this.state.info}</Text>
                    </View>
                  </View>
                </ScrollView>
             <View style={[styles.uline1,{marginTop:0,marginBottom:0,}]}/>
          </View>
        </View>
      </View>
      );
    },

  _render2: function() {
      return (
            <View>
            <View style={styles.uview1}>
            <View style={styles.uview2}>
              <Text allowFontScaling={false} style={[style_setting.font1_11,{textAlign: 'center'}]}>{Language.latestsoftversion}</Text>
              <Text allowFontScaling={false} style={[style_setting.font1_1,{textAlign: 'center'}]}>{'\n'}{Language.currentversion}{this.state.mNowVersion}</Text>
            </View>
        </View>
            </View>
        );
  },
   _render3: function() {
      return (
            <View>
            <View style={styles.uview1}>
            <View style={styles.uview2}>
              <Text allowFontScaling={false} style={[style_setting.font1_11,{textAlign: 'center'}]}>{Language.connectnetwork}</Text>
            </View>
        </View>
            </View>
        );
  },
  _render4: function() {
    return (
          <View>
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor: 'white',width:mwidth,height:mheight*0.8}}>
          </View>
          </View>
      );
  },
});

var Update= React.createClass({

  mixins: [TimerMixin],
  timer_delay:null,
  getInitialState:function (){
      return {
          url:'',
          code:1,
          version:'',
          info:'',
          mNowVersion:Varsion,
          msg:'',
          force:'',
          isNewest:false,
          visible: false,
          text1:Language.ok+'',
      }
  },


  componentWillUnmount:function(){
    sub_loading.remove();
    this.timer_delay && clearTimeout(this.timer_delay);
  },
  componentDidMount:function(){
    isloading=0;
    sub_loading = RCTDeviceEventEmitter.addListener('loading',(val)=>{
      console.log('loading2'+val);
      if(val==false){
        isloading=1;
        this.setState({
          visible:val,
        });
      }
      console.log('------qwe----------'+this.state.visible);
    });
      this.timer_delay = this.setTimeout(() => {
        this.fetchData(); 
      }, 500);
  },
  fetchData:function (){
    console.log('----fetchData-----'+isloading);
    if(isloading==1){
      isloading=0;
      return;
    }
    this.setState({
      visible: true,
    });
    console.log('----fetchData1111-----'+isloading);
    var REQUIRE_URL = config.server_base + config.upload_uri + '?var=' + config.varsion + '&os=' + config.os+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;
    console.log('REQUIRE_URL: ' + REQUIRE_URL);
    fetch (REQUIRE_URL,{timeout: 10000})
      .then ((response) => response.json())
      .then((responseData) => {
        code1=true;
         console.log('-------------------5----------------------------'+responseData.code+'==='+REQUIRE_URL);
         //console.log('-----------------------------------------------'+responseData.data.force);
        if(responseData.code == 101)
        {
          this.setState({
            code:responseData.code,
            msg:responseData.msg,
            url: responseData.data.url,
            force: responseData.data.force,
            visible: false,
            text1:Language.upgrade+'',
          });
          RCTDeviceEventEmitter.emit('update_force',this.state.force);
        }else{
          this.setState({
            code:responseData.code,
            msg:responseData.msg,
            visible: false,
            text1:Language.ok+'',
          });
        }
      })
      .catch((error) => {
        console.log('-------------------catch-------------------'+error);
          this.setState({
            visible: false,
            text1:Language.back+'',
          });  
          console.log('-------------------catch-------------------'+this.state.visible);               
          tools.alertShow(Language.network_request_failed);
          this.setState({
            NetStatus:true,
          });
      });
      
  },
  render: function() {
    var s,s1;
    var navigator = this.props.navigator;
    s=style_setting.imageNext;
    s1={fontSize:11,color:'#646464'};
    return (
      <View style = {styles.ubackgroundColor}>
       <Spinner ref={'ref_spinner'} visible={this.state.visible} />
        <View style={styles.uhead}>
            <TouchableOpacity style = {style_setting.touch_view} onPress={() => {this.gotoFeedback();UMeng.onEvent('Update_01');}}>
              <Image style={[style_setting.image,{resizeMode: 'stretch'}]} source={require('image!ic_back')}/>
            </TouchableOpacity>
            <Text allowFontScaling={false} style={[style_setting.font3,{flex:1,textAlign: 'center'}]}>{Language.softwareupdate}</Text>
            <View style={[style_setting.image,styles.uview]}></View>
        </View>
        <UpdateA/>
        <View style={styles.uview3}>
        <View style = {styles.uview4}>
            <View style={styles.uview5}>
                <TouchableOpacity  onPress = {()=> {this._goto();UMeng.onEvent('Update_02');}}>
                  <View style={styles.uview7}>
                      <Text allowFontScaling={false} style = {styles.utext}>{this.state.text1}</Text>
                  </View>
                </TouchableOpacity>
            </View>
        </View>
        </View>
      </View>
    );
  },
  gotoFeedback:function(){
    if(this.state.force===1){
      Alert.alert(
        Language.prompt+'',
        alertMessage,
        [
          {text: Language.ok+'', onPress: () => {BackAndroid.exitApp();UMeng.onEvent('Update_03');}},
          {text: Language.cancel+'', onPress: () => {UMeng.onEvent('Update_04');return true;}},
        ]
      )
     }else if(this.state.force===0){
        this.props.navigator.pop();
     } else {
        this.props.navigator.pop();
     }
  },
  _goto:function(){
      NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if(isConnected){
            if(this.state.code==101){
              DownLoud.show(this.state.url,this.state.isNewest,this.state.msg);
            }else if(this.state.code==100){
              this.props.navigator.pop();
            }else{
               this.props.navigator.pop();

            }
          }else{
             this.props.navigator.pop();
            //tools.alertShow(Language.connectnetwork);
            // ToastAndroid.show(Language.connectnetwork,ToastAndroid.SHORT);  
          }
        });
    
  },
});
module.exports = Update;