/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';


import  styles from './styles/style_setting';
import styles_feedback from './styles/style_feedback'
import Language from './Language'
var share = require('./share');
var async = require('async');
//var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');


import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
  TouchableWithoutFeedback,
  AsyncStorage,
  NativeModules,
  NetInfo,
  ToastAndroid,
  DeviceEventEmitter,
} from 'react-native';
var mwidth=Dimensions.get('window').width;
var mheight=Dimensions.get('window').height; 

var MyLogin= React.createClass({
    getInitialState() {
        return {
            value:mheight,
            m:mwidth,
            location:null,
            deviceName:'我的智能随身净化器',
            filterLeft:70,
            isConnect:'已连接',
            integral:125,
            number:1,
            isConnected: null,
            hidden:false,
            phonenumber:'',
        }
    },
    componentDidMount:function(){
      this.setState({phonenumber:this.props.phonenumber});
    },
    hidden(){

        if (!this.state.hidden){
            this.setState({hidden:true});
        }else {
           this.setState({hidden:false});
        }
    },
  render: function(){
    var num=this.state.phonenumber.substr(0, 3) + '****' + this.state.phonenumber.substr(7, 11);
    var s,s1;
    s=[styles.imageNext,{resizeMode: 'stretch'}];
    s1=styles.text;
    var navigator = this.props.navigator;

    return (
      <View>
        <View style={styles.head}>
            <TouchableOpacity style = {styles.touch_view}
              onPress={() => {navigator.pop();UMeng.onEvent('myLogin_01');}}>
              <Image style={[styles.image,{resizeMode: 'stretch'}]} source={require('image!ic_back')}/>
            </TouchableOpacity>
            <Text allowFontScaling={false} style={[styles.font3,styles.text1]}>个人中心</Text>
             <View style={[styles.image,]}></View>
        </View>
      <View style={{flex:1,height:mheight-mheight/10,backgroundColor:'#FFFFFF',marginTop:20}}>
        <View style={{marginRight:30,marginLeft:30,}}>
          <View style={{justifyContent: 'center',margin:15,alignItems: 'center',}}>
            <Image style={{resizeMode: 'stretch',height:56.5,width:56.5,marginRight: 10,}} source={require('image!ic_comment')}/>
              <View style={{marginTop:20,alignItems: 'center',}}>
                <Text allowFontScaling={false} style={{fontSize:14,color:'#323232',}}>智能随身净化器</Text>
                <View style={{flexDirection: 'row',marginTop:8,}}>
                  <Text allowFontScaling={false} style={s1}>{this.state.integral}</Text>
                  <Text allowFontScaling={false} style={s1}>积分</Text>
                  <View style={{width:10,alignItems: 'center',}}>
                     <View style={{height:15,borderColor:'#646464',borderWidth: 1 / PixelRatio.get(),}}/>
                  </View>
                  <Text allowFontScaling={false} style={s1}>{this.state.number}</Text>
                  <Text allowFontScaling={false} style={s1}>个智能设备</Text>
                </View>
                <View style={{marginTop:8,}}>
                <Text allowFontScaling={false} style={{fontSize:14,color:'#646464',}}>{num}</Text>
                </View>
              </View>
          </View>
          <View style={{marginTop:30}}>
            <View style={styles.line}/>
            <TouchableOpacity style={styles.view} onPress={()=>{this.gotoSafe_code();UMeng.onEvent('myLogin_02');}}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.devicesecuritycheck}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
            </TouchableOpacity> 
            <View style={styles.line}/>
            <TouchableOpacity style={styles.view} >
              <Text allowFontScaling={false} style={styles.font1}>{Language.passwordreset}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
            </TouchableOpacity> 
            <View style={styles.line}/>
            <TouchableOpacity style={styles.view} >
              <Text allowFontScaling={false} style={styles.font1}>{Language.discount}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
            </TouchableOpacity> 
          </View>                
        </View>
      </View>
        <View style = {{height:40,position:'absolute',marginLeft:20,marginRight:20,bottom:50,justifyContent:'center',alignItems: 'center',}}>
              <View style={{justifyContent:'center',alignItems: 'center',position:'absolute',width:mwidth-40,height:40,
                      borderWidth: 1 / PixelRatio.get(),borderRadius:20,bottom:10,}}>
                <TouchableOpacity
                    onPress = {this._handleSubmit();UMeng.onEvent('myLogin_03');}>
                   <View style = {styles_feedback.textBorder}>
                    <Text allowFontScaling={false} style={styles_feedback.commitText}>{Language.loginout}</Text>
                   </View>
                </TouchableOpacity>
              </View>
        </View>
    </View>
    );
  },
  gotoSafe_code:function(){
    this.props.navigator.push({
      id: 'Safe_code',
    });
  },

});
module.exports = MyLogin;