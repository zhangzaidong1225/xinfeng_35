'use strict';

var { NativeModules } = require('react-native');


var share = {
	'pullMenu':function(title, url, content, img, page, filtration, time, filternum, array){
		NativeModules.Share.pullMenu(title, url, content, img, page, filtration, time, filternum, array);
	},
	'pullMenuCount':function(title,img,filtration,time,filternum,array){
		//传键值
		NativeModules.Share.pullMenuCount(title,img,filtration,time,filternum,array);
	},
	'CanvasBitmap':function(title,filtration,time,filternum,array){
		NativeModules.Share.pullMenuCount(title,'',filtration,time,filternum,array);
	}
};

module.exports = share;