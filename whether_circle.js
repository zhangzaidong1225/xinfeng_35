'use strict'

var Icon = require ('./icon');
var NetWork=require('./NetWork');
import styles_circle from './styles/style_circle'
var TimerMixin = require ('react-timer-mixin');
var async = require('async');
import styles_actionbar  from './styles/style_actionbar'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var UMeng = require('./UMeng');
import Language from './Language'

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    PixelRatio,
    ToastAndroid,
    TouchableOpacity,
} from 'react-native';


var deviceWidth =Dimensions.get ('window').width;
var deviceHeight = Dimensions.get('window').height;

var subscription2;
var subscription3;

var Whether_Circle = React.createClass({

	mixins:[TimerMixin],

  _handle: null,
	

  getInitialState:function (){
      return {
      v_weather:'加载中...',
      v_icon:require('image!ic_unknow'),
      status:10,
      wind:10,
      temp:25,
      icon:10,
      loaded:false,
      v_cityid:null,
      v_load:false,
      v_pm25:'--',
      }
  },

  componentDidMount: function() {
  },
  componentWillMount: function(){
    //console.log('componentWillMount');
    subscription2 = RCTDeviceEventEmitter.addListener('PM25',(val) => {
      this.setState({v_pm25:val});
      console.log(this.state.v_pm25+'获取的pm2511111'); 
    });
    subscription3 = RCTDeviceEventEmitter.addListener('weather',(val) => {
      console.log(val+'获取的天气情况0'); 
      this.setState({v_weather:val});
      console.log(this.state.v_weather+'获取的天气情况'); 
      this.fetchWeatherData(this.state.v_weather);
    });
  },

  componentWillReceiveProps: function(){
    //console.log('componentWillReceiveProps');
    //this.getData();
  },

  componentWillUnmount: function(){
    subscription2.remove();
    subscription3.remove();
  },

   fetchWeatherData:function (weather){

    var icon = null;
    switch(weather){
          case '晴':{
            icon=require('image!ic_fine');
            break;
          }
          case '多云':
          case '晴间多云':
          case '大部多云':{
            icon=require('image!ic_cloud');

          }
          case '阴':{
            icon=require('image!ic_shade');
            break;
          }
          case '雷阵雨':
          case '雷阵雨伴有冰雹':{
            icon=require('image!ic_thundershower');
            break;
          }
          case '阵雨':
          case '小雨':
          case '大雨':
          case '暴雨':
          case '大暴雨':
          case '特大暴雨':
          case '中雨':
          case '冻雨':{
            icon=require('image!ic_rain');
            break;
          }
          case '雨夹雪':
          case '阵雪':
          case '小雪':
          case '中雪':
          case '大雪':
          case '暴雪':{
            icon=require('image!ic_snow');
            break;
          }
          case '浮尘':
          case '扬沙':{
            icon=require('image!ic_frift');
            break;
          }
          case '沙尘暴':
          case '强沙尘暴':{
            icon=require('image!ic_sand_storm');
            break;
          }
          case '雾':{
            icon=require('image!ic_fog');
            break;
          }
          case '霾':{
             icon=require('image!ic_haze');
             break;
          }
          case '风':
          case '大风':
          case '飓风':
          case '热带风暴':
          case '龙卷风':{
            icon=require('image!ic_wind');
            break;
          }
          default:{
            icon=require('image!ic_unknow');
            break;
          }
      }
      if(this.state.v_icon != icon){
        this.setState({
          v_icon:icon,
        });  
      }
},
  
  render:function (){
        return (
            <View style = {[styles_actionbar.view1],{marginTop:10}}>

                 <View style = {{alignItems:'center',justifyContent:'center',}}>
                  <View style = {styles_circle.text_frame}>
                    <Text allowFontScaling={false} style = {styles_circle.text}>
                      {Language.aqi} PM2.5 {this.state.v_pm25}
                    </Text>
                  </View>

                  <View
                    style = {{alignItems:'center',}}>
                    <View
                      style = {styles_circle.circle_frame}>
                        <View
                          style = {{marginTop:10,}}>
                          <Icon />
                        </View>
                        <View
                          style = {styles_circle.icon_frame}>
                          <Image
                              style = {styles_circle.icon}
                              source = {this.state.v_icon}/>
                        </View>
                    </View>
                  </View>
                </View>

                <TouchableOpacity style={{height:100,width:140,backgroundColor:'#FF000000',position:'absolute',top:deviceHeight/7,left:deviceWidth/3.4,}}
                        onPress = {()=>{this.gotoDeviceStatus();UMeng.onEvent('home_05');}}>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop:40,height:60,width:140,backgroundColor:'#FF000000',position:'absolute',top:deviceHeight/4,left:deviceWidth/3.4,}}
                        onPress = {()=>{this.gotoFilterPage();UMeng.onEvent('home_06');}}>
                </TouchableOpacity>
            </View>
           
        );
    },
   gotoDeviceStatus:function() {
      this.props.navigator.push({
      // id: 'Location',
        id: 'DeviceStatus',
      });
    },
    gotoFilterPage:function() {
      this.props.navigator.push({
      // id: 'Location',
        id: 'FilterXinfengPage',
      });
    },
  
});
var styles = StyleSheet.create ({
    container:{
      flexDirection:'column',
    },

});

module.exports = Whether_Circle;
