
'use strict';

//TODO fetch style from style_login to style_title
import style_login from './styles/style_login'
import  styles from './styles/style_setting';
import tools from './tools'
import styles_feedback from './styles/style_feedback'
import styles_health from './styles/style_health'
import style_register from './styles/style_register'
var RNFS = require('react-native-fs');
var Slider = require ('./Slider');
var Whether_Circle = require ('./whether_circle');
var Whether_PM = require ('./whether_pm');
import Tabbar from './tabbar'
import ModalBox from './modal'
import styles_actionbar  from './styles/style_actionbar'
import Language from './Language'
import styles_modal1 from './styles/style_home_modal'
var share = require('./share');
import  config from './config'


var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter'); /*用于界面组件之间传递消息*/
var AsyncStorage = require('./AsyncStorage');

import styles_hometitle  from './styles/style_hometitle'


import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    // AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    Platform,
    DeviceEventEmitter,
    NativeAppEventEmitter,
} from 'react-native';

var UMeng = require('./UMeng');
var BlueToothUtil=require('./BlueToothUtil');
var  DownLoud= NativeModules.Share;
var subscription3=null;

var DownLoad = require('./download');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
//忻风
var testDir1Path_tmp = RNFS.DocumentDirectoryPath + '/upload_tmp';
//tmp存储路径
var testFile3Path_tmp = RNFS.DocumentDirectoryPath + '/upload_tmp/dev_update.zip';
//存储目录
var testDir1Path = RNFS.DocumentDirectoryPath + '/upload';
var testFile3Path = RNFS.DocumentDirectoryPath + '/upload/dev_update.zip';

//单品
var testDir1PathSingle_tmp = RNFS.DocumentDirectoryPath + '/single/upload_tmp';
//tmp存储路径
var testFile3PathSingle_tmp = RNFS.DocumentDirectoryPath + '/single/upload_tmp/dev_update.zip';
//存储目录
var testDir1PathSingle = RNFS.DocumentDirectoryPath + '/single/upload';
var testFile3PathSingle = RNFS.DocumentDirectoryPath + '/single/upload/dev_update.zip';

//热更新
var Dir1PathHot_tmp = RNFS.DocumentDirectoryPath + '/hotupload/upload_tmp';
//tmp存储路径
var File3PathHot_tmp = RNFS.DocumentDirectoryPath + '/hotupload/upload_tmp/bundle.zip';
//存储目录
var Dir1PathHot = RNFS.DocumentDirectoryPath + '/hotupload/upload';
var File3PathHot = RNFS.DocumentDirectoryPath + '/hotupload/upload/bundle.zip';


var xinStatus = 1;
var singleStatus = 3;
var carStatus = 2;

var typeisf = -1;


var HomeTitle = React.createClass({
  	getDefaultProps: function () {
  		return {
        devicename:Language.xinfeng,
        deviceaddress:null,
        typeisf:-1,
        type:'',
  		};
  	},

    getInitialState:function(){
          //增加版本升级跳转
       AsyncStorage.getItem('local_version_msg_xin',(err,result) => {

           this.setState({localVer_xin:result});
             console.log(result+'home---local_version_msg');

        });
        AsyncStorage.getItem('local_version_info_xin',(err,result) => {

            this.setState({info_xin:result});
               console.log(result+'home---local_version_msg');
        });

        AsyncStorage.getItem('local_version_msg_single',(err,result) => {

           this.setState({localVer_single:result});
             console.log(result+'home---local_version_msg');

        });
        AsyncStorage.getItem('local_version_info_single',(err,result) => {

            this.setState({info_single:result});
               console.log(result+'home---local_version_msg');
        });



       AsyncStorage.getItem('shot_click',(err,result) => {
        if (result == null){
          this.setState({shot:'true'});
        }else {
          this.setState({shot:result});
        }

       });

        AsyncStorage.getItem('hotupload_version_local',(err,result) => {
          console.log(result+'home---hotupload_version_local');
            this.setState({hot_upload_local:result});
        });
       return {
          localVer_xin:1,
          info_xin:'',
          version_xin:1,

          localVer_single:1,
          info_single:'',
          version_single:1,

          localVer_var:1,
          info_car:'',
          version_car:1,

          md5_xin:'',
          md5_single:'',
          md5_car:'',

          md5Message:'',

           url_xin:'',
           url_single:'',
           url_car:'',

           hot_upload_server:'',
           hot_upload_local:'',

       }
    },

  componentDidMount:function(){
    subscription3 = RCTDeviceEventEmitter.addListener('DeviceUpload',(val) => {
      this.setState({shot:'true'});
       AsyncStorage.setItem('shot_click','true',(err) => {}); 
      console.log(this.state.shot+'``进入固件升级界面'); 
    });

    //检测网络变化
    NetInfo.addEventListener(
        'change',
        this._handleConnectionInfoChange
    );

   //  NetInfo.fetch().done(
   //    (connectionInfo) => {
   //      console.log('connectionInfo'+connectionInfo);

   //      if (connectionInfo === 'WIFI'){
   //          console.log('connectionInfo``````'+connectionInfo);
           
   //      }
   //    }
   // );
    // this.fetchData(xinStatus);
    // this.fetchDataSingle(singleStatus);
     // this.fetchDataHotUpload();

  },

  componentWillUnmount:function(){
    subscription3.remove();
    NetInfo.removeEventListener(
        'change',
        this._handleConnectionInfoChange
    );
  },


  _handleConnectionInfoChange: function(connectionInfo) {

    console.log('connectionInfo'+connectionInfo+'是否联网');
    // if (connectionInfo === 'WIFI'){
        
    // }
    // this.fetchData(xinStatus);
    // this.fetchDataSingle(singleStatus);
    // this.fetchDataHotUpload();
  },

  fetchData:function (status){


   NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if (isConnected) {
    console.log('有网路啦');
    var DEV_URL = config.server_base + config.dev_update_uri+'?item_id='+status+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;

    console.log(DEV_URL);
    fetch (DEV_URL)
      .then ((response) => response.json())
      .then((responseData) => {
        console.log(responseData);
        if(responseData.code == 0)
        {
          this.setState({
            version_xin:responseData.data.ver,
            url_xin:responseData.data.url,
            md5_xin:responseData.data.md5,
            info_xin: responseData.data.info,
          });
         
          if (this.state.localVer_xin == null){

             console.log('忻风版本号为空');

             this.download_md5(xinStatus,this.state.url_xin,testDir1Path_tmp,testFile3Path_tmp,testDir1Path,testFile3Path,this.state.md5_xin);
             console.log('忻风反馈回来的信息1.0');

          }


          if (this.state.version_xin > this.state.localVer_xin && this.state.localVer_xin > 0 ){

            console.log('忻风版本号不相等');

            this.download_md5(xinStatus,this.state.url_xin,testDir1Path_tmp,testFile3Path_tmp,testDir1Path,testFile3Path,this.state.md5_xin);
   
            console.log('忻风反馈回来的信息1.1');

          }
        }
        else
        {
          this.setState({
            code:responseData.code,
            msg:responseData.msg,
          });
        }
      })
      .catch((error) => {
         //ToastAndroid.show(''+error,ToastAndroid.SHORT);
      });
          }
        }
    );
 },

   fetchDataSingle:function (status){


   NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if (isConnected) {
    console.log('有网路啦');
    var DEV_URL = config.server_base + config.dev_update_uri+'?item_id='+status+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;

    console.log(DEV_URL);
    fetch (DEV_URL)
      .then ((response) => response.json())
      .then((responseData) => {
        console.log(responseData);

        if(responseData.code == 0)
        {
          this.setState({
            version_single:responseData.data.ver,
            url_single:responseData.data.url,
            md5_single:responseData.data.md5,
            info_single: responseData.data.info,
          });
         

          if (this.state.localVer_single == null){

             console.log('单品版本号为空');

             this.download_md5(singleStatus,this.state.url_single,testDir1PathSingle_tmp,testFile3PathSingle_tmp,testDir1PathSingle,testFile3PathSingle,this.state.md5_single);
             
             console.log('单品反馈回来的信息1.0');

          }


          if (this.state.version_single > this.state.localVer_single && this.state.localVer_single > 0 ){

            console.log('单品版本号不相等');

            this.download_md5(singleStatus,this.state.url_single,testDir1PathSingle_tmp,testFile3PathSingle_tmp,testDir1PathSingle,testFile3PathSingle,this.state.md5_single);
   
            console.log('单品反馈回来的信息1.1');

          }


        }
        else
        {
          this.setState({
            code:responseData.code,
            msg:responseData.msg,
          });
        }
      })
      .catch((error) => {
         //ToastAndroid.show(''+error,ToastAndroid.SHORT);
      });
          }
        }
    );
 },

 fetchDataHotUpload(){

     NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if (isConnected) {
    console.log('有网路啦');
    // var DEV_URL = config.server_base + config.hot_upload+'?version='+'1.0.0'+'&platform='+0;
    var DEV_URL = 'https://mops-xinfeng-dev.lianluo.com/iface/system/hot_update?version=1.0.0&platform=0';

    console.log(DEV_URL);
    fetch (DEV_URL)
      .then ((response) => response.json())
      .then((responseData) => {
        console.log(responseData);


        if(responseData.code == 100)
        {
          this.setState({
            hot_upload_server:responseData.data.ver,
          });
          console.log('有新版本更新');

          if (this.state.hot_upload_local == null){
             console.log('有新版本更新--null');
            this.download_md5(4,responseData.data.url,Dir1PathHot_tmp,File3PathHot_tmp,Dir1PathHot,File3PathHot,responseData.data.md5);
          }

          if (this.state.hot_upload_server > this.state.hot_upload_local && this.state.hot_upload_local > 0){
             console.log('有新版本更新--非null');
            this.download_md5(4,responseData.data.url,Dir1PathHot_tmp,File3PathHot_tmp,Dir1PathHot,File3PathHot,responseData.data.md5);

          }
        }
        else if (responseData.code == 101)
        {
          console.log('参数错误');
        }
        else if (responseData.code == 102){
          console.log('没有可以更新的资源');
        }
        else if (responseData.code == 103){
          console.log('当前最新版本');
        }

      })
      .catch((error) => {
         //ToastAndroid.show(''+error,ToastAndroid.SHORT);
      });
          }
        }
    );
 },



 download_md5:function(status,url,testDir1Path_tmp,testFile3Path_tmp,testDir1Path,testFile3Path,md5_msg) {
            //临时下载目录
            console.log('download_md5`````````````````');

            DownLoad.mkdirTest(testDir1Path_tmp);

            DownLoad.downloadFileTest(url,testFile3Path_tmp,() => {

              console.log(this.state.md5+'MD5'); 
             BlueToothUtil.verifyInstallPackage(testFile3Path_tmp,(message) => {

              //this.setState({md5Message:message,});

            
              if (md5_msg === message ){

                  DownLoad.exists(testFile3Path, (result) =>{
                    console.log('检测文件是否存在' + result);
                    if (result) {
                      DownLoad.deleteDirTest(testDir1Path);
                    }
                    DownLoad.mkdirTest(testDir1Path);
                    DownLoad.moveFile(testFile3Path_tmp,testFile3Path);

                    console.log('文件下载成功');

            

                    if (status == 1){
                      this.setState({localVer_xin:this.state.version_xin,shot:'false'}); 

                      AsyncStorage.setItem('local_version_msg_xin','' + this.state.version_xin,(err) => {});  
                      AsyncStorage.setItem('local_version_info_xin','' + this.state.info_xin,(err) => {});
                      AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {}); 

                      console.log(this.state.localVer_xin+'```忻风本地版本1.0');
                      console.log(this.state.info_xin+'```忻风本地版本1.0');
                      console.log(this.state.shot+'```忻风本地版本1.0shot');
                    }else if (status == 3){

                    this.setState({localVer_single:this.state.version_single}); 

                    AsyncStorage.setItem('local_version_msg_single','' + this.state.version_single,(err) => {});  
                    AsyncStorage.setItem('local_version_info_single','' + this.state.info_single,(err) => {});
                    //AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {}); 

                    console.log(this.state.localVer_single+'```单品本地版本1.0');
                    console.log(this.state.info_single+'```单品本地版本1.0');
                    //console.log(this.state.shot+'本地版本1.0shot');
                    } 
                    else 
                    {
                      console.log('文件下载成功---热更新');
                      BlueToothUtil.unZip(testFile3Path);
                      this.setState({hot_upload_local:this.state.hot_upload_server}); 
                      AsyncStorage.setItem('hotupload_version_local','' + this.state.hot_upload_server,(err) => {});

                    }


                  });

               //    DownLoad.mkdirTest(testDir1Path);
               //    DownLoad.moveFile(testFile3Path_tmp,testFile3Path);

               //  console.log('文件下载成功');

               //  this.setState({localVer:this.state.version,shot:'false'}); 

               //  AsyncStorage.setItem('local_version_msg','' + this.state.version,(err) => {});  
               //  AsyncStorage.setItem('local_version_info','' + this.state.info,(err) => {});
               //  AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {}); 

               // console.log(this.state.localVer+'本地版本1.0');
               // console.log(this.state.info+'本地版本1.0');
               // console.log(this.state.shot+'本地版本1.0shot');

              }
              else {

                console.log('文件下载失败');
                DownLoad.deleteDirTest(testDir1Path_tmp);

                // this.setState({localVer:'',shot:'true',info:''});

                if (status == 1){

                  this.setState({localVer_xin:'',shot:'true',info_xin:''});

                  AsyncStorage.setItem('local_version_msg_xin',''+this.state.localVer_xin,(err) => {});  
                  AsyncStorage.setItem('local_version_info_xin',''+this.state.info_xin,(err) => {});
                  AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {}); 

                 console.log(this.state.localVer_xin+'`````忻风本地版本1.0');
                 console.log(this.state.info_xin+'````忻风本地版本1.0');
                 console.log(this.state.shot+'````忻风本地版本1.0shot');
                } else if (status == 3){

                this.setState({localVer_single:'',info_single:''});

                 AsyncStorage.setItem('local_version_msg_single',''+this.state.localVer_single,(err) => {});  
                  AsyncStorage.setItem('local_version_info_single',''+this.state.info_single,(err) => {});
                  //AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {}); 

                 console.log(this.state.localVer_single+'`````单品本地版本1.0');
                 console.log(this.state.info_single+'````单品本地版本1.0');
                 //console.log(this.state.shot+'本地版本1.0shot');
                }
                else 
                {
                  console.log('文件下载失败---'+'热更新');

                }
              } 
       
            });
          });
  },




	render: function() {
        var DEFAULT_URL = config.server_base+config.share_uri;
       console.log('DEFAULT_URL: ' + DEFAULT_URL);
       var DEFAULT_SHARE_IOCN_URL = config.server_base+config.share_icon_uri;
       console.log('DEFAULT_SHARE_IOCN_URL: ' + DEFAULT_SHARE_IOCN_URL);

      var img;
      var TnameColor;
         // if (this.state.shot == 'true'){
         //  img = require('image!ic_update1');//绿色
          
         // }else{
         //   img = require('image!ic_update');//红色
         // }
        // if(this.props.type==0){
        //   var color1 = '#323C4B';
        //   TnameColor = '#E6E6E6';
        // } else {
        //   var color1 = '#DCDCDC';
        //   TnameColor = '#525252';
        // }
        
    if(this.props.type==-1){
          return (
            <View style = {[styles_hometitle.hometitle, {backgroundColor:'#DCDCDC',}]}>
                   <Text allowFontScaling={false} style={[styles_hometitle.firstPageText,]}>{Language.home_page}</Text>
            </View>
            );
    } 
    // else if(this.props.type==0) {
    // return (
    //       <View style = {[styles_hometitle.xinfengtitle,{backgroundColor:'#DCDCDC'}]}>

    //         <TouchableOpacity
    //           onPress = {this.gotoSharePage}
    //           style = {[styles_hometitle.touch,{width: 50,left:0, paddingRight: 10 ,position : 'absolute'}]}>
    //             <Image
    //               source = {require('image!ic_countly')}
    //               style = {[styles_hometitle.image,{backgroundColor:'#DCDCDC'}]}/>
    //         </TouchableOpacity>
            
    //         <View  style = {[styles_hometitle.xinfengtitle, {width : 30 + 17 + (this.props.devicename).length * 17}]}>
    //         <TouchableOpacity
    //             onPress = {this._handleClick}
    //             style = {[styles_hometitle.touch,{width : 17 + (this.props.devicename).length * 17,flexDirection:'row',}]}>

    //               <Text allowFontScaling={false} style = {[styles_hometitle.text, {width : (this.props.devicename).length * 19, color:'#525252'}]}>
    //                 {this.props.devicename}
    //               </Text>
    //           </TouchableOpacity>
    //         </View >

    //         <TouchableOpacity
    //           onPress = {() => {share.pullMenu('MOPS• ' + Language.xinfeng, DEFAULT_URL, Language.fearless,  DEFAULT_SHARE_IOCN_URL, 'home', 6, 210, 0, ['第一次使用', '1m³', '100min', '200min', '5m³']);UMeng.onEvent('home_03');}}
    //           style = {[styles_hometitle.touch,{width: 50,right:50, paddingLeft: 20, position : 'absolute'}]}>
    //             <Image
    //               source = {require ('image!img_selecte_share_gray')}
    //               style = {styles_hometitle.image}/>
    //         </TouchableOpacity>

    //         <TouchableOpacity
    //           onPress = {() => {this.gotoSetPage();}}
    //           style = {[styles_hometitle.touch,{width: 50,right:0, paddingLeft: 0, position : 'absolute'}]}>
    //             <Image
    //               source = {require('image!img_selecte_set_gray')}
    //               style = {styles_hometitle.image}/>
    //         </TouchableOpacity>

    //       </View>
    //   );

    // } 

    else {
        return (
          <View style = {[styles_hometitle.xinfengtitle,{backgroundColor:'#DCDCDC'}]}>

            <TouchableOpacity
              onPress = {this.gotoSharePage}
              style = {[styles_hometitle.touch,{width: 50,left:0, paddingRight: 10 ,position : 'absolute'}]}>
                <Image
                  source = {require('image!ic_countly')}
                  style = {[styles_hometitle.image,{backgroundColor:'#DCDCDC'}]}/>
            </TouchableOpacity>

            <View  style = {[styles_hometitle.xinfengtitle, {width : 30 + 17 + (this.props.devicename).length * 17}]}>

              <TouchableOpacity
                onPress = {this._handleClick}
                style = {[styles_hometitle.touch,{width : 17 + (this.props.devicename).length * 17,flexDirection:'row',}]}>

                  <Text allowFontScaling={false} style = {[styles_hometitle.text, {width : (this.props.devicename).length * 19, color:'#525252'}]}>
                    {this.props.devicename}
                  </Text>
              </TouchableOpacity>
            </View >

            <TouchableOpacity
              onPress = {() => {share.pullMenu('MOPS• '+ Language.xinfeng, DEFAULT_URL, Language.fearless,  DEFAULT_SHARE_IOCN_URL, 'home', 6, 210, 0, ['第一次使用', '1m³', '100min', '200min', '5m³']);UMeng.onEvent('home_03');}}
              style = {[styles_hometitle.touch,{width: 50,right:60, paddingLeft: 20, position : 'absolute'}]}>
                <Image
                  source = {require ('image!img_selecte_share_gray')}
                  style = {styles_hometitle.image}/>
            </TouchableOpacity>

            <TouchableOpacity
              onPress = {() => {this.gotoSetPage();}}
              style = {[styles_hometitle.touch,{width: 50,right:10, paddingLeft: 0, position : 'absolute'}]}>
                <Image
                  source = {require('image!img_selecte_set_gray')}
                  style = {styles_hometitle.image}/>
            </TouchableOpacity>
          </View>
      );
    }
		
	},
  gotoSharePage:function(){
        if (this.props.deviceaddress == null){
            tools.alertShow(Language.please_connect_device); 
        }else {
          this.props.navigator.push({
            id: 'SharePage',
              params:{
               deviceName:this.props.devicename,
              }
          });
        }
  },
  gotoSetPage:function(){
      this.props.navigator.push({
        id: 'SetPage',
          params:{
           typeisf:this.props.type,
          }
      });
      console.log('========hasFilter===='+this.props.type);
  },
  _handleClick:function (){
    RCTDeviceEventEmitter.emit('title_modalBox',true);
  },
    gotoDeviceUpdate:function(){

      NetInfo.isConnected.fetch().done(
        (isConnected) => { 
          if(isConnected){
            UMeng.onEvent('home_01');
            this.setState({shot:'true'});
            console.log(this.state.shot+'点击过');

             AsyncStorage.setItem('shot_click',''+this.state.shot,(err) => {});

            this.props.navigator.push({
              id:'DeviceUpdate',
            });
          }else{
            tools.alertShow(Language.connectnetwork);
          }
        });


    },

});

module.exports = HomeTitle;

            // <View  style = {[styles_hometitle.xinfengtitle, {width : 30 + 17 + (this.props.devicename).length * 19}]}>
            //   <TouchableOpacity
            //     onPress = {this.gotoDeviceUpdate}
            //     style = {[styles_hometitle.touch,{width : 30,}]}>
            //       <Image
            //         source = {img}
            //         style = {[styles_hometitle.image,{backgroundColor:'#DCDCDC'}]}/>
            //   </TouchableOpacity>

            //   <TouchableOpacity
            //     onPress = {this._handleClick}
            //     style = {[styles_hometitle.touch,{width : 17 + (this.props.devicename).length * 19,flexDirection:'row',}]}>
            //       <Text allowFontScaling={false}  style = {[styles_hometitle.add, {width : 17}]}>+</Text>
            //       <Text allowFontScaling={false} style = {[styles_hometitle.text, {width : (this.props.devicename).length * 19}]}>
            //         {this.props.devicename}
            //       </Text>
            //   </TouchableOpacity>
            // </View >