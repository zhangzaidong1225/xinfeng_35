'use strict'
import  styles from './styles/style_setting';
import styles_web from './styles/style_web'
import styles_feedback from './styles/style_feedback'
import style_login from './styles/style_login'
import tools from './tools'
import Spinner from 'react-native-loading-spinner-overlay';
var share = require('./share');
var Safe_code = require ('./safe_code');
var async = require('async');
import Title from './title'
import style_safe from './styles/style_safe_code'
import Safe from './safe_content'
import Language from './Language'
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
//var AsyncStorage = require('./AsyncStorage');
var UMeng = require('./UMeng');
var Utils = require('./utils');


var sub_loading;


import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Dimensions,
    Navigator,
    TouchableHighlight,
    TouchableOpacity,
    PixelRatio,
    NativeModules,
    // AsyncStorage,
    ToastAndroid,
    NetInfo,
    ListView,
    DeviceEventEmitter,
    NativeAppEventEmitter,
    ScrollView,
    Alert,
    Platform,
    AsyncStorage,
} from 'react-native';

import  config from './config'
var deviceWidth =Dimensions.get ('window').width;
var mwidth =Dimensions.get ('window').width;
var mheight = Dimensions.get('window').height-62;
var deviceHeight = Dimensions.get('window').height;
// var DEFAULT_URL = config.server_base+'/iface/user/login';
var sub_key;
var sub_key1;
var sub_number;
var isloading=0;
var alertMessage = '您确定要退出吗？';
var Login = React.createClass({

    getInitialState(){
    return {
      type:1,
      password:'',
      hidden:true,
      key:null,
      value:mheight,
      filterLeft:70,
      isConnect:'已连接',
      integral:125,
      number:1,
      isConnected: null,
      phonenumber:' ',
      visible:false,
      visible1: false,
      code1:null,
      exit1:null,
    }
  },

  hidden(){
    console.log(this.state.hidden);
    if (this.state.hidden){
        this.setState({hidden:false});
    }else {
       this.setState({hidden:true});
  }
    
  },
  componentWillUnmount:function(){
   // isloading=0;
    sub_key.remove();
    sub_key1.remove();
    sub_number.remove();
    sub_loading.remove();
  },
  componentDidMount:function(){

      // if (Platform.OS === 'android') {
      //   Utils.SetInputModeForTabBar();
      // }
      
    isloading=0;
    sub_key = RCTDeviceEventEmitter.addListener('key',(val)=>{
      this.setState({
        key:'',
      });
    });
    sub_number = RCTDeviceEventEmitter.addListener('login_number',(val)=>{
      this.setState({
        phonenumber:val,
      });
    });
    sub_key1 = RCTDeviceEventEmitter.addListener('login_key1',(val)=>{
      this.setState({
        key:val,
      });
    });
    sub_loading = RCTDeviceEventEmitter.addListener('loading',(val)=>{
      console.log('loading3'+val);
      if(val==false){
        isloading=1;
        this.setState({
          visible1:val,
        });
      }
    });
  },
  componentWillMount:function(){

      AsyncStorage.getItem('key',(error,result) => {
          if (error){}else {
            this.setState({
            key:result,
          });
        }
      });
      AsyncStorage.getItem('number',(error,result) => {
          if (error){}else {
            this.setState({
            phonenumber:result,
          });
        }
      });
    },
	render(){
      if (Platform.OS === 'android') {
        Utils.SetInputModeForTabBar();
      }
      var navigator = this.props.navigator;
      var img;
      var imgback;
      if (this.state.hidden){
          img = require('image!ic_hide'); 
      }else{
          img = require('image!ic_show');
      }
      if(this.props.codepage==1){
        imgback = require('image!ic_back');
      }else{
        imgback = require('image!ic_null');
      }
if(this.state.key==''||this.state.key==null){
    return (
      <View style={style_login.scrolls}>
         <Spinner visible={this.state.visible1}/>

                <Title title = {Language.personal} hasGoBack = {this.props.codepage==1?true : false}  navigator = {this.props.navigator}/>
     
                 <ScrollView style={[style_login.scroll,{backgroundColor:'white'}]} >
                <View style = {style_login.headView}>
                    <View>
                         <Image style={[styles.comment,{resizeMode: 'stretch'}]} source={require('image!ic_comment')}/>
                    </View>
                </View>
                <View style = {style_login.borderView}>
                        <View style = {style_login.borderCenter}>
                            <View style = {style_login.borderView1}>
                                <View style = {style_login.borderView2}>
                                    <View style = {style_login.imageView1}>
                                        <Image style = {style_login.image1} source = {require('image!ic_feedback')}/>
                                    </View>
                                   <View style={style_login.lineImageView}>
                                     <Image style={style_login.line} source={require('image!ic_dividing_line')}/>
                                    </View>
                                    <TextInput
                                          style = {{height:50,flex:1,paddingLeft:5}}
                                          keyboardType ='numeric'
                                          textAlignVertical='center'
                                          underlineColorAndroid='transparent'
                                          onChangeText = {(text) => this.setState({phonenumber:text})}
                                          value = {this.state.phonenumber}
                                          placeholderTextColor = '#B7B7B7'
                                          placeholder = {Language.phonenumber}/> 
                                </View>
                                <View style = {{flexDirection:'row',}}>
                                    <View style = {style_login.imageView2}>
                                        <Image style = {style_login.lockImage} source = {require('image!ic_lock')}/>
                                    </View>
                                    <View style ={style_login.lineImageView} >
                                     <Image style = {style_login.line} source={require('image!ic_dividing_line')}/>
                                    </View>

                                    <TextInput
                                          style = {{height:50,flex:1,paddingLeft:5,}}
                                          KeyboardType = 'default'
                                          autoFocus  = {this.state.visible}
                                          textAlignVertical='center'
                                          secureTextEntry ={this.state.hidden}
                                          underlineColorAndroid='transparent'
                                          onChangeText = {(text) => this.setState({password:text})}
                                          placeholderTextColor = '#B7B7B7'
                                          maxLength={16}
                                          placeholder = {Language.passwordaccount}/>
                                  <View style= {style_login.lineImageView}>
                                     <Image style={style_login.line} source={require('image!ic_dividing_line')}/>
                                    </View>

                                    <TouchableOpacity
                                      style = {style_login.eyeView}
                                        onPress = {() => {this.hidden();UMeng.onEvent('login_02');}}>
                                        <View >
                                        <Image style = {style_login.eyeImage} source = {img}/>
                                        </View>
                                    </TouchableOpacity>

                                </View>

                            </View>
                            <View style = {style_login.register}>
 
                                <View style = {style_login.registerView}>
                                 <TouchableOpacity
                                    onPress = {()=>{this.gotoRegister();UMeng.onEvent('login_03');}}>
                                    <Text allowFontScaling={false} style = {style_login.registerText}>{Language.register}</Text>
                                 </TouchableOpacity>
                                </View>
                           
                                <View style={style_login.passwordView}>
                                <TouchableOpacity
                                    onPress = {()=>{this.gotoForgot1();UMeng.onEvent('login_04');}}>
                                    <Text allowFontScaling={false} style = {style_login.passwordText}>{Language.forgetpassword}</Text>
                                </TouchableOpacity>
                                </View>
                               
                            </View>
                        </View>
                </View>          
            </ScrollView>
             <View style = {style_login.viewBottom}>
                  <View style={style_login.buttonView2}>
                    <TouchableOpacity
                        onPress = {()=>{this._handleSubmit();UMeng.onEvent('login_05');}}>
                       <View style = {styles_feedback.textBorder}>
                        <Text allowFontScaling={false} style={styles_feedback.commitText}>{Language.login}</Text>
                       </View>
                    </TouchableOpacity>
                  </View>
            </View>         
      </View>
    );
}else if(this.props.codepage==1&&this.state.key!=null){
  console.log('login log2');
  return(
      <View style={[style_login.scrolls,{backgroundColor:'white'}]}>
        <Spinner visible={this.state.visible1} />
          <View style = {style_safe.container}>
              <Title title = {Language.securitycheck} hasGoBack = {true} backAddress = {this.props.backAddress} navigator = {this.props.navigator}/>
              <Safe/>
          </View>
      </View>
  );
}else{
  var num;
    if(this.state.phonenumber==null||this.state.phonenumber==''){
      num='';
    }else{
      num=this.state.phonenumber.substr(0, 3) + '****' + this.state.phonenumber.substr(7, 11);
    }        
    var s,s1;
    s=[styles.imageNext,{resizeMode: 'stretch'}];
    s1=styles.text;
    console.log('login log3');
      return (
      <View style={[style_login.scrolls,{backgroundColor:'white'}]}>
      <Spinner visible={this.state.visible1} />

        <Title title = {Language.personal} hasGoBack = {false}  navigator = {this.props.navigator}/>

        <ScrollView style={[style_login.scroll,{backgroundColor:'white'}]} >

        
         <View style={{width:deviceWidth,height:380,}}>
      <View style= {style_login.view1}>
        <View style={style_login.view1Margin}>
            <View style= {style_login.view1_2}>
                <View style={style_login.view1_text}>
                    <View style= {style_login.view1_text1}>
                      <Image style={style_login.view1_image} source={require('image!ic_comment')}/>
                    </View>
                </View>                
                <View style={style_login.view1_text}>
                    <View style= {style_login.view1_text1}>
                      <Text allowFontScaling={false} style={style_login.view1_text2}>{num}</Text>
                    </View>
                </View>
            </View>
            <View style={{marginTop:30}}>
            <View style={styles.line}/>
            <TouchableOpacity style={styles.view} onPress={()=>{this.gotoForgot();UMeng.onEvent('login_06');}}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.passwordreset}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
            </TouchableOpacity> 
            <View style={styles.line}/>
            <TouchableOpacity style={styles.view} onPress={()=>{this.gotoPrivilege();UMeng.onEvent('login_07');}}>
              <Text allowFontScaling={false} style={styles.font1}>{Language.discount}</Text>
              <View style={styles.direction}>
              <Image style={s} source={require('image!ic_next')}/>
              </View>
            </TouchableOpacity> 
          </View>                
        </View>
      </View>
      </View>
      </ScrollView>
        <View style = {style_login.viewBottom}>
              <View style={style_login.viewBottom1}>
                <TouchableOpacity
                    onPress = {()=>{this.exit();UMeng.onEvent('login_08');}}>
                   <View style = {styles_feedback.textBorder}>
                    <Text allowFontScaling={false} style={styles_feedback.commitText1}>{Language.loginout}</Text>
                   </View>
                </TouchableOpacity>
              </View>
        </View>
    </View>
    );
}
},

    _handleSubmit:function(){

      var phone_number = this.state.phonenumber;

      if(phone_number == null) phone_number = '';
         RCTDeviceEventEmitter.emit('login_number',phone_number);
          AsyncStorage.setItem('number',
            phone_number,error => {
              if (error){
              }else {
              }
          });

        if (this.state.phonenumber === ''|| this.state.phonenumber == null ){
            tools.alertShow(Language.phonenumbernotempty);
          // ToastAndroid.show(Language.phonenumbernotempty,ToastAndroid.SHORT);
        }else {
            if(this.state.password === ''|| this.state.password == null ){
                tools.alertShow(Language.password_not_empty);
            }else{
              if (this.telRuleCheck2(this.state.phonenumber)){
                if(isloading==1){
                  isloading=0;
                  return;
                }
                this.setState({
                  visible1: true,
                });                
              NetInfo.isConnected.fetch().done(
                (isConnected) => {

              if (isConnected) {
                  fetch (config.server_base + config.user_login_uri+'?mobi='+this.state.phonenumber+'&pass='+this.state.password+'&os=1'+'&varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language,{timeout: 10000})
                  .then ((response) => response.json())
                  .then((responseData) => {
                    if(responseData.code == 0){
                      UMeng.onProfileSignIn(this.state.phonenumber);
                      this.setState({
                        key:responseData.data.key,
                        visible1: false,
                      });
                      if(this.props.codepage==1){
                          this.setState({
                            code1:1,
                            visible1: false,
                          });
                      }
                      RCTDeviceEventEmitter.emit('login_key1',this.state.key);
                        AsyncStorage.setItem('key',
                          this.state.key,error => {
                            if (error){
                            }else {
                            }
                        });
                      tools.alertShow(responseData.msg + '');
                    }
                    if(responseData.code == 303){
                      this.setState({
                        visible1: false,
                      });
                      tools.alertShow(responseData.msg+'');
                    }
                  })
                  .catch((error) => {
                    this.setState({
                      visible1: false,
                    });                 
                    tools.alertShow(Language.network_request_failed);
                  });
                }else{
                    this.setState({
                      visible1: false,
                    });
                    tools.alertShow(Language.connectnetwork);
                }
                });
            }else{
              this.setState({
                visible1: false,
              });
              tools.alertShow(Language.phoenformatnotcorrent);
            }

            }              
        }
    },

    telRuleCheck2(string) {
    var pattern = /^1[34578]\d{9}$/;
        if (pattern.test(string)) {
            return true;
        }
          return false;
     },
    exit:function(){
      Alert.alert(
        Language.prompt+'',
        Language.exit,
        [
          {text: Language.ok+'', onPress: () => {
            UMeng.onProfileSignOff();
            UMeng.onEvent('login_09');
            this.setState({
              password:'',
            });
            RCTDeviceEventEmitter.emit('key','');
            AsyncStorage.setItem('key',
                '',error => {
                  if (error){
                  }else {
                  }
            });

          }},
          {text: Language.cancel+'', onPress: () => {UMeng.onEvent('login_10');return true;}},
        ]
      )


     },
    gotoMyLogin:function() {
      this.setState({
        visible1: false,
      }); 
      this.props.navigator.push({
        id: 'MyLogin',
        params:{
           phonenumber:this.state.contact,
        }
      });

    },
    gotoRegister:function() {
      this.props.navigator.push({
        id: 'Register',
      });
    },
    gotoPrivilege:function() {
      // this.props.navigator.push({
      //   id: 'Privilege',
      // });
      var PRIVILEGE = config.server_base+config.privilege_uri+'?varsion='+config.varsion+'&platform='+config.platform+'&phone_modal='+config.phone_modal+'&language='+config.language;//优惠活动
      console.log('PRIVILEGE' + PRIVILEGE);
      this.props.navigator.push({
        id: 'WebViewPage',
          params:{
              title:Language.discount,
              url: PRIVILEGE,
              hasGoBack: true,
              webviewStyle:styles_web.webview_protocol,
          }
      });
    },    
    gotoForgot:function() {
      this.props.navigator.push({
        id: 'Forgot',
        params:{
           title:Language.passwordreset,
        }
      });
    },
    gotoForgot1:function() {
      this.props.navigator.push({
        id: 'Forgot',
        params:{
           title:Language.retrievepassword,
        }
      });
    },
});

module.exports =Login;





