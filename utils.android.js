'use strict';


var { NativeModules } = require('react-native');

var Utils = {
	'GotoGrade':function(){
		NativeModules.Utils.GotoGrade();
	},
	'GetAdAction':function(callback) {
		NativeModules.Utils.GetAdAction(callback);
	},
	'SetTestMarkStr':function(mark) {
		// NativeModules.Utils.set_test_mark_str(mark);
	},
	'ShowDialog':function(name,addr,callback) {
		NativeModules.Utils.ShowDialog(name,addr,callback);
	},
	'setInputModeForWebView':function() {
		NativeModules.Utils.SetInputModeForWebView();
	},
	'SetInputModeForTabBar':function() {
		NativeModules.Utils.SetInputModeForTabBar();
	},
	'getCurrentLanguage':function(callback) {
		NativeModules.Utils.getCurrentLanguage(callback);
	},
	'getSelectItemAddr':function(callback) {
	},

	'setSelectItemAddr':function(addr) {
	},

};
module.exports = Utils;